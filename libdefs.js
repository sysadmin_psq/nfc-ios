// @flow
declare module 'metro-bundler' {  declare var exports: any;  }
declare module 'react-native-camera' {  declare var exports: any;  }
declare module 'react-native-check-box' {  declare var exports: any;  }
declare module 'react-native-config' {  declare var exports: any;  }
declare module 'react-native-device-info' {  declare var exports: any;  }
declare module 'react-native-permissions' {  declare var exports: any;  }
declare module 'react-native-send-intent' {  declare var exports: any;  }
declare module 'react-native-sound' {  declare var exports: any;  }
declare module 'react-native-splash-screen' {  declare var exports: any;  }
declare module 'react-native-swiper' {  declare var exports: any;  }
declare module 'react-native-tab-view' {  declare var exports: any;  }
declare module 'react-native-video-player' {  declare var exports: any;  }
declare module 'react-native-video' {  declare var exports: any;  }
declare module 'react-native' {  declare var exports: any;  }
declare module 'react-navigation-redux-helpers' {  declare var exports: any;  }
declare module 'react-navigation' {  declare var exports: any;  }

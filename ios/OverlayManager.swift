//
//  OverlayManager.swift
//  NationalFitnessCampaign
//
//  Created by Rudy Mutter on 9/19/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

import UIKit
import AssetsLibrary
import AVFoundation
import Photos

// Code inspiration from: https://stackoverflow.com/questions/40530367/swift-3-how-to-add-watermark-on-video-avvideocompositioncoreanimationtool-ios
// TODO: Use https://github.com/rs/SDAVAssetExportSession to only make one compilation pass at the video and merge with croping, doubling speed, and boomerang processing
class OverlayManager: NSObject {
  func applyOverlay(video videoAsset:AVAsset, imageName name: String!, completion : ((_ status : AVAssetExportSessionStatus?, _ outputURL : URL?) -> ())?) {
    DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {

      let mixComposition = AVMutableComposition()

      let compositionVideoTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
      let clipVideoTrack = videoAsset.tracks(withMediaType: AVMediaType.video)[0]
      compositionVideoTrack?.preferredTransform = clipVideoTrack.preferredTransform
      do {
        try compositionVideoTrack?.insertTimeRange(CMTimeRangeMake(kCMTimeZero, videoAsset.duration), of: clipVideoTrack, at: kCMTimeZero)
      }
      catch {
        print(error.localizedDescription)
      }

      let videoSize = clipVideoTrack.naturalSize

      let parentLayer = CALayer()
      let videoLayer = CALayer()
      parentLayer.frame = CGRect(x: 0, y: 0, width: videoSize.width, height: videoSize.height)
      videoLayer.frame = CGRect(x: 0, y: 0, width: videoSize.width, height: videoSize.height)
      parentLayer.addSublayer(videoLayer)

      let watermarkImage = UIImage(named: name)
      let imageLayer = CALayer()
      imageLayer.contents = watermarkImage?.cgImage

      // Specific numbers based on overlay and video dimensions
      let imageHeight : CGFloat = 114.0
      let imageWidth : CGFloat = 640.0
      let yPosition : CGFloat = videoSize.height - imageHeight

      imageLayer.frame = CGRect(x: 0, y: yPosition, width: imageWidth, height: imageHeight)
      // Due to the rotation of the video, we need to give it a negative position
      imageLayer.position = CGPoint(x: imageLayer.frame.midX, y: imageLayer.frame.midY - yPosition)
      imageLayer.opacity = 0.70
      parentLayer.addSublayer(imageLayer)

      let videoComp = AVMutableVideoComposition()
      videoComp.renderSize = videoSize
      videoComp.frameDuration = CMTimeMake(1, 30)
      videoComp.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)

      let instruction = AVMutableVideoCompositionInstruction()
      instruction.timeRange = CMTimeRangeMake(kCMTimeZero, mixComposition.duration)
      let mixVideoTrack = mixComposition.tracks(withMediaType: AVMediaType.video)[0] as AVAssetTrack

      let layerInstruction = self.videoCompositionInstructionForTrack(mixVideoTrack: mixVideoTrack, originalVideoTrack: clipVideoTrack)

      instruction.layerInstructions = [layerInstruction]
      videoComp.instructions = [instruction]

      let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
      let url = URL(fileURLWithPath: documentDirectory).appendingPathComponent("OverlayedAssembledWorkoutVideo.mov")
      do {
        try FileManager.default.removeItem(at: url)
      }
      catch {}

      let exporter = SDAVAssetExportSession(asset: mixComposition)
      exporter?.outputURL = url
      exporter?.outputFileType = AVFileType.mov.rawValue
      exporter?.shouldOptimizeForNetworkUse = true
      exporter?.videoComposition = videoComp
      exporter?.videoSettings = [
        AVVideoCodecKey: AVVideoCodecH264,
        AVVideoWidthKey: 640,
        AVVideoHeightKey: 640,
        AVVideoCompressionPropertiesKey: [
          AVVideoAverageBitRateKey : NSInteger(750000),
          AVVideoProfileLevelKey : AVVideoProfileLevelH264BaselineAutoLevel
        ]
      ]

      exporter?.exportAsynchronously() {
        DispatchQueue.main.async {
          if exporter?.status == AVAssetExportSessionStatus.completed {
            let outputURL = exporter?.outputURL
            // Save to library
            if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(outputURL!.path) {
              PHPhotoLibrary.shared().performChanges({
                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: outputURL!)
              }) { saved, error in
                completion!(AVAssetExportSessionStatus.completed, outputURL!)
              }
            }
          } else {
            // Error
            completion!(exporter?.status, nil)
          }
        }
      }
    }
  }

  private func videoCompositionInstructionForTrack(mixVideoTrack: AVAssetTrack, originalVideoTrack: AVAssetTrack) -> AVMutableVideoCompositionLayerInstruction {
    let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: mixVideoTrack)
    // 80 pixel offset is a magic number to make the video line up perfectly, unfortunately not sure why - this may have something to do with 720p vs our cropped 640 video
    instruction.setTransform(originalVideoTrack.preferredTransform.translatedBy(x: 0, y: 80), at: kCMTimeZero)
    return instruction
  }
}

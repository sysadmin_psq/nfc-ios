//
//  VideoEditor
//  NationalFitnessCampaign
//
//  Created by Rudy Mutter on 9/17/17.
//  Copyright © 2017 Facebook. All rights reserved.
//
import AVFoundation
import Foundation
import Photos

// VideoEditor.swift

@objc(VideoEditor)
class VideoEditor: NSObject {

  @objc func transformWorkoutVideo(_ videoPath: String, exerciseName: String, callback: @escaping (RCTResponseSenderBlock)) -> Void {
    let assetUrl = URL(fileURLWithPath: videoPath)
    VideoEditor.reverse(AVAsset(url: assetUrl), outputPath: videoPath, exerciseName: exerciseName) {
      outputUrl in
      let returnData = [
        "videoPath": outputUrl.absoluteString,
        ]
      // NSNull() represents error code
      callback([NSNull(), returnData])
    }
  }

  @objc func transformChallengeVideo(_ videoLocations: Dictionary<String, Any>, exercises: Array<Dictionary<String, Any>>, totalScore: Int, locationCity: String, locationName: String, challengeTitle: String, challengeSubtitle: String, callback: @escaping (RCTResponseSenderBlock)) -> Void {
    // Specific numbers based on overlay and video dimensions
    let videoHeight : CGFloat = 640.0
    let videoWidth : CGFloat = 640.0
    let videoSize = CGSize(width: videoWidth, height: videoHeight)

    let parentLayer = CALayer()
    let videoLayer = CALayer()
    parentLayer.frame = CGRect(x: 0, y: 0, width: videoSize.width, height: videoSize.height)
    videoLayer.frame = CGRect(x: 0, y: 0, width: videoSize.width, height: videoSize.height)
    parentLayer.addSublayer(videoLayer)

    // Stitch all of the videos together
    let composition = AVMutableComposition()
    let track = composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID:Int32(kCMPersistentTrackID_Invalid))

    // Add a blank video for the first 3 seconds which we'll add the cover image to
    let blankVideoPath = Bundle.main.path(forResource: "blankVideo", ofType:"MOV")!
    let blankVideoAssetUrl = URL(fileURLWithPath: blankVideoPath)
    let blankVideoAsset = AVAsset(url: blankVideoAssetUrl)
    let blankVideoAssetTrack = blankVideoAsset.tracks(withMediaType: AVMediaType.video)[0] as AVAssetTrack
    // Set it to 6 seconds since we're going to speed this up by 2x
    try? track?.insertTimeRange(CMTimeRangeMake(kCMTimeZero, CMTime(seconds: 6, preferredTimescale: 30)), of: blankVideoAssetTrack, at: kCMTimeZero)

    var index = 0
    var sampleOriginalVideoTrack: AVAssetTrack?
    var firstFrameImage: UIImage?
    for exercise in ["CORE", "SQUAT", "PUSH", "LUNGE", "PULL", "AGILITY", "BEND"] {
      if let videoPathString = videoLocations[exercise] as? String {
        let assetUrl = URL(fileURLWithPath: videoPathString)
        let videoAsset = AVAsset(url: assetUrl)
        let assetTrack = videoAsset.tracks(withMediaType: AVMediaType.video)[0] as AVAssetTrack
        if index == 0 {
          sampleOriginalVideoTrack = assetTrack

          let imgGenerator = AVAssetImageGenerator(asset: videoAsset)
          imgGenerator.appliesPreferredTrackTransform = true
          let cgImage = try! imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
          firstFrameImage = UIImage(cgImage: cgImage)
          firstFrameImage = UIImage.resizedCroppedImage(image: firstFrameImage!, newSize: CGSize(width: 640, height: 640))
        }

        // Setup exercise name and rep count overlays
        let exerciseOverlayContainer = CALayer()
        exerciseOverlayContainer.frame = CGRect(x: 0, y: 0, width: videoWidth, height: videoHeight)
        exerciseOverlayContainer.opacity = 0

        let exerciseNameOverlay = CALayer()
        let exerciseNameOverlayImage = UIImage(named: "\(exercise.lowercased())Badge")
        exerciseNameOverlay.contents = exerciseNameOverlayImage!.cgImage
        exerciseNameOverlay.frame = CGRect(x: 282, y: 65, width: 358, height: 88)
        exerciseNameOverlay.zPosition = 5
        exerciseOverlayContainer.addSublayer(exerciseNameOverlay)

        var repCount = 0
        for exerciseScore in exercises {
          if exerciseScore["exercise"] as? String == exercise {
            repCount = exerciseScore["reps"] as! Int
            break
          }
        }

        let exerciseRepCountOverlay = CATextLayer()
        exerciseRepCountOverlay.string = "\(repCount)"
        exerciseRepCountOverlay.font = CTFontCreateWithName("ProximaNova-Bold" as CFString, 50, nil);
        exerciseRepCountOverlay.fontSize = 52
        exerciseRepCountOverlay.foregroundColor = UIColor.black.cgColor
        exerciseRepCountOverlay.alignmentMode = kCAAlignmentCenter
        exerciseRepCountOverlay.frame = CGRect(x: 255, y: -490, width: videoWidth, height: videoHeight)
        exerciseRepCountOverlay.zPosition = 10
        exerciseRepCountOverlay.displayIfNeeded()
        exerciseOverlayContainer.addSublayer(exerciseRepCountOverlay)

        // If this is the first exercise, delay the animation by another second
        let textOffset = index == 0 ? 1.0 : 0.0

        let showAnimation = CAKeyframeAnimation(keyPath: "opacity")
        // divide by 2 because it's running at 2x speed and subtract 2 since we delay it due to the intro animation
        showAnimation.duration = videoAsset.duration.seconds / 2 - 2 - textOffset
        showAnimation.keyTimes = [0, 0.01, 0.99, 1]
        showAnimation.values = [0, 1, 1, 0]

        // divide by 2 because it's running at 2x speed and add 1 so it shows after the intro animation
        showAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + (composition.duration.seconds / 2) + 1 + textOffset
        showAnimation.isRemovedOnCompletion = false
        showAnimation.fillMode = kCAFillModeForwards
        exerciseOverlayContainer.add(showAnimation, forKey: "animateOpacity")

        parentLayer.addSublayer(exerciseOverlayContainer)

        let exerciseIntroOverlay = CALayer()
        let exerciseIntroOverlayImage = UIImage(named: "\(exercise.lowercased())Transition")
        exerciseIntroOverlay.contents = exerciseIntroOverlayImage!.cgImage
        exerciseIntroOverlay.frame = CGRect(x: 1405, y: 0, width: 1405, height: videoHeight)
        exerciseIntroOverlay.zPosition = 10

        let exerciseAnimation = CABasicAnimation(keyPath: "position.x")
        exerciseAnimation.duration = 2
        exerciseAnimation.fromValue = 1405
        exerciseAnimation.toValue = -765

        // If this is any exercise besides the first, have the animation overlap the last exercise by 1 second
        let animationOffset = index == 0 ? 0.0 : 1.0

        // divide by 2 because it's running at 2x speed
        exerciseAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + (composition.duration.seconds / 2) - animationOffset
        exerciseAnimation.isRemovedOnCompletion = false
        exerciseAnimation.fillMode = kCAFillModeForwards
        exerciseIntroOverlay.add(exerciseAnimation, forKey: "animatePosition")

        parentLayer.addSublayer(exerciseIntroOverlay)

        try? track?.insertTimeRange(CMTimeRangeMake(kCMTimeZero, videoAsset.duration), of: assetTrack, at: composition.duration)
        index += 1
      }
    }

    track?.preferredTransform = sampleOriginalVideoTrack!.preferredTransform

    // Speed up each video 2x
    let videoDuration = composition.duration
    let finalTimeScale : Int64 = videoDuration.value / 2
    track?.scaleTimeRange(CMTimeRangeMake(kCMTimeZero, videoDuration), toDuration: CMTimeMake(finalTimeScale, videoDuration.timescale))

    let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    let videoURLToSave = URL(fileURLWithPath: documentDirectory).appendingPathComponent("CombinedChallengeVideo.mov")
    do {
      try FileManager.default.removeItem(at: videoURLToSave)
    }
    catch {}

    let exporter = SDAVAssetExportSession(asset: composition)

    let titleScreenLayer = CALayer()
    titleScreenLayer.contents = firstFrameImage?.cgImage

    titleScreenLayer.frame = CGRect(x: 0, y: 0, width: videoWidth, height: videoHeight)
    titleScreenLayer.opacity = 0

    let introAnimation = CABasicAnimation(keyPath: "opacity")
    introAnimation.duration = 0.01
    introAnimation.fromValue = 1.0
    introAnimation.toValue = 0
    introAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + 3
    introAnimation.isRemovedOnCompletion = false
    introAnimation.fillMode = kCAFillModeBoth
    titleScreenLayer.add(introAnimation, forKey: nil)

    let titleScreenOverlayIntro = CALayer()
    let titleScreenOverlayIntroImage = UIImage(named: "challengeIntro")
    titleScreenOverlayIntro.contents = titleScreenOverlayIntroImage!.cgImage
    titleScreenOverlayIntro.frame = CGRect(x: 0, y: 0, width: videoWidth, height: videoHeight)
    titleScreenOverlayIntro.zPosition = 10
    titleScreenLayer.addSublayer(titleScreenOverlayIntro)

    let titleScreenScore = CATextLayer()
    titleScreenScore.string = String(totalScore)
    titleScreenScore.font = CTFontCreateWithName("ProximaNova-Bold" as CFString, 75, nil)
    titleScreenScore.fontSize = 75
    titleScreenScore.foregroundColor = UIColor.black.cgColor
    titleScreenScore.alignmentMode = kCAAlignmentCenter
    titleScreenScore.frame = CGRect(x: 243, y: -490, width: videoWidth, height: videoHeight)
    titleScreenScore.zPosition = 15
    titleScreenScore.displayIfNeeded()
    titleScreenLayer.addSublayer(titleScreenScore)

    let titleScreenName = CATextLayer()
    titleScreenName.string = challengeTitle.uppercased()
    titleScreenName.font = CTFontCreateWithName("ProximaNova-Bold" as CFString, 33, nil)
    titleScreenName.fontSize = 33
    titleScreenName.foregroundColor = UIColor.white.cgColor
    titleScreenName.alignmentMode = kCAAlignmentLeft
    titleScreenName.frame = CGRect(x: 120, y: -485, width: videoWidth, height: videoHeight)
    titleScreenName.zPosition = 15
    titleScreenName.displayIfNeeded()
    titleScreenLayer.addSublayer(titleScreenName)

    let titleScreenCourt = CATextLayer()
    titleScreenCourt.string = "\(challengeSubtitle.uppercased()) | \(locationName.uppercased())"
    titleScreenCourt.font = CTFontCreateWithName("ProximaNova-Bold" as CFString, 16, nil)
    titleScreenCourt.fontSize = 16
    titleScreenCourt.foregroundColor = UIColor.white.cgColor
    titleScreenCourt.alignmentMode = kCAAlignmentLeft
    titleScreenCourt.frame = CGRect(x: 120, y: -525, width: videoWidth, height: videoHeight)
    titleScreenCourt.zPosition = 15
    titleScreenCourt.displayIfNeeded()
    titleScreenLayer.addSublayer(titleScreenCourt)

    parentLayer.addSublayer(titleScreenLayer)

    let videoComposition = AVMutableVideoComposition()
    videoComposition.renderSize = videoSize
    videoComposition.frameDuration = CMTimeMake(1, 30)
    videoComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)

    let compositionInstruction = AVMutableVideoCompositionInstruction()
    compositionInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, composition.duration)

    let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track!)
    layerInstruction.setTransform(sampleOriginalVideoTrack!.preferredTransform, at: kCMTimeZero)

    // Crop, scale, and rotate all video segments after the intro to 640 x 640
    // FIXME: It seems our camera viewport may not be exactly in the middle so the `y:` may need to be adjusted slightly
    let cropTransform = CGAffineTransform(translationX: sampleOriginalVideoTrack!.naturalSize.height, y: -(sampleOriginalVideoTrack!.naturalSize.width - sampleOriginalVideoTrack!.naturalSize.height) / 2 )
    let scaleTransform = cropTransform.scaledBy(x: 0.889, y: 0.889)
    let rotateTransform = scaleTransform.rotated(by: .pi / 2)
    let translateTransform = rotateTransform.translatedBy(x: 0, y: 90)
    layerInstruction.setTransform(translateTransform, at: CMTime(seconds: 3, preferredTimescale: 30))

    compositionInstruction.layerInstructions = [layerInstruction]
    videoComposition.instructions = [compositionInstruction]
    exporter?.videoComposition = videoComposition

    exporter?.outputURL = videoURLToSave
    exporter?.outputFileType = AVFileType.mov.rawValue
    exporter?.shouldOptimizeForNetworkUse = true
    exporter?.videoSettings = [
      AVVideoCodecKey: AVVideoCodecH264,
      AVVideoWidthKey: 640,
      AVVideoHeightKey: 640,
      AVVideoCompressionPropertiesKey: [
        AVVideoAverageBitRateKey : NSInteger(750000),
        AVVideoProfileLevelKey : AVVideoProfileLevelH264BaselineAutoLevel
      ]
    ]
    exporter?.exportAsynchronously(completionHandler: {
      DispatchQueue.main.async {
        if exporter?.status == AVAssetExportSessionStatus.completed {
          let outputURL = exporter?.outputURL
          // Save to library
          if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(outputURL!.path) {
            PHPhotoLibrary.shared().performChanges({
              PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: outputURL!)
            }) { saved, error in
              let returnData = [
                "videoPath": outputURL!.absoluteString,
                ]
              // NSNull() represents error code
              callback([NSNull(), returnData])
            }
          }
        } else {
          // Error
          callback([NSNull(), []])
        }
      }
    })
  }


  // TODO: Possibly put in separate file with attribution to https://github.com/whydna/Reverse-AVAsset-Efficient
  static func reverse(_ original: AVAsset, outputPath: String, exerciseName: String, completion: @escaping (URL) -> Void) {
    // Initialize the reader

    var reader: AVAssetReader! = nil
    do {
      reader = try AVAssetReader(asset: original)
    } catch let error {
      print(error)
      print("could not initialize reader.")
      return
    }

    guard let videoTrack = original.tracks(withMediaType: AVMediaType.video).last else {
      print("could not retrieve the video track.")
      return
    }

    let readerOutputSettings: [String: Any] = [kCVPixelBufferPixelFormatTypeKey as String : Int(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)]
    let readerOutput = AVAssetReaderTrackOutput(track: videoTrack, outputSettings: readerOutputSettings)
    reader.add(readerOutput)

    reader.startReading()

    // read in samples

    var samples: [CMSampleBuffer] = []
    while let sample = readerOutput.copyNextSampleBuffer() {
      autoreleasepool {
        samples.append(sample)
      }
    }

    // Initialize the writer
    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
    let videoOutputURL = URL(fileURLWithPath: documentsPath.appendingPathComponent("AssembledWorkoutVideo.mov"))

    do {
      try FileManager.default.removeItem(at: videoOutputURL as URL)
    }
    catch {}

    let writer: AVAssetWriter
    do {
      writer = try AVAssetWriter(outputURL: videoOutputURL, fileType: AVFileType.mov)
    } catch let error {
      fatalError(error.localizedDescription)
    }

    let videoCompositionProps = [AVVideoAverageBitRateKey: videoTrack.estimatedDataRate]
    let writerOutputSettings = [
      AVVideoCodecKey: AVVideoCodecH264,
      AVVideoWidthKey: 640,
      AVVideoHeightKey: 640,
      AVVideoCompressionPropertiesKey: videoCompositionProps,
      AVVideoScalingModeKey: AVVideoScalingModeResizeAspectFill,
      ] as [String : Any]

    let writerInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: writerOutputSettings)
    writerInput.expectsMediaDataInRealTime = false
    writerInput.transform = videoTrack.preferredTransform

    let pixelBufferAdaptor = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: writerInput, sourcePixelBufferAttributes: nil)

    writer.add(writerInput)
    writer.startWriting()
    writer.startSession(atSourceTime: CMSampleBufferGetPresentationTimeStamp(samples.first!))

    // Add all original samples to the buffer
    for sample in samples {
      // Divide this CMTime by 2 to double the speed of the video
      let presentationTime = CMTimeMultiplyByRatio(CMSampleBufferGetPresentationTimeStamp(sample), 1, 2)
      let imageBufferRef = CMSampleBufferGetImageBuffer(sample)

      while !writerInput.isReadyForMoreMediaData {
        Thread.sleep(forTimeInterval: 0.1)
      }

      pixelBufferAdaptor.append(imageBufferRef!, withPresentationTime: presentationTime)
    }

    // Add the reverse of all samples back to create the boomerang effect
    for (index, sample) in samples.enumerated() {
      // Divide this CMTime by 2 to double the speed of the video
      let presentationTime = CMTimeMultiplyByRatio(CMSampleBufferGetPresentationTimeStamp(sample) + original.duration, 1, 2)
      let imageBufferRef = CMSampleBufferGetImageBuffer(samples[samples.count - 1 - index])

      while !writerInput.isReadyForMoreMediaData {
        Thread.sleep(forTimeInterval: 0.1)
      }

      pixelBufferAdaptor.append(imageBufferRef!, withPresentationTime: presentationTime)
    }

    writer.finishWriting {
      OverlayManager().applyOverlay(video: AVAsset(url: videoOutputURL), imageName: "\(exerciseName)Overlay") {
        status, outputURL in
        // TODO: We may want to check if `status` is completed or an error
        completion(outputURL!)
      }
    }
  }
}

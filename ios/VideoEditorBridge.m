//
//  VideoEditorBridge.m
//  NationalFitnessCampaign
//
//  Created by Rudy Mutter on 9/17/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

// VideoEditorBridge.m
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(VideoEditor, NSObject)

RCT_EXTERN_METHOD(transformWorkoutVideo:(NSString *)videoPath exerciseName:(NSString *)exerciseName callback: (RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(transformChallengeVideo:(NSDictionary *)videoLocations exercises:(NSArray *)exercises totalScore:(NSInteger *)totalScore locationCity:(NSString *)locationCity locationName:(NSString *)locationName challengeTitle:(NSString *)challengeTitle challengeSubtitle:(NSString *)challengeSubtitle callback: (RCTResponseSenderBlock)callback)

@end

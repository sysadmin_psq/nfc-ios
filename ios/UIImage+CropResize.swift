//
//  UIImage+CropResize.swift
//  NationalFitnessCampaign
//
//  Created by Rudy Mutter on 10/23/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

import AVFoundation
import ImageIO
import UIKit

extension UIImage {
  static func resizedCroppedImage(image: UIImage, newSize:CGSize) -> UIImage? {
    var ratio: CGFloat = 0
    var delta: CGFloat = 0
    var offset = CGPoint.zero

    if image.size.width > image.size.height {
      ratio = newSize.width / image.size.width
      delta = (ratio * image.size.width) - (ratio * image.size.height)
      offset = CGPoint(x: delta / 2, y: 0)
    } else {
      ratio = newSize.width / image.size.height
      delta = (ratio * image.size.height) - (ratio * image.size.width)
      offset = CGPoint(x: 0, y: delta / 2)
    }

    let clipRect = CGRect(x: -offset.x, y: -offset.y, width: (ratio * image.size.width) + delta, height: (ratio * image.size.height) + delta)
    UIGraphicsBeginImageContextWithOptions(newSize, true, 0.0)
    UIRectClip(clipRect)
    image.draw(in: clipRect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    return newImage
  }
}

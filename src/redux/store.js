// @flow
import { applyMiddleware, createStore, compose } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { createReactNavigationReduxMiddleware, createReduxBoundAddListener } from 'react-navigation-redux-helpers';
import epics from './epics';
import reducer from './reducers';
import thunk from 'redux-thunk';

const navigationMiddleware = createReactNavigationReduxMiddleware(
    'root',
    state => state.appNavigation,
);
export const addListener = createReduxBoundAddListener('root');

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [
    thunk,
    createEpicMiddleware(epics),
    navigationMiddleware,
];

export default createStore(reducer,
  composeEnhancers(applyMiddleware(...middleware))
);

// @flow
import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import appNavigation from 'modules/auth/navigator/state/dux';
import auth from 'modules/auth/state/dux';
import challenge from 'modules/challenge/state/dux';
import main from 'modules/main/state/dux';
import shared from 'shared/state/dux';

export default combineReducers({
    appNavigation,
    auth,
    challenge,
    form,
    main,
    shared,
});

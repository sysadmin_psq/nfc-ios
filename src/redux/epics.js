// @flow
import { combineEpics } from 'redux-observable';
import authEpics from 'modules/auth/state/epics';
import challengeEpics from 'modules/challenge/state/epics';
import mainEpics from 'modules/main/state/epics';
import sharedEpics from 'shared/state/epics';

export default combineEpics(
    ...authEpics,
    ...challengeEpics,
    ...mainEpics,
    ...sharedEpics,
);

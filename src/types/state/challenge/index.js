// @flow
export type $ExercisesObject = { [key: string]: number };

export type $ExerciseScore = {
    exercise: string,
    points: number,
    reps: number,
    multiplierText: string,
}

export type $ChallengeState = {
    +activeExerciseIndex: number,
    +bottomNav: {
        +tabOffset: Object,
    },
    +exerciseTypeKey: string,
    +exerciseOverlay: {
        +largeText: string,
        +visible: boolean,
    },
    +onboardingStep: 0,
    +reps: $ExercisesObject,
    +scores: Array<$ExerciseScore>,
    +share: {
        uploadingChallengeSubmission: boolean,
        uploadedChallengeSubmission: boolean,
    },
    +showStartButtonWhenOnboarded: boolean,
    +timer: {
        +active: boolean,
        +progressOffset: Object,
        +showSlider: boolean,
        +startTime: number,
    },
    +totalScore: number,
    +recording: boolean,
    +videoLocations: $ExercisesObject,
    +finalizedVideoPath: string,
}

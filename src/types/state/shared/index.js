// @flow
export type $SharedState = {
    notification: {
        +text: string,
        +showCloseButton: boolean,
        +visible: boolean,
    },
};

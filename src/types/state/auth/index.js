// @flow
export type $AuthState = {
    +marketingOptIn: boolean,
    +passwordReset: boolean,
    +passwordResetEmail: string,
};

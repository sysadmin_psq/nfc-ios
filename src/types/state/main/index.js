// @flow
export type $FitnessCourt = {
    id: ?number,
    city: ?string,
    name: ?string,
    street: ?string,
    image: ?string,
    sponsorName: ?string,
    callToActionLink: ?string,
    callToActionDisplay: ?string,
    openingSoon: boolean,
    latitude: string,
    longitude: string,
    location_id: string,
};

export type $UserInfo = {
    email: ?string,
    id: ?number,
    marketingOptIn: ?boolean,
    name: ?string,
    username: ?string,
    gender: ?string,
    city: ?string,
    state: ?string,
    age_group: ?string,
};

export type $Video = {
    id: number,
    url: string,
};

export type $SponsorInfo = {|
    exerciseTimes: Array<number>,
    sponsorImage: string,
    sponsorLink: string,
|};

export type $TrainAudioItem = {|
  exerciseTimes: Array<number>,
  sponsorImage: string,
  sponsorLink: string,
  audioFileName: string,
  outroStartTime: number,
  thumbnailImage: string,
  trainerName: string,
  trainerTitle: string,
  trainerImage: string,
  trainerExternalLink: string,
  trainerVideo: string,
  seconds_to_skip: number
|};

export type $TrainAudio = $TrainAudioItem[];

type $_MainState = {|
    appPermissionsGranted: boolean,
    deviceId: string,
    currentCourt: ?number,
    courts: Array<$FitnessCourt>,
    fitnessCourt: $FitnessCourt,
    learnFeed: {
        page: number,
        refreshing: boolean,
        videos: Array<$Video>,
    },
    locationsEnabled: boolean,
    showResetPasswordMessage: boolean,
    train: {
        audioPaused: boolean,
        showTrainModal: boolean,
        routines: ?$TrainAudio,
        currentRoutineIdx: ?number,
    },
    onboarded: boolean,
    userInfo: $UserInfo,
|};

// https://github.com/facebook/flow/issues/2405
export type $MainState = $_MainState & $Shape<$_MainState>;

// @flow
import { AUTH_ROUTES, CHALLENGE_ROUTES, MAIN_ROUTES } from 'constants';

export type $AuthRoute = $Values<typeof AUTH_ROUTES>;
export type $ChallengeRoute = $Values<typeof CHALLENGE_ROUTES>;
export type $MainRoute = $Values<typeof MAIN_ROUTES>;

export type $Route = $AuthRoute | $ChallengeRoute | $MainRoute;

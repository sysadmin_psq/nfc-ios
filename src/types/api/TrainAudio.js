// @flow

type $Chapter = {
    id: number,
    name: string,
    start_time: number,
};

export type $TrainAudioResponse = {
    count: number,
    next: ?string,
    previous: ?string,
    results: {
        chapters: $Chapter[],
        sponsor_link: string,
        sponsor_image: string,
        audio_file_name: string,
        outro_start: number,
        thumbnail_image: string,
        trainer_name: string,
        trainer_title: string,
        trainer_image: string,
        trainer_external_link: string,
        trainer_video: string,
        seconds_to_skip: number,
    }[],
}

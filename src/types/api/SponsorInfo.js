// @flow
export type $SponsorInfoResponse = {
    count: number,
    next: ?string,
    previous: ?string,
    results: [
        {
            exercise_five_start: number,
            exercise_four_start: number,
            exercise_one_start: number,
            exercise_seven_start: number,
            exercise_six_start: number,
            exercise_three_start: number,
            exercise_two_start: number,
            outro_start: number,
            sponsor_link: string,
            sponsor_image: string,
        }
    ],
}

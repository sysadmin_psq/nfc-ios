// @flow
export type $ServerFitnessCourt = {
    id: number,
    name: string,
    street: ?string,
    city_name: string,
    court_image: ?string,
    sponsor_name: ?string,
    call_to_action_link: ?string,
    call_to_action_display: ?string,
    opening_soon: boolean,
    latitude: string,
    longitude: string,
    location_id: string,
};

export type $InvalidResponse = string;

export type $ValidResponse = {
    results: Array<$ServerFitnessCourt>,
};

export type $Response = $InvalidResponse | $ValidResponse;

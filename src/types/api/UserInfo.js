// @flow
export type $UserInfoResponse = {
    accepted_terms: string,
    content_type: number,
    device_id: string,
    email: string,
    fullname: string,
    id: number,
    marketing_opt_in: boolean,
    username: string,
    gender: string,
    city: string,

};

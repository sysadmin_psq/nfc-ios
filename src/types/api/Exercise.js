// @flow
export type $ServerExercise = {
    id: number,
    exercise: number,
    video: string,
};

export type $InvalidResponse = {|
    detail: string,
|};

export type $ValidResponse = {|
    count: number,
    next: ?string,
    previous: ?string,
    results: Array<$ServerExercise>,
|};

export type $Response = $InvalidResponse | $ValidResponse;

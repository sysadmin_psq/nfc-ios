// @flow
import { Dimensions } from 'react-native';

export const SCREEN_WIDTH = Dimensions.get('window').width;
export const SCREEN_HEIGHT = Dimensions.get('window').height;

// exercises
// -----------------------------------------------------------------------------
export const EXERCISE_POINT_MULTIPLIER = {
    CORE: 1,
    SQUAT: 2,
    PUSH: 1,
    LUNGE: 1,
    PULL: 3,
    AGILITY: 2,
    BEND: 1,
};

export const EXERCISES = {
    CORE: 'CORE',
    SQUAT: 'SQUAT',
    PUSH: 'PUSH',
    LUNGE: 'LUNGE',
    PULL: 'PULL',
    AGILITY: 'AGILITY',
    BEND: 'BEND'
};
// this should stay in order of exercises
export const EXERCISE_LIST = [
    EXERCISES.CORE,
    EXERCISES.SQUAT,
    EXERCISES.PUSH,
    EXERCISES.LUNGE,
    EXERCISES.PULL,
    EXERCISES.AGILITY,
    EXERCISES.BEND
];

export const NUMBER_OF_EXERCISES = EXERCISE_LIST.length;

// react-navigation
// -----------------------------------------------------------------------------
export const BACK_ACTION_IDENTIFIER = 'Navigation/BACK';

// routing
// -----------------------------------------------------------------------------
export const AUTH_ROUTES = {
    APP: 'App',
    CREATE_ACCOUNT_EMAIL: 'CreateAccountEmail',
    CREATE_ACCOUNT_NAME: 'CreateAccountName',
    LOADING: 'Loading',
    LOGIN: 'Login',
    RESET_PASSWORD: 'ResetPassword',
    TUTORIAL: 'Tutorial',
    WELCOME: 'Welcome',
};

export const CHALLENGE_ROUTES = {
    RECORD: 'Record',
    SCORE: 'Score',
    SHARE: 'Share',
    LEADERBOARD: 'LeaderBoard',
    MYENTRIES: 'MyEntries',
    TRAIN_VIDEO: 'TrainVideo',
    MY_CLASSES: 'MyClasses',
    EVENTDETAIL: 'EventDetail',
    COURTLIST: 'CourtList',
    ABOUT: 'About',
};

export const MAIN_ROUTES = {
    CHALLENGE: 'Challenge',
    CHALLENGE_MODE: 'ChallengeMode',
    EDIT_PROFILE: 'EditProfile',
    LEARN: 'Learn',
    LOCATIONS: 'Locations',
    LOCATION_DETAIL: 'LocationDetail',
    TRAIN: 'Train',
    TRAIN_DETAIL: 'TrainDetail',
    TEST: 'Test',
    COURTS: 'Courts',
    SETTINGS: 'Settings',
    ALLCOURTSDETAIL: 'AllCourtsDetail'

};

export const DRILLDOWN_ROUTES = [
  MAIN_ROUTES.TRAIN_DETAIL,
  MAIN_ROUTES.LOCATION_DETAIL,
  MAIN_ROUTES.ABOUT
];

// timing
// -----------------------------------------------------------------------------
export const NOTIFICATION_DURATION = 5000;
export const WORKOUT_DURATION = 5000;

export const CHALLENGE_MODE_IDS = {
    THREE_MINUTE: '3-minute-challenge',
    FIVE_MINUTE: '5-minute-challenge',
    SEVEN_MINUTE: '7-minute-challenge',
};

export const CHALLENGE_MODE = {
    [CHALLENGE_MODE_IDS.THREE_MINUTE]:{
        EXERCISE_TIME: 15000,
        REST_TIME: 15000,
    },
    [CHALLENGE_MODE_IDS.FIVE_MINUTE]:{
        EXERCISE_TIME: 30000,
        REST_TIME: 15000,
    },
    [CHALLENGE_MODE_IDS.SEVEN_MINUTE]: {
        EXERCISE_TIME: 45000,
        REST_TIME: 15000,
    },
};

// Learn Feed
// -----------------------------------------------------------------------------
export const FEED_LOCATIONS = {
    BOTTOM: 'bottom',
    TOP: 'top',
};

export const PAGINATION = {
    INCREMENT: 'increment',
    RESET: 'reset',
};

// FeedVideo
// -----------------------------------------------------------------------------
export const VIDEO_MARGIN = 16;
export const VIDEO_PADDING_BOTTOM = 5;
export const VIDEO_COMPONENT_HEIGHT = SCREEN_WIDTH + VIDEO_PADDING_BOTTOM + VIDEO_MARGIN;

// urls
// -----------------------------------------------------------------------------
export const S3_BUCKET_URL = 'https://d2k219ow21tyn6.cloudfront.net';

// TODO: This to be replaced with an API call
export const S3_DJ_SOUND_URL = 'https://d2k219ow21tyn6.cloudfront.net/guided_circuit_audio/';

// AsyncStorage
// -----------------------------------------------------------------------------
// the following variables need to be unique
export const ONBOARDING_STORAGE_STRING = 'onboarded';
export const STORAGE_USER_SESSION = 'userSession';

// US States Name
export const STATES_NAME = [
    {
        name: "Alabama",
        abbreviation: "AL"
    },
    {
        name: "Alaska",
        abbreviation: "AK"
    },
    {
        name: "American Samoa",
        abbreviation: "AS"
    },
    {
        name: "Arizona",
        abbreviation: "AZ"
    },
    {
        name: "Arkansas",
        abbreviation: "AR"
    },
    {
        name: "California",
        abbreviation: "CA"
    },
    {
        name: "Colorado",
        abbreviation: "CO"
    },
    {
        name: "Connecticut",
        abbreviation: "CT"
    },
    {
        name: "Delaware",
        abbreviation: "DE"
    },
    {
        name: "District Of Columbia",
        abbreviation: "DC"
    },
    {
        name: "Federated States Of Micronesia",
        abbreviation: "FM"
    },
    {
        name: "Florida",
        abbreviation: "FL"
    },
    {
        name: "Georgia",
        abbreviation: "GA"
    },
    {
        name: "Guam",
        abbreviation: "GU"
    },
    {
        name: "Hawaii",
        abbreviation: "HI"
    },
    {
        name: "Idaho",
        abbreviation: "ID"
    },
    {
        name: "Illinois",
        abbreviation: "IL"
    },
    {
        name: "Indiana",
        abbreviation: "IN"
    },
    {
        name: "Iowa",
        abbreviation: "IA"
    },
    {
        name: "Kansas",
        abbreviation: "KS"
    },
    {
        name: "Kentucky",
        abbreviation: "KY"
    },
    {
        name: "Louisiana",
        abbreviation: "LA"
    },
    {
        name: "Maine",
        abbreviation: "ME"
    },
    {
        name: "Marshall Islands",
        abbreviation: "MH"
    },
    {
        name: "Maryland",
        abbreviation: "MD"
    },
    {
        name: "Massachusetts",
        abbreviation: "MA"
    },
    {
        name: "Michigan",
        abbreviation: "MI"
    },
    {
        name: "Minnesota",
        abbreviation: "MN"
    },
    {
        name: "Mississippi",
        abbreviation: "MS"
    },
    {
        name: "Missouri",
        abbreviation: "MO"
    },
    {
        name: "Montana",
        abbreviation: "MT"
    },
    {
        name: "Nebraska",
        abbreviation: "NE"
    },
    {
        name: "Nevada",
        abbreviation: "NV"
    },
    {
        name: "New Hampshire",
        abbreviation: "NH"
    },
    {
        name: "New Jersey",
        abbreviation: "NJ"
    },
    {
        name: "New Mexico",
        abbreviation: "NM"
    },
    {
        name: "New York",
        abbreviation: "NY"
    },
    {
        name: "North Carolina",
        abbreviation: "NC"
    },
    {
        name: "North Dakota",
        abbreviation: "ND"
    },
    {
        name: "Northern Mariana Islands",
        abbreviation: "MP"
    },
    {
        name: "Ohio",
        abbreviation: "OH"
    },
    {
        name: "Oklahoma",
        abbreviation: "OK"
    },
    {
        name: "Oregon",
        abbreviation: "OR"
    },
    {
        name: "Palau",
        abbreviation: "PW"
    },
    {
        name: "Pennsylvania",
        abbreviation: "PA"
    },
    {
        name: "Puerto Rico",
        abbreviation: "PR"
    },
    {
        name: "Rhode Island",
        abbreviation: "RI"
    },
    {
        name: "South Carolina",
        abbreviation: "SC"
    },
    {
        name: "South Dakota",
        abbreviation: "SD"
    },
    {
        name: "Tennessee",
        abbreviation: "TN"
    },
    {
        name: "Texas",
        abbreviation: "TX"
    },
    {
        name: "Utah",
        abbreviation: "UT"
    },
    {
        name: "Vermont",
        abbreviation: "VT"
    },
    {
        name: "Virgin Islands",
        abbreviation: "VI"
    },
    {
        name: "Virginia",
        abbreviation: "VA"
    },
    {
        name: "Washington",
        abbreviation: "WA"
    },
    {
        name: "West Virginia",
        abbreviation: "WV"
    },
    {
        name: "Wisconsin",
        abbreviation: "WI"
    },
    {
        name: "Wyoming",
        abbreviation: "WY"
    }
];
// Age Ranges
export const AGE_RANGES = [
    { value: "14-17" },
    { value: "18-29" },
    { value: "30-49" },
    { value: "50+" }
];
// Genders
export const GENDERS = [
    { value: "Male" },
    { value: "Female" },
    { value: "Prefer not to answer" },
    { value: "Not listed" }
];
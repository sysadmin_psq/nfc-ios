// @flow
import { StyleSheet } from 'react-native';
import { ProximaNova } from 'shared/styles/fonts';
import { transparentBlue, white } from 'shared/styles/colors';
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';

export default StyleSheet.create({
    container: {
        top: 0,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        // marginTop:isIPhoneX()?13:0,
        backgroundColor: transparentBlue,
    },
    largeText: {
        fontSize: 80,
        color: white,
        ...ProximaNova.CondensedBold,
    },
    smallText: {
        fontSize: 24,
        letterSpacing: 0.5,
        color: white,
        ...ProximaNova.CondensedBold,
    },
});

// @flow
import React from 'react';
import { Text, View } from 'react-native';
import styles from './styles';

type $Props = {
    largeText: any,
    smallText: string,
    visible: boolean,
}

export default ({ largeText, smallText, visible }: $Props) => {
    if (visible) {
        return (
            <View style={styles.container}>
                <Text style={styles.smallText}>
                    {smallText}
                </Text>
                <Text style={styles.largeText}>
                    {largeText}
                </Text>
            </View>
        );
    }
    return null;
}

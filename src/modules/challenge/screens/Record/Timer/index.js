// @flow
import { Animated, View, Text } from 'react-native';
import { connect } from 'react-redux';
import { SCREEN_WIDTH } from 'constants';
import { TIMER_HEIGHT } from 'modules/challenge/styleConstants';
import Parallelogram from 'shared/components/Parallelogram';
import React from 'react';
import select from 'modules/challenge/state/selectors/Timer';
import OnboardingStep2 from './OnboardingStep2';
import styles from './styles';
import Time from './Time';

type $Props = {
    exerciseLabel: string,
    progressColor: string,
    progressOffset: any,
    showSlider: boolean,
}

const Timer = ({
    exerciseLabel,
    progressColor,
    progressOffset,
    showSlider,
}: $Props) => (
    <View style={styles.container}>
        <OnboardingStep2 />
        {showSlider &&
            <Animated.View
                style={[
                    styles.sliderContainer,
                    {
                        marginLeft: progressOffset,
                    },
                ]}
            >
                <Parallelogram
                    color={progressColor}
                    height={TIMER_HEIGHT}
                    width={SCREEN_WIDTH}
                />
            </Animated.View>
        }
        <Text style={styles.label}>
            {exerciseLabel}
        </Text>
        <Time />
    </View>
);

export default connect(select)(Timer);

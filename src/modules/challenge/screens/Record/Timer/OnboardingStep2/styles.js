// @flow
import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        position: "absolute",
        width: 244,
        height: 110,
        bottom: -20,
        right: 14,
        zIndex: 1,
    },
});

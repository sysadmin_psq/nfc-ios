// @flow
import { connect } from 'react-redux';
import OnboardingStep from 'modules/challenge/sharedComponents/OnboardingStep';
import { View } from 'react-native';
import * as challengeActions from 'modules/challenge/state/dux';
import React from 'react';
import select from 'modules/challenge/state/selectors/OnboardingSteps';
import styles from './styles';


type $Props = {
    dispatch: any,
    step2Visible: boolean,
}

const OnboardingStep2 = ({ dispatch, step2Visible }: $Props) => {
    if (!step2Visible) {
        return null;
    }

    const onPress = () => dispatch(challengeActions.continueOnboarding());

    const text = "Use the + and - buttons to count how many reps you do. This is how we'll score your entry.";
    return (
        <View style={styles.container}>
            <OnboardingStep
                onPress={onPress}
                text={text}
                triangleStyles={{
                    bottom: -16,
                    right: 14,
                    transform: [{rotate: '180deg'}],
                }}
            />
        </View>
    );
}

export default connect(select)(OnboardingStep2);

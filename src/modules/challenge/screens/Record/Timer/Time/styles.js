// @flow
import { black, transparent } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';


export default StyleSheet.create({
    time: {
        backgroundColor: transparent,
        fontSize: 43,
        lineHeight: 43,
        color: black,
        ...ProximaNova.Medium,
    },
})

// @flow
import { connect } from 'react-redux';
import { Text } from 'react-native';
import createReactClass from 'create-react-class';
import PropTypes from 'prop-types';
import React from 'react';
import select from 'modules/challenge/state/selectors/Time';
import styles from './styles';
import TimerMixin from 'react-timer-mixin';


const Time = createReactClass({  // eslint-disable-line
    propTypes: {
        active: PropTypes.bool.isRequired,
        recording: PropTypes.bool.isRequired,
        startTime: PropTypes.number.isRequired,
        challengeMode: PropTypes.object.isRequired,
    },

    mixins: [TimerMixin],

    componentWillReceiveProps(newProps) {
        if (newProps.active) {
            this.updateComponent();
        }
    },

    updateComponent() {
        this.requestAnimationFrame(() => {
            this.forceUpdate();
            this.updateComponent();
        });
    },

    _getTotalTime() {
      return this.props.recording
          ? this.props.challengeMode.EXERCISE_TIME
          : this.props.challengeMode.REST_TIME;
    },

    _getTimeRemaining(startTime) {
        const currentTime = new Date().getTime();

        let elapsedTime = 0;
        if (startTime) {
            elapsedTime = currentTime - startTime;
        }

        const totalTime = this._getTotalTime();

        const millisecondsRemaining = totalTime - elapsedTime;

        if (millisecondsRemaining < 0) {
            return 0;
        }

        const timeRemaining = new Date(millisecondsRemaining);
        let secondsRemaining = timeRemaining.getSeconds() + 1;

        return `${secondsRemaining}`;
    },

    render() {
        const { active, startTime } = this.props;
        let timeRemaining = active ? this._getTimeRemaining(startTime) : this._getTotalTime() / 1000;

        return (
            <Text style={styles.time}>
                {timeRemaining}
            </Text>
        );
    },
});

export default connect(select)(Time)

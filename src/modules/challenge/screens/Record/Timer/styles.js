// @flow
import { black, transparent } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';
import { TIMER_HEIGHT } from 'modules/challenge/styleConstants';

export default StyleSheet.create({
    container: {
        height: TIMER_HEIGHT,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        zIndex: 1,
    },
    label: {
        backgroundColor: transparent,
        fontSize: 18,
        letterSpacing: 1,
        color: black,
        ...ProximaNova.CondensedBold,
    },
    sliderContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
    },
});

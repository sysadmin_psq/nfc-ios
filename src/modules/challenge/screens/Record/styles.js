// @flow
import { isIOS, isIPhone4, isIPhoneX } from 'shared/utils';
import { StyleSheet } from 'react-native';
import { SCREEN_WIDTH } from 'constants';
import { white } from 'shared/styles/colors';

export default StyleSheet.create({
    camera: {
        flex: 1,
    },
    cameraContainer: {
        zIndex: 1,
        height: isIPhone4() ? 230 : SCREEN_WIDTH,
        backgroundColor: "#eee",
    },
    container: {
        flex: 1,
        backgroundColor: white,
    },
    overlayContainer: {
        zIndex: 5,
        height: SCREEN_WIDTH,
        width: SCREEN_WIDTH,
        position: 'absolute',
        top: isIOS() ? isIPhoneX() ? 80 : 65 : 45,
        left: 0,
    },
    step3Container: {
        position: 'absolute',
        top: 0,
        left: 0,
        paddingBottom: 20,
        alignItems: 'center',
        justifyContent: 'flex-end',
        height: SCREEN_WIDTH,
        width: SCREEN_WIDTH,
    },
});

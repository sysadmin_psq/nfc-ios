// @flow
import { connect } from 'react-redux';
import { Text, TouchableOpacity, View } from 'react-native';
import React from 'react';
import select from '../../../state/selectors/StartChallengeStep';
import styles from './styles';
import onboardingStyles from 'modules/challenge/sharedComponents/OnboardingStep/styles';
import Triangle from 'modules/challenge/sharedComponents/Triangle';


type $Props = {
    onPress: () => void,
    showTip: boolean,
    visible: boolean,
}

const StartChallengeStep = ({ onPress, showTip, visible }: $Props) => {
    if (!visible) {
        return null;
    }

    return (
        <View
            style={[
                onboardingStyles.container,
                {
                    width: 278,
                    height: showTip ? 170 : 100,
                },
            ]}
        >
            {showTip &&
                <View style={onboardingStyles.yellowRectangle} />
            }
            {showTip &&
                <Text style={[onboardingStyles.text, styles.text]}>
                    Keep an eye on the time to see when the 15 seconds is up.
                </Text>
            }
            <Triangle
                extraStyles={{
                    bottom: -16,
                    left: 129,
                    transform: [{rotate: '180deg'}],
                }}
            />
            <Text style={[onboardingStyles.text, styles.text]}>
                Get to your first station and hit start!
            </Text>
            <TouchableOpacity
                onPress={onPress}
                style={styles.startButton}
            >
                <Text style={styles.startButtonText}>
                    START
                </Text>
            </TouchableOpacity>
        </View>
    );
}

export default connect(select)(StartChallengeStep);

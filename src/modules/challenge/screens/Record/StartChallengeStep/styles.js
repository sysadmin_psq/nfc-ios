// @flow
import { blue, white } from "shared/styles/colors";
import { ProximaNova } from "shared/styles/fonts";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
    startButton: {
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center",
        height: 36,
        width: 135,
        backgroundColor: blue,
        borderRadius: 2,
    },
    startButtonText: {
        color: white,
        fontSize: 16,
        letterSpacing: 1,
        ...ProximaNova.CondensedBold,
    },
    text: {
        marginBottom: 20,
    },
});

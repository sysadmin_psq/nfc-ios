// @flow
import { connect } from 'react-redux';
import { View, Text, Platform, TouchableOpacity, Image } from 'react-native';
import * as challengeActions from '../../state/dux';
import Camera from 'react-native-camera';
import ExerciseNav from 'shared/components/ExerciseNav';
import OnboardingStep1 from './OnboardingStep1';
import Overlay from './Overlay';
import React, { Component } from 'react';
import RepCounter from './RepCounter';
import select from '../../state/selectors/RecordView';
import StartChallengeStep from './StartChallengeStep';
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import styles from './styles';
import Timer from './Timer';
import TopNav from '../../sharedComponents/TopNav'; // eslint-disable-line
import Permissions from 'react-native-permissions'
import { NavigationBar } from 'navigationbar-react-native';
import {MAIN_ROUTES, CHALLENGE_ROUTES} from "../../../../constants/index";
import { ProximaNova } from 'shared/styles/fonts';
import CloseButton from 'shared/components/CloseButton';

type $Props = {
    activeExerciseIndex: number,
    dispatch: any,
    exerciseOverlay: {
        largeText: number,
        visible: boolean,
    },
    navigation: *,
    tabOffset: *,
}



const ComponentLeft = ({navigation}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-start', paddingLeft: 20,}} >

            <CloseButton
                style={ {justifyContent:'center', flexDirection: 'row'}}
                onPress={() => { navigation.navigate(MAIN_ROUTES.CHALLENGE);}}
            />

        </View>
    );
};

const ComponentCenter = () => {
    return(
        <View style={{ flex: 1, marginLeft: 10}}>

            <Text style={{ color: 'white', ...ProximaNova.Regular, fontSize: 18}}>Permissions</Text>
        </View>
    );
};

const ComponentRight = () => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-end', }}>
            <View>

                <Image
                    source={require('assets/icons/Filter.png')}
                    style={{ resizeMode: 'contain', padding: 10 ,marginRight:16, alignSelf: 'center', zIndex: 999, display:'none' }}
                />

            </View>
        </View>
    );
};


class RecordScreen extends Component<$Props> {
    camera: *;


    componentWillMount() {

        // print("inside the component will mount")

        Permissions.checkMultiple(['camera', 'photo']).then(response => {
            //response is an object mapping type to permission


            // print("inside the checkMultiple in componentWillMount", response)


            if(response.camera == 'undetermined' || response.photo == 'undertermined') {


                console.log("is undetermined")
                Permissions.request('camera').then(cameraPermission => {
                    // Returns once the user has chosen to 'allow' or to 'not allow' access
                    // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'


                    if(cameraPermission == 'authorized') {


                        Permissions.request('photo').then(photoPermission => {

                            if(photoPermission == 'authorized') {


                                console.log("after request true")
                                this.setState({

                                    cameraPermission: true
                                })
                            }
                            else {

                                this.setState({

                                    cameraPermission: false,
                                    permissionText: 'Enable Photos',
                                    permissionErrorMessage: 'Photos Permission Required'
                                })
                            }
                        })
                    }
                    else {

                        console.log("after request fasle")

                        this.setState({

                            cameraPermission:false,
                            permissionText: 'Enable Camera',
                            permissionErrorMessage: 'Camera Permission Required'
                        })
                    }
                })
            }
            else {


                console.log("is determined", response)

                if(response.camera == 'authorized' && response.photo == 'undetermined') {

                    console.log("is determined and authorized")



                    Permissions.request('photo').then(photoPermission => {

                        if(photoPermission == 'authorized') {


                            console.log("after request true")
                            this.setState({

                                cameraPermission: true
                            })
                        }
                        else {

                            this.setState({

                                cameraPermission: false,
                                permissionText: 'Enable Photos',
                                permissionErrorMessage: 'Photos Permission Required'
                            })
                        }
                    })

                }
                else if(response.camera == 'authorized' && response.photo == 'authorized') {

                    this.setState({

                        cameraPermission: true
                    })
                }
                else {

                    var permissionText = ''
                    var permissionError = ''

                    console.log("is determined and but denied")
                    if(response.camera == 'denied' || response.camera == 'undetermined') {

                        permissionText = 'Enable Camera'
                        permissionError = 'Camera Permission Required'

                    }
                    else if(response.photo == 'denied' || response.photo == 'undetermined') {

                        permissionText = 'Enable Photos'
                        permissionError = 'Photos Permission Required'
                    }


                    this.setState({

                        cameraPermission:false,
                        permissionText: permissionText,
                        permissionErrorMessage: permissionError,

                    })
                }
            }

        })

    }

    state = {

        cameraPermission: null,
        permissionText: '',
        permissionErrorMessage: ''
    }


    render() {
        const {
            activeExerciseIndex,
            dispatch,
            tabOffset,
            exerciseOverlay,
        } = this.props;

        return (

            <View style={styles.container}>

            { this.state.cameraPermission == true &&

            <View style={styles.container}>

                <TopNav />
                <OnboardingStep1 />
                <View style={styles.overlayContainer}>
                    <View style={styles.step3Container}>
                        <StartChallengeStep
                            onPress={() => dispatch(challengeActions.startChallenge(this.camera))}
                        />
                    </View>
                    <Overlay
                        smallText='GET TO THE NEXT STATION'
                        largeText={exerciseOverlay.largeText}
                        visible={exerciseOverlay.visible}
                    />
                </View>

                <View style={styles.cameraContainer}>
                    <Camera
                        ref={(instance) => { this.camera = instance; }}
                        captureMode={Camera.constants.CaptureMode.video}
                        captureTarget={Camera.constants.CaptureTarget.disk}
                        style={styles.camera}
                        aspect={Camera.constants.Aspect.fill}
                        captureQuality={Camera.constants.CaptureQuality['720p']}>



                </Camera>

                </View>
                <Timer />
                <RepCounter />
                <ExerciseNav
                    activeExerciseIndex={activeExerciseIndex}
                    tabOffset={tabOffset}
                    checkmarkAfterFinish
                />
            </View>

            }

                {this.state.cameraPermission == false &&



                <View style={styles.container}>

                    <NavigationBar
                        componentLeft     = { () =>  <ComponentLeft navigation={this.props.navigation}/>   }
                        componentCenter   = { () =>  <ComponentCenter /> }
                        componentRight   = { () =>  <ComponentRight /> }
                        navigationBarStyle= {{ backgroundColor: '#0098D1', height:isIPhoneX()?80:55,paddingTop:isIPhoneX()?40:20,marginTop: Platform.OS === 'ios'? -20 : 0}}
                        statusBarStyle    = {{ barStyle: 'light-content', backgroundColor: '#215e79'}}
                    />

                    <View style={{position: 'relative', height: '90%', display: 'flex', alignItems: 'center', justifyContent:'center'}}>

                        <Text style={{ margin: 0, fontSize: 18, fontWeight: 'bold'}}>{this.state.permissionErrorMessage}</Text>
                        <Text style={{ margin: 0, marginTop: 5, color: '#9B9B9B'}}>Settings > App Settings > {this.state.permissionText}</Text>

                    </View>
                </View>



                }


            </View>

        );

    }
}

export default connect(select)(RecordScreen);

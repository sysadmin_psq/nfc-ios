// @flow
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        position: 'absolute',
        zIndex: 6,
        top: 64,
        right: 14,
    },
});

// @flow
import { connect } from 'react-redux';
import { View } from 'react-native';
import * as challengeActions from 'modules/challenge/state/dux';
import OnboardingStep from 'modules/challenge/sharedComponents/OnboardingStep';
import React from 'react';
import select from 'modules/challenge/state/selectors/OnboardingSteps';
import styles from './styles';


type $Props = {
    dispatch: any,
    step1Visible: boolean,
}

const Step1 = ({ dispatch, step1Visible }: $Props) => {
    if (!step1Visible) {
        return null;
    }

    const onPress = () => dispatch(challengeActions.continueOnboarding());
    const text = "We'll record 15 second clips automatically. This will turn red when we're recording.";
    return (
        <View style={styles.container}>
            <OnboardingStep
                onPress={onPress}
                text={text}
                triangleStyles={{
                    top: -16,
                    right: 14,
                }}
            />
        </View>
    );
}

export default connect(select)(Step1);

import { black, borderGrey } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        width: '100%',
        borderTopWidth: 1,
        borderColor: borderGrey,
    },
    box: {
        width: '33.3%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    boxMiddle: {
        flexDirection: 'column',
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderColor: borderGrey,
    },
    repsLabel: {
        color: black,
        fontSize: 13,
        ...ProximaNova.CondensedSemibold,
    },
    repsNumber: {
        color: black,
        fontSize: 28,
        ...ProximaNova.Semibold,
    },
})

// @flow
import { connect } from 'react-redux';
import { Image, View, Text, TouchableOpacity } from 'react-native';
import * as challengeActions from 'modules/challenge/state/dux';
import React from 'react';
import select from 'modules/challenge/state/selectors/RepCounter';
import styles from './styles';

type $Props = {
    addRepsEnabled: boolean,
    dispatch: any,
    reps: string | number,
    subtractRepsEnabled: boolean,
}

const RepCounter = ({ addRepsEnabled, dispatch, reps, subtractRepsEnabled }: $Props) => {
    const onMinusPress = () => {
        if (subtractRepsEnabled) {
            dispatch(challengeActions.subtractReps());
        }
    }

    const onAddPress = () => {
        if (addRepsEnabled) {
            dispatch(challengeActions.addReps());
        }
    }

    const addIconSource = addRepsEnabled ? require('assets/icons/Add.png') : require('assets/icons/AddDisabled.png');
    const minusIconSource = subtractRepsEnabled ? require('assets/icons/Minus.png') : require('assets/icons/MinusDisabled.png');

    return (
        <View style={styles.container}>
            <TouchableOpacity
                style={styles.box}
                onPress={() => onMinusPress()}
            >
                <Image source={minusIconSource} />
            </TouchableOpacity>
            <View style={[styles.box, styles.boxMiddle]}>
                <Text style={styles.repsLabel}>
                    REPS
                </Text>
                <Text style={styles.repsNumber}>
                    {reps}
                </Text>
            </View>
            <TouchableOpacity
                style={styles.box}
                onPress={() => onAddPress()}
            >
                <Image source={addIconSource} />
            </TouchableOpacity>
        </View>
    );
}

export default connect(select)(RepCounter);

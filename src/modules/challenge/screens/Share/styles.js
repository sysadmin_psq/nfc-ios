// @flow
import {
    black,
    blue,
    darkGrey,
    lightGrey,
    uploadedButtonGrey,
    uploadingButtonGrey,
    white,
} from 'shared/styles/colors';
import { StyleSheet } from 'react-native';
import { SCREEN_WIDTH } from 'constants';
import { ProximaNova } from 'shared/styles/fonts';


export default StyleSheet.create({
    backgroundVideo: {
        width: SCREEN_WIDTH,
        height: SCREEN_WIDTH,
    },
    bottomBox: {
        flex: 1,
        padding: 20,
        justifyContent: 'space-around',
    },
    container: {
        flex: 1,
        backgroundColor: black,
    },
    informationBody: {
        ...ProximaNova.Regular,
        fontSize: 14,
        color: white,
        textAlign: 'center',
        letterSpacing: 0.9,
        width: 300,
    },
    informationHeader: {
        ...ProximaNova.Bold,
        fontSize: 18,
        marginBottom: 16,
        color: white,
    },
    informationSection: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    savedText: {
        ...ProximaNova.Regular,
        fontSize: 14,
        color: lightGrey,
    },
    shareRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
        height: 56,
        backgroundColor: darkGrey,
    },
    submitButton: {
        width: '100%',
        height: 54,
        alignItems: 'center',
        justifyContent: 'center',
    },
    submitButtonEnabled: {
        backgroundColor: blue,
    },
    submitButtonUploading: {
        backgroundColor: uploadingButtonGrey,
    },
    submitButtonUploaded: {
        backgroundColor: uploadedButtonGrey,
    },
    submitButtonText: {
        ...ProximaNova.Bold,
        fontSize: 16,
        letterSpacing: 0.5,
    },
    submitButtonTextEnabled: {
        color: white,
    },
    submitButtonTextUploading: {
        color: black,
    },
    submitButtonTextUploaded: {
        color: white,
    },
});

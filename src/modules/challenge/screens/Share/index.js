// @flow
import { connect } from 'react-redux';
import { Image, View, Text, TouchableOpacity } from 'react-native';
import * as challengeActions from '../../state/dux';
import * as sharedActions from 'shared/state/dux';
import React from 'react';
import ReactNativeVideo from 'react-native-video';
import select from 'modules/challenge/state/selectors/ShareScreen';
import styles from './styles';
import TopNav from '../../sharedComponents/TopNav'; // eslint-disable-line

type $Props = {
    dispatch: *,
    finalizedVideoPath: string,
    uploadingChallengeSubmission: boolean,
    uploadedChallengeSubmission: boolean,
}

const ShareScreen = ({
    dispatch,
    finalizedVideoPath,
    uploadingChallengeSubmission,
    uploadedChallengeSubmission,
}: $Props) =>  {
    let videoComponent;
    if (finalizedVideoPath) {
        videoComponent = (
            <ReactNativeVideo
                rate={1}
                resizeMode='cover'
                source={{uri: finalizedVideoPath}}
                style={styles.backgroundVideo}
                muted
                repeat
            />
        )
    }

    const onInstagramSharePress = () => {
        if (finalizedVideoPath && !uploadingChallengeSubmission) {
            dispatch(sharedActions.deepLinkInstagram(finalizedVideoPath));
        }
    }

    // submit button props
    let onSubmitPress = () => dispatch(challengeActions.submitChallengeVideo());
    let submitButtonStyle = styles.submitButtonEnabled;
    let submitButtonTextStyle = styles.submitButtonTextEnabled;
    let submitButtonText = 'Submit Your Challenge Video!';

    if (uploadingChallengeSubmission) {
        onSubmitPress = () => {};
        submitButtonStyle = styles.submitButtonUploading;
        submitButtonTextStyle = styles.submitButtonTextUploading;
        submitButtonText = 'Uploading ... Please Wait.';
    } else if (uploadedChallengeSubmission) {
        onSubmitPress = () => {};
        submitButtonStyle = styles.submitButtonUploaded;
        submitButtonTextStyle = styles.submitButtonTextUploaded;
        submitButtonText = 'Done! Thanks for Entering.';
    }

    return (
        <View style={styles.container}>
            <TopNav
                closeButtonDisabled={uploadingChallengeSubmission}
                recordMode={false}
            />
            {videoComponent}
            <View style={styles.shareRow}>
                <Text style={styles.savedText}>
                    Saved to camera roll!
                </Text>
                <TouchableOpacity
                    style={styles.instagramShareContainer}
                    onPress={onInstagramSharePress}
                >
                    <Image source={require('assets/icons/InstagramShareButton.png')} />
                </TouchableOpacity>
            </View>
            <View style={styles.informationSection}>
                <Text style={styles.informationHeader}>
                    Send Us Your Video and Win
                </Text>
                <Text style={styles.informationBody}>
                    Enter below to be featured on our social media and appear on leaderboards.
                </Text>
            </View>
            <TouchableOpacity
                onPress={onSubmitPress}
                style={[styles.submitButton, submitButtonStyle]}
                text={submitButtonText}
            >
                <Text style={[styles.submitButtonText, submitButtonTextStyle]}>
                    {submitButtonText}
                </Text>
            </TouchableOpacity>
        </View>
    );
};

export default connect(select)(ShareScreen);

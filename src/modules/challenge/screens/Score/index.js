// @flow
import { Animated, NativeModules, View, Text } from 'react-native';
import { CHALLENGE_ROUTES, SCREEN_WIDTH } from 'constants';
import { connect } from 'react-redux';
import { isAndroid } from 'shared/utils';
import * as challengeActions from 'modules/challenge/state/dux';
import * as sharedActions from 'shared/state/dux'
import allChallenges from 'modules/main/state/selectors/ChallengeScreen';
import React, { Component } from 'react';
import select from 'modules/challenge/state/selectors/ScoreView';
import styles from './styles';
import type { $ExercisesObject, $ExerciseScore } from 'types/state/challenge';

type $Props = {
    city: string,
    dispatch: *,
    exercises: Array<$ExerciseScore>,
    exerciseTypeKey: string,
    fitnessCourtName: string,
    totalScore: number,
    videoLocations: $ExercisesObject
}

type $State = {
    progress: any,
}

class ScoreView extends Component<$Props, $State> {
    constructor(props) {
        super(props);
        this.state = {
            progress: new Animated.Value(0)
        }
    }





    componentDidMount() {
        const {
            city,
            dispatch,
            exercises,
            exerciseTypeKey,
            fitnessCourtName,
            totalScore,
            videoLocations
        } = this.props;

        // Higher-end guess on how long it will take to process the video
        let videoCompileEstimate = 20000;
        if (isAndroid()) {
            // Unfortunately Android + ffmpeg takes much longer to compile than iOS
            videoCompileEstimate = 130000;
        }

        Animated.timing(
            this.state.progress,
            {
                toValue: SCREEN_WIDTH,
                duration: videoCompileEstimate,
            }
        ).start();

        let challengeMode;
        allChallenges().challenges.forEach(challenge => {
          if (exerciseTypeKey == challenge.challengeMarker) {
            challengeMode = challenge;
          }
        });

        // Using setTimeout so that this component will render before running the native code and hanging
        setTimeout(() => {
            NativeModules.VideoEditor.transformChallengeVideo(
              videoLocations,
              exercises,
              totalScore,
              city,
              fitnessCourtName,
              challengeMode.title,
              challengeMode.subtitle.split('\n')[0],
              function (error, results) {
                dispatch(challengeActions.finalizeVideoPath(results.videoPath));
                dispatch(sharedActions.navigate(CHALLENGE_ROUTES.SHARE))
            });
        }, 0);
    }

    render() {
        const {
            exercises,
            totalScore,
        } = this.props;

        const { progress } = this.state;

        return (
            <View style={styles.container}>
                <Text style={styles.headerText}>
                    Nice Job!
                </Text>
                <View style={styles.scoreContainer}>
                    <View style={styles.scoreBorder} />
                    <View style={styles.scoreTopBox}>
                        <View style={styles.firstRow}>
                            <View style={styles.column1} />
                            <View style={[styles.column2, styles.firstColumn2]}>
                                <Text style={styles.columnLabel}>
                                    REPS
                                </Text>
                            </View>
                            <View style={styles.column3}>
                                <Text style={styles.columnLabel}>
                                    POINTS
                                </Text>
                            </View>
                        </View>
                        {exercises.map((
                            {
                                exercise,
                                points,
                                reps,
                                multiplierText,
                            },
                            index,
                        ) => (
                            <View
                                key={exercise}
                                style={styles.exerciseRow}
                            >
                                <View style={styles.column1}>
                                    <Text style={styles.exerciseIndexText}>
                                        {index+1}
                                    </Text>
                                    <Text style={styles.gridText}>
                                        {exercise}
                                    </Text>
                                </View>
                                <View style={styles.column2}>
                                    <Text style={styles.gridText}>
                                        {reps}
                                    </Text>
                                    <Text style={styles.multiplierText}>
                                        {multiplierText}
                                    </Text>
                                </View>
                                <View style={styles.column3}>
                                    <Text style={styles.gridText}>
                                        {points}
                                    </Text>
                                </View>
                            </View>
                        ))}
                    </View>
                    <View style={styles.divider} />
                    <Text style={styles.totalScoreText}>
                        TOTAL SCORE
                    </Text>
                    <Text style={styles.totalScoreNumber}>
                        {totalScore}
                    </Text>
                </View>
                <Text style={styles.bottomText}>
                    We&apos;re making your video
                </Text>
                <Animated.View
                    style={[
                        styles.progressBar,
                        {
                            width: progress,
                        }
                    ]}
                />
                <View style={styles.progressBarContainer} />
            </View>
        );
    }
}

export default connect(select)(ScoreView);

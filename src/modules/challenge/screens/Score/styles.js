// @flow
import { black, blue, challengeTextBlue, white, yellow } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    bottomText: {
        fontSize: 15,
        color: challengeTextBlue,
        ...ProximaNova.Semibold,
    },
    column1: {
        flexDirection: 'row',
        width: '50%',
    },
    column2: {
        flexDirection: 'row',
        width: '24%',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
    },
    column3: {
        width: '26%',
        alignItems: 'flex-end',
    },
    columnLabel: {
        fontSize: 11,
        color: black,
        opacity: 0.42,
        ...ProximaNova.Bold,
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
        backgroundColor: blue,
    },
    divider: {
        width: '100%',
        marginVertical: '10%',
        height: 2,
        backgroundColor: black,
    },
    exerciseIndexText: {
        color: black,
        fontSize: 15,
        opacity: 0.42,
        marginRight: 8,
        ...ProximaNova.CondensedBold,
    },
    exerciseRow: {
        flexDirection: 'row',
        marginBottom: 16,
    },
    firstColumn2: {
        paddingRight: '9%',
    },
    firstRow: {
        flexDirection: 'row',
        marginBottom: 24,
    },
    gridText: {
        color: black,
        fontSize: 15,
        letterSpacing: 1,
        ...ProximaNova.CondensedBold,
    },
    headerText: {
        fontSize: 24,
        marginTop: 20,
        color: white,
        ...ProximaNova.CondensedBold,
    },
    multiplierText: {
        fontSize: 13,
        textAlign: 'right',
        width: 20,
        ...ProximaNova.Medium,
    },
    progressBar: {
        alignSelf: 'flex-start',
        height: 15,
        width: 15,
        backgroundColor: yellow,
        zIndex: 5,
    },
    progressBarContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: '100%',
        height: 15,
        backgroundColor: white,
        opacity: 0.32,
    },
    scoreBorder: {
        position: 'absolute',
        width: '116%',
        height: '108%',
        top: 18,
        left: 18,
        borderRadius: 5,
        borderWidth: 2,
        borderColor: blue,
    },
    scoreContainer: {
        alignItems: 'center',
        width: '85%',
        height: '80%',
        padding: 36,
        borderRadius: 5,
        backgroundColor: white,
    },
    scoreTopBox: {
        flexDirection: 'column',
    },
    totalScoreNumber: {
        fontSize: 48,
        letterSpacing: 2.8,
        color: black,
        ...ProximaNova.CondensedBold,
    },
    totalScoreText: {
        fontSize: 18,
        letterSpacing: 1,
        color: black,
        ...ProximaNova.CondensedBold,
    },
});

// @flow
import { createNavigationContainer, createNavigator } from 'react-navigation';
import Router from './Router';
import View from './View';

export default createNavigationContainer(
    createNavigator(Router)(View)
);

// @flow
import { CHALLENGE_ROUTES } from 'constants';
import { StackRouter } from 'react-navigation';
import RecordScreen from 'modules/challenge/screens/Record';
import ScoreScreen from 'modules/challenge/screens/Score';
import ShareScreen from 'modules/challenge/screens/Share';

export default StackRouter(
    {
        [CHALLENGE_ROUTES.RECORD]: {
            screen: RecordScreen,
        },
        [CHALLENGE_ROUTES.SCORE]: {
            screen: ScoreScreen,
        },
        [CHALLENGE_ROUTES.SHARE]: {
            screen: ShareScreen,
        },
    }, {
        initialRouteName: CHALLENGE_ROUTES.SHARE,
    }
);

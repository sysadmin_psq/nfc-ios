// // @flow
// import { white } from 'shared/styles/colors';
// import { View } from 'react-native';
// import React from 'react';
// import TopNav from './TopNav'; // eslint-disable-line
//
//
// type $Props = {
//     router: any,
//     navigation: any
// }
//
// export default ({ router, navigation }: $Props) => {
//     const { routes, index } = navigation.state;
//     const routeName = routes[index].routeName;
//     const ActiveScreen = router.getComponentForRouteName(routeName);
//
//     return (
//         <View style={{backgroundColor: white, flex: 1}}>
//             <TopNav
//                 activeRoute={routeName}
//                 navigation={navigation}
//             />
//             <ActiveScreen navigation={navigation} />
//         </View>
//     );
// };

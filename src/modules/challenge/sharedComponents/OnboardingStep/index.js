// @flow
import { Text, TouchableOpacity, View } from 'react-native';
import React from 'react';
import styles from './styles';
import Triangle from '../Triangle';

type $Props = {
    onPress: () => void,
    text: string,
    triangleStyles: Object,
};

const OnboardingStep = ({ onPress, text, triangleStyles }: $Props) => (
    <View style={styles.container}>
        <View style={styles.yellowRectangle} />
        <Text style={styles.text}>
            {text}
        </Text>
        <Triangle extraStyles={triangleStyles} />
        <TouchableOpacity
            onPress={onPress}
            style={styles.nextButtonContainer}
        >
            <Text style={styles.nextButtonText}>
                NEXT
            </Text>
        </TouchableOpacity>
    </View>
);

export default OnboardingStep;

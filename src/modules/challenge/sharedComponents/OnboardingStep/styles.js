// @flow
import { blue, white, transparentBlack, yellow } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        height: 100,
        width: 250,
        padding: 12,
        backgroundColor: transparentBlack,
        borderRadius: 2,
    },
    nextButtonText: {
        color: blue,
        fontSize: 14,
        letterSpacing: 0.5,
        ...ProximaNova.CondensedBold,
    },
    nextButtonContainer: {
        position: 'absolute',
        bottom: 6,
        right: 10,
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 28,
    },
    text: {
        color: white,
        fontSize: 15,
        ...ProximaNova.Semibold,
    },
    yellowRectangle: {
        position: 'absolute',
        top: 16,
        left: 0,
        width: 3,
        height: 15,
        backgroundColor: yellow,
    },
});

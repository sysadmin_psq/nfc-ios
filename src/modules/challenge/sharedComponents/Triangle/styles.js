// @flow
import { StyleSheet } from 'react-native';
import { transparentBlack } from 'shared/styles/colors';

export default StyleSheet.create({
    container: {
        position: 'absolute',
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 10,
        borderRightWidth: 10,
        borderBottomWidth: 16,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: transparentBlack,
    },
});

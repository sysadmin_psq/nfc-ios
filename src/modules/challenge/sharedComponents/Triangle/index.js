// @flow
import { View } from 'react-native';
import React from 'react';
import styles from './styles';

type $Props = {
    extraStyles: Object,
}

export const Triangle = ({ extraStyles }: $Props) => (
    <View style={[styles.container, extraStyles]} />
);

export default Triangle;

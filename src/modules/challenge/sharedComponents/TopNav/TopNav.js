// @flow
import { connect } from 'react-redux';
import { dotActiveGrey, red } from 'shared/styles/colors';
import { MAIN_ROUTES } from 'constants';
import { Text, View } from 'react-native';
import CloseButton from 'shared/components/CloseButton';
import React from 'react';
import select from 'modules/challenge/state/selectors/TopNav';
import styles from './styles';
import * as challengeActions from 'modules/challenge/state/dux';
import * as sharedActions from 'shared/state/dux';

type $Props = {
    closeButtonDisabled: boolean,
    dispatch: any,
    recording: boolean,
    recordMode: boolean,
}

const TopNav = ({ closeButtonDisabled, dispatch, recording, recordMode }: $Props) => {
    const onCloseButtonPress = () => {
        if (!closeButtonDisabled) {
            dispatch(challengeActions.cancelChallengeMode());
            dispatch(sharedActions.navigateAndReset(MAIN_ROUTES.CHALLENGE));
        }
    }

    return (
        <View style={styles.container}>
            <View style={[styles.itemContainer, styles.closeButtonContainer]}>
                <CloseButton onPress={() => onCloseButtonPress()} />
            </View>
            <Text style={styles.challengeLabel}>
                {recordMode ? 'CHALLENGE MODE' : 'YOUR CHALLENGE VIDEO'}
            </Text>
            <View style={[styles.itemContainer, styles.recordButtonContainer]}>
                {recordMode &&
                    <View
                        style={[
                            styles.recordButton,
                            {
                                backgroundColor: recording ? red : dotActiveGrey,
                            },
                        ]}
                    />
                }
            </View>
        </View>
    );
}

TopNav.defaultProps = {
    closeButtonDisabled: false,
    recordMode: true,
};

export default connect(select)(TopNav);

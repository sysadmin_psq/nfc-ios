// @flow
import { BackHandler } from 'react-native';
import { connect } from 'react-redux';
import * as challengeActions from 'modules/challenge/state/dux';
import React, { Component } from 'react';
import TopNav from './TopNav';

type $Props = {
    dispatch: any,
}

class TopNavContainer extends Component<$Props> {
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => this.onBackPress());
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', () => this.onBackPress());
    }

    onBackPress() {
        const { dispatch } = this.props;
        dispatch(challengeActions.cancelChallengeMode());
    }

    render() {
        return (
            <TopNav {...this.props} />
        );
    }
}

export default connect(null)(TopNavContainer);

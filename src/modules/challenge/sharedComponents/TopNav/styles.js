// @flow
import { isIOS } from 'shared/utils';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import { black, white } from 'shared/styles/colors';

export default StyleSheet.create({
    container: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        height:isIPhoneX()?80:65,
        // paddingHorizontal: 16,
        paddingLeft:16,
        paddingRight:16,
        paddingTop:isIPhoneX()?40:10,
        width: '100%',
        zIndex: 1,
        backgroundColor: black,
    },
    challengeLabel: {
        fontSize: 18,
        letterSpacing: 1,
        color: white,
        ...ProximaNova.CondensedBold,
        alignSelf: 'center',
    },
    closeButtonContainer: {
        marginLeft: -8,
    },
    itemContainer: {
        width: 40,
    },
    recordButton: {
        height: 14,
        width: 14,
        borderRadius: 7,
    },
    recordButtonContainer: {
        alignItems: 'flex-end',
    },
});

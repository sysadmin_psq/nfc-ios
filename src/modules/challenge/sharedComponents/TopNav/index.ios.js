// @flow
import React from 'react';
import TopNav from './TopNav';

export default (props: Object) => (
    <TopNav {...props} />
);

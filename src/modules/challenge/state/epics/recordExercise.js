// @flow
import { Animated } from 'react-native';
import { CHALLENGE_MODE, SCREEN_WIDTH } from 'constants';
import * as challengeActions from '../dux';
import * as sharedActions from 'shared/state/dux';
import { get } from 'lodash';


export default (action$: *, store: *) => {

    let exerciseTime;
    return action$
        .ofType(challengeActions.recordExercise)
        .do((action) => {
            const {
                challenge: {
                    exerciseTypeKey,
                    timer: {
                        progressOffset,
                    },
                },
            } = store.getState();
            exerciseTime = get(CHALLENGE_MODE, `[${exerciseTypeKey}].EXERCISE_TIME`, 15000);
            progressOffset.setValue(-SCREEN_WIDTH);
            Animated.timing(
                progressOffset,
                {
                    toValue: 0,
                    duration: exerciseTime,
                },
            ).start();
            action.payload.camera.capture({audio: false})
                .then(({path}) => store.dispatch(challengeActions.exerciseVideoCaptured(path)))
                .catch(error => store.dispatch(sharedActions.logError(error)))
        })
        .delay(15000)
        .do((action) => {
            action.payload.camera.stopCapture();
        })
        .mapTo(sharedActions.emptyAction());
};

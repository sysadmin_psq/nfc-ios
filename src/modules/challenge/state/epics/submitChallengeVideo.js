// @flow
import _ from 'lodash';
import { Observable } from 'rxjs';
import * as challengeActions from 'modules/challenge/state/dux';
import * as sharedActions from 'shared/state/dux';
import allChallenges from 'modules/main/state/selectors/ChallengeScreen';
import Api from 'api';

export default (action$: *, store: *) =>
    action$
        .ofType(challengeActions.submitChallengeVideo)
        .switchMap(() => {
            const {
                challenge: {
                    exerciseTypeKey,
                    finalizedVideoPath,
                    totalScore,
                },
                main: {
                    deviceId,
                    fitnessCourt: {
                        id,
                    },
                    userInfo: {
                        email,
                    },
                },
            } = store.getState();

            const videoLocation = finalizedVideoPath;
            const challengeId = _.find(
                allChallenges().challenges,
                challenge => challenge.challengeMarker === exerciseTypeKey
            ).challengeId;

            const params = {
                fitnessCourtId: id,
                challengeId,
                deviceId,
                email,
                totalScore,
                videoLocation,
            };

            return Api
                .postChallengeVideo(params)
                .switchMap(() => Observable.concat(
                    Observable.of(challengeActions.setUploadingChallengeSubmission(false)),
                    Observable.of(challengeActions.setUploadedChallengeSubmission(true)),
                ))
                .catch(() => Observable.concat(
                    Observable.of(challengeActions.setUploadingChallengeSubmission(false)),
                    Observable.of(sharedActions.createAutoCloseNotification('An error occured. Please try again.')),
                ))
                .startWith(challengeActions.setUploadingChallengeSubmission(true))
        });

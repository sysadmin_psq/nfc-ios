// @flow
import { Animated } from 'react-native';
import * as challengeActions from 'modules/challenge/state/dux';
import * as sharedActions from 'shared/state/dux';
import * as utils from 'shared/components/ExerciseNav/utils';

export default (action$: *, store: *) =>
    action$
        .ofType(challengeActions.incrementActiveExerciseIndex)
        .do(() => {
            const {
                challenge: {
                    activeExerciseIndex,
                    bottomNav: {
                        tabOffset,
                    },
                }
            } = store.getState();

            const newOffset = utils.determineTabOffset(activeExerciseIndex);

            Animated.spring(tabOffset, {
                toValue: newOffset,
                duration: 200,
                friction: 11,
                tension: 120,
            }).start();
        })
        .mapTo(sharedActions.emptyAction())

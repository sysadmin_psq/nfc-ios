// @flow
import { Animated } from 'react-native';
import { CHALLENGE_MODE, SCREEN_WIDTH } from 'constants';
import * as challengeActions from '../dux';
import * as sharedActions from 'shared/state/dux';
import { get } from 'lodash';

export default (action$: *, store: *) => {
  let restTime;
  return action$
      .ofType(challengeActions.rest)
      .do(() => {
          const {
              challenge: {
                  timer: {
                      progressOffset,
                  },
                  exerciseTypeKey,
              },
          } = store.getState();
          restTime = get(CHALLENGE_MODE, `${exerciseTypeKey}.REST_TIME`, 15000);

          progressOffset.setValue(-SCREEN_WIDTH);
          Animated.timing(
              progressOffset,
              {
                  toValue: 0,
                  duration: restTime,
              },
          ).start();
      })
      .delay(restTime)
      .mapTo(sharedActions.emptyAction());
};

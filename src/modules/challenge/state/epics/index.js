// @flow
import cancelChallengeMode from './cancelChallengeMode';
import offsetTab from './offsetTab';
import submitChallengeVideo from './submitChallengeVideo';
import recordExercise from './recordExercise';
import rest from './rest';
import startChallenge from './startChallenge';
import startTimer from './startTimer';

export default [
    cancelChallengeMode,
    offsetTab,
    submitChallengeVideo,
    startChallenge,
    startTimer,
    recordExercise,
    rest,
];

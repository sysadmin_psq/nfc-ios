// @flow
import { CHALLENGE_MODE, CHALLENGE_MODE_IDS, CHALLENGE_ROUTES, NUMBER_OF_EXERCISES } from 'constants';
import { Observable } from 'rxjs';
import * as challengeActions from '../dux';
import * as mainActions from 'modules/main/state/dux';
import * as sharedActions from 'shared/state/dux';
import { get } from 'lodash';

const recordAndRest = (action, challengeMode) => {
    return Observable
            .concat(
                Observable.of(challengeActions.recordExercise(action.payload.camera)),
                Observable.of(challengeActions.rest()).delay(challengeMode.EXERCISE_TIME),
                Observable.of(challengeActions.incrementActiveExerciseIndex()).delay(challengeMode.REST_TIME),
            );
}

export default (action$: Object, store: *) => {
    return action$
        .ofType(challengeActions.startChallenge)
        .switchMap((action) => {
            const {
              challenge: {
                exerciseTypeKey,
              },
            } = store.getState();
            const challengeMode = get(CHALLENGE_MODE, exerciseTypeKey, CHALLENGE_MODE[CHALLENGE_MODE_IDS.THREE_MINUTE]);
            return Observable
                .concat(
                    Observable.of(mainActions.onboarded()),
                    recordAndRest(action, challengeMode).repeat(NUMBER_OF_EXERCISES - 1).delay(1000),
                    Observable.of(challengeActions.recordExercise(action.payload.camera)),
                    Observable.of(challengeActions.tallyScore()).delay(challengeMode.EXERCISE_TIME + 1000),
                    Observable.of(sharedActions.navigate(CHALLENGE_ROUTES.SCORE)),
                )
                .takeUntil(action$.ofType(challengeActions.cancelChallengeMode))
        });
}

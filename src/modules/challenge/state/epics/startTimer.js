// @flow
import * as challengeActions from '../dux';

export default (action$: *) =>
    action$
        .ofType(challengeActions.recordExercise, challengeActions.rest)
        .map(() => challengeActions.startTimer())

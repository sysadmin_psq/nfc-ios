// @flow
import * as challengeActions from 'modules/challenge/state/dux';


export default (action$: *) =>
    action$
        .ofType(challengeActions.cancelChallengeMode)
        .mapTo(challengeActions.resetState());

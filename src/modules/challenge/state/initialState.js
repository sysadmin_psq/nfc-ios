// @flow
import { Animated } from 'react-native';
import { EXERCISE_LIST, SCREEN_WIDTH, CHALLENGE_MODE_IDS } from 'constants';

const exerciseObject = {};
for (let exercise of EXERCISE_LIST) {
    exerciseObject[exercise] = 0;
}

const repsObject = Object.assign({}, exerciseObject);

export default {
    activeExerciseIndex: 0,
    exerciseTypeKey: CHALLENGE_MODE_IDS.THREE_MINUTE,
    bottomNav: {
        tabOffset: new Animated.Value(0),
    },
    exerciseOverlay: {
        largeText: '',
        visible: false,
    },
    onboardingStep: 0,
    reps: repsObject,
    share: {
        uploadingChallengeSubmission: false,
        uploadedChallengeSubmission: false,
    },
    showStartButtonWhenOnboarded: true,
    timer: {
        active: false,
        progressOffset: new Animated.Value(-SCREEN_WIDTH),
        showSlider: true,
        startTime: 0,
    },
    totalScore: 0,
    recording: false,
    videoLocations: exerciseObject,
    finalizedVideoPath: null
};

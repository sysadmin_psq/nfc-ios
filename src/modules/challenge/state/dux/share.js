// @flow
import { createAction } from 'redux-act';
import type { $ChallengeState } from 'types/state/challenge';

// actions
// -----------------------------------------------------------------------------
export const submitChallengeVideo = createAction('');

// action reducer pairs
// -----------------------------------------------------------------------------
export const setUploadingChallengeSubmission = createAction('');
export const setUploadingChallengeSubmissionReducer = (
    state: $ChallengeState,
    uploadingChallengeSubmission: boolean,
) => ({
    ...state,
    share: {
        ...state.share,
        uploadingChallengeSubmission,
    },
});

export const setUploadedChallengeSubmission = createAction('');
export const setUploadedChallengeSubmissionReducer = (
    state: $ChallengeState,
    uploadedChallengeSubmission: boolean,
) => ({
    ...state,
    share: {
        ...state.share,
        uploadedChallengeSubmission,
    },
});

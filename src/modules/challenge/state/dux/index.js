// @flow
import { EXERCISE_LIST, EXERCISE_POINT_MULTIPLIER } from 'constants';
import { createAction, createReducer } from 'redux-act';
import initialState from '../initialState';
import type { $ChallengeState, $ExerciseScore } from 'types/state/challenge';

import * as share from './share';

// export actions from sub modules
// -----------------------------------------------------------------------------
export * from './share';

// actions
// -----------------------------------------------------------------------------
export const cancelChallengeMode = createAction('');

// action reducer pairs
// -----------------------------------------------------------------------------
export const setChallengeMode = createAction('');
const setChallengeModeReducer = (state: $ChallengeState, challengeMode: string) => ({
    ...state,
    exerciseTypeKey: challengeMode,
});

export const addReps = createAction('');
const addRepsReducer = (state: $ChallengeState) => {
    const exercise = EXERCISE_LIST[state.activeExerciseIndex];

    const newRepsObject = Object.assign({}, state.reps);
    newRepsObject[exercise] = state.reps[exercise] + 1;

    return {
        ...state,
        reps: newRepsObject,
    };
};

export const continueOnboarding = createAction('');
const continueOnboardingReducer = (state: $ChallengeState) => ({
    ...state,
    onboardingStep: state.onboardingStep + 1,
});

export const exerciseVideoCaptured = createAction('');
const exerciseVideoCapturedReducer = (state: $ChallengeState, path: string) => {
    // const newVideoLocations = Object.assign({}, state.videoLocations);
    //
    // const activeExercise = EXERCISE_LIST[state.activeExerciseIndex];
    // newVideoLocations[activeExercise] = path;
    //
    // return {
    //     ...state,
    //     videoLocations: newVideoLocations,
    // };
    const activeExercise = EXERCISE_LIST[state.activeExerciseIndex];

    return {
        ...state,
        videoLocations: {
            ...state.videoLocations,
            [activeExercise]: path,
        },
    };
}

export const finalizeVideoPath = createAction('');
const finalizeVideoPathReducer = (state: $ChallengeState, finalizedVideoPath: string) => ({
    ...state,
    finalizedVideoPath,
});

export const incrementActiveExerciseIndex = createAction('');
const incrementActiveExerciseIndexReducer = (state: $ChallengeState) => ({
    ...state,
    activeExerciseIndex: state.activeExerciseIndex + 1,
});

export const recordExercise = createAction('', camera => ({ camera }));
export const recordExerciseReducer = (state: $ChallengeState) => ({
  ...state,
  recording: true,
  exerciseOverlay: {
      ...state.exerciseOverlay,
      visible: false,
  },
});

export const resetState = createAction('');
const resetStateReducer = () => initialState;

export const rest = createAction('');
const restReducer = (state: $ChallengeState) => ({
  ...state,
  recording: false,
  exerciseOverlay: {
      largeText: EXERCISE_LIST[state.activeExerciseIndex + 1],
      visible: true,
  },
});

export const startChallenge = createAction('', camera => ({ camera }));
const startChallengeReducer = (state: $ChallengeState) => ({
    ...state,
    showStartButtonWhenOnboarded: false,
});


export const startTimer = createAction('', () => new Date().getTime());
const startTimerReducer = (state: $ChallengeState, startTime: number) => ({
    ...state,
    timer: {
        ...state.timer,
        active: true,
        startTime,
    },
});

export const subtractReps = createAction('');
export const subtractRepsReducer = (state: $ChallengeState) => {
    const exercise = EXERCISE_LIST[state.activeExerciseIndex];

    const newRepsObject = Object.assign({}, state.reps);
    newRepsObject[exercise] = state.reps[exercise] - 1;

    return {
        ...state,
        reps: newRepsObject,
    };
};

export const tallyScore = createAction('');
const tallyScoreReducer = (state: $ChallengeState) => {
    let exercises: Array<$ExerciseScore> = [];
    let totalScore = 0;
    for (const key of Object.keys(state.reps)) {
        const multiplier = EXERCISE_POINT_MULTIPLIER[key];
        const multiplierText = multiplier > 1 ? `x${multiplier}` : "";
        const reps = state.reps[key];
        const points = reps * multiplier;

        totalScore = totalScore + points;

        exercises.push({
            exercise: key,
            multiplierText,
            points,
            reps,
        });
    }

    return {
        ...state,
        scores: exercises,
        totalScore,
    };
};

export const toggleRecording = createAction('');
const toggleRecordingReducer = (state: $ChallengeState) => ({
    ...state,
    recording: !state.recording,
});



// -----------------------------------------------------------------------------
// $FlowFixMe
export default createReducer({
    [addReps]: addRepsReducer,
    [continueOnboarding]: continueOnboardingReducer,
    [exerciseVideoCaptured]: exerciseVideoCapturedReducer,
    [finalizeVideoPath]: finalizeVideoPathReducer,
    [incrementActiveExerciseIndex]: incrementActiveExerciseIndexReducer,
    [recordExercise]: recordExerciseReducer,
    [resetState]: resetStateReducer,
    [rest]: restReducer,
    [setChallengeMode]: setChallengeModeReducer,
    [share.setUploadedChallengeSubmission]: share.setUploadedChallengeSubmissionReducer,
    [share.setUploadingChallengeSubmission]: share.setUploadingChallengeSubmissionReducer,
    [startChallenge]: startChallengeReducer,
    [startTimer]: startTimerReducer,
    [subtractReps]: subtractRepsReducer,
    [tallyScore]: tallyScoreReducer,
    [toggleRecording]: toggleRecordingReducer,
}, initialState);

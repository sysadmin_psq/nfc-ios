// @flow
import type { $ChallengeState } from 'types/state/challenge';

export default ({ challenge }: { challenge: $ChallengeState }) => ({
    recording: challenge.recording,
});

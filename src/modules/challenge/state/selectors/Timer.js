// @flow
import type { $ChallengeState } from 'types/state/challenge';
import { challengeCountdownGrey, yellow } from 'shared/styles/colors';
import { EXERCISE_LIST } from 'constants';

export default ({ challenge }: { challenge: $ChallengeState }) => ({
    exerciseLabel: challenge.recording ? EXERCISE_LIST[challenge.activeExerciseIndex] : 'REST',
    progressColor: challenge.recording ? yellow : challengeCountdownGrey,
    ...challenge.timer,
});

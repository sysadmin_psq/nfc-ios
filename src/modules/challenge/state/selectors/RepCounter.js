// @flow
import { EXERCISE_LIST } from 'constants';
import type { $ChallengeState } from 'types/state/challenge';

export default ({ challenge }: { challenge: $ChallengeState }) => {
    const activeExercise = EXERCISE_LIST[challenge.activeExerciseIndex];
    const reps = challenge.recording ? challenge.reps[activeExercise] : '—';

    return {
        subtractRepsEnabled: challenge.reps[activeExercise] > 0 && challenge.recording,
        addRepsEnabled:  challenge.recording,
        recording: challenge.recording,
        reps,
    }
}

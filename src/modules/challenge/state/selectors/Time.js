// @flow
import type { $ChallengeState } from 'types/state/challenge';
import { CHALLENGE_MODE, CHALLENGE_MODE_IDS } from 'constants';
import { get } from 'lodash';

export default ({ challenge }: { challenge: $ChallengeState }) => ({
    active: challenge.timer.active,
    recording: challenge.recording,
    startTime: challenge.timer.startTime,
    challengeMode: get(
      CHALLENGE_MODE,
      `${challenge.exerciseTypeKey}`,
      CHALLENGE_MODE[CHALLENGE_MODE_IDS.THREE_MINUTE],
    ),
});

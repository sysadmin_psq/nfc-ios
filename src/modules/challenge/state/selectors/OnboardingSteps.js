// @flow
import type { $ChallengeState } from 'types/state/challenge';
import type { $MainState } from 'types/state/main';

export default ({ main, challenge }: { main: $MainState, challenge: $ChallengeState }) => {
    const onboarded = main.onboarded;
    const step = challenge.onboardingStep;

    return {
        step1Visible: step === 0 && !onboarded,
        step2Visible: step === 1 && !onboarded,
    }
}

// @flow
import type { $MainState } from 'types/state/main';
import type { $ChallengeState } from 'types/state/challenge';

export default ({
    challenge,
    main,
}: {
    challenge: $ChallengeState,
    main: $MainState,
}) => ({
    city: main.fitnessCourt.city.toUpperCase(),
    finalizedVideoPath: challenge.finalizedVideoPath,
    fitnessCourtName: main.fitnessCourt.name.toUpperCase(),
    totalScore: challenge.totalScore,
    ...challenge.share,
});

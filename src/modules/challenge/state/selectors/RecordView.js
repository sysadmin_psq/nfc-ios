// @flow
import type { $MainState } from 'types/state/main';
import type { $ChallengeState } from 'types/state/challenge';

export default ({ main, challenge }: { main: $MainState, challenge: $ChallengeState }) => ({
    activeExerciseIndex: challenge.activeExerciseIndex,
    exerciseOverlay: challenge.exerciseOverlay,
    onboarded: main.onboarded,
    tabOffset: challenge.bottomNav.tabOffset,
});

// @flow
import type { $MainState } from 'types/state/main';
import type { $ChallengeState } from 'types/state/challenge';

export default ({ main, challenge }: { main: $MainState, challenge: $ChallengeState}) => {
    const onboarded = main.onboarded;
    const step = challenge.onboardingStep;
    const showStartButton = challenge.showStartButtonWhenOnboarded;

    return {
        showTip: !onboarded,
        visible: (step === 2 && !onboarded) || (showStartButton && onboarded),
    }
}

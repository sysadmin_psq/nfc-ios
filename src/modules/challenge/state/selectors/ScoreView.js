// @flow
import type { $MainState } from 'types/state/main';
import type { $ChallengeState } from 'types/state/challenge';

export default ({ main, challenge }: { main: $MainState, challenge: $ChallengeState }) => ({
    city: main.fitnessCourt.city,
    exercises: challenge.scores,
    exerciseTypeKey: challenge.exerciseTypeKey,
    fitnessCourtName: main.fitnessCourt.name,
    totalScore: challenge.totalScore,
    videoLocations: challenge.videoLocations
});

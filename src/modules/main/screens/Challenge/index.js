// @flow
import { connect } from 'react-redux';
import { Button, ThemeProvider } from 'react-native-elements';
import { ScrollView, View, Text, ImageBackground, Image, TouchableHighlight, TouchableOpacity, Share, AsyncStorage, Platform } from 'react-native';
import { isEqual } from 'lodash';
import React from 'react';
import Slider from './Slider';
import { isIPhone4,isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import styles from './styles';
import ChallengePanel from './ChallengePanel';
import ChallengeModal from './ChallengeModal';
import select from 'modules/main/state/selectors/ChallengeScreen';
import * as mainActions from 'modules/main/state/dux';
import { MAIN_ROUTES, CHALLENGE_ROUTES } from 'constants';
import { black, borderGrey, white, navBarColor } from 'shared/styles/colors';
import { NavigationBar } from 'navigationbar-react-native';
import { AUTH_ROUTES, STORAGE_USER_SESSION } from 'constants';
import MyEntriesScreen from 'modules/main/screens/MyEntries';
import openMap from 'react-native-open-maps';
import { isIphoneX } from 'react-native-iphone-x-helper';
import Orientation from 'react-native-orientation';


export type $Challenge = {|
    thumbnailImage: any,
    title: string,
    subtitle: string,
    imageLink: any,
    description: string,
    id: number,
    challengeId: number,
    challengeMarker: string,
|};


type $ChallengeModalState = {
    [number]: {
        ...$Challenge,
        isVisible: boolean,
    },
};

type $State = {
    challengeModalState: $ChallengeModalState,
};

type $Props = {
    dispatch: any,
    challenges: $Challenge[],
    userInfo: any,
};




const ComponentLeft = ({navigation}) => {

    console.log("log", navigation.getParam('userId', ''))
    return(
        <View style={{ flex: 1, alignItems: 'flex-start', paddingLeft: 10,}} >
            {/*<TouchableOpacity*/}
            {/*onPress={() => { navigation.navigate(MAIN_ROUTES.CHALLENGE);}}*/}
            {/*style={ {justifyContent:'center', flexDirection: 'row'}}>*/}
            {/*<Image*/}
            {/*source={require('assets/icons/Back_1.png')}*/}
            {/*style={{ resizeMode: 'contain', padding: 10 , alignSelf: 'center', zIndex: 999 }}*/}
            {/*/>*/}
            {/*</TouchableOpacity>*/}
        </View>
    )
};

const ComponentCenter = ({state}) => {


    return(
        <View style={{paddingTop:isIphoneX()?36:15 }}>

            <Image source={require('assets/icons/Logo.png')} />

        </View>
    );
};

const ComponentRight = ({state}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-end', }}>

        </View>
    );
};


class WelcomeView extends React.Component<$Props, $State> {

    constructor(props) {
        super(props);
        const challengeModalState = this.getInitialModalState(props);
        this.state = {
            challengeModalState,
        };
    }

    closeModal = (challengeId: number) => {
        this.setState({
            challengeModalState: {
                ...this.state.challengeModalState,
                [challengeId]: {
                    ...this.state.challengeModalState[challengeId],
                    isVisible: false,
                },
            },
        });
    }

    toggleModal = (challengeId: number) => {
        this.setState({
            challengeModalState: {
                ...this.state.challengeModalState,
                [challengeId]: {
                    ...this.state.challengeModalState[challengeId],
                    isVisible: !this.state.challengeModalState[challengeId].isVisible,
                }
            }
        })
    };

    getUserSession = () => {


        const retrievedItem = AsyncStorage.getItem(STORAGE_USER_SESSION);
        return retrievedItem
    }

    onClick = () => {
        Share.share({
            url: 'https://s3.amazonaws.com/gallery.fitnesscourt.com/assets/my-score.jpg',
        }, {
            // Android only:
            dialogTitle: '',
            // iOS only:
            excludedActivityTypes: [
            ]
        })

    }

    getInitialModalState = (props: $Props): $ChallengeModalState => {
        const challengeModalState = {};
        props.challenges.forEach(challenge => {
            challengeModalState[challenge.challengeId] = {
                ...challenge,
                isVisible: false,
            };
        });
        return challengeModalState;
    }

    // FIXME: We are calling fetchUserInfo here to make sure userInfo is populated for the submission screen
    componentWillMount() {
        const { dispatch } = this.props;
        dispatch(mainActions.fetchUserInfo());
        Orientation.lockToPortrait();


        this.getUserSession().then((item) => {
            //this callback is executed when your Promise is resolved

            var apiUrl = 'http://api.fitnesscourt.com/api/v1/get_user/?api_key=UkNNuT0bebf2bRoCMitx&token=' + item
            return fetch(apiUrl , {
                headers: new Headers({
                    'Content-Type': 'application/json'
                })
            })
                .then(response => response.json())
                .then(response => {

                    this.setState({
                        userId: response.id
                    })
                    console.log("response from userAPI", response)
                    console.log("userID", this.state.userId)

                    var apiUrl = 'http://api.fitnesscourt.com/api/v1/my_score/?api_key=UkNNuT0bebf2bRoCMitx&user_id=' + response.id

                    return fetch(apiUrl , {
                        headers: new Headers({
                            'Content-Type': 'application/json'
                        })
                    })
                        .then(response => response.json())
                        .then(response => {

                            console.log("response from get_score", response)

                            this.setState({
                                totalScore: response.total_score,
                                challengesCount: response.challenges_count,
                                challengesArray: response.challenges
                            })

                        });

                });

        }).catch((error) => {
            //this callback is executed when your Promise is rejected
        });
    }

    componentDidMount() {


    }


    onPress = () => {
        this.props.navigation.navigate(LeaderBoardScreen);
    };
    componentWillReceiveProps(nextProps) {
        if (!isEqual(nextProps.challenges, this.props.challenges)) {
            this.setState({
                challengeModalState: this.getInitialModalState(nextProps),
            });
        }
    }

    render() {
        //$FlowFixMe
        const values: {...$Challenge, isVisible: boolean}[] = Object.values(this.state.challengeModalState);
        const { navigate } = this.props.navigation;

        return (



            <View style={styles.container}>


                <NavigationBar
                    componentCenter   = { () =>  <ComponentCenter /> }
                    componentRight   = { () =>  <ComponentRight navigation={this.props.navigation}/> }
                    navigationBarStyle= {{ backgroundColor: navBarColor, height:isIPhoneX()?80:65,marginTop: Platform.OS === 'ios'? -20 : 0}}
                    statusBarStyle    = {{ barStyle: 'light-content', backgroundColor: '#215e79'}}
                />


                <ScrollView contentContainerStyle={styles.scrollContainer}>
                    <Slider />

                    <View style={{height: 207, marginBottom: 25}}>


                        <ScrollView  horizontal={true} contentContainerStyle={styles.horizontalScrollContainer} showsHorizontalScrollIndicator={false}>


                            <View style={styles.challengesContainer}>
                                {values.map(value => (
                                    <ChallengePanel
                                        {...value}
                                        key={`challengepanel_${Math.random()}`}
                                        toggleModal={this.toggleModal}
                                        closeModal={this.closeModal}
                                    />
                                ))}
                            </View>
                            {values.map(challenge => {
                                return (
                                    <ChallengeModal
                                        dispatch={this.props.dispatch}
                                        key={`challengeModal__${Math.random()}`}
                                        {...challenge}
                                        closeModal={this.closeModal}
                                    />
                                );})}

                        </ScrollView>

                    </View>


                    <View style={styles.myStats}>

                        <ImageBackground
                            style={styles.Imgbackground}
                            source={require('assets/images/My_Stats_Bg.png')}>

                            <View style={styles.myStatsContainer}>

                                <View style={styles.myStatsHeader}>
                                    <Text style={styles.myStatTextHeaderText1}>
                                        My Stats
                                    </Text>
                                    <TouchableOpacity style={styles.myEntriesView}
                                                      onPress={() => this.props.navigation.navigate(CHALLENGE_ROUTES.MYENTRIES, {challengesArray: this.state.challengesArray,})}>

                                        <View style={styles.emptyDivChallenges}>

                                        </View>
                                        <Text style={styles.myStatTextHeaderText2}>
                                            My Entries
                                        </Text>
                                        <Image
                                            style={styles.myEntiresBackIcon}
                                            source={require('assets/icons/Back.png')}
                                        />
                                    </TouchableOpacity>
                                </View>

                                <View style={styles.myStatDataContainer}>

                                    <View style={styles.myStatTopScoreContainer}>

                                        <View style={styles.myStatDataTextContainer}>
                                            <Text style={styles.myStatText1}>Total</Text>
                                            <Text style={styles.myStatText2}>Score</Text>
                                        </View>

                                        <ImageBackground
                                            style={styles.myStatBadgeContainer}
                                            source={require('assets/icons/Badge.png')}
                                            resizeMode="contain">

                                            <View style={styles.badgeTextView}>

                                                <Text style={styles.badgeText1}>{this.state.totalScore}</Text>
                                                <Text style={styles.badgeText2}>points</Text>
                                            </View>

                                        </ImageBackground>
                                    </View>

                                    <View style={styles.myStatDivider}>

                                        <ImageBackground
                                            style={styles.dividerContainer}
                                            source={require('assets/icons/Divider.png')}
                                            resizeMode="contain">
                                        </ImageBackground>

                                    </View>

                                    <View style={styles.myStatTotalScoreContainer}>

                                        <View style={styles.myStatDataTextContainer}>
                                            <Text style={styles.myStatText1}>Total</Text>
                                            <Text style={styles.myStatText2}>Challenges</Text>
                                        </View>
                                        <View>

                                            <Text style={styles.myStatChallenges}>{this.state.challengesCount}</Text>
                                        </View>

                                    </View>
                                </View>
                            </View>

                        </ImageBackground>

                    </View>


                    <View style={styles.ChallengeButtonContainer}>



                        <TouchableOpacity style={styles.viewboardButton}
                                          onPress={() => this.props.navigation.navigate(CHALLENGE_ROUTES.LEADERBOARD)}
                        >
                            <Text style={styles.viewboardButtonText}>View LeaderBoard</Text>
                        </TouchableOpacity>


                        <TouchableOpacity style={styles.challengeShareButton}
                                          onPress={() => this.onClick()}
                        >
                            <Image
                                style={styles.challengeIcon}
                                source={require('assets/icons/Challenge.png')}
                            />



                            <Text style={styles.challengeButtonText}>Challenge a friend</Text>

                        </TouchableOpacity>

                    </View>

                </ScrollView>

            </View>






        );
    }
}

export default connect(select)(WelcomeView);

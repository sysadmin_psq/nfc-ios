// @flow
import { StyleSheet } from 'react-native';
import { SCREEN_WIDTH } from 'constants';

export default StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginEnd: -1
  },
  container: {
    // width: SCREEN_WIDTH - 57,
    // height: (SCREEN_WIDTH) * (3/8,
      width: 331,
      height: 207,
      marginBottom: 20,
      marginEnd: 5,
  },
});

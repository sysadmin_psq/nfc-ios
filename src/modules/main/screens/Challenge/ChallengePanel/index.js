// @flow
import React from 'react';
import { TouchableOpacity, ImageBackground } from 'react-native';
import styles from './styles';
import type { $Challenge } from '../';

export type $Props = {
  ...$Challenge,
  toggleModal: any,
};

export default (props: $Props) => (
    <TouchableOpacity
      onPress={() => props.toggleModal(props.challengeId)}
      style={styles.container}
    >
        <ImageBackground
          style={styles.background}
          source={props.thumbnailImage}
          resizeMode="contain"
        />
    </TouchableOpacity>
);

// @flow
import { CHALLENGE_ROUTES, S3_BUCKET_URL } from 'constants';
import { Modal, Text, View } from 'react-native';
import * as challengeActions from 'modules/challenge/state/dux';
import * as sharedActions from 'shared/state/dux';
import CloseButton from 'shared/components/CloseButton';
import DynamicButton from 'shared/components/DynamicButton';
import React from 'react';
import styles from './styles';
import type { $Challenge } from '../';
import Video from 'shared/components/Video';

type $Props = {
  ...$Challenge,
  isVisible: boolean,
  closeModal: any,
  dispatch: any,
};

export default (props: $Props) => (
    <Modal
      visible={props.isVisible}
      animationType="fade"
    >
        <View style={styles.modalContainer}>
            <View style={styles.modalHeader}>
                <CloseButton
                  style={styles.buttonStyle}
                  onPress={() => props.closeModal(props.challengeId)}
                />
                <Text style={styles.modalHeaderText}>
                    {props.title}
                </Text>
            </View>
            <View style={styles.modalBody}>
                <Text style={styles.subtitle}>
                    {props.subtitle}
                </Text>
                <View style={styles.videoContainer}>
                    <Video
                        source={`${S3_BUCKET_URL}/challenge_mode_intro/newChallengeIntro.mp4`}
                        isNewAspectRatio
                    />
                </View>
                <Text style={styles.description}>
                    {props.description}
                </Text>
            </View>
            <DynamicButton
              enabled
              style={styles.challengeButton}
              text={'ENTER CHALLENGE'}
              onPress={() => {
                props.dispatch(challengeActions.setChallengeMode(props.challengeMarker));
                props.dispatch(sharedActions.navigate(CHALLENGE_ROUTES.RECORD));
              }}
            />
        </View>
    </Modal>
);

// @flow
import { StyleSheet } from 'react-native';
import { blueTopNav, white } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';
import { SCREEN_WIDTH } from 'constants';
import { isIOS } from 'shared/utils';

export default StyleSheet.create({
  buttonStyle: {
    position: 'absolute',
    top: 18,
    left: 18,
    padding: 8,
  },
  challengeButton: {
    width: SCREEN_WIDTH - 80,
    marginBottom: SCREEN_WIDTH / 10,
    alignSelf: 'center',
    marginTop: 30,
  },
  modalContainer: {
    backgroundColor: blueTopNav,
    flex: 1,
    paddingTop: isIOS() ? 30 : 0,
  },
  modalBody: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  description: {
    color: white,
    fontSize: 20,
    ...ProximaNova.CondensedSemibold,
  },
  subtitle: {
    paddingVertical: 35,
    color: white,
    fontSize: 22,
    textAlign: 'center',
    ...ProximaNova.CondensedSemibold,
  },
  modalHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 60,
  },
  modalHeaderText: {
    alignSelf: 'center',
    color: white,
    fontSize: 26,
    ...ProximaNova.Bold,
  },
  videoContainer: {
      width: '70%',
      marginTop: 16,
      paddingHorizontal: 4,
      alignItems: 'center',
  },
});

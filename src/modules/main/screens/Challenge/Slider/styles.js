// @flow
import { black, dotActiveGrey, dotGrey, white } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';
import { isIPhone4 } from 'shared/utils';

export const swiperStyleProps = {
    paginationStyle: {
        bottom: 10,
        zIndex: 1,
    },
    dotStyle: {
        marginTop: 5,
        backgroundColor: dotGrey,
        width: 6,
        height: 6,
        borderRadius: 3,
    },
    activeDotStyle: {
        marginTop: 5,
        backgroundColor: dotActiveGrey,
        width: 6,
        height: 6,
        borderRadius: 3,
    },
};

export default StyleSheet.create({
    boldSlideText: {
        width: isIPhone4() ? 280 : 280,
        fontSize: isIPhone4() ? 14 : 16,
        lineHeight: isIPhone4() ? 16 : 18.5,
        letterSpacing: 1,
        color: black,
        textAlign: 'center',
        ...ProximaNova.Condensed,
    },
    container: {
        marginVertical: '5%',
        height: 129,
    },
    descriptionSlideText: {
        maxWidth: 325,
        marginTop: isIPhone4() ? 2 : 14,
        fontSize: isIPhone4() ? 8 : 12,
        lineHeight: isIPhone4() ? 11 : 20,
        color: black,
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '95%',
        textAlign: 'center',
        ...ProximaNova.Regular,
    },
    famePrizesText: {

        height: 17,
        display: 'flex',
        color: '#3B3B3B',
        fontSize: 14,
        lineHeight: 17,
        textAlign: 'center',
        marginTop:11.9,
        ...ProximaNova.Semibold,
    },
    slide: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: white,
        maxWidth: 600,
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '95%'
    },
});

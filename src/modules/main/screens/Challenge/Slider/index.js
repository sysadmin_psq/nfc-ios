// @flow
import React from 'react';
import Swiper from 'react-native-swiper';
import styles, { swiperStyleProps } from './styles';
import { View, Text, Image } from 'react-native';

export default () => (
    <View style={styles.container}>

            <View style={styles.slide}>
                <Text style={styles.boldSlideText}>
                    How High Can You Score?
                </Text>
                <Text style={styles.descriptionSlideText}>
                    See how you stack up with friends and competitors across
                    the country in a scored, timed challenge. Grab a buddy to film
                    your entry and work your way up the leaderboard
                </Text>

                <View style={{display:'flex', flexDirection:'row'}}>

                    <Text style={styles.famePrizesText}>
                        Fame and prizes are on the line
                    </Text>

                    {/*<Image*/}
                        {/*source={{uri: 'assets/images/learnTheMoves.png'}}*/}
                    {/*/>*/}

                    <Image
                        style={{marginTop:11.9, marginLeft:4.5, alignItems:'center'}}
                        source={require('/assets/icons/Fame.png')}
                    />
                </View>




            </View>
    </View>
);

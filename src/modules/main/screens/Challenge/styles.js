// @flow
import { black, borderGrey, white } from 'shared/styles/colors';
import { Dimensions, StyleSheet, Platform } from 'react-native';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from 'constants';
import { ProximaNova } from 'shared/styles/fonts';
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';

const { height } = Dimensions.get('window');

export default StyleSheet.create({
    buttonContainer: {
        paddingHorizontal: 20,
        marginTop: 20,
    },
    challengesContainer: {
        alignItems: 'center',
        flex: 10,
        flexDirection:'row',
        marginStart: 20,
        marginBottom: 0,
    },

    myStats: {

        height: isMediumScreen()? 130 : 150,
        borderRadius: 2,
        // maxWidth: 600,
        width: isMediumScreen()? '90%' : '90%',
        marginTop: 0,
        marginRight: 'auto',
        marginBottom: 0,
        marginLeft: 'auto',
        // backgroundColor: black

    },

    myStatsContainer: {

        width: '100%',
        height: 'auto',
        display: 'flex',
        flexDirection: 'column',
    },
    myStatsHeader: {

        display: 'flex',
        flexDirection: 'row',
        height: 30,
        justifyContent: 'space-between',
        width: '95%',
        height: '25%',
        paddingLeft: 13,
        paddingTop: isMediumScreen()? 5 : 10
    },

    myStatTextHeaderText1: {

       color: white,
        fontSize: 14,
        width: '50%'
    },
    myStatTextHeaderText2: {

        color: white,
        fontSize: 12,
        width: '31%',
        // display: 'none'


    },

    myEntiresBackIcon: {

        marginTop: 3,
        // display: 'none'
    },

    myEntriesView: {

        display: 'flex',
        flexDirection: 'row',
        //display: 'none'
    },

    emptyDivChallenges: {

      width: '37%'
    },


    horizontalScrollView: {


        height: 136,

    },

    ViewButtonText: {

        color: '#00AEEF',
        fontSize: 12,

    },

    myStatDataContainer: {

        height: '75%',
        width: '100%',
        display: 'flex',
        flexDirection: 'row',

    },

    myStatTopScoreContainer: {

        height: '70%',
        width: '47%',
        color: white,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 20,

    },

    myStatTotalScoreContainer: {

        height: '70%',
        width: '49%',
        color: white,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',

    },

    myStatDivider: {

        height: '70%',
        width: '2%',

    },

    myStatDataTextContainer: {

        flex: 0.7,

    },

    myStatChallenges: {

        fontSize: 36,
        color: white,
        ...ProximaNova.CondensedBold

    },

    myStatText1: {

        fontSize: 12,
        color: white,
        ...ProximaNova.Regular
    },

    myStatText2: {

        fontSize: isMediumScreen() ? 14 : 16,
        color: white,
        ...ProximaNova.Semibold,
    },

    myStatBadgeContainer: {

        height: 69,
        width: 43.51,
        flex: 1,
        alignItems: 'center',

    },

    dividerContainer: {

        height: '100%',
        width: '100%',
    },

    badgeTextView: {

        paddingTop: '15%',
        alignItems: 'center'
    },
    badgeText1: {

        fontSize: 22,
        color: white,
        paddingBottom: 0,
        height: 22,
        ...ProximaNova.CondensedBold,
    },
    badgeText2: {

        fontSize: 8.93,
        color: white,
        lineHeight: 10,
        paddingBottom: 10,
        ...ProximaNova.Regular

    },


    scrollContainer: {
        backgroundColor: white,
        height: isMediumScreen() ? SCREEN_HEIGHT - '10%' : isIPhoneX()? SCREEN_HEIGHT - 160 : isPixelScreen()? SCREEN_HEIGHT - 60 : SCREEN_HEIGHT - '30%',
    },

    container: {
        flex: 1,
        backgroundColor: white,
    },


    horizontalScrollContainer: {
        backgroundColor: white,

    },


    listContainer: {
        alignItems: 'center',
    },

     Imgbackground: {

        width: '100%',
         height: '100%'
    },

    howItWorks: {
        position: 'absolute',
        backgroundColor: 'white',
        top: isIPhone4() ? 290 : SCREEN_WIDTH,
        left: 0,
        width: '100%',
        zIndex: 2,
        height: height < 650 ? 30 : 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: borderGrey,
    },
    howItWorksText: {
        fontSize: 15,
        color: black,
        letterSpacing: 0.5,
        ...ProximaNova.CondensedBold,
    },

    submit:{
        // marginRight:40,
        // marginLeft:40,
        // marginTop:10,
        // paddingTop:20,
        // paddingBottom:20,
        backgroundColor: white,
        // borderRadius:10,
        // borderWidth: 1,
        // borderColor: '#fff'
        height: 40,
        width: 160,
        borderColor: '#00AEEF'
    },


    ChallengeButtonContainer: {

        display: 'flex',
        flexDirection: 'row',
        width: isMediumScreen()? '90%' : '90%',
        maxWidth: 600,
        flex: 1,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: isPixelScreen()? 15 : 15,
        marginBottom: 15,


    },

    viewboardButton: {

        padding:10,
        borderRadius: 50,
        height: 40,
        width: '49%',
        paddingBottom: 10,
        backgroundColor: white,
        borderColor: '#00AEEF',
        borderWidth: 1,
        alignItems: 'center',


    },

    viewboardButtonText: {

        color: '#00AEEF',
        fontSize: 12,
        paddingTop: 2,
    },


    challengeShareButton: {

        padding:10,
        borderRadius: 50,
        height: 40,
        width: '49%',
        paddingBottom: 10,
        backgroundColor: '#00AEEF',
        alignItems: 'center',
        marginLeft: 10,
        display: 'flex',
        flexDirection: 'row',
        paddingLeft: isMediumScreen()? 15 : isLargeScreen() ? 25 : 20,
        paddingRight: isMediumScreen()? 10 : 15

    },

    challengeButtonTextContainer: {


        // width: '90%'
    },

    challengeIconContainer: {


        // width: '10%'

    },

    challengeButtonText: {

        color: white,
        fontSize: isMediumScreen() ? 10 : 12,
        paddingTop: 1,
        textAlign: 'center'

    },

    challengeIcon: {

        height: 14.55,
        width: 14.95,
        justifyContent: 'center',
        //flex: isMediumScreen()?  1 : 0.3,
        marginRight: isMediumScreen() ? '2%' : isLargeScreen() ? '8%' : '5%',
        // marginLeft: isMediumScreen() ? '5%' : '10%'


    },






});

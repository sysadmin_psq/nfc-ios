// @flow
import { black, blue, lightGrey, white } from "shared/styles/colors";
import { isIPhone4,isIPhoneX } from "shared/utils";
import { ProximaNova } from "shared/styles/fonts";
import { StyleSheet } from "react-native";

const text = {
    textAlign: 'center',
    color: black,
    fontSize: isIPhone4() ? 11 : 18,
    lineHeight: isIPhone4() ? 12 : 22,
    ...ProximaNova.Regular,
};

export default StyleSheet.create({
    bottomLinksContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 40,
    },
    container: {
        flex: 1,
        backgroundColor: white,
        justifyContent: 'center',
        alignItems: 'center',
    },
    fitnessCourtLink: {
        marginLeft: 4,
    },
    headerText: {
        color: white,
        fontSize: 22,
        ...ProximaNova.CondensedBold,
    },
    image: {
        marginHorizontal: 16,
    },
    iconsContainer: {
        marginTop: 18,
    },
    inlineContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    linkText: {
        fontSize: 16,
        color: blue,
        ...ProximaNova.Semibold,
    },
    linkTextCenter: {
        marginHorizontal: 4,
    },
    loadingProfile: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        resizeMode: "cover",
        width: isIPhone4() ? 80 : 180,
        height: isIPhone4() ? 71: 160,
        marginBottom: 14,
    },
    middleContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingHorizontal: 20,
        paddingTop: 16,
        paddingBottom: 40
    },
    paragraph: {
        marginBottom: 14,
        ...text,
    },
    profileContainer: {
        // paddingHorizontal: 16,
        // paddingVertical: 18,
        width: '100%',
        // borderColor: lightGrey,
        // borderBottomWidth: 1,
    },
    profileNameText: {
        color: black,
        fontSize: 19,
        ...ProximaNova.Semibold,
    },
    profileUsernameText: {
        ...text,
        textAlign: 'left',
    },
    profileRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    profileRowRightBox: {
        flexDirection: 'row',
    },
    restContainer: {
        flexGrow: 1,
        paddingHorizontal: 16,
        paddingVertical: 16,
        height:isIPhoneX()?'110%':'100%',
    },
    text: {
        ...text,
    },
});

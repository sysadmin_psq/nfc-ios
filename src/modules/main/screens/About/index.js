// @flow
import { MAIN_ROUTES } from 'constants';
import { connect } from 'react-redux';
import {
  ActivityIndicator, Image, Linking,
  Text, TouchableOpacity, View, ScrollView, Platform
} from 'react-native';
import { lightGrey } from 'shared/styles/colors';
import React, { Component } from 'react';
import select from 'modules/main/state/selectors/AboutScreen';
import { isIPhone4,isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import styles from './styles';
import * as sharedActions from 'shared/state/dux';
import * as mainActions from 'modules/main/state/dux';
import { NavigationBar } from 'navigationbar-react-native';
import { ProximaNova } from 'shared/styles/fonts';
import { black, borderGrey, white, navBarColor } from 'shared/styles/colors';
import { isIphoneX } from 'react-native-iphone-x-helper';




type $Props = {
    dispatch: *,
    name: ?string,
    showProfile: boolean,
    username: ?string,
    id: ?number,
};

const ABOUT_PARAGRAPH_1 = `\
National Fitness Campaign creates
Fitness Courts across America,
bringing free, world class, outdoor
fitness to your community.
`

const ABOUT_PARAGRAPH_2 = `\
Get a coach. Discover Videos.
Take the Challenge.
`




const ComponentLeft = ({navigation}) => {

    return(
        <View style={{ flex: 1.1, alignItems: 'flex-start',paddingLeft:5}} >
            <TouchableOpacity
                onPress={() => { navigation.navigate(MAIN_ROUTES.SETTINGS);}}
                style={ {justifyContent:'flex-start', flexDirection: 'row',width:50,height:50,}}>
                <Image
                    source={require('assets/icons/Back_1.png')}
                    style={{ resizeMode: 'contain', padding: 10 , alignSelf: 'center', zIndex: 999 }}
                />
            </TouchableOpacity>
        </View>
    );
};

const ComponentCenter = () => {
    return(
        <View style={{ flex: 0.5,}}>

            <Text style={{ color: 'white', ...ProximaNova.Regular, fontSize: 18}}>About</Text>
        </View>
    );
};

const ComponentRight = ({state}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-end', }}>
            <TouchableOpacity onPress={() => { state.setState({modalVisible: true})}}>


                {state.state.isFiltered == false &&

                <Image
                    source={require('assets/icons/Filter.png')}
                    style={{ resizeMode: 'contain', padding: 10 ,marginRight:16, alignSelf: 'center', zIndex: 999 }}
                />
                }

                {state.state.isFiltered == true &&

                <Image
                    source={require('assets/icons/Filtered.png')}
                    style={{ resizeMode: 'contain', padding: 10 ,marginRight:16, alignSelf: 'center', zIndex: 999 }}
                />
                }

            </TouchableOpacity>
        </View>
    );
};



class AboutScreen extends Component<$Props> {
    componentWillMount() {

        const { dispatch } = this.props;
        dispatch(mainActions.fetchUserInfo());
    }

    render() {
        const {
            dispatch,
            name,
            showProfile,
            username,
        } = this.props;

        let profile = (

        <NavigationBar
            componentLeft     = { () =>  <ComponentLeft navigation={this.props.navigation}/>   }
            componentCenter   = { () =>  <ComponentCenter /> }
            navigationBarStyle= {{ backgroundColor: navBarColor, height:isIPhoneX()?80:55,paddingTop:isIphoneX()?40:20,marginTop: Platform.OS === 'ios'? -20 : 0}}
            statusBarStyle    = {{ barStyle: 'light-content', backgroundColor: 'red'}}
            style={styles.profileNavigation}
        />
        );

        return (
            <View style={styles.container}>
                <View style={styles.profileContainer}>
                    {profile}
                </View>
                <ScrollView style={styles.restContainer}>
                    <View style={styles.middleContainer}>
                        <Image
                            source={require('assets/images/Main-Shield.png')}
                            style={styles.logo}
                        />
                        <Text style={styles.paragraph}>
                            {ABOUT_PARAGRAPH_1}
                        </Text>
                        <Text style={styles.paragraph}>
                            {ABOUT_PARAGRAPH_2}
                        </Text>
                        <View style={styles.inlineContainer}>
                            <Text style={styles.text}>
                                Learn more at
                            </Text>
                            <Text
                                style={[styles.linkText, styles.fitnessCourtLink]}
                                onPress={() => Linking.openURL('http://www.fitnesscourt.com')}
                            >
                                fitnesscourt.com
                            </Text>
                        </View>
                        <View style={[styles.inlineContainer, styles.iconsContainer]}>
                            <TouchableOpacity
                              onPress={() => Linking.openURL('https://www.facebook.com/NatlFitCampaign')}
                            >
                                <Image
                                    style={styles.image}
                                    source={require('assets/icons/Facebook.png')}

                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                              onPress={() => Linking.openURL('https://instagram.com/fitnesscourt/')}
                            >
                                <Image
                                    style={styles.image}
                                    source={require('assets/icons/Instagram.png')}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

export default connect(select)(AboutScreen);

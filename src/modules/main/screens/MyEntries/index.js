
import React from 'react';
import { View, ListView, StyleSheet, Text, ScrollView, Dimensions, ActivityIndicator, Platform, TouchableOpacity, Image } from 'react-native';
import { black, borderGrey, white } from 'shared/styles/colors';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { ProximaNova } from 'shared/styles/fonts';
import { NavigationBar } from 'navigationbar-react-native';
import styles from './styles';
import EntriesRow from "./myentryRows";
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import {MAIN_ROUTES, CHALLENGE_ROUTES} from "../../../../constants/index";



const ComponentLeft = ({navigation}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-start', }} >
            <TouchableOpacity
                onPress={() => { navigation.navigate(MAIN_ROUTES.CHALLENGE);}}
                style={ {justifyContent:'center', flexDirection: 'row',width:50,height:50}}>
                <Image
                    source={require('assets/icons/Back_1.png')}
                    style={{ resizeMode: 'contain', padding: 10 , alignSelf: 'center', zIndex: 999 }}
                />
            </TouchableOpacity>
        </View>
    );
};

const ComponentCenter = () => {
    return(
        <View style={{ flex: 1, paddingLeft: 20}}>

            <Text style={{ color: 'white', ...ProximaNova.Regular, fontSize: 18}}>My Entries</Text>
        </View>
    );
};

const ComponentRight = ({state}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-end', }}>
            <TouchableOpacity onPress={() => { state.setState({modalVisible: true})}}>


                <Image
                    source={require('assets/icons/Filter.png')}
                    style={{ resizeMode: 'contain', padding: 10 ,marginRight:16, alignSelf: 'center', zIndex: 999 }}
                />

            </TouchableOpacity>
        </View>
    );
};



class MyEntries extends React.Component {
    /*
     * Removed for brevity
     */
    //

    constructor(){
        super();

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {

            dataSource:  ds.cloneWithRows([]),
            isLoading: true

        }

    }

    componentWillMount(){


        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        var values = this.props.navigation.state.routes.find((e) => e.routeName === 'MyEntries')
        this.setState({
            challengesArray: ds.cloneWithRows(values.params.challengesArray)
        })

    }



    render() {


        return (


                <View style={styles.viewContainer}>

                    <NavigationBar
                        componentLeft     = { () =>  <ComponentLeft navigation={this.props.navigation}/>   }
                        componentCenter   = { () =>  <ComponentCenter /> }
                        navigationBarStyle= {{ opacity:1,height:isIPhoneX()?80:55,paddingTop:isIPhoneX()?40:20,backgroundColor:'#0098D1', marginTop: Platform.OS === 'ios'? -20 : 0 }}
                        statusBarStyle    = {{ barStyle: 'light-content',opacity:1 }}
                    />


                    <ListView
                        style={styles.listContainer}
                        dataSource={this.state.challengesArray}
                        renderRow={(data, i, j) => {

                            let props = {

                                data: data,
                                index: j,

                            }
                                return (

                                    <View>
                                        <EntriesRow {...props}/>
                                    </View>

                                )

                        }}
                        renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
                    />



                </View>


        );
    }
}

export default MyEntries;
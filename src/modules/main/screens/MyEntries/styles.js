import { black, borderGrey, white } from 'shared/styles/colors';
import { Dimensions, StyleSheet, Platform } from 'react-native';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from 'constants';
import { ProximaNova } from 'shared/styles/fonts';
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';

const { height } = Dimensions.get('window');

export default StyleSheet.create({


    viewContainer: {

        width: '100%',
        height:isIPhoneX()?'110%':'100%',
        backgroundColor: '#ffffff'
    },

    listContainer: {

        backgroundColor: white,
        width: '100%'
    },


    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#E4E8EC',
        marginLeft: 20,
        marginRight: 20,

    },


});
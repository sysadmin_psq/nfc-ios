import React from 'react';
import { View, Text, StyleSheet, Image, ImageBackground, Platform } from 'react-native';
import { ProximaNova } from 'shared/styles/fonts';
import { black, borderGrey, white } from 'shared/styles/colors';
import MaterialInitials from 'react-native-material-initials/native';
import { isIPhone4, isMediumScreen } from 'shared/utils';


const styles = StyleSheet.create({
    rowContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        height: 67,
    },
    text: {
        marginLeft: 20,
        fontSize: 12,
        ...ProximaNova.Regular,
        color: '#9B9B9B',
        width: 15
    },
    userPhoto: {
        width: 46,
        borderRadius: 23,
        marginLeft: 20,
        marginTop: 10,
        height: isMediumScreen()? 46 : 46,
        borderRadius: isMediumScreen()? 23 : 23,
        alignItems: 'center'
    },

    myviewContainer: {

        flex: 1,
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        height: 90,
    },

    infoView: {

        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        // paddingTop: 8,
        width: isMediumScreen()? '55%' : '59%',
        marginLeft: 15,
    },

    nameText: {

        ...ProximaNova.Semibold,
        color: '#575757',
        fontSize: 14,
        width: '90%'
    },
    locationText: {

        color: '#575757',
        fontSize: 10,
        paddingTop: 1,
        ...ProximaNova.Regular
    },

    championView: {

        display: 'flex',
        flexDirection: 'row'
    },

    championVideoIcon: {

        marginLeft: 8

    },

    championText: {

        color: white,
        fontSize: 12,
        marginTop: 2,
        ...ProximaNova.SemiboldItalic,
    },

    medalView: {

        width: '20%',
        height: '100%',
    },
    medal: {

        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        height: '100%',
        marginTop: 5,
        height: 56,
        width: 44,
        alignItems: 'center',
    },

    medalTitleText: {

        ...ProximaNova.CondensedBold,
        fontSize: 20,
        marginTop: 5,
        paddingBottom: 0,
        height: 18,

    },

    medalPointsFont: {

        fontSize: 8.93,
        color: black,
        paddingBottom: 10,
        ...ProximaNova.Regular
    }


});

const EntriesRow = (props) => {


    var object = {...props.data}
    var userObject = {...object.user}
    var court = {...object.fitness_court}
    var picture = {...object.picture}



    var date =  new Date(object.created)

    if(Platform.OS === 'ios') {

        date =  new Date(object.created).toLocaleString([], { year: 'numeric', month: 'long', day: 'numeric' });
    }
    else {

        var
            dayOfWeek = ["Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun"],
            monthName = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"],

        date = monthName[date.getMonth()] + " " + date.getDate() + ",  " + date.getFullYear()
    }


    return (
        <View style={styles.rowContainer}>


            <View style={styles.userPhoto}>
                <Image
                    source={require('assets/icons/Video.png')}
                    style={{ resizeMode: 'contain',  width: '80%', height: '80%'}}
                />
            </View>
            <View style={styles.infoView}>

                <Text numberOfLines={1} style={styles.nameText}>{`${object.challenge.name}`}</Text>
                <Text numberOfLines={1} style={styles.locationText}>{`${court.name}`}, {`${court.city_name}`}</Text>
                <Text numberOfLines={1} style={{fontSize: 10, paddingTop: 2, ...ProximaNova.Medium}}>{`${date}`}</Text>

            </View>

            <View style={styles.medalView}>

                <ImageBackground
                    style={styles.medal}
                    source={require('assets/icons/Badge_LB.png')}
                    resizeMode="contain">

                    <Text style={styles.medalTitleText}>{object.score}</Text>
                    <Text style={styles.medalPointsFont}>points</Text>
                </ImageBackground>

            </View>

        </View>
    )
};

export default EntriesRow;



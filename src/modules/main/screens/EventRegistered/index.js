
import React from 'react';
import { View, ListView, StyleSheet, Text, ScrollView, Dimensions, ActivityIndicator, Platform, TouchableOpacity, Image,Button,Alert,ImageBackground, AsyncStorage} from 'react-native';
import { black, borderGrey, white } from 'shared/styles/colors';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import DynamicButton from 'shared/components/DynamicButton';
import { ProximaNova } from 'shared/styles/fonts';
import styles from './styles';
import ChallengePanel from 'modules/main/screens/Challenge/ChallengePanel';
import ChallengeModal from 'modules/main/screens/Challenge/ChallengeModal';
import select from 'modules/main/state/selectors/LocationScreen';
import select2 from 'modules/main/state/selectors/AboutScreen';
import openMap from 'react-native-open-maps';
import * as mainActions from 'modules/main/state/dux';
import { FEED_LOCATIONS, PAGINATION, STORAGE_USER_SESSION } from 'constants';
import SplashScreen from 'react-native-splash-screen';
import { NavigationBar } from 'navigationbar-react-native';
import FilterModal from 'modules/main/screens/LeaderBoard/FilterModal';
import Modal from "react-native-modal";
import ModalStyle from 'modules/main/screens/LeaderBoard/FilterModal/styles'
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen, isNotchDevice} from 'shared/utils';
import Confirm from './Confirm';
import Cancel from "./Cancel";
import Registration from "./RegistrationSuccess";
import Moment from 'react-moment';
import moment from 'moment';
import { connect } from 'react-redux';
import Config from 'react-native-config'


// type $Props = {
//     courts: Array<$FitnessCourt>,
//     locationsEnabled: boolean,
// };


type $Props = {
    dispatch: *,
    name: ?string,
    showProfile: boolean,
    username: ?string,
    id: ?number,
};

class EventDetail extends  React.Component<$Props> {
    /*
     * Removed for brevity
     */
    //


    constructor(){
        super();

        this.availableObject = []

    }

    state = {

        isRegistred: false,
        modalVisible: false,
        modalValue: false,
        availableObject: [],
        isCancel: false

    };



    _toggleModal(visible) {


        this.setState({modalVisible: visible})
    }


    numberCalculation(number){

        return  moment(number).format("h:mm A").replace(":00", "")
        // return moment(number, ["Hmm"]).format("h:mm A");
    }


    getUserSession = () => {


        console.log("session storage:", STORAGE_USER_SESSION)
        const retrievedItem = AsyncStorage.getItem(STORAGE_USER_SESSION);

        console.log("retrievedItem", retrievedItem)
        return retrievedItem
    }



    componentWillMount(){


        var values = this.props.navigation.state.routes.find((e) => e.routeName === 'EventDetail')

        this.setState({
            court: values.params.court,
            event: values.params.event,

        })

        this.getUserSession().then((item) => {

            var userURL = `${Config.SERVER_URL}/api/v1/get_user/?api_key=UkNNuT0bebf2bRoCMitx&token=` + item
            return fetch(userURL , {
                headers: new Headers({
                    'Content-Type': 'application/json'
                })
            })
                .then(response => response.json())
                .then(response => {

                    console.log("userURL", response)

                    var bookapiUrl = `${Config.SERVER_URL}/api/v1/bookings/${response.id}/?api_key=UkNNuT0bebf2bRoCMitx&service_id=${values.params.event.serviceId}`

                    return fetch(bookapiUrl , {
                        headers: new Headers({
                            'Content-Type': 'application/json'
                        })
                    })
                        .then(response => response.json())
                        .then(response => {


                            console.log("bookapiUrl", response)



                            if(response.length > 0) {

                                this.setState({

                                    isRegistred: true,
                                    confirmationObject : response[0],

                                })

                            }
                            var apiUrl = `${Config.SERVER_URL}/api/v1/availability/${values.params.event.serviceId}/2019-05-28/2019-05-28?location_id=${values.params.court.location_id}&resource_id=${values.params.event.resourceId}`


                            return fetch(apiUrl , {
                                headers: new Headers({
                                    // 'Authorization':  'Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IkJEN0FDRjNDMjZFMzIyNzg5NTUzRjNGRDg0MENEMEI3MEQ4NjMwMkMiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJ2WHJQUENiakluaVZVX1A5aEF6UXR3MkdNQ3cifQ.eyJuYmYiOjE1NTc3MjY3ODIsImV4cCI6MTU1NzczMDM4MiwiaXNzIjoiaHR0cHM6Ly9zYW5kYm94LWlkZW50aXR5Lm9uc2NoZWQuY29tIiwiYXVkIjpbImh0dHBzOi8vc2FuZGJveC1pZGVudGl0eS5vbnNjaGVkLmNvbS9yZXNvdXJjZXMiLCJPblNjaGVkQXBpIl0sImNsaWVudF9pZCI6ImNsaWVudHNiMTU1NDc1MDg5MSIsImNsaWVudF9jbGllbnQueGNpZCI6IjU5MmU4ZGZiLWYwMjMtNGE5Ny04MjcxLThkMGZiOWQzMGIwMCIsInNjb3BlIjpbIk9uU2NoZWRBcGkiXX0.w-iikFnvdPB7r36TSrbYVv7rrUsGndv7HPvdvLS2PpKsBXEKL044rzDqGBZa_6UB70cZVj6JO1_-7PiOzzgKKtxK3ZdCMVqABnH8zfA0neg_ufGT9nuIcJ6kdIuKPVKJyUEwGOrzCPSabVtsH435b0I4EBf5NWV2ZaBC-TdAzLASiNjntONHL_q7grwWAX_X5BJmzzy2cXe8w3qasLYO0CjFv2Y06xgCcnkIZ6akFDfXqw10G2HF8dvQKK4kDUNW2bUPW75hfASpTNWFdnGsxPlax8if4ed6liPmnWwKlUDQGF0IrO9bJjXUQwH3Ir3TZnzZEBj6uUI-CgEgWg1SAg',
                                    'Content-Type': 'application/json'
                                })
                            })
                                .then(response => response.json())
                                .then(response => {


                                    this.availableObject = response
                                    this.modalValue = true

                                    if(response.availableTimes.length > 0) {

                                        this.setState({

                                            modalValue : true
                                        })


                                    }

                                });

                        });
                });
        })

    }


    goToMap = (courtObject) => {

        openMap({ provider: 'google', latitude: parseFloat(courtObject.latitude), longitude: parseFloat(courtObject.longitude) });

    }

    setRegister = (isRegistred, confirm) => {


        console.log("inside", isRegistred, confirm)
        this.setState({

            isRegistred : isRegistred,
            confirmationObject : confirm,
            modalVisible: false
        })

    }



    render() {


        return (

            <View style={styles.eventMain}> 
                <View>
                    <ScrollView  onScroll={this.handleScroll} style={styles.eventScroll}>
                        {/* <ScrollView horizontal={true} contentContainerStyle={styles.horizontalScrollContainer} showsHorizontalScrollIndicator={false}> 
                            <View style={styles.challengesContainer}>
                                {SampleNameArray.map(value => (
                                    <ChallengePanel
                                        {...value}
                                        key={`challengepanel_${Math.random()}`}
                                        toggleModal={this.toggleModal}
                                        closeModal={this.closeModal}
                                    />
                                ))}
                            </View>
                        </ScrollView> */}
                        <View style={styles.bgevent}>
                            <ImageBackground
                                            style={styles.eventMainBg} 
                                            source={require('assets/images/competition-challenge-7m.png')}
                                            resizeMode="contain">
                                            <View style={styles.eventBtn}>
                                                <TouchableOpacity style={styles.backBtn}
                                                                  onPress={() => {  this.props.navigation.goBack(null)}}>
                                                <Text style={styles.hidebtn}></Text>
                                                <ImageBackground
                                                    style={styles.eventbtnbg}
                                                    source={require('assets/icons/EventbackArrow.png')}
                                                    resizeMode="contain"></ImageBackground>
                                                </TouchableOpacity>
                                                <TouchableOpacity style={styles.eventShareBtn}>
                                                <Text style={styles.hidebtn}></Text>
                                                    <ImageBackground
                                                        style={styles.eventShare}
                                                        source={require('assets/icons/share.png')}
                                                        resizeMode="contain"></ImageBackground>
                                                </TouchableOpacity>
                                            </View>
                                            </ImageBackground>
                        </View>
                        {this.state.modalValue == true &&

                            <View style={styles.eventAttend}><Text tyle={styles.eventAttendtext}>{this.availableObject.availableTimes[0].allowableBookings - this.availableObject.availableTimes[0].availableBookings} Attending</Text></View>
                        }
                        <View style={styles.EventDetailscreen}>
                                    <View>
                                        <View><Text style={styles.eventTitle}>{this.state.event.serviceName}</Text></View>
                                        <View style={styles.eventBorder}><Text style={styles.eventTrainer}>Trainer: {this.state.event.resourceName} </Text></View>

                                        <View style={styles.eventTimeDate}>
                                        <View style={styles.locationWidth}>
                                            <ImageBackground
                                            style={styles.dividerContainer}
                                            source={require('assets/icons/calender.png')}
                                            resizeMode="contain">
                                            </ImageBackground>
                                            </View>
                                            <View style={styles.eventTimecont}>
                                            <Text style={styles.eventTime}><Moment element={Text} format="ddd, MMM D, YYYY">{this.state.event.startDate}</Moment>  </Text>
                                            </View>
                                            <View style={styles.eventDatecont}><Text style={styles.eventDate}>{this.numberCalculation(this.state.event.start_date_time} - {this.numberCalculation(this.state.event.end_date_time)}</Text></View>
                                        </View>
                                        <View style={styles.eventTimeDate}>
                                            <View style={styles.locationWidth}>
                                                <ImageBackground
                                                style={styles.eventLocation}
                                                source={require('assets/icons/Event_Location.png')}
                                                resizeMode="contain">
                                                </ImageBackground>  
                                            </View>
                                             
                                            <View style={styles.eventAddress}>
                                                <Text style={styles.courtName}>{this.state.court.name}</Text>
                                                <Text style={styles.courtAddress}>{this.state.court.street} {this.state.court.city}</Text>
                                            </View>
                                            <View>
                                                <TouchableOpacity style={styles.eventLocatioicon}
                                                                  onPress={() => this.goToMap(this.state.court)}>
                                                <ImageBackground
                                                style={styles.eventDirection}
                                                source={require('assets/icons/Event_direction.png')}
                                                resizeMode="contain">
                                                </ImageBackground>
                                                </TouchableOpacity>
                                                <Text style={styles.directionText}>Directions</Text>
                                            </View>
                                          
                                        </View>

                                        {this.state.isRegistred === false &&


                                        <View style={styles.eventDesc}>
                                            <Text style={styles.eventDescTitle}>About</Text>
                                            <Text style={styles.eventDescription}>See how you stack up with friends and
                                                competitors across the country in a scored, timed challenge.
                                                Grab a buddy to film your entry and work your
                                                way up the leaderboard See how you stack up with friends and… Read
                                                more</Text>
                                        </View>

                                        }
                                        {this.state.isRegistred === true &&
                                        <View style={styles.eventDesc}>
                                            <ImageBackground
                                                style={styles.eventTicket}
                                                source={require('assets/images/Event_tickets.png')}
                                                resizeMode="contain">
                                                <Text style={styles.eventCoupncode}>confirmationObject.confirmationNumber</Text>
                                            </ImageBackground>
                                            <Text style={styles.eventTicketNote}>Please show this code at court</Text>
                                        </View>
                                        }

                                        <View style={styles.eventContactline}>
                                            <Text style={styles.eventContact}>Contact Organizer </Text>
                                        </View>
                                    </View>
                        </View>
                </ScrollView>
            </View>

            <View style={styles.detailsFooter}> 
            <Text style={styles.freeText}>Free</Text>

                {this.state.isRegistred == false &&

                <TouchableOpacity style={styles.registrationBtn}
                                  onPress={() => { this.setState({modalVisible: true, isCancel: false})}}>
                    <Text style={styles.registerBtn}>Register</Text>
                </TouchableOpacity>
                }

                {this.state.isRegistred == true &&

                <TouchableOpacity style={styles.registrationBtn}
                                  onPress={() => { this.setState({modalVisible: true, isCancel: true})}}>
                    <Text style={styles.registerBtn}>Cancel registration</Text>
                </TouchableOpacity>
                }


            </View>



                <View style={{width: '100%', margin: 0}}>
                    <Modal style={ModalStyle.modalContainer} visible={this.state.modalVisible}>

                        <TouchableOpacity style={{width: '100%', height: '100%'}} activeOpacity={1}
                                          onPressOut={() => {this._toggleModal(false)}}>


                            {this.state.isCancel == false &&


                            <Confirm eventObject={this.state.event}
                                     availability={this.availableObject}
                                     setRegister={this.setRegister}/>
                            }

                            {this.state.isCancel == true &&

                                <Cancel/>

                            }



                        </TouchableOpacity>

                    </Modal>
                </View>
    </View>

        );
    }
}

export default connect(select2)(EventDetail);

import React from 'react';
import { View, ListView, StyleSheet, Text, ScrollView, Dimensions, ActivityIndicator, Platform, TouchableOpacity, Image,Button,Alert,ImageBackground, AsyncStorage, AppState} from 'react-native';
import { black, borderGrey, white, navBarColor } from 'shared/styles/colors';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import DynamicButton from 'shared/components/DynamicButton';
import { ProximaNova } from 'shared/styles/fonts';
import styles from './styles';
import { connect } from 'react-redux';
import select from 'modules/main/state/selectors/LocationScreen';
import openMap from 'react-native-open-maps';
import * as mainActions from 'modules/main/state/dux';
import { FEED_LOCATIONS, PAGINATION, STORAGE_USER_SESSION } from 'constants';
import SplashScreen from 'react-native-splash-screen';
import { NavigationBar } from 'navigationbar-react-native';
import { EVENTDETAIL} from 'constants';
import { MAIN_ROUTES, CHALLENGE_ROUTES } from 'constants';
import * as sharedActions from 'shared/state/dux';
import CourtList from 'modules/main/screens/CourtList';
import Moment from 'react-moment';
import { isIPhone4,isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import moment from 'moment';
import Config from 'react-native-config'
import DeviceInfo from 'react-native-device-info';
import { FitnessCourtSerializer } from 'api/serializers';
import RNSettings from 'react-native-settings';
import Updater from 'update-react-native-app'
import AppUpdate from 'react-native-appupdate'
var _ = require('lodash');
import Orientation from 'react-native-orientation';



type $Props = {
    courts: Array<$FitnessCourt>,
    locationsEnabled: boolean,
};





const ComponentLeft = ({navigation}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-start', paddingLeft: 10,}} >
            <TouchableOpacity
                // onPress={() => { navigation.navigate(CHALLENGE_ROUTES.COURTLIST);}}
                onPress={() => navigation.navigate(CHALLENGE_ROUTES.COURTLIST)}
                style={ {justifyContent:'center', flexDirection: 'row'}}>
                <Image
                    source={require('assets/icons/Back_1.png')}
                    style={{ resizeMode: 'contain', padding: 10 , alignSelf: 'center', zIndex: 999 }}
                ></Image>
            </TouchableOpacity>
        </View>
    )
};

const ComponentCenter = ({state}) => {


    return(
        <View style={{ paddingTop:10}}>

            <Image source={require('assets/icons/Logo.png')}></Image>

        </View>
    );
};

const ComponentRight = ({navigation}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-end',}}>
            <TouchableOpacity onPress={() => navigation.navigate(CHALLENGE_ROUTES.COURTLIST)} style={{flexDirection:'row',flex:1,justifyContent:'space-around'}}>

                <Text style={{color:'#ffffff',fontSize:16,paddingTop:isIPhoneX()?45:30}}>All Locations</Text>
                <Image
                    source={require('assets/images/Allcourts.png')}
                    style={{ resizeMode: 'contain', padding: 10 , width:6,height:10,alignSelf: 'center', zIndex: 999, paddingRight:20 }}
                ></Image>
            </TouchableOpacity>
        </View>
    );
};




const LocationLeft = ({navigation}) => {

    console.log("log", navigation.getParam('userId', ''))
    return(
        <View style={{ flex: 1, alignItems: 'flex-start', paddingLeft: 10,}} >
            {/*<TouchableOpacity*/}
            {/*onPress={() => { navigation.navigate(MAIN_ROUTES.CHALLENGE);}}*/}
            {/*style={ {justifyContent:'center', flexDirection: 'row'}}>*/}
            {/*<Image*/}
            {/*source={require('assets/icons/Back_1.png')}*/}
            {/*style={{ resizeMode: 'contain', padding: 10 , alignSelf: 'center', zIndex: 999 }}*/}
            {/*/>*/}
            {/*</TouchableOpacity>*/}
        </View>
    )
};

const LocationCenter = ({state}) => {


    return(
        <View style={{ }}>

            <Image source={require('assets/icons/Logo.png')} />

        </View>
    );
};

const LocationRight = ({state}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-end', }}>

        </View>
    );
};


class Courts extends  React.Component<$Props> {
    /*
     * Removed for brevity
     */
    //

    constructor(){
        super();


        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: [],
            timeOutState: false,
            isScrollUp: false,
            isLoading: true,
            locationAvailable: false,
            isLoadingData: true,
            firstLaunch: null
        }
    }


    tagSymbols() {
        return new AppUpdate({
            iosAppId: '563421203',
            apkVersionUrl: 'https://github.com/version.json',
            needUpdateApp: (needUpdate) => {
                Alert.alert(
                    'Update Available',
                    'A new version of Fitness Court is available. Please update',
                    [
                        {text: 'Cancel', onPress: () => {

                            AsyncStorage.setItem('alreadyLaunched', JSON.stringify('yes'));

                            navigator.geolocation.getCurrentPosition((position) => {

                                this.fetchCourtData(position.coords.latitude, position.coords.longitude)

                            }, (error) => {
                                // alert(JSON.stringify(error))
                                // RNSettings.openSetting(RNSettings.ACTION_LOCATION_SOURCE_SETTINGS).
                                // then((result) => {
                                //     if (result === RNSettings.ENABLED) {
                                //         console.log('location is enabled')
                                //     }
                                // })


                                this.setState({

                                    locationAvailable: true,
                                    timeOutState: true
                                })
                            }, {
                                enableHighAccuracy: true,
                            });

                        }},
                        {text: 'Update', onPress: () => needUpdate(true)}
                    ]
                );
            },
            forceUpdateApp: () => {
                console.log("Force update will start")
            },
            notNeedUpdateApp: () => {
                console.log("App is up to date")



                navigator.geolocation.getCurrentPosition((position) => {

                    this.fetchCourtData(position.coords.latitude, position.coords.longitude)

                }, (error) => {
                    // alert(JSON.stringify(error))
                    // RNSettings.openSetting(RNSettings.ACTION_LOCATION_SOURCE_SETTINGS).
                    // then((result) => {
                    //     if (result === RNSettings.ENABLED) {
                    //         console.log('location is enabled')
                    //     }
                    // })
                    this.setState({

                        locationAvailable: true,
                        timeOutState: true
                    })
                }, {
                    enableHighAccuracy: true,
                });
            },
            downloadApkStart: () => { console.log("Start") },
            downloadApkProgress: (progress) => { console.log(`Downloading ${progress}%...`) },
            downloadApkEnd: () => { console.log("End") },
            onError: () => { console.log("downloadApkError") }
        });

    }

    goToMap = (courtObject) => {

        var endString = courtObject.street + "," + courtObject.city
        openMap({end: endString });

    }



    fetchData(court) {

        // const [ firstCourt, ...otherCourts ] = this.props.courts;



        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        var today = moment(new Date()).format("YYYY-MM-DD")

        if(court.location_id){

            var apiUrl = `${Config.SERVER_URL}/api/v1/service/0/allocations/?api_key=UkNNuT0bebf2bRoCMitx&limit=200&offset=0&location_id=${court.location_id}&start_date=${today}`

            console.log("apiURL", apiUrl)
            return fetch(apiUrl , {
                headers: new Headers({
                    'Content-Type': 'application/json'
                })
            })
                .then(response => response.json())
                .then(response => {
                    console.log("response of services", response.data, court)


                    this.getUserSession().then((item) => {


                        var userURL = `${Config.SERVER_URL}/api/v1/get_user/?api_key=UkNNuT0bebf2bRoCMitx&token=` + item
                        return fetch(userURL , {
                            headers: new Headers({
                                'Content-Type': 'application/json'
                            })
                        })
                            .then(user => user.json())
                            .then(user => {


                                console.log("user", user)


                                var postCountUrl = `${Config.SERVER_URL}/api/v1/near_by_users/`


                                return fetch(postCountUrl , {

                                    method: 'POST',
                                    headers: new Headers({
                                        'Content-Type': 'application/json'
                                    }),
                                    body: JSON.stringify({
                                        "fitness_court": court.id,
                                        "user_id": user.id,
                                        "api_key": 'UkNNuT0bebf2bRoCMitx'

                                    })
                                })
                                    .then(countResponse => countResponse.json())
                                    .then(countResponse => {


                                        console.log("inside the cousnt reposne", countResponse)
                                        var date = _.sortBy(date, function(o){return moment(o.startTime, "hmm")})

                                        var sortByTime = _.sortBy(response.data, function(o){return o.startDate})

                                        console.log("sortByTime", sortByTime)
                                        if(response.data.length > 0) {


                                            this.setState({
                                                dataSource: sortByTime,
                                                isLoading: false,
                                                isLoadingData: false
                                            })
                                        }
                                        else {

                                            this.setState({
                                                isLoadingData: false,
                                                isLoading: false,
                                            })
                                        }
                                    }).catch(error => {

                                        console.log("error", error)

                                    })

                            })




                    })

                });

        }
        else {



            this.getUserSession().then((item) => {


                var userURL = `${Config.SERVER_URL}/api/v1/get_user/?api_key=UkNNuT0bebf2bRoCMitx&token=` + item
                return fetch(userURL , {
                    headers: new Headers({
                        'Content-Type': 'application/json'
                    })
                })
                    .then(user => user.json())
                    .then(user => {


                        console.log("user", user)


                        var postCountUrl = `${Config.SERVER_URL}/api/v1/near_by_users/`


                        return fetch(postCountUrl , {

                            method: 'POST',
                            headers: new Headers({
                                'Content-Type': 'application/json'
                            }),
                            body: JSON.stringify({
                                "fitness_court": court.id,
                                "user_id": user.id,
                                "api_key": 'UkNNuT0bebf2bRoCMitx'

                            })
                        })
                            .then(countResponse => countResponse.json())
                            .then(countResponse => {



                                this.setState({
                                    isLoadingData: false,
                                    isLoading: false,
                                })
                                console.log("inside the cousnt reposne", countResponse)


                            }).catch(error => {

                                console.log("error", error)

                            })

                    })




            })


            //
            // this.setState({
            //     isLoadingData: false,
            //     isLoading: false,
            // })
        }


    }



    numberCalculation(number){

        return  moment(number, "hmm").format("h:mm A").replace(":00", "")
    }


    getUserSession = () => {


        const retrievedItem = AsyncStorage.getItem(STORAGE_USER_SESSION);
        return retrievedItem
    }

    _orientationDidChange = (orientation) => {

        if (orientation === 'LANDSCAPE') {

            Orientation.lockToPortrait();
        } else {

            // do something with portrait layout
        }
    }



    fetchCourtData = (latitude, longitude) => {

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        var uri = `${Config.SERVER_URL}/api/v1/fitness_court/?api_key=UkNNuT0bebf2bRoCMitx&challenge_id=1&latitude=${latitude}&longitude=${longitude}`
        //var uri = `${Config.SERVER_URL}/api/v1/fitness_court/?api_key=UkNNuT0bebf2bRoCMitx&challenge_id=1&latitude=37.806118&longitude=-122.435117`

        return fetch(uri, {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
            .then(response => response.json())
            .then(response => {


                if(response.length > 0) {




                    this.setState({

                        firstCourt: FitnessCourtSerializer(response),
                        timeOutState: true
                    })
                    this.fetchData(FitnessCourtSerializer(response))

                }
                else {

                }
            });
    }



    componentWillMount(){

        SplashScreen.hide();
        Orientation.lockToPortrait();
        this.props.dispatch(mainActions.fetchLearnFeed())
        //var appupdate = this.tagSymbols()
        // console.log("this is symbols", this.tagSymbols)
        // console.log("inside the function", appupdate, appUpdate)


        //
        // AsyncStorage.getItem("alreadyLaunched").then(value => {
        //
        //     console.log('alreadyLauncehd', value)
        //     if(value == null || value == JSON.stringify('no')){
        //
        //         appupdate.checkUpdate()
        //         AsyncStorage.setItem('alreadyLaunched', JSON.stringify('yes'));
        //
        //     }
        //     else{

                navigator.geolocation.getCurrentPosition((position) => {

                    this.fetchCourtData(position.coords.latitude, position.coords.longitude)

                }, (error) => {
                    // alert(JSON.stringify(error))
                    // RNSettings.openSetting(RNSettings.ACTION_LOCATION_SOURCE_SETTINGS).
                    // then((result) => {
                    //     if (result === RNSettings.ENABLED) {
                    //         console.log('location is enabled')
                    //     }
                    // })
                    this.setState({

                        locationAvailable: true,
                        timeOutState: true
                    })
                }, {
                    enableHighAccuracy: true,
                });
            // }})

        console.log("did mount", AppState.currentState)
        // AsyncStorage.setItem('alreadyLaunched', true); // No need to wait for `setItem` to finish, although you might want to handle errors
    }

    componentDidMount() {

        Orientation.addOrientationListener(this._orientationDidChange);
        //AppState.addEventListener('change', this.checkAppState);

    }

    componentWillUnmount() {
        //AppState.removeEventListener('change', this.checkAppState);
    }


    checkAppState = (state) =>  {

        if(AppState.currentState === 'active') {

            AsyncStorage.getItem("alreadyLaunched").then(value => {

                if(value == null || value == JSON.stringify('no')){


                    var appupdate = this.tagSymbols()
                    appupdate.checkUpdate()
                    AsyncStorage.setItem('alreadyLaunched',  JSON.stringify('yes')); // No need to wait for `setItem` to finish, although you might want to handle errors
                }
                else{

                    navigator.geolocation.getCurrentPosition((position) => {

                        this.fetchCourtData(position.coords.latitude, position.coords.longitude)

                    }, (error) => {
                        // alert(JSON.stringify(error))
                        // RNSettings.openSetting(RNSettings.ACTION_LOCATION_SOURCE_SETTINGS).
                        // then((result) => {
                        //     if (result === RNSettings.ENABLED) {
                        //         console.log('location is enabled')
                        //     }
                        // })
                        this.setState({

                            locationAvailable: true,
                            timeOutState: true
                        })
                    }, {
                        enableHighAccuracy: true,
                    });

                }})


        }
        else {

            AsyncStorage.setItem('alreadyLaunched',  JSON.stringify('no')); // No need to wait for `setItem` to finish, although you might want to handle errors

            console.log("else mount", AppState.currentState)


        }

    }



    render() {

        // const [ firstCourt, ...otherCourts ] = this.props.courts;

        var firstCourt = this.state.firstCourt

            if(!this.state.timeOutState) {

                return (


                    <View style={{height: '110%'}}>
                        {/*need to check this*/}
                        <ActivityIndicator style={{height: 100, backgroundColor: white, flex: 1, height: '100%'}} size="large" color="#127DAD" />

                    </View>
                )

            }

            else {


                return (


                    <View style={styles.container}>


                    {this.state.locationAvailable == false &&

                    <View style={styles.container}>


                        <NavigationBar
                            componentCenter   = { () =>  <ComponentCenter /> }
                            componentRight   = { () =>  <ComponentRight navigation={this.props.navigation}/> }
                            navigationBarStyle= {{ backgroundColor: '#0098D1;', height:isIPhoneX()?110:80,marginTop: Platform.OS === 'ios'? -20 : 0}}
                            statusBarStyle    = {{ barStyle: 'light-content', backgroundColor: '#000'}}
                        />

                        <ScrollView >

                            <View style={styles.courtmain}>
                                <View style={styles.courtinner}>
                                    {firstCourt.sponsorName &&
                                    <Text style={styles.sponsorsTittle}>{firstCourt.sponsorName}</Text>
                                    }
                                    <Text style={styles.courttitle} numberOfLines={1}>{`${firstCourt.name}`}</Text>
                                    <Text style={styles.courtsubtitle}>{`${firstCourt.city}`}</Text>
                                </View>
                                <View style={styles.courtbg}>
                                    <ImageBackground
                                        style={{width:131,height:132}}
                                        source={require('assets/images/Location.png')} >
                                    </ImageBackground>
                                </View>
                                <View>
                                    <View
                                        style={styles.courtbutton}
                                    ><Text style={styles.nearmebtn}>Near me</Text></View>

                                    {firstCourt.openingSoon == true &&

                                    <View
                                        style={styles.opensoonbutton}><Text style={styles.opensoonbtn}>Open Soon</Text></View>
                                    }

                                </View>
                                <View style={styles.courtdirectionbutton}>
                                    <TouchableOpacity
                                        onPress={() => this.goToMap(firstCourt)}>
                                        <ImageBackground
                                            style={{width:'100%', height:'100%'}}
                                            source={require('assets/images/direction-court.png')} >
                                        </ImageBackground>
                                    </TouchableOpacity>
                                </View>
                            </View>


                            {this.state.isLoadingData == true &&




                            <View style={styles.courtcontainer}>
                                <View style={styles.courtbody} >
                                    {/*need to check this*/}
                                    <ActivityIndicator style={{height: 100, backgroundColor: white, flex: 1, height: '100%'}} size="large" color="#127DAD" />

                                </View>
                            </View>
                            }

                            {this.state.dataSource && this.state.dataSource.length > 0  && this.state.isLoadingData == false &&

                            <View style={styles.courtcontainer}>
                                <View style={styles.courtbody} >
                                    {this.state.dataSource.map(value => (
                                        <View>


                                            <TouchableOpacity
                                                onPress={() => this.props.navigation.navigate(CHALLENGE_ROUTES.EVENTDETAIL, {court: firstCourt, event: value, isPastEvent: false})}>

                                                {value.resourceImageUrl &&

                                                <ImageBackground source={{uri: value.resourceImageUrl}} style={styles.courtCard}>
                                                    <View>
                                                        <ImageBackground
                                                            style={styles.courtCardbg}
                                                            source={require('assets/images/bgTransparent.png')} >
                                                            <Text style={styles.cardtitle} numberOfLines={1}>{value.serviceName.toUpperCase()}</Text>
                                                            <Text style={styles.cardtrainer}>Trainer: {value.resourceName}</Text>
                                                            <View style={styles.courtdatetime}>
                                                                <Text style={styles.courtdate}><Moment element={Text} format="ddd, MMM D, YYYY">{value.startDate}</Moment></Text>
                                                                <Text style={styles.courttime}>{this.numberCalculation(value.startTime.toString())} - {this.numberCalculation(value.endTime.toString())}</Text>
                                                            </View>
                                                        </ImageBackground>
                                                    </View>
                                                </ImageBackground>
                                                }

                                                {!value.resourceImageUrl &&


                                                <ImageBackground source={''} style={styles.courtCard}>

                                                    <Text style={styles.cardtitle} numberOfLines={1}>{value.serviceName.toUpperCase()}</Text>
                                                    <Text style={styles.cardtrainer}>Trainer: {value.resourceName}</Text>
                                                    <View style={styles.courtdatetime}>
                                                        <Text style={styles.courtdate}><Moment element={Text} format="ddd, MMM D, YYYY">{value.startDate}</Moment></Text>
                                                        <Text style={styles.courttime}>{this.numberCalculation(value.startTime.toString())} - {this.numberCalculation(value.endTime.toString())}</Text>
                                                    </View>
                                                </ImageBackground>
                                                }


                                            </TouchableOpacity>

                                        </View>
                                    ))}

                                </View>
                            </View>
                            }

                            { this.state.dataSource.length == 0 && this.state.isLoadingData == false &&


                            <View style={styles.nocourts}>
                                <ImageBackground style={styles.comingSoon}
                                                 source={require('assets/images/comingsoon.png')}>
                                    <View>
                                        <Text style={styles.classSoon}>No classes currently scheduled, check back for updates...</Text>
                                    </View>
                                </ImageBackground>
                            </View>
                            }

                        </ScrollView>

                    </View>

            }

                {this.state.locationAvailable == true &&

                <View style={styles.container}>



                    <NavigationBar
                        componentCenter   = { () =>  <LocationCenter /> }
                        componentRight   = { () =>  <LocationRight navigation={this.props.navigation}/> }
                        navigationBarStyle= {{ backgroundColor: navBarColor, height:isIPhoneX()?110:55,paddingTop:isIPhoneX()?40:20,marginTop: Platform.OS === 'ios'? -20 : 0}}
                        statusBarStyle    = {{ barStyle: 'light-content', backgroundColor: '#215e79'}}
                    />


                    <View style={{position: 'relative', height: '95%', display: 'flex', alignItems: 'center', justifyContent:'center', backgroundColor: white}}>

                        <Text style={{ margin: 0, fontSize: 18, fontWeight: 'bold'}}>Enable Locations</Text>
                        <Text style={{ margin: 0, marginTop: 5, color: '#9B9B9B', textAlign: 'center'}}>Please enable location permission to view courts & classes</Text>

                    </View>
                </View>
                }

                    </View>
                    

                );

            }


        }
}

export default connect(select)(Courts);
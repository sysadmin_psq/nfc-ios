// @flow
import React from 'react';
import { connect } from 'react-redux';
import { View, Text, ScrollView } from 'react-native';
import type { $FitnessCourt } from 'types/state/main';
import select from 'modules/main/state/selectors/LocationScreen';

import FirstCourtImage from 'shared/components/FirstCourtImage';
import FirstCourtPanel from 'shared/components/FirstCourtPanel';
import CallToAction from 'shared/components/CallToAction';
import Location from './Location';
import styles from './styles';

function LocationsScreen ({ courts, locationsEnabled }) {

    // console.log("insdi the ", courts)
    const [ firstCourt, ...otherCourts ] = courts;
    return (
        <View style={styles.container}>
            <ScrollView contentContainerStyle={styles.scrollView}>
                {firstCourt &&
                  (<CallToAction
                    display={firstCourt.callToActionDisplay}
                    link={firstCourt.callToActionLink}
                  />)
                }
                <FirstCourtImage court={firstCourt} />
                <FirstCourtPanel
                  court={firstCourt}
                  firstCourt={firstCourt.id}
                  locationsEnabled={locationsEnabled}
                  standalone={false}
                />
                <View style={styles.exploreC2A}>
                    <Text style={styles.explore}>Explore</Text>
                    <Text style={styles.boldLight}> {courts.length} Fitness Courts</Text>
                </View>
                <View style={styles.otherCourts}>
                    {otherCourts.map((court: $FitnessCourt, idx: number) =>
                        (<Location
                           key={`court__${court.id || 0}`}
                           court={court}
                           index={idx}
                        />)
                    )}
                </View>
            </ScrollView>
        </View>
      );
  }


export default connect(select)(LocationsScreen);

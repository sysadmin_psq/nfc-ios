import { StyleSheet } from 'react-native';
import { mediumBlue, white } from 'shared/styles/colors';

export default StyleSheet.create({
    container: {
        position: "relative",
        flex: 1,
        borderColor: mediumBlue,
        borderTopWidth: 4,
        backgroundColor: white
    },
    scrollContainer: {
      flex: 1
    }
});

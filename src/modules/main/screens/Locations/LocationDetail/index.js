// @flow
import React from 'react';
import { connect } from 'react-redux';
import type { $FitnessCourt } from 'types/state/main';
import { View, ScrollView } from 'react-native';
import FirstCourtImage from 'shared/components/FirstCourtImage';
import FirstCourtPanel from 'shared/components/FirstCourtPanel';
import CallToAction from 'shared/components/CallToAction';
import select from 'modules/main/state/selectors/LocationDetail';
import styles from './styles';

type $Props = {
  court: $FitnessCourt,
  firstCourt: ?number,
  locationsEnabled: boolean
};

function LocationDetail({ court, firstCourt, locationsEnabled }: $Props) {
  return (
      <View style={styles.container}>
          <ScrollView style={styles.scrollContainer}>
              <CallToAction display={court.callToActionDisplay} link={court.callToActionLink} />
              <FirstCourtImage court={court} />
              <FirstCourtPanel
                court={court}
                firstCourt={firstCourt}
                locationsEnabled={locationsEnabled}
                standalone
              />
          </ScrollView>
      </View>
  );
}

export default connect(select)(LocationDetail);

// @flow
import React from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity, Text } from 'react-native'
import type { $FitnessCourt } from 'types/state/main';
import City from 'shared/components/City';
import OpenSoon from 'shared/components/OpenSoon';
import SponsorName from 'shared/components/SponsorName';
import * as sharedActions from 'shared/state/dux';
import * as mainActions from 'modules/main/state/dux';
import { MAIN_ROUTES } from 'constants';
import styles from './styles';

type $Props = {
  court: $FitnessCourt,
  index: number,
  navigate: *,
  setCourtId: *
};

const getTheme = (idx: number): * => {
  switch (idx) {
    case 0:
      return [
        styles.backgroundTheme1,
        styles.textTheme1
      ];
    case 1:
      return [
        styles.backgroundTheme2,
        styles.textTheme2
      ];
    case 2:
    default:
      return [
        styles.backgroundTheme3,
        styles.textTheme3
      ];
  }
}

function Location ({ court, index, navigate, setCourtId }: $Props) {
  const [ containerTheme, textTheme ] = getTheme(index % 3);
  const containerStyle = court.sponsorName
    ? [containerTheme, styles.sponsoredCourt]
    : containerTheme;
  return (
      <TouchableOpacity
        key={`court__${court.id || 0}`}
        style={containerStyle}
        onPress={() => {
          setCourtId(court.id);
          navigate(MAIN_ROUTES.LOCATION_DETAIL);
        }}
      >
          <OpenSoon
            style={styles.openSoon}
            isVisible={court.openingSoon}
            index={index % 3}
          />
          <SponsorName name={court.sponsorName} index={index % 3} />
          {
            court.city && <City city={court.city} style={textTheme} />
          }
          <Text
            style={styles.courtName}
          >
              {court.name}
          </Text>
      </TouchableOpacity>
  );
}

export default connect(null, {
  navigate: sharedActions.navigate,
  setCourtId: mainActions.setCurrentCourt
})(Location);

import { StyleSheet } from 'react-native';
import { location } from '../styles';
import { mediumBlue, lighterBlue, greyBlue, blue, skyBlue } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';

const cityTextSize = {
  fontSize: 26,
  lineHeight: 28,
  letterSpacing: 1.3
};

export default StyleSheet.create({
  courtName: {
      ...ProximaNova.Bold,
      height: 18,
      fontSize: 14,
      lineHeight: 16,
      letterSpacing: 0.4,
      color: skyBlue
  },
  sponsoredCourt: {
      minHeight: 96
  },
  openSoon: {
    opacity: 0.8
  },
  backgroundTheme1: {
      ...location,
      backgroundColor: mediumBlue
  },
  textTheme1: {
      ...cityTextSize,
      color: lighterBlue,
  },
  backgroundTheme2: {
      ...location,
      backgroundColor: lighterBlue,
  },
  textTheme2: {
      ...cityTextSize,
      color: greyBlue
  },
  backgroundTheme3: {
      ...location,
      backgroundColor: blue
  },
  textTheme3: {
      ...cityTextSize,
      color: greyBlue
  },
});

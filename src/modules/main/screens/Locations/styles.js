import { StyleSheet } from "react-native";
import { white, borderGrey, greyBlue, mediumBlue } from "shared/styles/colors";
import { ProximaNova } from "shared/styles/fonts";

const exploreText = {
  fontSize: 16,
  lineHeight: 28,
  letterSpacing: 0.8,
};

export const header = {
  ...ProximaNova.Bold,
  fontSize: 14,
  lineHeight: 18,
  letterSpacing: 0.3,
  color: greyBlue
};

export const location = {
  justifyContent: "center",
  minHeight: 72,
  paddingHorizontal: 16,
  paddingVertical: 8,
  borderBottomWidth: 4,
  borderColor: borderGrey
};

export const topLineText = {
  lineHeight: 14,
  fontSize: 12,
  letterSpacing: 1.4,
  color: greyBlue
};


export default StyleSheet.create({
    container: {
        flex: 1,
        borderColor: mediumBlue,
        borderTopWidth: 4,
    },
    boldLight: {
        ...ProximaNova.CondensedBold,
        ...exploreText,
        color: white
    },
    explore: {
        ...ProximaNova.Condensed,
        ...exploreText,
        color: white
    },
    otherCourts: {
        flex: 1,
        alignItems: "stretch",
        paddingBottom: 120
    },
    exploreC2A: {
        height: 44,
        paddingHorizontal: 16,
        flexDirection: "row",
        alignItems: "center",
        flexWrap: "nowrap",
        backgroundColor: greyBlue,
        borderColor: borderGrey,
        borderBottomWidth: 4
    },
    scrollView: {
        paddingBottom: 132, // height of Location + Bottom Nav
    },
});

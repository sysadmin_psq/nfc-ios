
import React from 'react';
import { View, ListView, StyleSheet, Text, ScrollView, Dimensions, ActivityIndicator, Platform, TouchableOpacity, Image,Button,Alert,ImageBackground} from 'react-native';
import { black, borderGrey, white } from 'shared/styles/colors';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import DynamicButton from 'shared/components/DynamicButton';
import { ProximaNova } from 'shared/styles/fonts';
import styles from './styles';
import { connect } from 'react-redux';
import { isIPhone4,isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import select from 'modules/main/state/selectors/LocationScreen';
import openMap from 'react-native-open-maps';
import * as mainActions from 'modules/main/state/dux';
import { FEED_LOCATIONS, PAGINATION } from 'constants';
import SplashScreen from 'react-native-splash-screen';
import { NavigationBar } from 'navigationbar-react-native';
import { EVENTDETAIL} from 'constants';
import { MAIN_ROUTES, CHALLENGE_ROUTES } from 'constants';
import * as sharedActions from 'shared/state/dux';
import CourtList from 'modules/main/screens/CourtList';
import Moment from 'react-moment';
import moment from 'moment';
import Config from 'react-native-config';
var _ = require('lodash');


type $Props = {
    courts: Array<$FitnessCourt>,
    locationsEnabled: boolean,
};

//
//
const ComponentLeft = ({navigation}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-start', paddingLeft:5,}} >
            <TouchableOpacity
                onPress={() => navigation.goBack(null)}
                style={ {justifyContent:'flex-start', flexDirection: 'row',width:50,height:50}}>
                <Image
                    source={require('assets/icons/Back_1.png')}
                    style={{ resizeMode: 'contain', padding: 10 , alignSelf: 'center', zIndex: 999 }}
                />
            </TouchableOpacity>
        </View>
    );
};


const ComponentCenter = ({state}) => {


    return(
        <View style={{ paddingTop:isIPhoneX()?20:10}}>

            <Image source={require('assets/icons/Logo.png')}></Image>

        </View>
    );
};


const ComponentRight = ({navigation}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-end',}}>

        </View>
    );
};


class AllCourtsDetail extends  React.Component<$Props> {
    /*
     * Removed for brevity
     */
    //

    constructor(){
        super();


        this.state = {
            dataSource: [],
        }
    }

    goToMap = (courtObject) => {

        var endString = courtObject.street + "," + courtObject.city
        openMap({end: endString });

        //openMap({ provider: 'google', latitude: parseFloat(courtObject.latitude), longitude: parseFloat(courtObject.longitude) });

    }


    setTimePassed() {
        this.setState({timeOutState: true});
    }


    componentWillMount(){

        var values = _.findLast(this.props.navigation.state.routes, function(e){ return e.routeName == 'AllCourtsDetail' })

        this.setState({
            court: values.params.court,
            isLoading: true}, () => this.fetchData())

    }

    fetchData() {


        var today = moment(new Date()).format("YYYY-MM-DD")
        if(this.state.court.location_id){


            var apiUrl = `${Config.SERVER_URL}/api/v1/service/0/allocations/?api_key=UkNNuT0bebf2bRoCMitx&limit=200&offset=0&location_id=${this.state.court.location_id}&start_date=${today}`

            return fetch(apiUrl , {
                headers: new Headers({
                    'Content-Type': 'application/json'
                })
            })
                .then(response => response.json())
                .then(response => {

                    var date = _.sortBy(date, function(o){return moment(o.startTime, "hmm")})

                    var sortByTime = _.sortBy(response.data, function(o){return o.startDate})
                    if(response.data.length > 0) {


                        this.setState({
                            dataSource: sortByTime,
                            isLoading: false,
                            isLoadingData: false
                        })
                    }
                    else {

                        this.setState({
                            isLoadingData: false,
                            isLoading: false,
                        })
                    }
                });
        }
        else {

            this.setState({
                isLoading: false,
                isLoadingData: false
            })
        }

    }

    numberCalculation(number){

        return  moment(number, "hmm").format("h:mm A").replace(":00", "")
    }

    render() {

        return (


                <View style={styles.container}>

                    <NavigationBar
                        componentLeft     = { () =>  <ComponentLeft navigation={this.props.navigation}/>   }
                        componentCenter   = { () =>  <ComponentCenter /> }
                        componentRight   = { () =>  <ComponentRight state={this}/> }
                        navigationBarStyle= {{ backgroundColor: '#0098D1;',height:isIPhoneX()?110:80,marginTop: Platform.OS === 'ios'? -20 : 0}}
                        statusBarStyle    = {{ barStyle: 'light-content', backgroundColor: '#215e79'}}
                    />

                    <ScrollView>

                        <View style={styles.courtmain}>
                            <View style={styles.courtinner}>
                                {this.state.court.sponsorName &&
                                <Text style={styles.sponsorsTittle}>{this.state.court.sponsorName}</Text>
                                }
                                <Text style={styles.courttitle} numberOfLines={1}>{`${this.state.court.name}`}</Text>
                                <Text style={styles.courtsubtitle}>{`${this.state.court.city}`}</Text>
                            </View>
                            <View style={styles.courtbg}>
                                <ImageBackground
                                    source={require('assets/images/Location.png')} style={{width:131,height:132}}>
                                </ImageBackground>
                            </View>

                            <View style={styles.courtdirectionbutton}>
                                <TouchableOpacity
                                    onPress={() => this.goToMap(this.state.court)}
                                >
                                    <ImageBackground source={require('assets/images/direction-court.png')}  style={{width:'100%', height:'100%'}}>

                                </ImageBackground>
                                </TouchableOpacity>
                            </View>
                        </View>


                        {this.state.isLoading == true &&

                        <View style={styles.courtcontainer}>
                            <View style={styles.courtbody} >
                            {/*need to check this*/}
                            <ActivityIndicator style={{height: 100, backgroundColor: white, flex: 1, height: '100%'}} size="large" color="#127DAD" />

                            </View>
                        </View>
                        }

                        {this.state.dataSource.length > 0 && this.state.isLoading == false &&

                        <View style={styles.courtcontainer}>
                            <View style={styles.courtbody} >
                                {this.state.dataSource.map(value => (
                                    <View>


                                        <TouchableOpacity
                                            onPress={() => this.props.navigation.navigate(CHALLENGE_ROUTES.EVENTDETAIL, {court: this.state.court, event: value, isPastEvent: false})}>


                                            {value.resourceImageUrl &&


                                            <ImageBackground source={{uri: value.resourceImageUrl}} onError={this._onError} style={styles.courtCard}>
                                                 <View>
                                                        <ImageBackground
                                                            style={styles.courtCardbg}
                                                            source={require('assets/images/bgTransparent.png')} >
                                                <Text style={styles.cardtitle} numberOfLines={1}>{value.serviceName.toUpperCase()}</Text>
                                                <Text style={styles.cardtrainer}>Trainer: {value.resourceName}</Text>
                                                <View style={styles.courtdatetime}>
                                                    <Text style={styles.courtdate}><Moment element={Text} format="ddd, MMM D, YYYY">{value.startDate}</Moment></Text>
                                                    <Text style={styles.courttime}>{this.numberCalculation(value.startTime.toString())} - {this.numberCalculation(value.endTime.toString())}</Text>
                                                </View>
                                                </ImageBackground>
                                                </View>
                                            </ImageBackground>

                                            }

                                            {!value.resourceImageUrl &&
                                                <ImageBackground source={''} onError={this._onError} style={styles.courtCard}>

                                                    <Text style={styles.cardtitle} numberOfLines={1}>{value.serviceName.toUpperCase()}</Text>
                                                    <Text style={styles.cardtrainer}>Trainer: {value.resourceName}</Text>
                                                    <View style={styles.courtdatetime}>
                                                        <Text style={styles.courtdate}><Moment element={Text} format="ddd, MMM D, YYYY">{value.startDate}</Moment></Text>
                                                        <Text style={styles.courttime}>{this.numberCalculation(value.startTime.toString())} - {this.numberCalculation(value.endTime.toString())}</Text>
                                                    </View>
                                                </ImageBackground>


                                            }


                                        </TouchableOpacity>

                                    </View>
                                ))}

                            </View>
                        </View>
                        }

                        {this.state.dataSource.length == 0 && this.state.isLoading == false &&


                        <View style={styles.nocourts}>
                            <ImageBackground style={styles.comingSoon}
                                             source={require('assets/images/comingsoon.png')}>
                                <View>
                                    <Text style={styles.classSoon}>No classes currently scheduled, check back for updates</Text>
                                </View>
                            </ImageBackground>
                        </View>
                        }
                    </ScrollView>

                </View>

            );


    }
}

export default connect(select)(AllCourtsDetail);
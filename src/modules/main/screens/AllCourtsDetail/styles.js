import { black, borderGrey, white } from 'shared/styles/colors';
import { Dimensions, StyleSheet, Platform } from 'react-native';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from 'constants';
import { ProximaNova } from 'shared/styles/fonts';
import { isIPhone4, isIPhone5, isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import { red } from 'ansi-colors';
import { single } from 'rxjs/operator/single';
// import { relative } from 'path';
// import leftPad = require('left-pad');

const { height } = Dimensions.get('window');

export default StyleSheet.create({


    scrollContainer: {
        backgroundColor: white,
        // height: isMediumScreen() ? SCREEN_HEIGHT - '10%' : isIPhoneX()? SCREEN_HEIGHT - 160 : isPixelScreen()? SCREEN_HEIGHT - 60 : SCREEN_HEIGHT - '30%',
        height:'100%',
    },

    container: {

        flex: 1,
    },

    viewContainer: {

        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff'
    },

    listContainer: {

        backgroundColor: white,
        width: '100%',
    },

    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        // top: '3%',
        bottom: 0,
        opacity: 0.5,
        backgroundColor: white,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },


    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#E4E8EC',
        marginLeft: 20,
        marginRight: 20,

    },
    courtmain: {
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor:'rgb(0,152,209)',
        height:140,
        marginBottom:20,
        position:"relative",
    },
    courtinner: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    sponsorsTittle:{
        fontSize:12,
        position:'absolute',
        left:'-45%',
        top:15,
        color:'#ffffff'
    },
    courttitle: {
        color: 'white',
        fontSize: 32,
        ...ProximaNova.CondensedBold,
        textAlign:'left',
        position:"absolute",
        left:'-45%',
        top:30,
    },
    courtsubtitle: {
        fontSize:16,
        color:'white',
        textAlign:'left',
        position:"absolute",
        left:'-45%',
        top:70,
    },
    courtbutton: {
        width: 81,
        borderWidth:1,
        borderRadius:20,
        height:21,
        fontWeight:'normal',
        fontSize:14,
        ...ProximaNova.Regular,
        borderColor:'#ffffff',
        color:'#ffffff',
        position:"absolute",
        left:'-45%',
        bottom:'15%'
    },
    nearmebtn: {
        color: 'white',
        fontSize:14,
        fontWeight:'normal',
        paddingLeft:10,
    },
    courtdirectionbutton: {
        width: 141,
        textAlign:'left',
        height:40,
        fontWeight:'normal',
        fontSize:14,
        ...ProximaNova.Regular,
        borderColor:'#ffffff',
        marginBottom:20,
        color:'#ffffff',
        position:"absolute",
        zIndex:999,
        top:'100%',
        left: isIPhone5()? '50%' : '60%',
        overflow:'visible'
    },
    directionbtn:{
        color: '#0098D1',
        fontSize:16,
        fontWeight:'normal',
        paddingLeft:10,
        zIndex:999,
    },
    courtcontainer: {
        backgroundColor:'white',
        zIndex:-1,
        minHeight: '100%'

    },
    courtbg:{
        position:'absolute',
        right:'0%',
        bottom:'0%'
    },
    courtbody: {
        backgroundColor:'white',
        marginTop:40,
    },
    courtCard: {
        width:'90%',
        height:170,
        backgroundColor:'#0098D1',
        marginBottom:20,
        marginRight:'5%',
        marginLeft:'5%',
        // paddingTop:90,
        // paddingBottom:12,
        // paddingLeft:15,
        // paddingRight:15,
    },
    courtCardbg:{
        width:'100%',
        height:170,
        paddingTop:90,
        paddingBottom:12,
        paddingLeft:15,
        paddingRight:15,
    },
    cardtitle: {
        fontSize:26,
        color:'white',
        ...ProximaNova.CondensedBold,
        marginBottom:5,
    },
    cardtrainer: {
        fontSize:14,
        color:'#FFC44E',
        ...ProximaNova.CondensedSemibold,
        marginBottom:5,
    },

    courtdatetime: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    courtdate: {
        fontSize:12,
        color:'#ffffff',
        width:isIPhone5()?'69%':'70%',
        ...ProximaNova.CondensedSemibold,
    },
    courttime: {
        fontSize:11,
        color:'#ffffff',
        width:isIPhone5()?'33%':'30%',
        ...ProximaNova.CondensedSemibold,
        textAlign: 'right'
    },
    courtbottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 36
    },


    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: '5%',
        bottom: 0,
        opacity: 0.5,
        backgroundColor: white,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },

    nocourts:{
        zIndex:-1,
        minHeight:'100%',
    },

    classSoon:{
        color:'#ffffff',
        fontSize:20,
        paddingTop:'50%',
        textAlign:'center'
    },
    comingSoon:{
        height:'100%',
        width:'100%'
    },
});
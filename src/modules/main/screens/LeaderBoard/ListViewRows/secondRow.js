import React from 'react';
import { View, Text, StyleSheet, Image, ImageBackground } from 'react-native';
import { ProximaNova } from 'shared/styles/fonts';
import { black, borderGrey, white } from 'shared/styles/colors';
import MaterialInitials from 'react-native-material-initials/native';
import { isIPhone4, isMediumScreen } from 'shared/utils';

const styles = StyleSheet.create({
    rowContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        height: 67,
    },
    text: {
        marginLeft: 20,
        fontSize: 12,
        ...ProximaNova.Regular,
        color: '#9B9B9B',
        width: 15
    },
    userPhoto: {
        width: 46,
        borderRadius: 23,
        marginLeft: 8,
        height: isMediumScreen()? 46 : 46,
        borderRadius: isMediumScreen()? 23 : 23,
    },

    myviewContainer: {

        flex: 1,
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        height: 90,
    },

    infoView: {

        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        paddingTop: 12,
        width: isMediumScreen()? '48%' : '52%',
        marginLeft: 12,
    },

    nameText: {

        ...ProximaNova.Semibold,
        color: '#575757',
        fontSize: 16,
        width: '90%'
    },
    locationText: {

        color: '#575757',
        fontSize: 10,
        ...ProximaNova.Regular
    },

    championView: {

        display: 'flex',
        flexDirection: 'row'
    },

    championVideoIcon: {

        marginLeft: 8

    },

    championText: {

        color: white,
        fontSize: 12,
        marginTop: 2,
        ...ProximaNova.SemiboldItalic,
    },

    medalView: {

        width: '20%',
        height: '100%',
    },
    medal: {

        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        height: '100%',
        marginTop: 5,
        height: 56,
        width: 44,
        alignItems: 'center',
    },

    medalTitleText: {

        ...ProximaNova.CondensedBold,
        fontSize: 20,
        marginTop: 5,
        paddingBottom: 0,
        height: 18,



    },

    medalPointsFont: {

        fontSize: 8.93,
        color: black,
        paddingBottom: 10,
        ...ProximaNova.Regular
    }

});

const SecondRow = (props) => {


    var object = {...props.data}
    var userObject = {...object.user}
    var court = {...object.fitness_court}
    var picture = {...object.picture}


    return (
        <View style={styles.rowContainer}>


                <Text style={styles.text}>{parseInt(parseInt(props.index) + parseInt(1))}</Text>


            <View style={styles.userPhoto}>
                <MaterialInitials
                    style={{alignItems: 'center'}}
                    backgroundColor={'#f7f7f7'}
                    color={'#0098D1'}
                    size={45}
                    text={`${userObject.fullname}`}
                    single={false}
                />
            </View>
                <View style={styles.infoView}>

                    <Text numberOfLines={1} style={styles.nameText}>{`${userObject.fullname}`}</Text>
                    <Text numberOfLines={1} style={styles.locationText}>{`${court.name}`}, {`${court.city_name}`}</Text>
                    <View style={styles.championView}>

                        <Text style={styles.championText}>Fitness Court Champion</Text>

                    </View>
                </View>

                <View style={styles.medalView}>

                    <ImageBackground
                        style={styles.medal}
                        source={require('assets/icons/Silver.png')}
                        resizeMode="contain">

                        <Text style={styles.medalTitleText}>{object.score}</Text>
                        <Text style={styles.medalPointsFont}>points</Text>
                    </ImageBackground>

                </View>

        </View>
    )
};

export default SecondRow;



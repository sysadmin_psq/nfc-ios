
import React from 'react';
import { connect } from 'react-redux';
import { View, ListView, StyleSheet, Text, ScrollView, Dimensions,TouchableOpacity, Image, Platform, TouchableWithoutFeedback, ImageBackground, AsyncStorage } from 'react-native';
import { black, borderGrey, white } from 'shared/styles/colors';
import Row from './row';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { ProximaNova } from 'shared/styles/fonts';
import QuickListView from './quickChallenge.js'
import CompetitionChallenge from './competitionChallenge.js'
import EnduranceChallenge from './enduranceChallenge.js'
import { NavigationBar } from 'navigationbar-react-native';
import {MAIN_ROUTES, CHALLENGE_ROUTES} from "../../../../constants/index";
import { StackNavigator } from 'react-navigation';
import Modal from "react-native-modal";
import FilterModal from './FilterModal';
import ModalStyle from './FilterModal/styles'
import select from 'modules/main/state/selectors/LocationScreen';
import MaterialInitials from 'react-native-material-initials/native';
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import { AUTH_ROUTES, STORAGE_USER_SESSION } from 'constants';



type $Props = {
    courts: Array<$FitnessCourt>,
    locationsEnabled: boolean,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
        backgroundColor: white,
    },

    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E',
    },

    scene: {
        flex: 1,
    },
    tabBarHeight: {

        height: '7%',
        backgroundColor:'#127DAD',
        paddingTop: 5,
    },

    tabBarTextTitle: {

        color: white,
        ...ProximaNova.Semibold,
        fontSize: 12,
    }
});



const userStyles = StyleSheet.create({
    wrapper:{
        display:'flex',
        width:'100%',
        paddingTop:9,
        paddingBottom:9,
        flexDirection: 'row',
        paddingLeft:'5%',
        paddingRight:'5%',
    },
    text:{
        color:'#ffffff',
        display:'flex',
        flexDirection:'row',
        width:15,
        paddingTop:10,
        paddingBottom:10,
        paddingRight:32,
    },
    userPhoto: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 23,
        height: 35,
        paddingRight:16,
        borderRadius: isMediumScreen()? 23 : 23,
    },
    infoView:{
        display: 'flex',
        flexDirection: 'row',
        width:'58%',
        color:'#ffffff',

    },
    medalView:{
        display:'flex',
        flexDirection:'column',
        width: 22,
        height: '100%',
        flex:1,
        paddingLeft:'1%',
        alignSelf: 'center'
    },
    championText: {
        color: '#ffffff',
        paddingTop: 10,
        paddingBottom: 10,
    },
    medalTitleText:{
        color:'#ffffff',
        fontWeight:'bold',
        fontSize:18,
        alignSelf: 'center'
    },
    medalPointsFont:{
        color:'#ffffff',
        fontSize:8,
        alignSelf: 'center'

    }

});



const FirstRoute = () => (


    <QuickListView />
);
const SecondRoute = () => (

    <EnduranceChallenge/>
);
const ThirdRoute = () => (

    <CompetitionChallenge/>
);

const ComponentLeft = ({navigation}) => {

    console.log("log", navigation.getParam('userId', ''))
    return(
        <View style={{ flex: 1, alignItems: 'flex-start',paddingLeft:5, }} >
            <TouchableOpacity
                onPress={() => { navigation.navigate(MAIN_ROUTES.CHALLENGE);}}
                style={ {justifyContent:'flex-start', flexDirection: 'row',width:50,height:50}}>
                <Image
                    source={require('assets/icons/Back_1.png')}
                    style={{ resizeMode: 'contain', padding: 10 , alignSelf: 'center', zIndex: 999 }}
                />
            </TouchableOpacity>
        </View>
    );
};

const ComponentCenter = () => {
    return(
        <View style={{ flex: 1, }}>

            <Text style={{ color: 'white', ...ProximaNova.Regular, fontSize: 18}}>Leaderboard</Text>
        </View>
    );
};

const ComponentRight = ({state}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-end', }}>
            <TouchableOpacity onPress={() => { state.setState({modalVisible: true})}}>


                {state.state.isFiltered == false &&

                <Image
                    source={require('assets/icons/Filter.png')}
                    style={{ resizeMode: 'contain', padding: 10 ,marginRight:16, alignSelf: 'center', zIndex: 999 }}
                />
                }

                {state.state.isFiltered == true &&

                <Image
                    source={require('assets/icons/Filtered.png')}
                    style={{ resizeMode: 'contain', padding: 10 ,marginRight:16, alignSelf: 'center', zIndex: 999 }}
                />
                }

            </TouchableOpacity>
        </View>
    );
};


class TabViewExample extends React.Component<$Props> {



    constructor(props){
        super(props);

        this.showDetails = this.showDetails.bind(this);// you should bind this to the method that call the props

    }

    getUserSession = () => {

        console.log("session stprage leaderboard", STORAGE_USER_SESSION)

        const retrievedItem = AsyncStorage.getItem(STORAGE_USER_SESSION);


        console.log("retrievedItem", retrievedItem)
        return retrievedItem
    }


    componentWillMount() {


        const [ firstCourt, ...otherCourts ] = this.props.courts;

        console.log(...otherCourts)

        this.setState({courtName: firstCourt})

        this.getUserSession().then((item) => {

            var apiUrl = 'http://api.fitnesscourt.com/api/v1/get_user/?api_key=UkNNuT0bebf2bRoCMitx&token=' + item
            return fetch(apiUrl , {
                headers: new Headers({
                    'Content-Type': 'application/json'
                })
            })
                .then(response => response.json())
                .then(response => {

                    this.setState({
                        userId: response.id
                    })

                });
        })

    }

    componentDidMount() {



    }



    showDetails(){
        this.props.navigation.navigate(MAIN_ROUTES.CHALLENGE);
    }

    _toggleModal(visible) {


        this.setState({modalVisible: visible})
    }



    setFilterValues = (courtname, filterWeek, courtMe) => {

        this.setState({

            courtName : courtname,
            filterThisWeek: filterWeek,
            courtNearMe: courtMe,
            modalVisible: false,
            isRefresh: true,
            isFiltered: true
        })

    }


    unsetFilterValues = (courtname, filterWeek, courtMe) => {


        this.setState({

            courtName : courtname,
            filterThisWeek: filterWeek,
            courtNearMe: courtMe,
            modalVisible: false,
            isRefresh: true,
            isFiltered: false
        })
    }


    setUserScore = (userScore, name, image) => {

        this.setState({

            userScore: userScore,
            name: name,
            image: image
        })

    }

    _renderLabel = ({ route }) => (
        <Text style={styles.tabBarTextTitle}>{route.title}</Text>
    );
    _renderTabBar = props => {

        return (

            <TabBar
                {...props}
                style={styles.tabBarHeight}
                tabStyle={styles.tab}
                renderLabel={this._renderLabel}
                indicatorStyle={{backgroundColor: white, alignItems: 'center'}}
                // renderIndicator={this._indicator}
            />
        );
    };


    renderScene = ({ route }) => {
        switch (route.key) {
            case 'first':
                return (
                    <QuickListView courtName={this.state.courtName}
                                   filterThisWeek={this.state.filterThisWeek}
                                   courtNearMe={this.state.courtNearMe}
                                   isRefresh={this.state.isRefresh}
                    />
                )
            case 'second':
                return (
                    <EnduranceChallenge
                        courtName={this.state.courtName}
                        filterThisWeek={this.state.filterThisWeek}
                        courtNearMe={this.state.courtNearMe}
                        isRefresh={this.state.isRefresh}
                    />
                )
            case 'third':
                return (
                    <CompetitionChallenge
                        courtName={this.state.courtName}
                        filterThisWeek={this.state.filterThisWeek}
                        courtNearMe={this.state.courtNearMe}
                        isRefresh={this.state.isRefresh}
                    />
                )
            default:
                return null;
        }
    }


    state = {
        index: 0,
        routes: [
            { key: 'first', title: 'QUICK' },
            { key: 'second', title: 'ENDURANCE' },
            { key: 'third', title: 'COMPETITION'}
        ],
        modalVisible: false,
        courtName: '',
        filterThisWeek: false,
        courtNearMe: false,
        isRefresh: false,
        isFiltered: false,

    };



    render() {

        return (

            <View style={{height:isIPhoneX()?'110%':'100%',}}>

                <NavigationBar
                    componentLeft     = { () =>  <ComponentLeft navigation={this.props.navigation}/>   }
                    componentCenter   = { () =>  <ComponentCenter /> }
                    componentRight   = { () =>  <ComponentRight state={this}/> }
                    navigationBarStyle= {{ backgroundColor: '#0098D1;', height:isIPhoneX()?80:55,paddingTop:isIPhoneX()?40:20,marginTop: Platform.OS === 'ios'? -20 : 0}}
                    statusBarStyle    = {{ barStyle: 'light-content', backgroundColor: '#215e79'}}
                />

                <TabView
                    navigationState={this.state}
                    renderScene={this.renderScene}
                    renderTabBar={this._renderTabBar}
                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{ width: Dimensions.get('window').width }}
                />

                {/*footer code here*/}



                <View style={{width: '100%', margin: 0}}>
                    <Modal style={ModalStyle.modalContainer} visible={this.state.modalVisible}>

                        <TouchableOpacity style={{width: '100%', height: '100%'}} activeOpacity={1}
                                          onPressOut={() => {this._toggleModal(false)}}>


                            <FilterModal
                                courtName={this.state.courtName}
                                filterThisWeek={this.state.filterThisWeek}
                                courtNearMe={this.state.courtNearMe}
                                setFilterValues={this.setFilterValues}
                                unsetFilterValues={this.unsetFilterValues}/>

                        </TouchableOpacity>

                    </Modal>
                </View>

            </View>

        );
    }
}

export default connect(select)(TabViewExample)

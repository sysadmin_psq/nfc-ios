
import React from 'react';
import { View, ListView, StyleSheet, Text, ScrollView, Dimensions, ActivityIndicator, AsyncStorage, Image } from 'react-native';
import { black, borderGrey, white } from 'shared/styles/colors';
import Row from './row';
import FirstRow from './ListViewRows/firstRow'
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { ProximaNova } from 'shared/styles/fonts';
import SecondRow from "./ListViewRows/secondRow";
import ThirdRow from "./ListViewRows/thirdRow";
import { AUTH_ROUTES, STORAGE_USER_SESSION } from 'constants';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: white,
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center'
    },


    listContainer: {

        backgroundColor: white,
        width: '100%'
    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#E4E8EC',
        marginLeft: 20,
        marginRight: 35,

    },

    scene: {
        flex: 1,
    },
    tabBarHeight: {

        height: '7%',
        backgroundColor:'#127DAD',
        paddingTop: 5,
    },

    tabBarTextTitle: {

        color: white,
        ...ProximaNova.Semibold,
        fontSize: 12,
    },

    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: '5%',
        bottom: 0,
        opacity: 0.5,
        backgroundColor: white,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    }

});


class QuickChallenge extends React.Component {


    /*
     * Removed for brevity
     */
    //

    constructor(props){
        super(props);


        AsyncStorage.getItem(STORAGE_USER_SESSION).then((item) => {console.log(item)})
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {

            dataSource:  ds.cloneWithRows([]),
            isLoading: true,
            courtNearMe: props.courtNearMe,
            filterThisWeek: props.filterThisWeek,
            lengthZero: false
        }

    }


    componentWillReceiveProps(props) {



        if(props.isRefresh == true) {


            const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            this.setState({
                dataSource:  ds.cloneWithRows([]),
                filterThisWeek: props.filterThisWeek,
                courtNearMe: props.courtNearMe,
                isLoading: true,
                lengthZero: false
            }, function(){

                //console.log("listView", this.state.filterThisWeek, this.state.courtNearMe)
                this.fetchData()


            })


        }

    }
    componentWillMount(){


        this.fetchData()
    }


    getUserSession = () => {


        const retrievedItem = AsyncStorage.getItem(STORAGE_USER_SESSION);

        return retrievedItem
    }


    fetchData = () => {



        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        var uri = 'https://api.fitnesscourt.com/api/v1/leader_board/?api_key=UkNNuT0bebf2bRoCMitx&challenge_id=1'


        if(this.props.courtNearMe == true) {


            uri = uri + `&court=${encodeURIComponent(this.props.courtName.id)}`

        }


        if(this.state.filterThisWeek == true) {

            uri = uri + `&for_week=${encodeURIComponent(this.state.filterThisWeek)}`

        }


        return fetch(uri, {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
            .then(response => response.json())
            .then(response => {
                // console.log("inside hte valie", uri)


                if(response.length > 0) {


                    this.setState({
                        dataSource: ds.cloneWithRows(response),
                        isLoading: false,
                        isRefresh: false,
                        lengthZero: false
                    })
                }
                else {

                    this.setState({
                        dataSource: ds.cloneWithRows(response),
                        isLoading: false,
                        isRefresh: false,
                        lengthZero: true
                    })
                }
            });
    }


    render() {


        return (



            <View style={styles.container}>

                {this.state.isLoading && this.state.lengthZero == false &&


                <ActivityIndicator style={styles.loading} size="large" color="#127DAD" />
                }


                {this.state.lengthZero == true && this.state.isLoading == false &&


                <View style={styles.container}>

                    <Image
                        source={require('assets/images/No_Entries.png')}>
                    </Image>

                </View>
                }

                {this.state.lengthZero == false  &&  this.state.isLoading == false &&


                <ListView
                    style={styles.listContainer}
                    dataSource={this.state.dataSource}
                    renderRow={(data, i, j) => {

                        let props = {

                            data: data,
                            index: j,

                        }
                        if(j == 0){
                            return (

                                <View>
                                    <FirstRow {...props}/>
                                </View>

                            )
                        }
                        else if(j == 1){

                            return(

                                <SecondRow {...props}/>
                            )
                        }
                        else if(j == 2) {


                            return(

                                <ThirdRow {...props}/>
                            )
                        }
                        else {

                            return(

                                <Row {...props}/>
                            )
                        }
                    }}
                    renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
                />
                }


            </View>

        );

    }
}

export default QuickChallenge;
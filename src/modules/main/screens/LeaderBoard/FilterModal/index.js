
import React from 'react';
import { View, ListView, StyleSheet, Text, ScrollView, Dimensions,TouchableOpacity, Image, Platform, TouchableWithoutFeedback} from 'react-native';
import { black, borderGrey, white } from 'shared/styles/colors';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { ProximaNova } from 'shared/styles/fonts';
import { NavigationBar } from 'navigationbar-react-native';
import { StackNavigator } from 'react-navigation';
import styles from './styles';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen, isNotchDevice} from 'shared/utils';


const ComponentLeft = ({state}) => {

    var unsetFilterValues = state.props.unsetFilterValues


    return(
        <View style={{ flex: 1, alignItems: 'flex-start', paddingLeft: 16,}} >
            <TouchableOpacity
                onPress={() => { unsetFilterValues(state.state.courtName, false, false)}}
                style={ {justifyContent:'center', flexDirection: 'row'}}>
                            <Text style={{ color: 'white', ...ProximaNova.Regular, fontSize: 18}}>Reset</Text>
            </TouchableOpacity>
        </View>
    );
};

const ComponentCenter = () => {


    return(
        <View style={{ flex: 1, }}>

            <Text style={{ color: 'white', ...ProximaNova.Regular, fontSize: 18,textAlign: 'center'}}>Filter</Text>

        </View>
    );
};

const ComponentRight = ({state}) => {


    var setFilterValues = state.props.setFilterValues

    return(
        <View style={{ flex: 1, alignItems: 'flex-end', paddingRight: 16,}}>
            <TouchableOpacity onPress={() => {setFilterValues(state.state.courtName, state.state.filterThisWeek, state.state.courtNearMe)}}>
                <Text style={{ color: 'white', ...ProximaNova.Regular, fontSize: 18 }}> Apply </Text>
            </TouchableOpacity>
        </View>
    );
};



class FilterModalView extends React.Component {



    constructor(props){
        super(props);

        this.state = {
            courtName: props.courtName,
            filterThisWeek: props.filterThisWeek,
            courtNearMe: props.courtNearMe,
        }


    }


    componentWillReceiveProps(props) {


        this.setState({

            filterThisWeek: props.filterThisWeek,
            courtNearMe: props.courtNearMe,

        })

    }


    courtApplyFunction(value) {

        this.setState({ courtNearMe: value })
    }

    filterWeekApplyFunction(value) {


        this.setState({ filterThisWeek: value })
    }


    render() {


        return(


            <TouchableWithoutFeedback>

            <View style={{marginTop: isNotchDevice()? 25 : 0}}>

                <NavigationBar
                    componentLeft     = { () =>  <ComponentLeft state={this}/>   }
                    componentCenter   = { () =>  <ComponentCenter state={this}/> }
                    componentRight    = { () => <ComponentRight state={this}/> }
                    navigationBarStyle= {{ opacity:1,height:50,backgroundColor:'#0098D1' }}
                    statusBarStyle    = {{ barStyle: 'light-content',opacity:1 }}
                />

                <View style={styles.filter}>


                     <View style={styles.duration}>
                            <Text style={styles.title}>{'Duration'.toUpperCase()}</Text>
                        <View style={styles.filterbtnwrapper}>


                            <TouchableOpacity style={[this.state.filterThisWeek == false ? styles.filterbtnactive: styles.filterbtn]}
                                              onPress={() => {this.filterWeekApplyFunction(false)}}>
                                <Text style={[this.state.filterThisWeek == false ? styles.activeTextColor: styles.normalTextColor]}>All time</Text>
                            </TouchableOpacity>
                         <TouchableOpacity style={[this.state.filterThisWeek == true ? styles.filterbtnactive : styles.filterbtn]}
                                           onPress={() => {this.filterWeekApplyFunction(true)}}>
                             <Text style={[this.state.filterThisWeek == true ? styles.activeTextColor : styles.normalTextColor]}>This week</Text>
                         </TouchableOpacity>

                        </View>
                     </View>

                    <View style={styles.fitnesscourt}>
                        <Text style={styles.title}>{'Fitness Court'.toUpperCase()}</Text>
                        <View style={styles.filterbtnwrapper}>

                            <TouchableOpacity style={[this.state.courtNearMe == false ? styles.filterbtnactive: styles.filterbtn]}
                                              onPress={() => {this.courtApplyFunction(false)}}>
                                <Text style={[this.state.courtNearMe == false ? styles.activeTextColor: styles.normalTextColor]}>All courts</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={[this.state.courtNearMe == true ? styles.filterbtnactive : styles.filterbtn]}
                                              onPress={() => {this.courtApplyFunction(true)}}>
                                <Text style={[this.state.courtNearMe == true ? styles.activeTextColor : styles.normalTextColor]}>Near me</Text>
                                <Text style={[this.state.courtNearMe == true ? styles.activeCurrentlocation : styles.normalCurrentLocation]}>({`${this.state.courtName.name}`})</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>

            </View>

        </TouchableWithoutFeedback>


        );

    }

}


export default FilterModalView
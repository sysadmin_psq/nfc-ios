import { black, borderGrey, white } from 'shared/styles/colors';
import { Dimensions, StyleSheet, Platform } from 'react-native';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from 'constants';
import { ProximaNova } from 'shared/styles/fonts';
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';

const { height } = Dimensions.get('window');

export default StyleSheet.create({


    modalContainer: {

        width: '100%',
        height: 370,
        margin: 0,
        justifyContent: "flex-start",
        backgroundColor: 'rgba(52, 52, 52, 0.8)'
    },

    filter: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'stretch', 
        backgroundColor: white,
        width:"100%",
        paddingLeft:'10%',
        paddingRight:'10%',
        justifyContent: "flex-start",
        margin: 0
    },
    duration: {
        width: '100%',
        paddingTop:24,
    },
    fitnesscourt:{
        width: '100%',
        paddingBottom:53,
        paddingTop:30,
    },
    filterbtnwrapper: {  
        display:'flex',
        flexDirection: 'row',
        borderWidth: 1,
        width:'100%',
        borderRadius:4,
        borderColor: "#0098D1",
    },
    filterbtn: {
        width:'50%',
        paddingTop:23,
        paddingBottom:23,
        fontSize:14,
        height:70,
        color:'#3B3B3B',
        alignItems:'center',
        textAlign: 'center'
    },

    activeTextColor:{
        color:'#ffffff',
        width: '100%',
        textAlign: 'center'
    },

    normalTextColor: {

        color:'#3B3B3B',
        width: '100%',
        textAlign: 'center'
    },

    filterbtnactive:{
        backgroundColor:'#00AEEF',
        color:'#ffffff',
        paddingTop:23,
        paddingBottom:23,
        textAlign: 'center',
        width:'50%',
        // paddingLeft:'8%',
        // paddingRight:'8%',
        height:70,
        fontSize:14,
    },
    title : {
        fontSize: 14,
        color:'#575757',
        paddingBottom:12,
    },
    activeCurrentlocation : {
        color:'#ffffff',
        fontSize:12,
        width: '100%',
        textAlign: 'center'
    },

    normalCurrentLocation : {

        color:'#00AEEF',
        fontSize:12,
        width: '100%',
        textAlign: 'center'


    }

});
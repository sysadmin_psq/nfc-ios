
import React from 'react';
import { View, ListView, StyleSheet, Text, ScrollView, Dimensions, ActivityIndicator, Platform, TouchableOpacity, Image } from 'react-native';
import { black, borderGrey, white } from 'shared/styles/colors';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { ProximaNova } from 'shared/styles/fonts';
import { NavigationBar } from 'navigationbar-react-native';
import styles from './styles';
import EntriesRow from "modules/main/screens/MyEntries/myentryRows";
import { isIPhone4,isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import {MAIN_ROUTES, CHALLENGE_ROUTES} from "../../../../constants/index";
import CourtRow from './CourtListRow/CourtRow';
import OpenSoon from './CourtListRow/OpenSoon';
import select from 'modules/main/state/selectors/LocationScreen';
import { connect } from 'react-redux';
import Orientation from 'react-native-orientation';



type $Props = {
    courts: Array<$FitnessCourt>,
    locationsEnabled: boolean,
};

//
const ComponentLeft = ({navigation}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-start',paddingLeft:5}} >
            <TouchableOpacity
                onPress={() => { navigation.navigate(MAIN_ROUTES.COURTS);}}
                style={ {justifyContent:'flex-start', flexDirection: 'row',width:50,height:50}}>
                <Image
                    source={require('assets/icons/Back_1.png')}
                    style={{ resizeMode: 'contain', padding: 10 , alignSelf: 'center', zIndex: 999 }}
                />
            </TouchableOpacity>
        </View>
    );
};

const ComponentCenter = () => {
    return(
        <View style={{ flex: 1, paddingLeft: 20}}>

            <Text style={{ color: 'white', ...ProximaNova.Regular, fontSize: 18}}>All Locations</Text>
        </View>
    );
};




class CourtList extends React.Component<$Props>  {
    /*
     * Removed for brevity
     */
    //

    constructor(){
        super();
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {

            dataSource:  ds.cloneWithRows([]),
            isLoading: true

        }
    }

    componentWillMount(){

        const [ ...otherCourts ] = this.props.courts;
        Orientation.lockToPortrait();
        console.log("inside the will load", otherCourts)

    }

    

    render() {


        const [ ...otherCourts ] = this.props.courts;

        return (


                <View style={styles.courtListMain}>
                    
                    <NavigationBar
                        componentLeft     = { () =>  <ComponentLeft navigation={this.props.navigation}/>   }
                        componentCenter   = { () =>  <ComponentCenter /> }
                        navigationBarStyle= {{ opacity:1,height:isIPhoneX()?80:80,paddingTop:isIPhoneX()?35:10,backgroundColor:'#0098D1', marginTop: Platform.OS === 'ios'? -20 : 0 }}
                        statusBarStyle    = {{ barStyle: 'light-content',opacity:1 }}
                    />
                    <ScrollView style={styles.courtListContainer}>
                    <View><Text style={styles.courtList}>Explore {otherCourts.length} Fitness Locations</Text></View>
                    <View>

                        {otherCourts.map(value => {

                            if(value.openingSoon == false){


                                return (

                                <CourtRow court={value}
                                                 navigation={this.props.navigation}/>
                                )
                            }
                            else if(value.openingSoon == true){

                                return (

                                    <OpenSoon court={value}
                                              navigation={this.props.navigation}/>
                                    )

                            }

                        })}
                    </View>
                    </ScrollView>
                </View>


        );
    }
}

export default connect(select)(CourtList);;
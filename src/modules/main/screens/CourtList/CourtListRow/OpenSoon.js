import React from 'react';
import { View, Text, StyleSheet, Image, ImageBackground,TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import { ProximaNova } from 'shared/styles/fonts';
import { black, borderGrey, white } from 'shared/styles/colors';
import MaterialInitials from 'react-native-material-initials/native';
import { isIPhone4, isMediumScreen } from 'shared/utils';
import styles from './styles';
import { MAIN_ROUTES, CHALLENGE_ROUTES } from 'constants';
import openMap from 'react-native-open-maps';





const OpenSoon = (props) => {


    var court = props.court

    // var object = {...props.data}
    // var userObject = {...object.user}
    // var court = {...object.fitness_court}
    // var picture = {...object.picture}

    return (
    <View style={styles.rowContainer}>

        <View style={styles.myviewContaineropen}>
            <TouchableWithoutFeedback style={styles.courtNameAddress}
                                      onPress={() => { props.navigation.navigate(MAIN_ROUTES.ALLCOURTSDETAIL, {court: court,});}}

            >

                <View style={{flex: 1}}>

                    <View style={styles.addresscont}>
                        <Text style={styles.courtName}
                              numberOfLines={1} ellipsizeMode={'tail'}>{court.name}
                        </Text>
                        <Text style={styles.courtCity}>{court.city}</Text>
                    </View>
                    {/*<View style={styles.classes}>*/}

                        {/*<View style={styles.opensoontclass}>*/}

                            {/*{court.classes &&*/}

                            {/*<Text style={styles.opennumberClasses}>3 classes</Text>*/}

                            {/*}*/}
                            {/*{!court.classes &&*/}

                            {/*<Text style={styles.opennumberClasses}>No classes</Text>*/}

                            {/*}*/}
                        {/*</View>*/}

                        {/*{court.isRegistered &&*/}
                        {/*<Text style={styles.registeredClass}>1 Registered</Text>*/}

                        {/*}*/}
                    {/*</View>*/}
                    <View>
                        <Text style={styles.opensoon}>Open Soon</Text>
                    </View>
                </View>

            </TouchableWithoutFeedback>
            <View style={styles.courtsDirection}>
            <TouchableOpacity style={styles.eventLocatioicon} onPress={() => {openMap({end: court.street + "," + court.city })}}>
                                                <ImageBackground
                                                style={styles.eventDirection}
                                                source={require('assets/icons/Event_direction.png')}
                                                resizeMode="contain">
                                                </ImageBackground>
                                                </TouchableOpacity>
            </View>
        </View>
    </View>
    )
};

export default OpenSoon;


import React from 'react';
import { View, Text, StyleSheet, Image, ImageBackground,TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import { ProximaNova } from 'shared/styles/fonts';
import { black, borderGrey, white } from 'shared/styles/colors';
import MaterialInitials from 'react-native-material-initials/native';
import { isIPhone4, isMediumScreen } from 'shared/utils';
import styles from './styles';
import { MAIN_ROUTES, CHALLENGE_ROUTES } from 'constants';
import openMap from 'react-native-open-maps';


const CourtRow = (props) => {


    var court = props.court
    return (
    <View style={styles.rowContainer}>

        <View style={styles.myviewContainer}>
            <TouchableWithoutFeedback style={styles.courtNameAddress}
                                      onPress={() => {
                                          props.navigation.navigate(MAIN_ROUTES.ALLCOURTSDETAIL, {court: props.court,});}}

            >

                <View style={{flex: 1}}>

                    <View style={styles.addresscont}>
                        <Text style={styles.courtName}
                              numberOfLines={1} ellipsizeMode={'tail'}>{court.name}

                            {court.openingSoon &&
                            <View style={{width:90,height:15,paddingLeft:20}}></View>

                            }
                        </Text>
                        <Text style={styles.courtCity}>{court.city}</Text>
                    </View>

                </View>

            </TouchableWithoutFeedback>
            <View style={styles.courtsDirection}>
            <TouchableOpacity style={styles.eventLocatioicon} onPress={() => {openMap({end: court.street + "," + court.city })}}>
                                                <ImageBackground
                                                style={styles.eventDirection}
                                                source={require('assets/icons/Event_direction.png')}
                                                resizeMode="contain">
                                                </ImageBackground>
                                                </TouchableOpacity>
            </View>
        </View>
    </View>
    )
};

export default CourtRow;


import { black, borderGrey, white } from 'shared/styles/colors';
import { Dimensions, StyleSheet, Platform } from 'react-native';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from 'constants';
import { ProximaNova } from 'shared/styles/fonts';
import { isIPhone4, isIPhone5, isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';

const { height } = Dimensions.get('window');

export default StyleSheet.create({ 
    rowContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',

    },
    text: {
        marginLeft: 12,
        fontSize: 14,
        ...ProximaNova.Regular,
        color: '#0098D1',
        marginLeft: 20
    },

    myviewContainer: {
        flex: 1,
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        borderBottomColor: '#f0f0f0',
        borderBottomWidth: 1,
        paddingBottom:20,
        paddingTop:20,
        height:110,
    },
    myviewContaineropen:{
        flex: 1,
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        borderBottomColor: '#f0f0f0',
        borderBottomWidth: 1,
        paddingBottom:20,
        paddingTop:20,
        height:150,
    },
    courtName:{
        fontSize:20,
        color:'#3b3b3b',
        paddingBottom:5,
        width:'100%',
    },
    courtCity:{
        color:'#676767',
        fontSize:16
    },  
    numberClasses:{
        color:'#0098d1',
        fontSize:14,
        width:'100%',
        paddingBottom:20,
        borderRightWidth:1,
        borderRightColor:'#9b9b9b',
        paddingRight:10,
    },
    addresscont:{
        paddingBottom:10,
    },
    courtRowbottomline:{
        borderBottomColor: '#f0f0f0',
        borderBottomWidth: 1,
    },
    eventDirection:{
        width:35,
        height:35
    },
    courtNameAddress:{
        width:'90%',
    },
    courtsDirection:{
        width:'10%',
        alignItems:'flex-end'
    },
    opensoon:{
        borderWidth:1,
        color:'#22b116',
        borderColor:'#22b116',
        fontSize:10,
        height:20,
        paddingTop:2,
        width:66,
        marginTop:10,
        textAlign:'center',
        borderRadius:10,
    },
    classes:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-start',
    },
    registeredClass:{
        color:'#22b116',
        fontSize:10,
        height:20,
        paddingTop:2,
        width:'100%',
        paddingLeft:10,
        borderLeftWidth:1,
        borderRightColor:'red',
    },
    eventTime:{
        paddingLeft:10,
        height:20
    },
    opensoontclass:{
        width:isIPhone5()?'35%':'30%',
        borderRightWidth:1,
        borderRightColor:'#9b9b9b',
        height:15
    },
    noclass:{
        width:isIPhone5()?'35%':'30%',
    },
    opennumberClasses:{
        color:'#0098d1',
        fontSize:14,
        width:'100%',
        paddingRight:10,
    },
    noregister:{
        borderLeftWidth:1,
        borderLeftColor:'#9b9b9b',
        height:15
    }
});
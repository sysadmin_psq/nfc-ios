import { black, borderGrey, white } from 'shared/styles/colors';
import { Dimensions, StyleSheet, Platform } from 'react-native';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from 'constants';
import { ProximaNova } from 'shared/styles/fonts';
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import { red } from 'ansi-colors';
import { single } from 'rxjs/operator/single';
import { isIphoneX } from 'react-native-iphone-x-helper';
// import { relative } from 'path';
// import leftPad = require('left-pad');

const { height } = Dimensions.get('window');

export default StyleSheet.create({ 
    courtListMain:{
        height:isIPhoneX()?'110%':'100%',
    },
    courtListContainer:{
        height:'100%',
        backgroundColor:'#ffffff',
        paddingLeft:20,
        paddingTop:30,
        paddingRight:20,
    },
    courtList:{
        fontSize:16,
        color:'#9b9b9b'
    },
    
});
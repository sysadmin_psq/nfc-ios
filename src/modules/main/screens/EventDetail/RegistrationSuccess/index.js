
import React from 'react';

import { View, ListView, StyleSheet, Text, ScrollView, Dimensions,TouchableOpacity, Image, Platform, TouchableWithoutFeedback,ImageBackground, AsyncStorage} from 'react-native';
import { black, borderGrey, white } from 'shared/styles/colors';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { ProximaNova } from 'shared/styles/fonts';
import { NavigationBar } from 'navigationbar-react-native';
import { StackNavigator } from 'react-navigation';
import styles from './styles';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen, isNotchDevice} from 'shared/utils';
import Moment from 'react-moment';
import moment from 'moment';
import { connect } from 'react-redux';
import select from 'modules/main/state/selectors/AboutScreen';
import Config from 'react-native-config';
import * as mainActions from 'modules/main/state/dux';



type $Props = {
    dispatch: *,
    name: ?string,
    showProfile: boolean,
    username: ?string,
    id: ?number,
};




class Registration extends React.Component {



    constructor(props){
        super(props);


        this.eventObject = props.eventObject
        this.availability = props.availability
        this.setRegister = props.setRegister

    }


    componentWillMount() {


        const { dispatch } = this.props;
        dispatch(mainActions.fetchUserInfo());

    }


    makeAppointment(){


        var apiUrl = `${Config.SERVER_URL}/api/v1/appointments/?api_key=UkNNuT0bebf2bRoCMitx`


        return fetch(apiUrl , {
            method: 'POST',
            headers: new Headers({
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify({service_id: this.eventObject.serviceId,
                 resource_id: this.eventObject.resourceId,
                 location_id: this.eventObject.locationId,
                 end_date_time: this.availability.availableTimes[0].endDateTime,
                 start_date_time: this.availability.availableTimes[0].startDateTime,
            })
        })
            .then(response => response.json())
            .then(response => {


                if(!response.error){

                    this.makeBooking(response)

                }


            })
            .catch(error => {

                console.log("error", error)
            });


    }

    makeBooking(appointmentObject) {

        const {
            dispatch,
            name,
            email,
            showProfile,
            username,
        } = this.props;


        var apiUrl = `${Config.SERVER_URL}/api/v1/appointments/${appointmentObject.id}/book/?api_key=UkNNuT0bebf2bRoCMitx`


        console.log("apiURL", apiUrl, email, name)
        return fetch(apiUrl , {
            method: 'PUT',
            headers: new Headers({
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify({email: email, first_name: name})
        })
            .then(response => response.json())
            .then(response => {

                console.log()
                console.log("response of booking", response)

                if(!response.error){

                    this.setRegister(true, response)
                }



            })
            .catch(error => {

                console.log("eroor", error)
            });

    }


    numberCalculation(number) {


        return moment(number, "hmm").format("H:mm A").replace(":00", "")

    }



    render() {


        var setCloseValues = this.props.setCloseValues
        console.log("event", this.eventObject)
        return(



         <TouchableWithoutFeedback>
                <View style={{marginTop: isNotchDevice()? 25 : 0, alignItems: 'center', height:224,width:'90%',paddingTop:isIPhoneX()?'75%':'60%' ,marginLeft:'5%',marginRight:'5%',position:'relative'}}>

                <View style={styles.confirmCont}>

                <View style={styles.closeMain}>
                    <TouchableOpacity style={styles.closeBtn} onPress={() => {setCloseValues()}}>
                        <Image
                            source={require('assets/icons/cancel.png')}
                            style={{ resizeMode: 'contain',  width: 15, height: 15}}
                        />
                    </TouchableOpacity>
                </View>
                    <View style={styles.registersuccess}>
                    <ImageBackground
                                        style={styles.registersucc}
                                        source={require('assets/icons/successIcon.png')}
                                        resizeMode="contain">
                                        </ImageBackground>
                    </View>
                <Text style={styles.confirmRegistration}>Registration confirmed</Text>
                <Text style={styles.confirmTitle}>See you on the Fitness Court</Text>
                </View>

                </View>
             </TouchableWithoutFeedback>

        );

    }

}


export default connect(select)(Registration)
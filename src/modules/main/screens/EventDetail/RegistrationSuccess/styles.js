

import { black, borderGrey, white } from 'shared/styles/colors';
import { Dimensions, StyleSheet, Platform } from 'react-native';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from 'constants';
import { ProximaNova } from 'shared/styles/fonts';
import { isIPhone4, isIPhone5, isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';

const { height } = Dimensions.get('window');

export default StyleSheet.create({ 
    confirmCont:{
        backgroundColor:'#ffffff',
       height:194,
       width:'100%',
       paddingTop:40,
       paddingLeft:isIPhone5()?'7%':'10%',
       paddingBottom:20,
       paddingRight:isIPhone5()?'0%':'10%'
    },
    confirmRegistration:{
        color:'#3b3b3b',
        fontSize:20,
        textAlign:'center',
        ...ProximaNova.CondensedSemibold,
        paddingBottom:20,
    },
    confirmTitle:{
        color:'#676767',
        fontSize:14,
        textAlign:'center'
    },  
    timeDate:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
    },
    eventTime:{
        borderRightWidth:1,
        borderRightColor:'#9b9b9b',
        paddingRight:10,
        fontSize:14,
    },
    eventDate:{
        color:'#9b9b9b'
    },
    eventDatecont:{
        width:'45%',
        paddingLeft:15,
    },
    eventTimecont:{
        // width:'50%',
        width:isIPhone5()? '51%' : '55%',
        paddingLeft:isIPhone5()?'0':'5%',
        borderRightWidth:1,
        borderRightColor:'#9b9b9b',
    },
    registerBtn:{
        color:'#ffffff',
        fontSize:14,
        textAlign:'center',
        ...ProximaNova.CondensedSemibold,
        alignItems: 'center',
    },
    registrationBtn:{
        width: 130,
        borderWidth:1,
        borderRadius:20,
        height:40,
        fontWeight:'normal',
        fontSize:14,
        backgroundColor:'#00aeef',
        ...ProximaNova.Regular,
        borderColor:'#00aeef',
        paddingTop:10,
        marginLeft:isIPhone5()?'25%':'30%',
    },
    closebtn:{
        paddingTop:20,
        paddingRight:20,
        flexGrow:1,
        height:15,
    },
    closeicon:{
        height:15,
        flex:1,
    },
    closeBtn:{
        width:30,
        height:30,
        padding:7
    },
    closeMain:{
        width:30,
        height:30,
        position:'absolute',
        right:10,
        top:15,
    },
    registersuccess:{
        width:52,
        height:52,
        paddingLeft:'40%',
        paddingBottom:10,
    },
    registersucc:{
        flex:1,
        flexDirection:'row',
        width:52,
        height:52
    }
});
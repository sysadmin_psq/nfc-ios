import { black, borderGrey, white } from 'shared/styles/colors';
import { Dimensions, StyleSheet, Platform } from 'react-native';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from 'constants';
import RF from "react-native-responsive-fontsize";
import { ProximaNova } from 'shared/styles/fonts';
import { isIPhone4, isIPhone5, isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import { red } from 'ansi-colors';
import { single } from 'rxjs/operator/single';
// import { relative } from 'path';
// import leftPad = require('left-pad');

const { height } = Dimensions.get('window');

export default StyleSheet.create({
    eventScroll:{
        height:'88%',
        backgroundColor:'white',
    },
    eventMain:{
        backgroundColor:'#ffffff',
        height:'100%'
    },
    EventDetailscreen:{
        height:'100%',
        paddingLeft:20,
        paddingRight:20,
        paddingTop:20,
        paddingBottom:20,
        backgroundColor:'#ffffff'
    },
    detailsFooter:{
        backgroundColor:'#f5f5f5',
        maxHeight: 30
    },
    eventCard:{
        height:223,

    },
    eventBorder:{
        borderBottomColor: '#f0f0f0',
        borderBottomWidth: 1,
        paddingBottom:20,
    },
    bgevent:{
        height:180,
        width:'100%',
    },
    eventMainBg:{
        flex: 1,
        // width: 395,
        width:'100%',
        // height: '100%',
        backgroundColor:'#0098D1',
    },
    eventBtn:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-start',
        position:'relative',
    },
    backBtn:{
        width:'80%',
        paddingTop:10,
    },
    backBtnlink:{
        justifyContent:"flex-start",
    },
    eventShareBtn:{
        width:'20%',
        paddingTop:10,
    },
    eventbtnbg:{
        width:27,
        height:27,
        position:'absolute',
        top:'10%',
        left:10
    },
    eventShare:{
        width:18,
        height:19,
        position:'absolute',
        top:'10%',
        right:isIPhone5()? '50%' : '50%',
        // right:'50%'
    },
    hidebtn:{
        fontSize:0
    },
    eventAttend:{
        backgroundColor:'#e3f8ff',
        paddingLeft:20,
        paddingRight:20,
        paddingTop:5,
        paddingBottom:5,
    },
    eventAttendtext:{
        fontSize:12,
        color:'#3b3b3b'
    },
    eventTitle:{
        fontSize:20,
        color:'#3b3b3b',
        ...ProximaNova.CondensedSemibold,
    },
    eventTrainer:{
        color:'#9b9b9b',
        fontSize:14,
    },
    eventTimeDate:{
        flex: 1,
        flexDirection: 'row',
        justifyContent:'flex-start',
        paddingTop:20,
        borderBottomColor: '#f0f0f0',
        borderBottomWidth: 1,
        paddingBottom:20,
    },
    eventContact:{
        color:'#0098d1',
        fontSize:14,
    },
    dividerContainer:{
        width:15,
        height:15,
    },
    eventDirection:{
        width:35,
        height:35
    },
    eventLocation:{
        width:15,
        height:24,
    },
    locationWidth:{
        width:'10%'
    },
    directionText:{
        textAlign:'center',
        fontSize:12,
    },
    eventLocatioicon:{
        textAlign:'center',
        paddingLeft:10,
        paddingBottom:5,
        resizeMode: 'cover'
    },
    eventAddress:{
        width:'70%',
    },
    eventDatecont:{
        width:isIPhone5()? '48%' : '42%',
        paddingLeft:15,
    },
    eventTimecont:{
        width:isIPhone5()? '44%' : '40%',
        borderRightWidth:1,
        borderRightColor:'#9b9b9b',
    },
    eventTime:{
        borderRightWidth:1,
        borderRightColor:'#9b9b9b',
    },
    eventDate:{
        color:'#9b9b9b',
        fontSize:12,
    },
    courtName:{
        color:'#676767',
        fontSize:20,
        ...ProximaNova.CondensedSemibold,
    },
    courtAddress:{
        color:'#676767',
        fontSize:14,
        ...ProximaNova.Regular,
        width:'80%'

    },
    eventDesc:{
        paddingTop:20,
        borderBottomColor: '#f0f0f0',
        borderBottomWidth: 1,
        paddingBottom:20,
    },
    eventDescTitle:{
        fontSize:16,
        color:'#3b3b3b',
        paddingBottom:10,
        ...ProximaNova.CondensedSemibold,
    },
    eventDescription:{
        fontSize:14,
        ...ProximaNova.Regular,
    },
    eventContactline:{
        paddingTop:20,
    },
    challengesContainer: {
        alignItems: 'center',
        flex: 10,
        flexDirection:'row',
        marginStart: 20,
        marginBottom: 0,
    },
    eventTicket:{
        width:'100%',
        height:119,
        paddingTop:33,
    },
    eventTicketNote:{
        color:'#9b9b9b',
        fontSize:14,
        textAlign:'center',
        paddingTop:10,
        
    },
    eventCoupncode:{
        color:'#3b3b3b',
        fontSize:RF(5.5),        
        textAlign:'center',
        ...ProximaNova.CondensedSemibold,
        paddingTop:10,
        paddingRight:'15%',

    },


    eventCoupncodeDisabled:{
        color:'#D1D1D1',
        fontSize:RF(5.5),
        textAlign:'center',
        ...ProximaNova.CondensedSemibold,
        paddingTop:10,
        paddingRight:'15%',

    },
    detailsFooter:{
        flex: 1,
        flexDirection: 'row',
        justifyContent:'space-between',
        paddingLeft:'5%',
        paddingRight:'5%',
        paddingTop:'4%',
        maxHeight: isIPhoneX() ? '13%' : '12%',
        marginBottom: isIPhoneX() ? -50 : 0,
        backgroundColor:'#f5f5f5',
    },
    registrationBtn:{
        width: 218,
        borderWidth:1,
        borderRadius:20,
        height:40,
        fontWeight:'normal',
        fontSize:14,
        backgroundColor:'#00aeef',
        ...ProximaNova.Regular,
        borderColor:'#00aeef',
        paddingTop:10,
    },
    registrationBtnDisabled:{
        width: 218,
        borderWidth:1,
        borderRadius:20,
        height:40,
        fontWeight:'normal',
        fontSize:14,
        backgroundColor:'#D1D1D1',
        ...ProximaNova.Regular,
        borderColor:'#D1D1D1',
        paddingTop:10,
    },
    freeText:{
        fontSize:22,
        color:'#4a4a4a',
        height:30,
        paddingTop:5,
    },
    registerBtn:{
        color:'#ffffff',
        fontSize:14,
        textAlign:'center',
        ...ProximaNova.CondensedSemibold,
    },

});
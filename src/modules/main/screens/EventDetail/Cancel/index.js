
import React from 'react';
import { View, ListView, StyleSheet, Text, ScrollView, Dimensions,TouchableOpacity, Image, Platform, TouchableWithoutFeedback,ImageBackground} from 'react-native';
import { black, borderGrey, white } from 'shared/styles/colors';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { ProximaNova } from 'shared/styles/fonts';
import { NavigationBar } from 'navigationbar-react-native';
import { StackNavigator } from 'react-navigation';
import styles from './styles';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen, isNotchDevice} from 'shared/utils';
import Moment from 'react-moment';
import moment from 'moment';
import { connect } from 'react-redux';
import select from 'modules/main/state/selectors/AboutScreen';
import Config from 'react-native-config';
import * as mainActions from 'modules/main/state/dux';




class Cancel extends React.Component {



    constructor(props){
        super(props);


        this.eventObject = props.eventObject
        this.availability = props.availability
        this.cancelRegister = props.cancelRegister
        this.confirmationObject = props.confirmationObject


    }



    state = {

        isLoading: false,

    };


    componentWillReceiveProps(props) {
        //
        //
        // this.setState({
        //
        //     filterThisWeek: props.filterThisWeek,
        //     courtNearMe: props.courtNearMe,
        //
        // })

    }

    cancelRegistration(){


        console.log("cancelObject", this.confirmationObject)

        this.setState({

            isLoading: true,
        })


        if(!this.confirmationObject.booking_id) {

            this.booking_id = this.confirmationObject.id

        }
        else {

            this.booking_id = this.confirmationObject.booking_id
        }

        var apiUrl = `${Config.SERVER_URL}/api/v1/appointments/${this.booking_id}/cancel/?api_key=UkNNuT0bebf2bRoCMitx`


        console.log("apiURL", apiUrl)
        return fetch(apiUrl , {
            method: 'DELETE',
            headers: new Headers({
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }),
        })
            .then(response => response.json())
            .then(response => {

                console.log("response of cancelled", response)

                if(!response.error){

                    this.cancelRegister(false)

                }

            })
            .catch(error => {

                console.log("eroor", error)
            });


    }



    numberCalculation(number) {


        return moment(number, "hmm").format("h:mm A").replace(":00", "")

    }

    render() {


        console.log("cancel", this.props)
        var setFilterValues = this.props.setFilterValues

        return(


            <TouchableWithoutFeedback>

<View style={{marginTop: isNotchDevice()? 25 : 0, alignItems: 'center', height:224,width:'90%',paddingTop:isIPhoneX()?'75%':'60%',marginLeft:'5%',marginRight:'5%',position:'relative'}}>

                    <View style={styles.confirmCont}>

                    <View style={styles.closeMain}>
                        <TouchableOpacity style={styles.closeBtn} onPress={() => {setFilterValues(false, false, false)}}>
                            <Image
                                source={require('assets/icons/cancel.png')}
                                style={{ resizeMode: 'contain',  width: 15, height: 15}}
                            />
                        </TouchableOpacity>
                    </View>

                      <Text style={styles.confirmRegistration}>Cancel registration</Text>
                      <Text style={styles.confirmTitle}>{this.eventObject.serviceName}</Text>
                      <View style={styles.timeDate}>
                        <View style={styles.eventTimecont}>
                            <Text style={styles.eventTime}><Moment element={Text} format="ddd, MMM D, YYYY">{this.eventObject.startDate}</Moment></Text>
                        </View>
                        <View style={styles.eventDatecont}>
                            <Text style={styles.eventDate}>{this.numberCalculation(this.eventObject.startTime.toString())} - {this.numberCalculation(this.eventObject.endTime.toString())}</Text>
                        </View>
                      </View>


                        {this.state.isLoading == false &&


                        <TouchableOpacity style={styles.registrationBtn}
                                          onPress={() => { this.cancelRegistration()}}>
                            <Text style={styles.registerBtn}>Yes, Cancel </Text>
                        </TouchableOpacity>

                        }

                        {this.state.isLoading == true &&


                        <TouchableOpacity style={styles.registrationBtn}>
                            <Text style={styles.registerBtn}>Cancelling...</Text>
                        </TouchableOpacity>

                        }






                    </View>
                    
                </View>

            </TouchableWithoutFeedback>


        );

    }

}


export default Cancel
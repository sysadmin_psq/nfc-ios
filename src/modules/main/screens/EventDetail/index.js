
import React from 'react';
import { View, ListView, StyleSheet, Text, ScrollView, Dimensions, ActivityIndicator, Platform, TouchableOpacity, Image,Button,Alert,ImageBackground, AsyncStorage, Share, Animated, StatusBar, Linking} from 'react-native';
import RF from "react-native-responsive-fontsize";
import { black, borderGrey, white, navBarColor } from 'shared/styles/colors';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import ViewMoreText from 'react-native-view-more-text';
import DynamicButton from 'shared/components/DynamicButton';
import { ProximaNova } from 'shared/styles/fonts';
// import eventstyles from './styles';
import ChallengePanel from 'modules/main/screens/Challenge/ChallengePanel';
import ChallengeModal from 'modules/main/screens/Challenge/ChallengeModal';
import select from 'modules/main/state/selectors/LocationScreen';
import select2 from 'modules/main/state/selectors/AboutScreen';
import openMap from 'react-native-open-maps';
import * as mainActions from 'modules/main/state/dux';
import { FEED_LOCATIONS, PAGINATION, STORAGE_USER_SESSION } from 'constants';
import SplashScreen from 'react-native-splash-screen';
import { NavigationBar } from 'navigationbar-react-native';
import FilterModal from 'modules/main/screens/LeaderBoard/FilterModal';
import Modal from "react-native-modal";
import ModalStyle from 'modules/main/screens/LeaderBoard/FilterModal/styles'
import { isIPhone4, isIPhone5, isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import Confirm from './Confirm';
import Cancel from "./Cancel";
import Registration from "./RegistrationSuccess";
import Moment from 'react-moment';
import moment from 'moment';
import { connect } from 'react-redux';
import Config from 'react-native-config'
import Spinner from 'react-native-loading-spinner-overlay';
import eventStyle from './styles'
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';
import { isIphoneX } from 'react-native-iphone-x-helper';
var _ = require('lodash')


// type $Props = {
//     courts: Array<$FitnessCourt>,
//     locationsEnabled: boolean,
// };

const HEADER_MAX_HEIGHT = 230;
const MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 73;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - MIN_HEIGHT;



const styles = StyleSheet.create({
    fill: {
        flex: 1,
        
    },
    content: {
        flex: 1,
    },

    image: {
        // displa
        // height: HEADER_MAX_HEIGHT,
        // width: Dimensions.get('window').width,
        // alignSelf: 'stretch',
        // resizeMode: 'cover',
        flex: 1,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingBottom: '8%',
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: navBarColor,
        overflow: 'hidden',
        height: HEADER_MAX_HEIGHT,
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
        flex: 1,
        // width: 395,
        // width:'100%',
        // height: '100%',
        // backgroundColor:'#0098D1',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 20 : 28,
        height: 20,
        width: '100%',
        // alignItems: 'center',
        // justifyContent: 'center',
        position: 'absolute',
        // top: 0,
        // left: 0,
        // right: 0,
        // justifyContent:'flex-start',
        // position:'relative',
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
});



const addstyles = StyleSheet.create({
    image: {
        height: HEADER_MAX_HEIGHT,
        width: Dimensions.get('window').width,
        alignSelf: 'stretch',
        resizeMode: 'cover',
    },
    title: {
        fontSize: 20,
    },
    name: {
        fontWeight: 'bold',
    },
    section: {
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#cccccc',
        backgroundColor: 'white',
    },
    sectionTitle: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    sectionContent: {
        fontSize: 16,
        textAlign: 'justify',
    },
    keywords: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
    },
    keywordContainer: {
        backgroundColor: '#999999',
        borderRadius: 10,
        margin: 10,
        padding: 10,
    },
    keyword: {
        fontSize: 16,
        color: 'white',
    },
    titleContainer: {
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageTitle: {
        color: 'white',
        backgroundColor: 'transparent',
        fontSize: 24,
    },
    navTitleView: {
        height: MIN_HEIGHT,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 16,
        opacity: 0,
    },
    navTitle: {
        color: 'white',
        fontSize: 18,
        backgroundColor: 'transparent',
    },
    sectionLarge: {
        height: 600,
    },
});


type $Props = {
    dispatch: *,
    name: ?string,
    showProfile: boolean,
    username: ?string,
    id: ?number,
};

class EventDetail extends  React.Component<$Props> {
    /*
     * Removed for brevity
     */
    //


    constructor(){
        super();

        this.availableObject = []

    }

    state = {

        isRegistred: false,
        modalVisible: false,
        modalValue: false,
        availableObject: [],
        isCancel: false,
        isConfirmed: false,
        isLoadingData: false,
        isPastEvent: false,
        scrollY: new Animated.Value(
            // iOS has negative initial scroll value because content inset...
            Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
        ),

    };


    _renderScrollViewContent() {
        const data = Array.from({ length: 30 });
        return (
            <View style={styles.scrollViewContent}>
                {data.map((_, i) => (
                    <View key={i} style={styles.row}>
                        <Text>{i}</Text>
                    </View>
                ))}
            </View>
        );
    }


    _toggleModal(visible) {


        this.setState({modalVisible: visible})
    }


    chanegToggle(visible){


        this.setState({modalVisible: false})

    }

    numberCalculation(number){

        return  moment(number, "hmm").format("h:mm A").replace(":00", "")
    }



    getUserSession = () => {


        this.setState({

            isLoadingData: true
        })

        const retrievedItem = AsyncStorage.getItem(STORAGE_USER_SESSION);
        return retrievedItem
    }


    fetchDataWithAllocations(event, court, appointmentObject) {


        console.log("inside the fetchDataWithAllocations")
        this.getUserSession().then((item) => {

            var userURL = `${Config.SERVER_URL}/api/v1/get_user/?api_key=UkNNuT0bebf2bRoCMitx&token=` + item
            return fetch(userURL , {
                headers: new Headers({
                    'Content-Type': 'application/json'
                })
            })
                .then(user => user.json())
                .then(user => {

                    console.log("userURL", event, user.email)

                    var startDate = moment(event.startDate).format('YYYY-MM-DD')
                    var endDate = moment(event.endDate).format('YYYY-MM-DD')


                    var locationsApi = `${Config.SERVER_URL}/api/v1/locations/?api_key=UkNNuT0bebf2bRoCMitx&location_id=${court.location_id}`

                    return fetch(locationsApi , {
                        headers: new Headers({
                            'Content-Type': 'application/json'
                        })
                    })
                        .then(location => location.json())
                        .then(location => {


                            console.log("this is it locations", location, this.state.event)
                            this.email = location.email
                            var apiUrl = `${Config.SERVER_URL}/api/v1/availability/${event.serviceId}/${startDate}/${endDate}?location_id=${court.location_id}&resource_id=${event.resourceId}`


                            return fetch(apiUrl , {
                                headers: new Headers({
                                    'Content-Type': 'application/json'
                                })
                            })
                                .then(response => response.json())
                                .then(response => {


                                    console.log("availableObject",response)
                                    this.availableObject = response


                                    if(response.availableTimes && response.availableTimes.length > 0) {

                                        this.setState({

                                            modalValue : true
                                        })


                                        this.attendingCount = response.availableTimes[0].allowableBookings - response.availableTimes[0].availableBookings
                                        console.log("times", response.availableTimes)

                                        if(!this.state.appointmentObject) {


                                            var bookapiUrl = `${Config.SERVER_URL}/api/v1/bookings/${user.id}/?api_key=UkNNuT0bebf2bRoCMitx&service_id=${event.serviceId}&start_date_time=${response.availableTimes[0].startDateTime}&end_date_time=${response.availableTimes[0].endDateTime}`



                                            return fetch(bookapiUrl , {
                                                headers: new Headers({
                                                    'Content-Type': 'application/json'
                                                })
                                            })
                                                .then(booking => booking.json())
                                                .then(booking => {


                                                    console.log("booking", booking)

                                                    if(booking.length > 0) {




                                                        if(appointmentObject) {



                                                            this.setState({

                                                                isRegistred: true,
                                                                confirmationObject : appointmentObject,
                                                                isLoadingData: false

                                                            })



                                                        }
                                                        else {



                                                            var appointMentUrl = `${Config.SERVER_URL}/api/v1/appointments/?api_key=UkNNuT0bebf2bRoCMitx&location_id=${court.location_id}&email=${user.email}&service_id=${event.serviceId}`
                                                            console.log("appointmentArray", appointMentUrl)

                                                            return fetch(appointMentUrl , {
                                                                headers: new Headers({
                                                                    'Content-Type': 'application/json'
                                                                })
                                                            })
                                                                .then(appointment => appointment.json())
                                                                .then(appointment => {


                                                                    console.log("appointmentId", appointment)

                                                                    if(appointment.data.length > 0) {



                                                                        var obj = _.find(appointment.data, function(o){return o.id == booking[0].booking_id})
                                                                        console.log("is book", obj)


                                                                        if(obj) {

                                                                            this.setState({

                                                                                isRegistred: true,
                                                                                confirmationObject : obj,
                                                                                isLoadingData: false

                                                                            })
                                                                        }
                                                                        else {


                                                                            this.setState({

                                                                                isLoadingData: false

                                                                            })
                                                                        }

                                                                    }
                                                                    else {


                                                                        this.setState({

                                                                            isLoadingData: false

                                                                        })
                                                                    }

                                                                }).catch(err => {

                                                                    console.log("error", err)
                                                                });
                                                        }





                                                    }
                                                    else {


                                                        this.setState({

                                                            isLoadingData: false

                                                        })
                                                    }

                                                });

                                        }else {

                                            this.setState({
                                                isRegistred: true,
                                                confirmationObject : this.state.appointmentObject,
                                                isLoadingData: false

                                            })

                                        }


                                    }
                                    else {

                                        this.setState({

                                            isLoadingData: false

                                        })



                                    }


                                });


                        })


                });
        })



    }


    fetchDataWithoutAllocations(court, appointmentObject){


        console.log("isnide the fetchDataWithoutAllocations", appointmentObject)
        var today = moment(new Date()).format("YYYY-MM-DD")


        var allociationsApi = `${Config.SERVER_URL}/api/v1/service/${appointmentObject.serviceId}/allocations/?api_key=UkNNuT0bebf2bRoCMitx&limit=200&offset=0&location_id=${court.location_id}&start_date=${today}`
        console.log(allociationsApi)


        return fetch(allociationsApi , {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
            .then(allocationObject => allocationObject.json())
            .then(allocationObject => {


                console.log("allocationObject", allocationObject)

                if(allocationObject.data.length > 0) {

                    this.getUserSession().then((item) => {

                        var userURL = `${Config.SERVER_URL}/api/v1/get_user/?api_key=UkNNuT0bebf2bRoCMitx&token=` + item
                        return fetch(userURL , {
                            headers: new Headers({
                                'Content-Type': 'application/json'
                            })
                        })
                            .then(user => user.json())
                            .then(user => {

                                console.log("userURL", allocationObject, user.email)

                                var startDate = moment(allocationObject.startDate).format('YYYY-MM-DD')
                                var endDate = moment(allocationObject.endDate).format('YYYY-MM-DD')


                                var locationsApi = `${Config.SERVER_URL}/api/v1/locations/?api_key=UkNNuT0bebf2bRoCMitx&location_id=${court.location_id}`

                                return fetch(locationsApi , {
                                    headers: new Headers({
                                        'Content-Type': 'application/json'
                                    })
                                })
                                    .then(location => location.json())
                                    .then(location => {


                                        console.log("this is it locations", location,)
                                        this.email = location.email
                                        var apiUrl = `${Config.SERVER_URL}/api/v1/availability/${allocationObject.data[0].serviceId}/${startDate}/${endDate}?location_id=${court.location_id}&resource_id=${allocationObject.data[0].resourceId}`


                                        return fetch(apiUrl , {
                                            headers: new Headers({
                                                'Content-Type': 'application/json'
                                            })
                                        })
                                            .then(response => response.json())
                                            .then(response => {


                                                this.availableObject = response

                                                console.log("inside hte availability", response.availableTimes, apiUrl)

                                                if(response.availableTimes && response.availableTimes.length > 0) {

                                                    this.setState({

                                                        modalValue : true
                                                    })


                                                    this.attendingCount = response.availableTimes[0].allowableBookings - response.availableTimes[0].availableBookings
                                                    console.log("times", response.availableTimes)



                                                    if(appointmentObject) {


                                                        this.setState({

                                                                isRegistred: true,
                                                                confirmationObject : appointmentObject,
                                                                isLoadingData: false,
                                                                event: allocationObject

                                                                })

                                                    }else {

                                                        this.setState({

                                                            isLoadingData: false,
                                                            event: allocationObject

                                                        })

                                                    }


                                                }
                                                else {

                                                    this.setState({

                                                        isLoadingData: false,
                                                        event: allocationObject

                                                    })



                                                }


                                            });


                                    })


                            }).catch(err => {

                                console.log("err", err)
                            });
                    })





                }
                else {


                    this.setState({

                        isLoadingData: false

                    })
                }

            }).catch(err => {

                console.log("error", err)
            });

    }


    componentWillMount(){


        var values = this.props.navigation.state.routes.find((e) => e.routeName === 'EventDetail')

        this.attendingCount = 0


        this.setState({
            court: values.params.court,
            event: values.params.event,
            appointmentObject: values.params.appointmentObject,
            isPastEvent: values.params.isPastEvent

        })


            this.fetchDataWithAllocations(values.params.event, values.params.court)
        // else if(values.params.appointmentObject && !values.params.event) {
        //
        //     console.log("insdie the function")
        //
        //     this.fetchDataWithoutAllocations(values.params.court, values.params.appointmentObject)
        //
        // }

    }




    goToMap = (courtObject) => {

        var endString = courtObject.street + "," + courtObject.city
        openMap({end: endString });
        //openMap({ provider: 'google', latitude: parseFloat(courtObject.latitude), longitude: parseFloat(courtObject.longitude) });

    }

    setRegister = (isRegistred, confirm) => {


        this.setState({

            isRegistred : isRegistred,
            confirmationObject : confirm,
            isConfirmed: true
        })

    }

    onClick() {

        var message = this.state.event.serviceName.toUpperCase() +  ' - ' + ' Workout on the Fitness Court'

        var url = `https://nationalfitness.azurewebsites.net/${this.state.event.locationId}/${this.state.event.serviceId}`

        Share.share({
            message: message,
            url: `https://nationalfitness.azurewebsites.net/${this.state.event.locationId}/${this.state.event.serviceId}`,
            title: 'Wow, did you see that?'
        }, {
            // Android only:
            dialogTitle: 'Share BAM goodness',
            // iOS only:
            excludedActivityTypes: [
                'com.apple.UIKit.activity.PostToTwitter'
            ]
        })
    }


    cancelRegister = (isRegistred) => {

        this.setState({

            isRegistred : isRegistred,
            confirmationObject : null,
            modalVisible: false,
            isConfirmed: false
        })
    }


    setFilterValues = (courtname, filterWeek, courtMe) => {


        this.setState({

            // isRegistred : false,
            modalVisible: false,
            // isConfirmed: false
        })

    }

    setCloseValues = () => {

        this.setState({

            modalVisible: false,
            isConfirmed: false,
        })

    }

    renderViewMore(onPress){
        return(
          <Text onPress={onPress} style={{ color:'#0098d1',fontSize:14,}}>Read more</Text>
        )
      }
      renderViewLess(onPress){
        return(
          <Text onPress={onPress} style={{ color:'#0098d1',fontSize:14,}}>Read less</Text>
        )
      }


    render() {

        // const scrollY = Animated.add(
        //     this.state.scrollY,
        //     Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        // );
        // const headerTranslate = scrollY.interpolate({
        //     inputRange: [0, HEADER_SCROLL_DISTANCE],
        //     outputRange: [0, -HEADER_SCROLL_DISTANCE],
        //     extrapolate: 'clamp',
        // });
        //
        // const imageOpacity = scrollY.interpolate({
        //     inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
        //     outputRange: [1, 1, 0],
        //     extrapolate: 'clamp',
        // });
        // const imageTranslate = scrollY.interpolate({
        //     inputRange: [0, HEADER_SCROLL_DISTANCE],
        //     outputRange: [0, 100],
        //     extrapolate: 'clamp',
        // });
        //
        // const titleScale = scrollY.interpolate({
        //     inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
        //     outputRange: [1, 1, 0.8],
        //     extrapolate: 'clamp',
        // });
        // const titleTranslate = scrollY.interpolate({
        //     inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
        //     outputRange: [0, 0, -8],
        //     extrapolate: 'clamp',
        // });


        return (

            <View style={styles.fill}>


                <StatusBar barStyle="light-content" />

                <HeaderImageScrollView
                    maxHeight={200}
                    minHeight={isIphoneX()? 80 : 55}
                    renderHeader={() => <Image source={{uri: this.state.event.resourceImageUrl}} style={styles.image} />}
                    renderFixedForeground={() => (
                        <View style={{ height: 150, justifyContent: "center", alignItems: "center" }} >
                            <View style={{width: '50%', position: 'absolute', top: isIphoneX()?'30%':'13%', left: 10}}>

                                <TouchableOpacity style={eventStyle.backBtnlink}
                                                  onPress={() => {  this.props.navigation.goBack(null)}}>
                                    <Text style={eventStyle.hidebtn}></Text>
                                    <ImageBackground
                                        style={eventStyle.eventbtnbg}
                                        source={require('assets/icons/EventbackArrow.png')}
                                        resizeMode="contain"></ImageBackground>
                                </TouchableOpacity>

                            </View>

                            <View style={{width: '50%', position: 'absolute', top: isIphoneX()?'30%':'13%', right: '-20%'}}>
                            <TouchableOpacity style={eventStyle.eventShareBtnlink}
                            onPress={() => this.onClick()}>
                            <Text style={eventStyle.hidebtn}></Text>
                            <ImageBackground
                            style={eventStyle.eventShare}
                            source={require('assets/icons/share.png')}
                            resizeMode="contain"></ImageBackground>
                            </TouchableOpacity>
                            </View>

                        </View>
                    )}
                >
                    <View style={styles.fill}>


                        <View style={eventStyle.eventAttend}><Text tyle={eventStyle.eventAttendtext}>{this.attendingCount} Attending</Text></View>
                        <View style={eventStyle.EventDetailscreen}>
                            <View>
                                <View><Text style={eventStyle.eventTitle}>{this.state.event.serviceName.toUpperCase()}</Text></View>
                                <View style={eventStyle.eventBorder}><Text style={eventStyle.eventTrainer}>Trainer: {this.state.event.resourceName} </Text></View>

                                <View style={eventStyle.eventTimeDate}>
                                    <View style={eventStyle.locationWidth}>
                                        <ImageBackground
                                            style={eventStyle.dividerContainer}
                                            source={require('assets/icons/calender.png')}
                                            resizeMode="contain">
                                        </ImageBackground>
                                    </View>
                                    <View style={eventStyle.eventTimecont}>
                                        <Text style={eventStyle.eventTime}><Moment element={Text} format="ddd, MMM D, YYYY">{this.state.event.startDate}</Moment>  </Text>
                                    </View>
                                    <View style={eventStyle.eventDatecont}><Text style={eventStyle.eventDate}>{this.numberCalculation(this.state.event.startTime.toString())} - {this.numberCalculation(this.state.event.endTime.toString())}</Text></View>
                                </View>
                                <View style={eventStyle.eventTimeDate}>
                                    <View style={eventStyle.locationWidth}>
                                        <ImageBackground
                                            style={eventStyle.eventLocation}
                                            source={require('assets/icons/Event_Location.png')}
                                            resizeMode="contain">
                                        </ImageBackground>
                                    </View>

                                    <View style={eventStyle.eventAddress}>
                                        <Text style={eventStyle.courtName}>{this.state.court.name}</Text>
                                        <Text style={eventStyle.courtAddress}>{this.state.court.street} {this.state.court.city}</Text>
                                    </View>
                                    <View>
                                        <TouchableOpacity style={eventStyle.eventLocatioicon}
                                                          onPress={() => this.goToMap(this.state.court)}>
                                            <ImageBackground
                                                style={eventStyle.eventDirection}
                                                source={require('assets/icons/Event_direction.png')}
                                                resizeMode="contain">
                                            </ImageBackground>
                                        </TouchableOpacity>
                                        <Text style={eventStyle.directionText}>Directions</Text>
                                    </View>

                                </View>

                                {this.state.isRegistred === false &&



                                <View style={eventStyle.eventDesc}>

                                    <Text style={eventStyle.eventDescTitle}>About</Text>
                                    {this.state.event.serviceDescription.length > 0 &&
                                    <ViewMoreText
                                        numberOfLines={4}
                                        renderViewMore={this.renderViewMore}
                                        renderViewLess={this.renderViewLess}
                                        textStyle={{textAlign: 'left'}}
                                    >
                                        <Text style={eventStyle.eventDescription}>{this.state.event.serviceDescription}</Text>
                                    </ViewMoreText>
                                    }
                                </View>

                                }
                                {this.state.isRegistred === true &&
                                <View>
                                    <View style={eventStyle.eventDesc}>
                                        <ImageBackground
                                            style={eventStyle.eventTicket}
                                            source={require('assets/images/Event_tickets.png')}
                                            resizeMode="contain">

                                            {this.state.isPastEvent == false &&

                                            <Text style={eventStyle.eventCoupncode}>{this.state.confirmationObject.confirmationNumber}</Text>

                                            }

                                            {this.state.isPastEvent == true &&

                                            <Text style={eventStyle.eventCoupncodeDisabled}>{this.state.confirmationObject.confirmationNumber}</Text>

                                            }


                                        </ImageBackground>
                                        <Text style={eventStyle.eventTicketNote}>Please show this code at court</Text>
                                    </View>
                                    <View style={eventStyle.eventDesc}>

                                        <Text style={eventStyle.eventDescTitle}>About</Text>

                                        {this.state.event.serviceDescription.length > 0 &&
                                        <ViewMoreText
                                            numberOfLines={4}
                                            renderViewMore={this.renderViewMore}
                                            renderViewLess={this.renderViewLess}
                                            textStyle={{textAlign: 'left'}}
                                        >
                                            <Text style={eventStyle.eventDescription}>{this.state.event.serviceDescription}</Text>
                                        </ViewMoreText>
                                        }
                                    </View>
                                </View>
                                }

                                <TouchableOpacity style={eventStyle.eventContactline}onPress={()=>{
                                    Linking.openURL(`mailto:${this.email}`);}}>
                                    <Text style={eventStyle.eventContact}>Contact Organizer </Text>
                                </TouchableOpacity>
                            </View>
                        </View>




                    </View>
                </HeaderImageScrollView>




                <View style={eventStyle.detailsFooter}>
                    <Text style={eventStyle.freeText}>Free</Text>

                    {this.state.isRegistred == false && this.state.isLoadingData == false && this.state.isPastEvent == false &&

                    <TouchableOpacity style={eventStyle.registrationBtn}
                                      onPress={() => { this.setState({modalVisible: true, isCancel: false})}}>
                        <Text style={eventStyle.registerBtn}>Register</Text>
                    </TouchableOpacity>
                    }

                    {this.state.isRegistred == false && this.state.isLoadingData == false && this.state.isPastEvent == true &&

                    <View style={eventStyle.registrationBtnDisabled}>
                        <Text style={eventStyle.registerBtn}>Register</Text>
                    </View>
                    }

                    {this.state.isRegistred == true && this.state.isLoadingData == false && this.state.isPastEvent == false &&

                    <TouchableOpacity style={eventStyle.registrationBtn}
                                      onPress={() => { this.setState({modalVisible: true, isCancel: true})}}>
                        <Text style={eventStyle.registerBtn}>Cancel registration</Text>
                    </TouchableOpacity>
                    }

                    {this.state.isRegistred == true && this.state.isLoadingData == false && this.state.isPastEvent == true &&

                    <View style={eventStyle.registrationBtnDisabled}>
                        <Text style={eventStyle.registerBtn}>Cancel registration</Text>
                    </View>
                    }


                </View>

                <View style={{width: '100%', margin: 0}}>
                    <Modal style={ModalStyle.modalContainer} visible={this.state.modalVisible}>

                        <TouchableOpacity style={{width: '100%', height: '100%'}} activeOpacity={1}>


                            {this.state.isCancel == false && this.state.isConfirmed == false &&


                            <Confirm eventObject={this.state.event}
                                     availability={this.availableObject}
                                     setRegister={this.setRegister}
                                     setFilterValues={this.setFilterValues}/>
                            }

                            {this.state.isCancel == true &&

                            <Cancel
                                eventObject={this.state.event}
                                availability={this.availableObject}
                                cancelRegister={this.cancelRegister}
                                confirmationObject={this.state.confirmationObject}
                                setFilterValues={this.setFilterValues}/>

                            }

                            {this.state.isCancel == false && this.state.isConfirmed == true &&

                            <Registration
                                setCloseValues={this.setCloseValues}/>

                            }

                        </TouchableOpacity>

                    </Modal>
                </View>

            </View>


        );
    }
}



export default connect(select2)(EventDetail);




// {this.state.isLoadingData == true &&
//
// <Spinner
//     visible={this.state.isLoadingData == true}
//     textContent={'Loading...'}
//     textStyle={{color: '#FFF'}}
// />
// }
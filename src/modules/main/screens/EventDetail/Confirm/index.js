
import React from 'react';

import { View, ListView, StyleSheet, Text, ScrollView, Dimensions,TouchableOpacity, Image, Platform, TouchableWithoutFeedback,ImageBackground, AsyncStorage} from 'react-native';
import { black, borderGrey, white } from 'shared/styles/colors';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { ProximaNova } from 'shared/styles/fonts';
import { NavigationBar } from 'navigationbar-react-native';
import { StackNavigator } from 'react-navigation';
import styles from './styles';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen, isNotchDevice} from 'shared/utils';
import Moment from 'react-moment';
import moment from 'moment';
import { connect } from 'react-redux';
import select from 'modules/main/state/selectors/AboutScreen';
import Config from 'react-native-config';
import * as mainActions from 'modules/main/state/dux';



type $Props = {
    dispatch: *,
    name: ?string,
    showProfile: boolean,
    username: ?string,
    id: ?number,
};




class Confirm extends React.Component {



    constructor(props){
        super(props);


        this.eventObject = props.eventObject
        this.availability = props.availability
        this.setRegister = props.setRegister
        this.chanegToggle = props.chanegToggle
    }


    state = {

        isLoading: false,
        isConfirmed : false,

    };


    componentWillMount() {


        const { dispatch } = this.props;
        dispatch(mainActions.fetchUserInfo());

    }


    makeAppointment(){

        this.setState({

            isLoading: true
        })


        var apiUrl = `${Config.SERVER_URL}/api/v1/appointments/?api_key=UkNNuT0bebf2bRoCMitx`

        console.log("this.availability", this.availability, apiUrl)

        if(this.availability &&  this.availability.availableTimes && this.availability.availableTimes.length > 0) {


            return fetch(apiUrl , {
                method: 'POST',
                headers: new Headers({
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }),
                body: JSON.stringify({service_id: this.eventObject.serviceId,
                    resource_id: this.eventObject.resourceId,
                    location_id: this.eventObject.locationId,
                    end_date_time: this.availability.availableTimes[0].endDateTime,
                    start_date_time: this.availability.availableTimes[0].startDateTime,
                    service_allocation_id: this.eventObject.id
                })
            })
                .then(response => response.json())
                .then(response => {


                    if(!response.error){

                        this.makeBooking(response)

                    }


                })
                .catch(error => {

                    console.log("error", error)
                });

        }
        else {

                this.setState({

                    isLoading: false
                })
        }




    }

    makeBooking(appointmentObject) {

        const {
            dispatch,
            name,
            email,
            showProfile,
            username,
        } = this.props;


        console.log(appointmentObject)

        var apiUrl = `${Config.SERVER_URL}/api/v1/appointments/${appointmentObject.id}/book/?api_key=UkNNuT0bebf2bRoCMitx`


        console.log("apiURL", apiUrl, email, name)
        return fetch(apiUrl , {
            method: 'PUT',
            headers: new Headers({
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify({email: email, first_name: name})
        })
            .then(response => response.json())
            .then(response => {

                console.log()
                console.log("response of booking", response)

                if(!response.error){

                    this.setRegister(true, response)

                }

            })
            .catch(error => {

                console.log("eroor", error)
            });

    }


    numberCalculation(number){

        return  moment(number, "hmm").format("h:mm A").replace(":00", "")
    }


    test(){

        this.chanegToggle(true)
    }


    render() {


        var setFilterValues = this.props.setFilterValues
        console.log("event", this.eventObject)


        return(



         <TouchableWithoutFeedback>

         <View style={{marginTop: isNotchDevice()? 25 : 0, alignItems: 'center', height:224,width:'90%',paddingTop:isIPhoneX()?'75%':'60%',marginLeft:'5%',marginRight:'5%',position:'relative',position:'relative'}}>
            <View style={styles.confirmCont}>

                <View style={styles.closeMain}>
                        <TouchableOpacity style={styles.closeBtn} onPress={() => {setFilterValues(false, false, false)}}>
                            <Image
                                source={require('assets/icons/cancel.png')}
                                style={{ resizeMode: 'contain',width: 15, height: 15, }}
                            />
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.confirmRegistration}>Confirm registration</Text>
                     <Text style={styles.confirmTitle}>{this.eventObject.serviceName}</Text>
                     <View style={styles.timeDate}>
                         <View style={styles.eventTimecont}>
                             <Text style={styles.eventTime}><Moment element={Text} format="ddd, MMM D, YYYY">{this.eventObject.startDate}</Moment></Text>
                         </View>
                         <View style={styles.eventDatecont}>
                                <Text style={styles.eventDate}>{this.numberCalculation(this.eventObject.startTime.toString())} - {this.numberCalculation(this.eventObject.endTime.toString())}</Text>
                         </View>

            </View>


                {this.state.isLoading == false && this.availability &&  this.availability.availableTimes && this.availability.availableTimes.length > 0 &&

                <TouchableOpacity style={styles.registrationBtn}
                                  onPress={() => { this.makeAppointment()}}>
                    <Text style={styles.registerBtn}>Confirm</Text>
                </TouchableOpacity>
                }


                {this.state.isLoading == false && this.availability &&  this.availability.availableTimes && this.availability.availableTimes.length == 0 &&

                <TouchableOpacity style={styles.registrationBtnDisabled}
                                  onPress={() => { this.makeAppointment()}}>
                    <Text style={styles.registerBtn}>Confirm</Text>
                </TouchableOpacity>
                }




                {this.state.isLoading == true &&

                    <TouchableOpacity style={styles.registrationBtn}>

                    <Text style={styles.registerBtn}>Registering...</Text>

                </TouchableOpacity>
                }

            </View>

         </View>

             </TouchableWithoutFeedback>

        );

    }

}


export default connect(select)(Confirm)
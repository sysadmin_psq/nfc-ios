// @flow
import { white } from 'shared/styles/colors';
import { Text, TextInput, View } from 'react-native';
import React from 'react';
import styles from './styles';

type $Props = {
    autoFocus: boolean,
    input: *,
    keyboardType: string,
    label: string,
    secureTextEntry: boolean,
    isEnabled: boolean,
    user_name: string
};

const FormInput = ({
    input: {
        onChange,
        ...rest,
    },
    keyboardType,
    label, isEnabled, user_name
}: $Props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.label}>
                {label}
            </Text>

            {!isEnabled&&

            <TextInput
                autoCapitalize='none'
                keyboardType={keyboardType}
                onChangeText={onChange}
                selectionColor={white}
                underlineColorAndroid='transparent'
                style={styles.textInput}
                {...rest}
            />
            }

            {isEnabled&&
                <Text style={[styles.textInput, styles.userNameField]}>
                    {user_name}
                </Text>

            }

        </View>
    );
};

FormInput.defaultProps = {
    keyboardType: 'default',
};

export default FormInput;

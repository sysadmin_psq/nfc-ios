// @flow
import { lightBlack, white } from 'shared/styles/colors';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    label: {
        color: white,
        fontSize: 16,
        width: 100,
    },
    textInput: {
        backgroundColor: lightBlack,
        fontSize: 16,
        color: white,
        height: 40,
        width: '100%',
    },
    userNameField: {

        marginTop: 20,
    }
});

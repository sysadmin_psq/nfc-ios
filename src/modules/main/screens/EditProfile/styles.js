// @flow
import { black, blue, editProfileGrey, lightBlack, red, white } from 'shared/styles/colors';
import { isIOS } from 'shared/utils';
import { ProximaNova } from 'shared/styles/fonts';
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import { SCREEN_WIDTH } from 'constants';
import { StyleSheet } from 'react-native';

export const checkboxStyle = {
    width: '100%',
    paddingLeft: 30,
    paddingRight: 16,
};

export const checkboxTextStyle = {
    color: white,
    fontSize: 16,
    marginLeft: 48,
    ...ProximaNova.Regular,
};

const text = {
    color: white,
    fontSize: 16,
    ...ProximaNova.Regular,
}

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: lightBlack,
    },
    divider: {
        height: 1,
        width: '100%',
        backgroundColor: editProfileGrey,
    },
    errorBox: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: red,
        height: 50,
    },
    errorText: {
        color: white,
        fontSize: 14,
        ...ProximaNova.Regular,
    },
    headerText: {
        color: white,
        fontSize: 16,
        ...ProximaNova.CondensedBold,
    },
    inputDivider: {
        height: 1,
        marginLeft: 100,
        width: SCREEN_WIDTH - 32 - 100, // horizontal padding and label width
        backgroundColor: editProfileGrey,
    },
    linkText: {
        color: blue,
        fontSize: 16,
        ...ProximaNova.Bold,
    },
    nav: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: isIPhoneX()?80:65,
        paddingTop: isIPhoneX()?50:35,
        padding: 16,
        backgroundColor: black,
    },
    passwordLabel: {
        width: 100,
        ...text,
    },
    passwordResetNotification: {
        width: '100%',
        height: 75,
        flexDirection: 'row',
        paddingHorizontal: '20%',
        backgroundColor: white,
        alignItems: 'center',
    },
    passwordResetText: {
        color: black,
        fontSize: 14,
        textAlign: 'center',
        ...ProximaNova.Regular,
    },
    passwordResetTextEmail: {
        color: black,
        fontSize: 13,
        ...ProximaNova.Bold,
    },
    passwordRow: {
        flexDirection: 'row',
        marginTop: 10,
    },
    privateSection: {
        marginBottom: 10,
    },
    section: {
        marginTop: 24,
        paddingHorizontal: 18,
    },
    sectionHeader: {
        color: white,
        marginBottom: 20,
        fontSize: 16,
        ...ProximaNova.CondensedBold,
    },
    text: {
        ...text,
    },
});

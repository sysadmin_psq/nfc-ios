// @flow
import { lightBlack, white } from "shared/styles/colors";

import { Text, TextInput, View, Picker, Keyboard } from "react-native";
import React, {useRef} from "react";
import styles from "./styles";
import ActionSheet from 'react-native-actionsheet'
// import { Dropdown } from "react-native-material-dropdown";

type $Props = {
  autoFocus: boolean,
  input: *,
  keyboardType: string,
  label: string,
  secureTextEntry: boolean,
  isEnabled: boolean,
  user_name: string
};

const DropDownInput = ({
  input: { onChange, value, ...rest },
  keyboardType,
  label,
  user_name,
  data,
  isEnabled,
  fieldName,
}: $Props) => {

  var arrayData = []

  var stateArray = []
  var genderArray = []
  var age_groupArray = []
  arrayData = data.map(el =>  el.value)

  switch(fieldName) {

      case 'state':
          stateArray = data.map(el => el.value)
          break;

      case 'range':
          age_groupArray = data.map(el => el.value)
          break;

      case 'gender':
          genderArray = data.map(el => el.value)
          break;
      default:
          console.log("ERROR")

  }

  showActionSheet = () => {

        Keyboard.dismiss()
        this.ActionSheet.show()
    }

    showActionSheet2 = () => {

        Keyboard.dismiss()

        this.ActionSheet2.show()
    }

    showActionSheet3 = () => {

        Keyboard.dismiss()

        this.ActionSheet3.show()
    }
    console.log(stateArray, age_groupArray, genderArray)

  return (
      <View style={styles.container}>
          <Text style={styles.label}>{label}</Text>

          {fieldName == 'state' &&

            <View style={{marginTop: 10, marginBottom: 15, width: 300}}>

                  <Text style={styles.innerLabel} onPress={showActionSheet2}>{value}</Text>
                  <ActionSheet

                      ref={o => this.ActionSheet2 = o}
                      title={'Which one do you like ?'}
                      options={stateArray}
                      onPress={(event) => {
                        onChange(stateArray[event]);}
                      }
                  />
            </View>
          }


          {fieldName == 'gender' &&

                <View style={{marginTop: 10, marginBottom: 15}}>

                  <Text style={styles.innerLabel} onPress={this.showActionSheet3}>{value}</Text>
                  <ActionSheet

                     ref={o => this.ActionSheet3 = o}
                     title={'Which one do you like ?'}
                     options={genderArray}
                      onPress={(event) => {
                        onChange(genderArray[event]);}}
                  />
                </View>
          }

          {fieldName == 'range' &&

          <View style={{marginTop: 10, marginBottom: 15}}>

            <Text style={styles.innerLabel} onPress={this.showActionSheet}>{value}</Text>
            <ActionSheet

                ref={o => this.ActionSheet = o}
                title={'Which one do you like ?'}
                options={age_groupArray}
                onPress={(event) => {
                    onChange(age_groupArray[event]);}}
            />
          </View>
          }


      </View>
  );
};

// DropDownInput.defaultProps = {
//   keyboardType: "default"
// };

export default DropDownInput;

// @flow
import { lightBlack, white } from "shared/styles/colors";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center"

  },
  label: {
    color: white,
    fontSize: 14,
    width: 100
  },

  innerLabel: {

      color: white,
      fontSize: 14,
      width: 400
  } ,
  textInput: {
    backgroundColor: lightBlack,
    fontSize: 16,
    color: white,
    height: 40,
    width: "100%"
  },
  userNameField: {
    marginTop: 20
  },

  dropdown: {
    color: white,
    width: "50%"
  }
});


// @flow
import { blue } from "shared/styles/colors";
import { connect } from "react-redux";
import { Field, reduxForm, SubmissionError } from "redux-form";
import { View, Text, ScrollView, Keyboard } from "react-native";
import * as mainActions from "modules/main/state/dux";
import * as sharedActions from "shared/state/dux";
import Api from "api";
import CheckBox from "react-native-check-box";
import React from "react";
import select from "modules/main/state/selectors/EditProfileScreen";
import styles, { checkboxStyle, checkboxTextStyle } from "./styles";
import FormInput from "./FormInput";
import DropDownInput from "./DropDownInput";
import { STATES_NAME, AGE_RANGES, GENDERS } from "../../../../constants/index";
import ActionSheet from 'react-native-actionsheet'

const validate = values => {
    const errors = {};
    console.log("values", values);
    // email validation
    if (!values.email) {
        errors.email = "Please enter an email.";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = "Please enter a valid email";
    }
    // name validation
    if (!values.name) {
        errors.name = "Please enter your name.";
    }
    // username validation
    if (!values.username) {
        errors.username = "Please enter a username.";
    }

    if(values.city == 'N/A' || values.city.length === 0) {

        errors.username = "City cannot be N/A";
    }

    if(values.city.length === 0) {

        errors.username = "City cannot be empty";
    }


    if(values.state == 'N/A') {

        errors.username = "State cannot be N/A.";
    }
    if(values.gender == 'N/A') {

        errors.username = "Gender cannot be N/A.";
    }
    if(values.age_range == 'N/A') {

        errors.username = "Age Range cannot be N/A.";
    }

    return errors;
};
const checkboxText =
    "Let me know if i'm eligible for prizes or challenges and about important announcements";
type $Props = {
    dispatch: *,
    email: string,
    handleSubmit: (*) => void,
    marketingOptIn: boolean,
    showResetPasswordMessage: boolean,
    username: string,
    validationError: ?string
};
const stateMappedData = STATES_NAME.map(el => {
    return {
        value: el.name
    };
});
const ageRangeMappedData = AGE_RANGES.map(el => {
    return {
        value: el.value
    };
});
const genderMappedData = GENDERS.map(el => {
    return {
        value: el.value
    };
});
let EditProfileScreen = ({
                             dispatch,
                             email,
                             handleSubmit,
                             marketingOptIn,
                             showResetPasswordMessage,
                             username,
                             validationError,
                             city,
                             province,
                             age_range,
                             gender
                         }: $Props) => {
    const onSubmit = values => {
        console.log(values);
        const submitToServer = () =>
            dispatch(
                mainActions.editProfile({
                    marketingOptIn,
                    ...values
                })
            );
        if (values.username !== username) {
            return Api.checkUsername(values.username)
                .toPromise()
                .then(response => {
                    if (response.exists) {
                        throw new SubmissionError({
                            error: "That username isn't available. Please pick another."
                        });
                    } else {
                        submitToServer();
                    }
                });
        } else {
            submitToServer();
        }
    };
    return (
        <ScrollView style={styles.container}>
            <View style={styles.nav}>
                <Text
                    onPress={() => dispatch(sharedActions.goBack())}
                    style={[styles.text, styles.navItem]}
                >
                    Cancel
                </Text>
                <Text style={styles.headerText}>EDIT PROFILE</Text>
                <Text
                    onPress={handleSubmit(onSubmit)}
                    style={[styles.text, styles.navItem]}
                >
                    Done
                </Text>
            </View>
            {validationError && (
                <View style={styles.errorBox}>
                    <Text style={styles.errorText}>{validationError}</Text>
                </View>
            )}
            {showResetPasswordMessage && (
                <View style={styles.passwordResetNotification}>
                    <Text style={styles.passwordResetText}>
                        We just emailed a link to reset your password to{" "}
                        <Text style={styles.passwordResetTextEmail}>{email}</Text>
                    </Text>
                </View>
            )}
            <View style={styles.section}>
                <Text style={styles.sectionHeader}>Public Information</Text>
                <Field component={FormInput} label="Name" name="name" />
                <View style={styles.inputDivider} />
                <Field
                    component={FormInput}
                    label="Username"
                    name="username"
                    user_name={username.replace(/\s/g,'')}
                    isEnabled={true}
                />
            </View>
            <View style={styles.divider} />
            <View style={[styles.section, styles.privateSection]}>
                <Text style={styles.sectionHeader}>Private Information</Text>
                <Field
                    component={FormInput}
                    keyboardType="email-address"
                    label="Email"
                    name="email"
                />
                <View style={styles.inputDivider} />
                <Field
                    component={FormInput}
                    // keyboardType="characters"
                    label="City"
                    name="city"
                    returnKeyType="Done"
                />
                <View style={styles.inputDivider} />
                <Field
                    component={DropDownInput}
                    data={stateMappedData}
                    label="State"
                    name="province"
                    value={province}
                    fieldName = "state"
                ></Field>
                <View style={styles.inputDivider} />
                <Field
                    component={DropDownInput}
                    data={ageRangeMappedData}
                    label="Age Range"
                    name="age_range"
                    fieldName="range"
                ></Field>
                <View style={styles.inputDivider} />
                <Field
                    component={DropDownInput}
                    data={genderMappedData}
                    label="Gender"
                    name="gender"
                    fieldName="gender"
                ></Field>
                <View style={styles.inputDivider} />
                <View style={styles.passwordRow}>
                    <Text style={styles.passwordLabel}>Password</Text>
                    <Text
                        onPress={() => dispatch(mainActions.resetPassword())}
                        style={styles.linkText}
                    >
                        Reset Password
                    </Text>
                </View>
            </View>
            <View style={styles.divider} />
            <View style={styles.section}>
                <Text style={styles.sectionHeader}>Communications</Text>
                <CheckBox
                    checkBoxColor={blue}
                    isChecked={marketingOptIn}
                    onClick={() => dispatch(mainActions.toggleMarketingOptIn())}
                    rightText={checkboxText}
                    rightTextStyle={checkboxTextStyle}
                    style={checkboxStyle}
                />
            </View>
        </ScrollView>
    );
};
EditProfileScreen = reduxForm({
    form: "editProfile",
    validate
})(EditProfileScreen);
export default connect(select)(EditProfileScreen);


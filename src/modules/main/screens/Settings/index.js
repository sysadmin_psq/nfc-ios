
import React from 'react';
import { View, ListView, StyleSheet, Text, ScrollView, Dimensions, ActivityIndicator, Platform, TouchableOpacity, Image,Avatar,Button,Alert,ImageBackground} from 'react-native';
import { black, borderGrey, white } from 'shared/styles/colors';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { ProximaNova } from 'shared/styles/fonts';
import { NavigationBar } from 'navigationbar-react-native';
import styles from './styles';
import { connect } from 'react-redux';
import select from 'modules/main/state/selectors/AboutScreen';
import * as mainActions from 'modules/main/state/dux';
import MaterialInitials from 'react-native-material-initials/native';
import * as sharedActions from 'shared/state/dux';
import { MAIN_ROUTES, CHALLENGE_ROUTES } from 'constants';


type $Props = {
    dispatch: *,
    name: ?string,
    showProfile: boolean,
    username: ?string,
    id: ?number,
};



const ComponentLeft = ({navigation}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-start', paddingLeft: 10,}} >
            <TouchableOpacity
                onPress={() => { navigation.navigate(MAIN_ROUTES.CHALLENGE);}}
                style={ {justifyContent:'center', flexDirection: 'row'}}>
                <Image
                    source={require('assets/icons/Back_1.png')}
                    style={{ resizeMode: 'contain', padding: 10 , alignSelf: 'center', zIndex: 999 }}
                />
            </TouchableOpacity>
        </View>
    );
};

const ComponentCenter = () => {
    return(
        <View style={{ flex: 1, }}>

            <Text style={{ color: 'white', ...ProximaNova.Regular, fontSize: 18}}>Settings</Text>
        </View>
    );
};


class Settings extends  React.Component<$Props> {
    /*
     * Removed for brevity
     */
    //

    constructor(){
        super();

    }


    componentWillMount() {

        const { dispatch } = this.props;
        dispatch(mainActions.fetchUserInfo());
    }

    render() {


        const {
            dispatch,
            name,
            email,
            showProfile,
            username,
            id,
        } = this.props;


        console.log("username", email, id)

        return (


            <ScrollView style={styles.settingBackground}>
                <View style={styles.profileNavigation}>
                    <Image source={require('assets/icons/Logo.png')}></Image>

                </View>
                <View style={styles.settingsMain}>
                    <View style={styles.settingProfile}>
                        <View style={styles.profilePic}>
                            <MaterialInitials
                                style={{alignItems: 'center'}}
                                backgroundColor={'#f7f7f7'}
                                color={'#0098D1'}
                                size={50}
                                text={`${name}`}
                                single={false}
                            />
                        </View>
                        <View style={styles.profileName}>
                            <Text style={styles.profileTittle}>{name}</Text>
                            <Text style={styles.profileEmail}>{email}</Text>
                        </View>
                        <View style={styles.editProfileSection}>
                            <TouchableOpacity
                                style={styles.courtbutton}
                                onPress={() => dispatch(sharedActions.navigate(MAIN_ROUTES.EDIT_PROFILE))}
                            ><Text style={styles.editProfile}>Edit</Text></TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.settings}>
                        <View style={styles.classesList}>
                            <TouchableOpacity style={styles.profilebutton}
                                              onPress={() => dispatch(sharedActions.navigate(CHALLENGE_ROUTES.MY_CLASSES, {user_id: id, email: email}))}>
                                <Text>My classes</Text>
                                <Image source={require('assets/images/profile_arrow.png')} style={styles.profilearrow}/>
                            </TouchableOpacity>
                        </View>
                        {/*<View style={styles.classesList}>*/}
                            {/*<TouchableOpacity style={styles.profilebutton}>*/}
                                {/*<Text>Feedback</Text>*/}
                                {/*<Image source={require('assets/images/profile_arrow.png')} style={styles.profilearrow}/>*/}
                            {/*</TouchableOpacity>*/}
                        {/*</View>*/}
                        {/*<View style={styles.classesList}>*/}
                            {/*<TouchableOpacity style={styles.profilebutton}>*/}
                                {/*<Text>Share and refer</Text>*/}
                                {/*<Image source={require('assets/images/profile_arrow.png')} style={styles.profilearrow}/>*/}
                            {/*</TouchableOpacity>*/}
                        {/*</View>*/}
                        <View style={styles.classesList}>
                            <TouchableOpacity style={styles.profilebutton} onPress={() => dispatch(sharedActions.navigate(CHALLENGE_ROUTES.ABOUT))}
                            >
                                <Text>About</Text>
                                <Image source={require('assets/images/profile_arrow.png')} style={styles.profilearrow}/>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.logoutStyle}>
                            <TouchableOpacity style={styles.profilebutton} onPress={() => dispatch(mainActions.logout())}>
                                <Text>Logout</Text>
                                {/* <Image source={require('assets/images/profile_arrow.png')} style={styles.profilearrow}/> */}
                            </TouchableOpacity>
                        </View>
                        {/* <View style={styles.classesList}><Text>Feedback</Text></View>
                                <View style={styles.classesList}><Text>Share and refer</Text></View>
                                <View style={styles.classesList}><Text>About</Text></View>
                                <View style={styles.classesList}><Text>Log out</Text></View> */}

                    </View>
                </View>
            </ScrollView>
        );
    }
}

export default connect(select)(Settings);
import { black, borderGrey, white } from 'shared/styles/colors';
import { Dimensions, StyleSheet, Platform } from 'react-native';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from 'constants';
import { ProximaNova } from 'shared/styles/fonts';
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import { red } from 'ansi-colors';
import { single } from 'rxjs/operator/single';
// import { relative } from 'path';
// import leftPad = require('left-pad');

const { height } = Dimensions.get('window');

export default StyleSheet.create({
    settingBackground:{
        backgroundColor:'white',
        height:'100%',
    },
    settingsScroll:{
        backgroundColor:'white',
        height:'100%',
    },
    profileNavigation:{
        backgroundColor:'#0098D1',
        paddingBottom:7,
        paddingTop:isIPhoneX()?40:22,
        height:isIPhoneX()?80:65,
        alignItems: 'center'
    },
    settingsNav:{
        color:'#ffffff',
        fontSize:18,
        textAlign:'center',
    },
    settingsMain:{
        backgroundColor:'#ffffff',
        height:'100%',
        paddingLeft:20,
        paddingRight:20,
        paddingTop:30,
        height:'100%',
    },
    settingTitle:{
        color:'white',
    },
    settingProfile:{
        flex: 1,
        flexDirection: 'row',
        justifyContent:'space-between',
        flexGrow: 1,
        borderBottomColor: '#f0f0f0',
        borderBottomWidth: 1,
        height:35,
    },
    profilePic:{
        marginRight:20,
        width:50,
        height:50,
        backgroundColor:'#0098d1',
        borderRadius:100,
        flexBasis:50,
    },
    profileImage:{
        width:50,
        height:50,
    },
    profileName:{
        textAlign:'left',
        width:'70%',
        marginTop:5,
    },
    profileTittle:{
        fontSize:20,
        color:'#3b3b3b',
        ...ProximaNova.CondensedBold,
        textAlign:'left'
    },
    profileEmail:{
        color:'#676767',
        fontSize:14,
        ...ProximaNova.Regular,
    },
    editProfile:{
        color:'#0098d1',
        fontSize:16,
        ...ProximaNova.Regular,
    },
    classesList:{
        color:'#3b3b3b',
        fontSize:18,
        borderBottomColor: '#f0f0f0',
        borderBottomWidth: 1,
        width:'99%'
    },
    profilebutton:{
        flex: 1,
        flexGrow: 1,
        flexDirection: 'row',
        justifyContent:'space-between',
        paddingTop:20,
        paddingBottom:20,
    },
    profilearrow:{
        width:'1%',
        height:14,
        paddingRight:10
    },
    logoutStyle:{
        color:'#3b3b3b',
        fontSize:18,
        width:'99%'
    }
});
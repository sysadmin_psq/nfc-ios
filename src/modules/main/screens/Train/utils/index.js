// @flow
export function getDisplayTime({
    audioPlayer,
    currentTime,
}: {
    audioPlayer: Object,
    currentTime: ?number,
}): string {
    // all in seconds
    const totalTime = audioPlayer.getDuration();

    // audio still loading
    if (totalTime === -1) {
        return "";
    }

    let timeLeft = totalTime;
    if (currentTime) {
        timeLeft = totalTime - currentTime;
    }

    const minutes = Math.floor(timeLeft / 60);
    let seconds = Math.floor(timeLeft % 60);
    if (seconds < 10) {
        seconds = `0${seconds}`;
    }

    return `${minutes}:${seconds}`;
}

export function getNextChapterTime(exerciseTimes: Array<number>, currentTime: number): number {
    for (let i = 0; i < exerciseTimes.length; i++) {
        if (currentTime < exerciseTimes[i]) {
            return exerciseTimes[i] ? exerciseTimes[i] : 0;
        }
    }
    // go back to beginning if we're at last chapter
    return 0;
}

export function getPreviousChapterTime(exerciseTimes: Array<number>, currentTime: number): number {
    // add buffer so the user can double tap past an exercise
    const buffer = 2.5; // in seconds

    for (let i = exerciseTimes.length - 1; i >= 0; i--) {
        if (currentTime > (exerciseTimes[i] + buffer)) {
            return exerciseTimes[i];
        }
    }
    // default to catch errors or when the current time is less than the buffer
    return 0;
}

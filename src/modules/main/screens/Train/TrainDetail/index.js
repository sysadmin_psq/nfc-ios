// @flow
import { connect } from 'react-redux';
import { getNextChapterTime, getPreviousChapterTime } from '../utils';
import { Image, View, ScrollView, Text, TouchableOpacity } from 'react-native';
import { isSmallScreen } from 'shared/utils';
import { S3_DJ_SOUND_URL } from 'constants';
import * as actions from 'modules/main/state/dux';
import * as sharedActions from 'shared/state/dux';
import type { $TrainAudioItem } from 'types/state/main';
import React, { Component } from 'react';
import select from 'modules/main/state/selectors/TrainDetail';
import Sound from 'react-native-sound';
import styles from './styles';
import Timer from '../Timer';

import Video from 'shared/components/Video';

type $Props = {
    dispatch: *,
    currentRoutine: $TrainAudioItem,
    audioPaused: boolean,
};

type $State = {
    audioLoaded: boolean,
    audioPlayer: any,
    initialized: boolean,
    playingVideo: boolean,
};

class TrainDetail extends Component<$Props, $State> {
    videoPlayer: *;

    constructor(props: $Props) {
        super(props);
        this.state = {
            audioLoaded: false,
            audioPlayer: null,
            initialized: false,
            playingVideo: true,
        };
        this.videoPlayer = null;
        // allow background play in ios
        Sound.setCategory('Playback');
        Sound.setActive(true);
    }

    componentDidMount() {
      const {
          dispatch,
          currentRoutine: {
              audioFileName,
          },
      } = this.props;
      const soundFileLink = `${S3_DJ_SOUND_URL}${audioFileName}`;
      const audioPlayer = new Sound(soundFileLink, '', error => {
          if (error) {
              dispatch(sharedActions.logError(error));
              return;
          }

          this.setState({
              audioLoaded: true,
              audioPlayer,
          });
      });
    }

    componentWillUnmount() {
        const { audioPaused, dispatch } = this.props;
        const { audioPlayer } = this.state;
        if (audioPlayer) {
            audioPlayer.release();
        }
        if (!audioPaused) {
            dispatch(actions.setTrainAudioPaused(true));
        }
        dispatch(actions.resetTrainState());
    }

    onMediaSwitchPress = () => {
        const { playingVideo } = this.state;

        if (playingVideo) {
            // reset and pause video and go to audio
            this.playAudioAndResetVideo();
        } else {
            // pause audio and go back to beginning of video
            if (this.videoPlayer) {
                this.videoPlayer.onReplayPress();
                this.setState({ playingVideo: true });
            }
            this.pauseAudio();
        }
    }

    onNextPress() {
        const { audioPlayer } = this.state;
        const { exerciseTimes } = this.props.currentRoutine;

        if (audioPlayer) {
            audioPlayer.getCurrentTime(currentTime => {
                const newTime = getNextChapterTime(exerciseTimes, currentTime);
                audioPlayer.setCurrentTime(newTime);
            });
        }
    }

    playAudioAndResetVideo() {
        if (this.videoPlayer) {
            this.setState({ playingVideo: false });

            // make sure paused prop propagates and react-native-video
            // has time to pause video
            setTimeout(() => {
                this.videoPlayer.onReplayPress();
                this.playAudio();
            }, 500);
        }
    }

    pauseAudio = () => {
        const { dispatch } = this.props;
        const {
            audioPlayer,
        } = this.state;

        if (!audioPlayer) {
            return;
        }

        dispatch(actions.setTrainAudioPaused(true));

        audioPlayer.pause();
    }

    playAudio = () => {
        const { dispatch } = this.props;
        const {
            audioPlayer,
            initialized,
        } = this.state;

        if (!audioPlayer) {
            return;
        }

        audioPlayer.play();
        dispatch(actions.setTrainAudioPaused(false));

        if (!initialized) {
            this.setState({ initialized: true });
        }
    }

    onPlayPress() {
        const {
            audioPaused,
            dispatch,
        } = this.props;
        const {
            audioPlayer,
            initialized,
        } = this.state;

        if (!audioPlayer) {
            return;
        }

        if (audioPaused) {
            audioPlayer.play();
            dispatch(actions.setTrainAudioPaused(false));
        } else {
            audioPlayer.pause();
            dispatch(actions.setTrainAudioPaused(true));
        }

        if (!initialized) {
            this.setState({ initialized: true });
        }
    }

    onPreviousPress() {
        const { audioPlayer } = this.state;
        const { exerciseTimes } = this.props.currentRoutine;

        if (audioPlayer) {
            audioPlayer.getCurrentTime(currentTime => {
                const newTime = getPreviousChapterTime(exerciseTimes, currentTime);
                audioPlayer.setCurrentTime(newTime);
            });
        }
    }

    onTutorialEnd = () => {
        this.playAudioAndResetVideo();
    }

    render() {
        const { audioPlayer, playingVideo } = this.state;
        const {
            audioPaused,
            currentRoutine: {
                trainerVideo,
            },
            dispatch,
        } = this.props;

        const videoStyleOverrides = isSmallScreen() ? styles.videoStyleOverrides : {};

        return (
            <ScrollView contentContainerStyle={styles.TrainDetail}>
                <TouchableOpacity
                    style={styles.backButton}
                    onPress={() => dispatch(sharedActions.goBack())}
                >
                    <Image
                        source={require('assets/icons/BackArrow.png')}
                        resizeMode="contain"
                    />
                </TouchableOpacity>
                <Text style={styles.title}>
                    Trainer
                </Text>
                <Video
                    muted={false}
                    onEnd={this.onTutorialEnd}
                    paused={!playingVideo}
                    ref={(ref) => {this.videoPlayer = ref;}}
                    repeat={false}
                    source={trainerVideo}
                    styleOverrides={videoStyleOverrides}
                    fullScreenMode={false}
                />
                <TouchableOpacity
                    onPress={this.onMediaSwitchPress}
                    style={styles.mediaSwitchContainer}
                >
                    {playingVideo && (
                        <Image
                            style={styles.mediaSwitchImage}
                            source={require('assets/icons/SkipToAudioGuideIcon.png')}
                        />
                    )}
                    {!playingVideo && (
                        <Image
                            style={styles.mediaSwitchImage}
                            source={require('assets/icons/ReplayDemoVideoIcon.png')}
                        />
                    )}
                </TouchableOpacity>
                <View style={styles.bottomSection}>
                    {playingVideo && (
                        <View style={styles.controlsOverlay} />
                    )}
                    <Timer audioPlayer={audioPlayer} />
                    <View style={styles.controlRow}>
                        <TouchableOpacity onPress={() => this.onPreviousPress()}>
                            <Image
                                source={require('assets/icons/Previous.png')}
                                style={styles.control}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.pauseButton}
                            onPress={() => this.onPlayPress()}
                        >
                            <Text style={styles.pauseButtonText}>
                                {audioPaused ? 'play' : 'pause'}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.onNextPress()}>
                            <Image
                                source={require('assets/icons/Next.png')}
                                style={styles.control}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

export default connect(select)(TrainDetail);

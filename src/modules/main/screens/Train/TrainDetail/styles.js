// @flow
import {
    blue,
    blueActive,
    greyBlue,
    white,
} from 'shared/styles/colors';
import { sizeStyleSelector,isIPhonex } from 'shared/utils';
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet, Platform } from 'react-native';


export default StyleSheet.create({
    TrainDetail: {
        flex: 1,
        backgroundColor: greyBlue,
        flexDirection: 'column',
        alignItems: 'center',
    },
    //
    backButton: {
        padding: 10,
        position: 'absolute',
        left: 12,
        ...sizeStyleSelector({
            small: {
                top: 4,
            },
            medium: {
                top: 0,
            },
            default: {
                top:isIPhoneX()?40: 20,
            },
        }),
    },
    bottomSection: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    controlRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: 240,
        backgroundColor: 'transparent',
    },
    controlsOverlay: {
        flex: 1,
        backgroundColor: greyBlue,
        position: 'absolute',
        zIndex: 5,
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        opacity: 0.85,
    },
    mediaSwitchContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(3, 40, 58)',
        ...sizeStyleSelector({
            small: {
                width: 280,
                height: 33,
            },
            medium: {
                height: 42,
                width: '100%',
            },
            default: {
                height: 52,
                width: '100%',
            },
        }),
    },
    mediaSwitchImage: {
        height: 18,
        resizeMode: 'contain',
    },
    pauseButton: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: white,
        ...sizeStyleSelector({
            small: {
                width: 54,
                height: 54,
                borderRadius: 27,
            },
            medium: {
                width: 68,
                height: 68,
                borderRadius: 34,
            },
            default: {
                height: 84,
                width: 84,
                borderRadius: 42,
            },
        }),
    },
    pauseButtonText: {
        fontSize: 16,
        color: blueActive,
        ...ProximaNova.CondensedBold,
    },
    title: {
        color: blue,
        paddingTop:40,
        paddingBottom:10,
        ...sizeStyleSelector({
            small: {
                fontSize: 24,
                paddingVertical: 4,
            },
            medium: {
                fontSize: 26,
                // paddingVertical: 8,
            },
            default: {
                fontSize: 32,
                // paddingVertical: 10,
                paddingTop:isIPhoneX()?40:20,
                paddingBottom:10,
            },
        }),
        ...ProximaNova.CondensedSemibold,
    },
    videoStyleOverrides: {
        width: 280,
        height: 280,
        overflow: 'hidden',
    },
});

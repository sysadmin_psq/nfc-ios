// @flow
import { connect } from 'react-redux';
import {
    FlatList,
    ImageBackground,
    ScrollView,
    TouchableOpacity,
    View,
    Platform,
    Image,
    Text,

} from 'react-native';
import React from 'react';
import select from 'modules/main/state/selectors/TrainScreen';
import * as mainActions from 'modules/main/state/dux';
import styles from './styles';
import type { $TrainAudioItem } from 'types/state/main';
import TrainerInfoModal from './TrainerInfoModal/TrainerInfoModal';
import { isIPhone4,isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import { NavigationBar } from 'navigationbar-react-native';
import { black, borderGrey, white, navBarColor } from 'shared/styles/colors';



type $Props = {
    routines: ?$TrainAudioItem[],
    dispatch: *,
};




const ComponentLeft = ({navigation}) => {

    console.log("log", navigation.getParam('userId', ''))
    return(
        <View style={{ flex: 1, alignItems: 'flex-start', paddingLeft: 10,}} >
            <TouchableOpacity
                // onPress={() => { navigation.navigate(CHALLENGE_ROUTES.COURTLIST);}}
                onPress={() => navigation.navigate(CHALLENGE_ROUTES.COURTLIST)}
                style={ {justifyContent:'center', flexDirection: 'row'}}>
                <Image
                    source={require('assets/icons/Back_1.png')}
                    style={{ resizeMode: 'contain', padding: 10 , alignSelf: 'center', zIndex: 999 }}
                />
            </TouchableOpacity>
        </View>
    )
};

const ComponentCenter = ({state}) => {


    return(
        <View style={{paddingTop:isIPhoneX()?36:15  }}>

            <Image source={require('assets/icons/Logo.png')} />

        </View>
    );
};

const ComponentRight = ({navigation}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-end',}}>


        </View>
    );
};




class TrainScreen extends React.Component<$Props> {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const {
            dispatch,
            routines,
        } = this.props;

        if (!routines) {
            dispatch(mainActions.fetchTrainAudio());
        }

    }

    render() {
        const {
            dispatch,
            routines,
        } = this.props;

        if (!routines) {
            return (
                <View style={styles.Train} />
            );
        }

        return (
            <View style={styles.Train}>

                <NavigationBar
                    componentCenter   = { () =>  <ComponentCenter /> }
                    componentRight   = { () =>  <ComponentRight navigation={this.props.navigation}/> }
                    navigationBarStyle= {{ backgroundColor: navBarColor, height:isIPhoneX()?80:65,marginTop: Platform.OS === 'ios'? -20 : 0}}
                    statusBarStyle    = {{ barStyle: 'light-content', backgroundColor: '#215e79'}}
                />

                <TrainerInfoModal navigationRoute={this.props.navigation}/>
                <FlatList
                    style={styles.Train}
                    contentContainerStyle={styles.listContainer}
                    data={routines}
                    keyExtractor={(item, idx) => `${item.sponsorLink}_${item.audioFileName}_${idx}`}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity
                              style={styles.audioFileContainer}
                              onPress={() => {
                                  dispatch(mainActions.setCurrentRoutineIdx(index));
                                  dispatch(mainActions.setShowTrainModal({showTrainModal: true, navigationRoute: false}))
                              }}
                            >
                                <ImageBackground
                                    style={styles.audioFileBackground}
                                    source={{ uri: item.thumbnailImage }}
                                    resizeMode="contain"
                                />
                            </TouchableOpacity>
                        );
                    }}
                />
            </View>
        );
    }
}

export default connect(select)(TrainScreen);

// @flow
import { white } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';
import { SCREEN_WIDTH } from 'constants';


export default StyleSheet.create({
    Train: {
        flex: 1,
        backgroundColor: white,
    },
    //
    audioFileContainer: {
        width: SCREEN_WIDTH - 40, // padding of list container
        height: (SCREEN_WIDTH - 40) / (3 / 2), // 3:2 aspect ratio
        marginBottom: 21,
        alignItems: 'center',
    },
    audioFileBackground: {
      flex: 1,
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    audioFileText: {
        width: '60%',
        fontSize: 40,
        color: white,
        letterSpacing: 0.9,
        textAlign: 'center',
        ...ProximaNova.CondensedBold,
    },
    audioFileIcon: {
        margin: '1 auto',
    },
    listContainer: {
        backgroundColor: white,
        alignItems: 'center',
        padding: 20,
    },
});

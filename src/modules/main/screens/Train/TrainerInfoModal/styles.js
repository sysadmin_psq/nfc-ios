// @flow
import { StyleSheet } from 'react-native';
import { blueActive, mediumBlue, white } from 'shared/styles/colors';
import { deviceStyleSelector } from 'shared/utils';
import { ProximaNova } from 'shared/styles/fonts';

export default StyleSheet.create({
    buttonStyle: {
        position: 'absolute',
        left: 18,
        padding: 8,
        ...deviceStyleSelector({
            android: {
                top: 18,
            },
            ios: {
                top: 38,
            },
            iphoneX: {
                top: 38,
            },
        }),
    },
    imageBackground: {
        flex: 1,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingBottom: '8%',
    },
    modalContainer: {
        flex: 1,
    },
    startButton: {
        height: 120,
        width: 120,
        borderRadius: 60,
        backgroundColor: white,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    startButtonText: {
        fontSize: 28,
        color: blueActive,
        ...ProximaNova.CondensedBold,
    },
    trainerInfoLink: {
        padding: 15,
        marginRight: 5,
    },
    trainerInfoSection: {
        ...deviceStyleSelector({
            android: {
                height: 90,
            },
            ios: {
                height: 90,
            },
            iphoneX: {
                height: 110,
            },
        }),
        backgroundColor: mediumBlue,
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 16,
    },
    trainerInfoLeftSection: {
    },
    trainerInfoTrainerName: {
        fontSize: 24,
        color: white,
        ...ProximaNova.CondensedBold,
        ...deviceStyleSelector({
            android: {
                marginBottom: 0,
            },
            ios: {
                marginBottom: 0,
            },
            iphoneX: {
                marginBottom: 4,
            },
        }),
    },
    trainerInfoTrainerTitle: {
        fontSize: 14,
        color: white,
        ...ProximaNova.Regular,
    },
});

// @flow
import { MAIN_ROUTES, CHALLENGE_ROUTES } from 'constants';
import { connect } from 'react-redux';
import { Image, ImageBackground, Linking, Modal, Text, TouchableOpacity, View } from 'react-native';
import * as mainActions from 'modules/main/state/dux';
import * as sharedActions from 'shared/state/dux';
import CloseButton from 'shared/components/CloseButton';
import React from 'react';
import select from './select';
import styles from './styles';
import type { $TrainAudioItem } from 'types/state/main';


type $Props = {
    currentRoutine: $TrainAudioItem,
    navigationRoute: any,
    dispatch: any,
    isVisible: boolean
};

const TrainerInfoModal = ({
    currentRoutine,
    dispatch,
    isVisible,
    navigationRoute
}: $Props) => {
    if (!isVisible) {
        return null;
    }

    const {
        trainerName,
        trainerTitle,
        trainerImage,
        trainerExternalLink,
        audioFileName,
        seconds_to_skip

    } = currentRoutine;


    console.log("currentRoutine", seconds_to_skip)

    return (
        <Modal
          visible={isVisible}
          supportedOrientations={['portrait', 'landscape', 'landscape-left', 'landscape-right']}
          animationType="fade"
        >
            <View style={styles.modalContainer}>
                <ImageBackground
                    style={styles.imageBackground}
                    source={{uri: trainerImage}}
                >
                    <CloseButton
                      style={styles.buttonStyle}
                      onPress={() => dispatch(mainActions.setShowTrainModal(false))}
                    />

                    {audioFileName != null &&

                    <TouchableOpacity
                        style={styles.startButton}
                        onPress={() => {

                            dispatch(mainActions.setShowTrainModal(false));
                            dispatch(sharedActions.navigate(MAIN_ROUTES.TRAIN_DETAIL));
                        }}
                    >
                        <Text style={styles.startButtonText}>
                            Start
                        </Text>
                    </TouchableOpacity>
                    }

                    {audioFileName == null &&

                    <TouchableOpacity
                        style={styles.startButton}
                        onPress={() => {

                            dispatch(mainActions.setShowTrainModal(false));
                            dispatch(sharedActions.navigate(CHALLENGE_ROUTES.TRAIN_VIDEO, {navigationRoute: navigationRoute}))

                        }}
                    >
                        <Text style={styles.startButtonText}>
                            Start
                        </Text>
                    </TouchableOpacity>
                    }

                </ImageBackground>
                <View style={styles.trainerInfoSection}>
                    <View style={styles.trainerInfoLeftSection}>
                        <Text style={styles.trainerInfoTrainerName}>
                            {trainerName}
                        </Text>
                        <Text style={styles.trainerInfoTrainerTitle}>
                            {trainerTitle}
                        </Text>
                    </View>
                    <TouchableOpacity
                        style={styles.trainerInfoLink}
                        onPress={() => Linking.openURL(trainerExternalLink)}
                    >
                        <Image source={require('assets/icons/ExternalLink.png')} />
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    );
};

export default connect(select)(TrainerInfoModal);

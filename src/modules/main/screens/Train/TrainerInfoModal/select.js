// @flow
import type { $MainState } from 'types/state/main';

export default ({
    main: {
        train: {
            currentRoutineIdx,
            routines,
            showTrainModal,
        },
    },
}: { main: $MainState}) => {
    let currentRoutine;
    if (routines) {
        currentRoutine = routines[currentRoutineIdx];
    }

    return {
        isVisible: showTrainModal,
        currentRoutine,
    };
}

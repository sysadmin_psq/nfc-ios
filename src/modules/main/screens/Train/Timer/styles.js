// @flow
import { transparent, white } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';
import { sizeStyleSelector } from 'shared/utils';


export default StyleSheet.create({
    Timer: {
        backgroundColor: transparent,
        color: white,
        ...sizeStyleSelector({
            small: {
                lineHeight: 24,
                fontSize: 24,
            },
            medium: {
                lineHeight: 50,
                fontSize: 50,
            },
            default: {
                lineHeight: 64,
                fontSize: 64,
            },
        }),
        ...ProximaNova.Medium,
    },
})

// @flow
import { connect } from 'react-redux';
import { getDisplayTime } from '../utils';
import { Text } from 'react-native';
import React from 'react';
import select from 'modules/main/state/selectors/Timer';
import styles from './styles';

type $Props = {
    audioPlayer: ?*,
}

type $State = {
    currentTime: ?number,
}

class Timer extends React.Component<$Props, $State> {
    interval: *;

    constructor(props) {
        super(props);
        this.state = {
            currentTime: null,
        };
    }

    getCurrentTime = () => {
        const { audioPlayer } = this.props;

        if (audioPlayer) {
            audioPlayer.getCurrentTime((currentTime) => {
                this.setState({ currentTime });
            });
        }
    };

    componentDidMount() {
        this.interval = setInterval(() => this.getCurrentTime(), 500);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        const { audioPlayer } = this.props;
        const { currentTime } = this.state;

        let displayTime = '0:00';
        if (audioPlayer && currentTime && currentTime > 0) {
            displayTime = getDisplayTime({ audioPlayer, currentTime });
        }

        return (
            <Text style={styles.Timer}>
                {displayTime}
            </Text>
        );
    }
}


export default connect(select)(Timer);

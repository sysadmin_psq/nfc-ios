// @flow
import { blue, white } from "shared/styles/colors";
import { ProximaNova } from "shared/styles/fonts";
import { StyleSheet } from "react-native";
export default StyleSheet.create({
    button: {
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: blue,
        height: 50,
        marginBottom: 8,
        borderRadius: 4
    },
    buttonText: {
        fontSize: 16,
        color: white,
        ...ProximaNova.Bold
    }
});
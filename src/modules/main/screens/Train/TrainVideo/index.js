import { connect } from 'react-redux';
import { Image, View, ScrollView, Text, TouchableOpacity, Platform, Dimensions, SafeAreaView } from 'react-native';
import { isIPhone4,isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen } from 'shared/utils';
import { S3_DJ_SOUND_URL, MAIN_ROUTES } from 'constants';
import * as actions from 'modules/main/state/dux';
import * as sharedActions from 'shared/state/dux';
import type { $TrainAudioItem } from 'types/state/main';
import React, { Component } from 'react';
import select from 'modules/main/state/selectors/TrainDetail';
import Sound from 'react-native-sound';
import styles from './styles';
import Timer from '../Timer';
import Video from 'shared/components/Video';
import ReactNativeVideo from 'react-native-video';
import {navigation} from 'react-navigation'
import VideoPlayer from 'react-native-video-controls';
import { black, borderGrey, white, navBarColor } from 'shared/styles/colors';
import Orientation from 'react-native-orientation';


type $Props = {
    dispatch: *,
    currentRoutine: $TrainAudioItem,
};

type $State = {
    initialized: boolean,
    playingVideo: boolean,
};



class TrainVideo extends Component<$Props, $State> {
    videoPlayer: *;


    constructor(props: $Props) {
        super(props);
        this.state = {
            initialized: false,
            playingVideo: true,
            skippedClicked: false
        };
        this.videoPlayer = null;
        this.player = null
        // allow background play in ios
    }

    componentWillMount() {


        Orientation.addOrientationListener(this._orientationDidChange);


    }

    componentDidMount() {
        const initial = Orientation.getInitialOrientation();

        Orientation.lockToLandscape();
    }
    componentWillUnmount() {
        Orientation.lockToPortrait();
    }

    _orientationDidChange = (orientation) => {

        if (orientation === 'LANDSCAPE') {
            this.setState({
                marginLeftInLandscape : isIPhoneX() ? -50 : 0,
                marginRightInLandscape : isIPhoneX() ? -55 : 0,

            })
        } else {

            this.setState({
                marginLeftInLandscape : 0,
                marginRightInLandscape : 0

            })

            // do something with portrait layout
        }
    }

    onSeekVideo = () => {

        const {
            currentRoutine: {
                seconds_to_skip
            },
        } = this.props;

        this.setState({
            skippedClicked: true
        });
        this.player.player.ref.seek(seconds_to_skip);
    };
    onProgress = e => {
        const {
            currentRoutine: {
                seconds_to_skip
            },
        } = this.props;

        if (e.currentTime > seconds_to_skip && this.state.skippedClicked == false) {
            this.setState({
                skippedClicked: true
            });
        }
    };



    dismissFunction(navigationRoute) {

        // console.log("isndie the dismiss fucntion in the TrainVideo", navigationRoute)
        navigationRoute.navigate(MAIN_ROUTES.TRAIN);

    }

    render() {

        const { playingVideo } = this.state;
        const {
            currentRoutine: {
                trainerVideo,
                seconds_to_skip
            },
        } = this.props;



        return (


            <View

                supportedOrientations={['portrait', 'landscape', 'landscape-left', 'landscape-right']}
                style={{height:'100%', backgroundColor: black}}>


                <VideoPlayer
                    source={{ uri: trainerVideo}}
                    navigator={ this.props.navigation }
                    fullscreenOrientation={true}
                    style={{ backgroundColor: black,marginTop: Platform.OS === 'ios'? 30 : 0,  width: "100%"}}
                    onProgress={this.onProgress}
                    ref={ref => {
                        this.player = ref;
                    }}
                    onLoad={() => {
                        this.setState({
                            skippedClicked: false
                        });
                    }}
                />

                {!this.state.skippedClicked && (
                    <TouchableOpacity
                        // disable={submitting}
                        style={styles.button}
                        onPress={this.onSeekVideo}
                    >
                        <Text style={styles.buttonText}>Skip</Text>
                    </TouchableOpacity>
                )}

            </View>

        );
    }


}


export default connect(select)(TrainVideo);



{/*<Video*/}
{/*muted={false}*/}
{/*onEnd={this.onTutorialEnd}*/}
{/*paused={!playingVideo}*/}
{/*ref={(ref) => {this.videoPlayer = ref;}}*/}
{/*repeat={false}*/}
{/*source={trainerVideo}*/}
{/*styleOverrides={{width:'100%', height:'100%'}}*/}
{/*fullScreenMode={true}*/}
{/*dismissFunction={this.dismissFunction}*/}
{/*navigationRoute={this.props.navigation}*/}
{/*/>*/}

//height:isIPhoneX()?80:65
// @flow
import { SCREEN_WIDTH, VIDEO_MARGIN, VIDEO_PADDING_BOTTOM } from 'constants';
import { StyleSheet } from 'react-native';

// any styles that influence the height of the component should be in styleConstants
// for autoplay calculations
export default StyleSheet.create({
    container: {
        width: '100%',
        paddingBottom: VIDEO_PADDING_BOTTOM,
        backgroundColor: '#eee',
        height: SCREEN_WIDTH,
        marginTop: VIDEO_MARGIN,
    },
    menuDotsContainer: {
        position: 'absolute',
        top: 0,
        right: 4,
    },
});

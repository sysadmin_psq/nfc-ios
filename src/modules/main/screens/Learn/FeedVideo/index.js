// @flow
import { VIDEO_COMPONENT_HEIGHT } from 'constants';
import { View, TouchableOpacity } from 'react-native';
import React from 'react';
import styles from './styles';
import Video from 'shared/components/Video';
import withAutoplay from 'shared/hocs/withAutoplay';

type $Props = {
    index: number, // eslint-disable-line
    item: {
        id: number,
        url: string,
    },
    onLoad: () => void,
    scrollY: number, // eslint-disable-line
    shouldAutoplay: boolean,
    shouldRender: boolean,
}

const FeedVideo = ({ item, onLoad, shouldAutoplay, shouldRender }: $Props) => (
    <TouchableOpacity style={styles.container}>
        {shouldRender && (
            <Video
                onLoad={onLoad}
                paused={!shouldAutoplay}
                source={item.url}
                withControls={shouldAutoplay}
            />
        )}
    </TouchableOpacity>
);

export default withAutoplay(FeedVideo, VIDEO_COMPONENT_HEIGHT);

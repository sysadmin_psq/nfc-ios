// @flow
import { StyleSheet } from "react-native";
import { black, white } from "shared/styles/colors";
import { ProximaNova } from "shared/styles/fonts";

export default StyleSheet.create({
    learncontainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",

    },
    flatlistContentContainer: {
        paddingBottom: 100,
        backgroundColor: white,
        minHeight: '100%',
    },
    headerText: {
        paddingVertical: 12,
        fontSize: 13,
        color: black,
        ...ProximaNova.Bold,
    },
    label: {
        fontSize: 22,
        fontWeight: "bold",
        color: black,
    },
});

// @flow
import { blue } from 'shared/styles/colors';
import { connect } from 'react-redux';
import { FEED_LOCATIONS, PAGINATION } from 'constants';
import { FlatList, RefreshControl, View, Platform, Image, Text, AsyncStorage, AppState, Alert} from 'react-native';
import { Subject } from 'rxjs';
import * as mainActions from 'modules/main/state/dux';
import FeedVideo from './FeedVideo';
import { isIPhone4,isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import React, { Component } from 'react';
import select from 'modules/main/state/selectors/LearnScreen';
import SplashScreen from 'react-native-splash-screen';
import styles from './styles';
import type { $Video } from 'types/state/main';
import { NavigationBar } from 'navigationbar-react-native';
import AppUpdate from 'react-native-appupdate'
import { black, borderGrey, white, navBarColor } from 'shared/styles/colors';
import * as sharedActions from "shared/state/dux";
import { MAIN_ROUTES } from "constants";
import Orientation from 'react-native-orientation';



type $Props = {
    dispatch: *,
    videos: Array<$Video>,
    refreshing: boolean,
    renderFeed: boolean,
    userInfo: Object
};

type $State = {
    scrollY: number,
};




const ComponentLeft = ({navigation}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-start', paddingLeft: 10,}} >

        </View>
    )
};

const ComponentCenter = ({state}) => {


    return(
        <View style={{ paddingTop:isIPhoneX()?36:15  }}>

            <Image source={require('assets/icons/Logo.png')} />

        </View>
    );
};

const ComponentRight = ({navigation}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-end',}}>


        </View>
    );
};

class LearnScreen extends Component<$Props, $State> {
    flatList: ?FlatList;
    onScroll$: *;
    subscription: *;

    constructor(props) {
        super(props);
        this.state = {
            scrollY: 0,
        };

        this.onScroll$ = new Subject();
    }



    _orientationDidChange = (orientation) => {

        if (orientation === 'LANDSCAPE') {

            Orientation.lockToPortrait();
        } else {

            // do something with portrait layout
        }
    }

    tagSymbols() {
        return new AppUpdate({
            iosAppId: '563421203',
            apkVersionUrl: 'https://github.com/version.json',
            needUpdateApp: (needUpdate) => {

                console.log("needUpdate", needUpdate)
                Alert.alert(
                    'Update Available',
                    'A new version of Fitness Court is available. Please update',
                    [
                        {text: 'Cancel', onPress: () => {

                            AsyncStorage.setItem('alreadyLaunched', JSON.stringify('yes'));

                             this.props.dispatch(mainActions.fetchLearnFeed())

                        }},
                        {text: 'Update', onPress: () => needUpdate(true)}
                    ]
                );
            },
            forceUpdateApp: () => {
                console.log("Force update will start")
            },
            notNeedUpdateApp: () => {
                console.log("App is up to date")


                this.props.dispatch(mainActions.fetchLearnFeed())
            },
            downloadApkStart: () => { console.log("Start") },
            downloadApkProgress: (progress) => { console.log(`Downloading ${progress}%...`) },
            downloadApkEnd: () => { console.log("End") },
            onError: () => { console.log("downloadApkError") }
        });

    }





    componentWillMount() {

        SplashScreen.hide();
        Orientation.lockToPortrait();
        this.props.dispatch(mainActions.fetchLearnFeed())
        var appupdate = this.tagSymbols()
        console.log("this is symbols", this.tagSymbols)



        AsyncStorage.getItem("alreadyLaunched").then(value => {

            console.log('alreadyLauncehd', value)
            if(value == null || value == JSON.stringify('no')){

                appupdate.checkUpdate()
                AsyncStorage.setItem('alreadyLaunched', JSON.stringify('yes'));

            }
            else{

                console.log("inide the else")

            }})

    }

    componentDidMount() {

        Orientation.addOrientationListener(this._orientationDidChange);
    }

    // componentDidMount() {
    //     SplashScreen.hide();
    //
    //     // const { userInfo } = this.props;
    //     this.props.dispatch(mainActions.fetchLearnFeed());
    //
    //     // if (userInfo) {
    //     //     if (
    //     //         userInfo.city === "NA" ||
    //     //         userInfo.province === "NA" ||
    //     //         userInfo.age_range === "NA" ||
    //     //         userInfo.gender === "NA"
    //     //     ) {
    //     //         this.props.dispatch(sharedActions.navigate(MAIN_ROUTES.EDIT_PROFILE));
    //     //     }
    //     // }
    //
    //     console.log("Learn screen props", this.props)
    //     // const { userInfo } = this.props;
    //     AppState.addEventListener('change', this.checkAppState);
    //
    //
    //     this.subscription = this.onScroll$
    //         .throttleTime(300)
    //         .subscribe(scrollY => this.setState({scrollY}));
    // }


    // componentDidUpdate(prevProps) {
    //     console.log("I am runned ", prevProps.userInfo);
    //     // const { userInfo } = prevProps;
    //     if (userInfo) {
    //         if (
    //             userInfo.city === "NA" ||
    //             userInfo.province === "NA" ||
    //             userInfo.age_range === "NA" ||
    //             userInfo.gender === "NA"
    //         ) {
    //             this.props.dispatch(sharedActions.navigate(MAIN_ROUTES.EDIT_PROFILE));
    //         }
    //
    //         console.log(prevProps.userInfo);
    //     }
    // }
    //
    //
    // componentWillUnmount() {
    //
    //
    //
    //     if (this.subscription) {
    //         this.subscription.unsubscribe();
    //     }
    //     AppState.removeEventListener('change', this.checkAppState);
    //
    // }


    componentDidMount() {
        SplashScreen.hide();
        const { userInfo } = this.props;
        this.props.dispatch(mainActions.fetchLearnFeed());
        if (userInfo) {
            if (
                userInfo.city === "N/A" ||
                userInfo.province === "N/A" ||
                userInfo.age_range === "N/A" ||
                userInfo.gender === "N/A"
            ) {
                this.props.dispatch(sharedActions.navigate(MAIN_ROUTES.EDIT_PROFILE));
            }
        }

        AppState.addEventListener('change', this.checkAppState);

        this.subscription = this.onScroll$
            .throttleTime(300)
            .subscribe(scrollY => this.setState({ scrollY }));
    }
    componentDidUpdate(prevProps) {
        console.log("I am runned ", prevProps.userInfo);
        const { userInfo } = prevProps;
        if (userInfo) {
            if (
                userInfo.city === "N/A" ||
                userInfo.province === "N/A" ||
                userInfo.age_range === "N/A" ||
                userInfo.gender === "N/A"
            ) {
                this.props.dispatch(sharedActions.navigate(MAIN_ROUTES.EDIT_PROFILE));
            }
            console.log(prevProps.userInfo);
        }
    }
    componentWillUnmount() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }

        AppState.removeEventListener('change', this.checkAppState);

    }

    checkAppState = (state) =>  {

        console.log("did mount", AppState.currentState, state)

        if(AppState.currentState === 'active') {



            AsyncStorage.getItem("alreadyLaunched").then(value => {

                console.log('alreadyLauncehd', value)
                if(value == null || value == JSON.stringify('no')){


                    var appupdate = this.tagSymbols()
                    appupdate.checkUpdate()
                    console.log("did mount", AppState.currentState)
                    AsyncStorage.setItem('alreadyLaunched',  JSON.stringify('yes')); // No need to wait for `setItem` to finish, although you might want to handle errors
                }
                else{

                    // navigator.geolocation.getCurrentPosition((position) => {
                    //
                    //     this.fetchCourtData(position.coords.latitude, position.coords.longitude)
                    //
                    // }, (error) => {
                    //     // alert(JSON.stringify(error))
                    //     // RNSettings.openSetting(RNSettings.ACTION_LOCATION_SOURCE_SETTINGS).
                    //     // then((result) => {
                    //     //     if (result === RNSettings.ENABLED) {
                    //     //         console.log('location is enabled')
                    //     //     }
                    //     // })
                    //     this.setState({
                    //
                    //         locationAvailable: true,
                    //         timeOutState: true
                    //     })
                    // }, {
                    //     enableHighAccuracy: true,
                    // });

                }})


        }
        else {

            AsyncStorage.setItem('alreadyLaunched',  JSON.stringify('no')); // No need to wait for `setItem` to finish, although you might want to handle errors

            console.log("else mount", AppState.currentState)


        }

    }

    onScroll(event) {
        this.onScroll$.next(event.nativeEvent.contentOffset.y);
    }

    render() {
        const {
            dispatch,
            refreshing,
            renderFeed,
            videos,
            userInfo
        } = this.props;

        console.log(this.props)

        return (
            <View style={styles.learncontainer}>

                <NavigationBar
                    componentCenter   = { () =>  <ComponentCenter /> }
                    componentRight   = { () =>  <ComponentRight navigation={this.props.navigation}/> }
                    navigationBarStyle= {{ backgroundColor: navBarColor, height:isIPhoneX()?80:65,marginTop: Platform.OS === 'ios'? -20 : 0}}
                    statusBarStyle    = {{ barStyle: 'light-content', backgroundColor: '#215e79'}}
                />

                {renderFeed && (
                    <FlatList
                        contentContainerStyle={styles.flatlistContentContainer}
                        ref={(instance) => {this.flatList = instance}}
                        data={videos}
                        keyExtractor={item => item.id}
                        onEndReached={() => dispatch(mainActions.fetchLearnFeed({
                            overwrite: false,
                            pagination: PAGINATION.INCREMENT,
                            position: FEED_LOCATIONS.BOTTOM,
                        }))}
                        onEndReachedThreshold={0}
                        onScroll={event => this.onScroll(event)}
                        refreshControl={
                        <RefreshControl
                            onRefresh={() => dispatch(mainActions.fetchLearnFeed({
                                overwrite: true,
                                pagination: PAGINATION.RESET,
                                position: FEED_LOCATIONS.TOP,
                            }))}
                            refreshing={refreshing}
                            tintColor={blue}
                            colors={[blue]}
                        />
                    }
                        renderItem={({item, index}) => (
                            <FeedVideo
                                item={item}
                                index={index}
                                scrollY={this.state.scrollY}
                            />
                        )}
                        style={{width: '100%'}}
                    />
                )}
            </View>
        );
    }
}

export default connect(select)(LearnScreen);


import React from 'react';
import { View, ListView, StyleSheet, Text, ScrollView, Dimensions, ActivityIndicator, AsyncStorage, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { black, borderGrey, white } from 'shared/styles/colors';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { ProximaNova } from 'shared/styles/fonts';
import { AUTH_ROUTES, STORAGE_USER_SESSION, MAIN_ROUTES, CHALLENGE_ROUTES } from 'constants';
import ChallengePanel from 'modules/main/screens/Challenge/ChallengePanel';
import courtStyles from 'modules/main/screens/Courts/styles';
import Config from 'react-native-config'
import Moment from 'react-moment';
import moment from 'moment';
import { connect } from 'react-redux';
import select from 'modules/main/state/selectors/LocationScreen';
var _ = require('lodash');



type $Props = {
    courts: Array<$FitnessCourt>,
    locationsEnabled: boolean,
};


const styles = StyleSheet.create({

    horizontalScrollContainer: {
        backgroundColor: white,

    },
    courtdate: {
        fontSize:12,
        color:'#ffffff',
        width:'70%',
        ...ProximaNova.CondensedSemibold,
    },
    courttime: {
        fontSize:12,
        color:'#ffffff',
        width:'30%',
        ...ProximaNova.CondensedSemibold,
    },
    upcomingContainer: {
        alignItems: 'center',
        flex: 10,
        flexDirection:'row',
        marginStart: 20,
        marginBottom: 0,
    },
    courtcontainer:{
        minHeight:'100%',
    },
    emptycontainer: {
        flex: 1,
        backgroundColor: white,
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        minHeight:'100%',
    },
    noEnteries:{
        fontSize:20,
        color:'#979797'
    }
});





type $Props = {
    courts: Array<$FitnessCourt>,
    locationsEnabled: boolean,
};


class Upcoming extends React.Component {


    /*
     * Removed for brevity
     */
    //

    constructor(props) {
        super(props);

        this.state = {
            dataSource: [],
            isLoadingData: true
        }

        this.user_id = props.user_id
        this.email = props.email
        this.navigation = props.navigation
    }

    componentWillMount(){


        this.fetchData()
    }


    fetchData = () => {


        var today = moment(new Date()).format('YYYY-MM-DD')
        var apiUrl = `${Config.SERVER_URL}/api/v1/appointments/?api_key=UkNNuT0bebf2bRoCMitx&start_date=${today}&email=${this.email}&status=BK`

        return fetch(apiUrl , {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
            .then(response => response.json())
            .then(response => {


                console.log("upcoming data ", response.data, apiUrl)
                var sortArray = _.sortBy(response.data, function(o){return o.startDateTime})
                this.setState({

                    dataSource: sortArray,
                    isLoadingData: false
                })
            });
    }


    numberCalculation(date, timezone){


        return moment(date).utcOffset(-420).format('h:mm A').replace(":00", "")
    }


    numberToDateCalculation(date, timezone) {

        return moment(date).utcOffset(-420).format('ddd, MMM D, YYYY')
    }

    goToFunction(navigation, value, otherCourts) {


        if(value.serviceAllocationId != '') {

            var court = _.find(otherCourts, function(o){
                console.log(" our id:", o.location_id)
                   console.log("their locartion", value.locationId)
                return o.location_id == value.locationId})

            // console.log("court in gotTo", court, value.location_id)
            var today = moment(new Date()).format("YYYY-MM-DD")

            if(court) {

                var allociationsApi = `${Config.SERVER_URL}/api/v1/service/allocations/${value.serviceAllocationId}/?api_key=UkNNuT0bebf2bRoCMitx`

                return fetch(allociationsApi , {
                    headers: new Headers({
                        'Content-Type': 'application/json'
                    })
                })
                    .then(allocationObject => allocationObject.json())
                    .then(allocationObject => {

                        console.log(allocationObject)

                        this.navigation.navigate(CHALLENGE_ROUTES.EVENTDETAIL, {court: court, appointmentObject: value, event: allocationObject, isPastEvent: false})

                    }).catch(err => {

                        console.log("err", err)
                    })

            }



        }




    }



    render() {

        const [ ...otherCourts ] = this.props.courts;
        // console.log("otherCOurts in upcoming", otherCourts)


        return (



            <View style={styles.container}>
            <ScrollView style={{minHeight:'100%'}}>


                {this.state.isLoadingData == true &&




                <View style={courtStyles.courtcontainer}>
                    <View style={courtStyles.courtbody} >
                        {/*need to check this*/}
                        <ActivityIndicator style={{height: 100, backgroundColor: white, flex: 1, height: '100%'}} size="large" color="#127DAD" />

                    </View>
                </View>
                }


                {this.state.dataSource.length == 0 && this.state.isLoadingData == false &&


                <View style={styles.emptycontainer}>

                {/* <Image
                        source={require('assets/images/No_Entries.png')}>
                   </Image> */}<Text style={styles.noEnteries}>No Upcoming Classes</Text>

                    </View>
                }


{this.state.dataSource.length > 0 && this.state.isLoadingData == false &&

                    <View style={courtStyles.courtcontainer}>
                        <View style={courtStyles.courtbody} >
                            {this.state.dataSource.map(value => (
                                <View>


                                    <TouchableOpacity   onPress={() => { this.goToFunction(this.navigation, value, otherCourts)}}>

                                        <ImageBackground source={{uri: value.resourceImageUrl}} style={courtStyles.courtCard}>
                                        <View>
                                            <ImageBackground
                                                style={courtStyles.courtCardbg}
                                                source={require('assets/images/bgTransparent.png')} >
                                            <Text style={courtStyles.cardtitle} numberOfLines={1}>{value.serviceName.toUpperCase()}</Text>
                                            <Text style={courtStyles.cardtrainer}>Trainer:{value.resourceName}</Text>
                                            <View style={courtStyles.courtdatetime}>
                                                <Text style={courtStyles.myclassdate}>{this.numberToDateCalculation(value.startDateTime, value.timezone)}</Text>
                                                <Text style={courtStyles.myclasstime}>{this.numberCalculation(value.startDateTime, value.timezone)} - {this.numberCalculation(value.endDateTime, value.timezone)}</Text>
                                            </View>
                                            </ImageBackground>
                                            </View>
                                        </ImageBackground>

                                    </TouchableOpacity>

                                </View>
                            ))}

                        </View>
                    </View>
}
                </ScrollView>



            </View>

        );

    }
}

export default connect(select)(Upcoming);
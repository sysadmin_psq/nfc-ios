
import React from 'react';
import { connect } from 'react-redux';
import { View, ListView, StyleSheet, Text, ScrollView, Dimensions,TouchableOpacity, Image, Platform, TouchableWithoutFeedback, ImageBackground, AsyncStorage } from 'react-native';
import { black, borderGrey, white } from 'shared/styles/colors';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { ProximaNova } from 'shared/styles/fonts';
import Upcoming from './upComing.js'
import Attended from './attended.js'
import { NavigationBar } from 'navigationbar-react-native';
import {MAIN_ROUTES, CHALLENGE_ROUTES} from "../../../../constants/index";
import Modal from "react-native-modal";
import select from 'modules/main/state/selectors/LocationScreen';
import MaterialInitials from 'react-native-material-initials/native';
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import { AUTH_ROUTES, STORAGE_USER_SESSION } from 'constants';



type $Props = {
    courts: Array<$FitnessCourt>,
    locationsEnabled: boolean,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
        backgroundColor: white,
    },

    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E',
    },

    scene: {
        flex: 1,
    },
    tabBarHeight: {

        height: '7%',
        backgroundColor:'#127DAD',
        paddingTop: 5,
    },

    tabBarTextTitle: {

        color: white,
        ...ProximaNova.Semibold,
        fontSize: 12,
    }
});



const userStyles = StyleSheet.create({
    wrapper:{
        display:'flex',
        width:'100%',
        paddingTop:9,
        paddingBottom:9,
        flexDirection: 'row',
        paddingLeft:'5%',
        paddingRight:'5%',
    },
    text:{
        color:'#ffffff',
        display:'flex',
        flexDirection:'row',
        width:15,
        paddingTop:10,
        paddingBottom:10,
        paddingRight:32,
    },
    userPhoto: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 23,
        height: 35,
        paddingRight:16,
        borderRadius: isMediumScreen()? 23 : 23,
    },
    infoView:{
        display: 'flex',
        flexDirection: 'row',
        width:'58%',
        color:'#ffffff',

    },
    medalView:{
        display:'flex',
        flexDirection:'column',
        width: 22,
        height: '100%',
        flex:1,
        paddingLeft:'1%',
        alignSelf: 'center'
    },
    championText: {
        color: '#ffffff',
        paddingTop: 10,
        paddingBottom: 10,
    },
    medalTitleText:{
        color:'#ffffff',
        fontWeight:'bold',
        fontSize:18,
        alignSelf: 'center'
    },
    medalPointsFont:{
        color:'#ffffff',
        fontSize:8,
        alignSelf: 'center'

    }

});


const ComponentLeft = ({navigation}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-start',paddingLeft:5 }} >
            <TouchableOpacity
                onPress={() => { navigation.navigate(MAIN_ROUTES.SETTINGS);}}
                style={ {justifyContent:'flex-start', flexDirection: 'row',width:50,height:50}}>
                <Image
                    source={require('assets/icons/Back_1.png')}
                    style={{ resizeMode: 'contain', padding: 10 , alignSelf: 'center', zIndex: 999, }}
                />
            </TouchableOpacity>
        </View>
    );
};

const ComponentCenter = () => {
    return(
        <View style={{ flex: 0.8, }}>

            <Text style={{ color: 'white', ...ProximaNova.Regular, fontSize: 18}}>My classes</Text>
        </View>
    );
};

const ComponentRight = ({state}) => {

    return(
        <View style={{ flex: 1, alignItems: 'flex-end', }}>

        </View>
    );
};


class MyClasses extends React.Component<$Props> {



    constructor(props){
        super(props);

        // this.showDetails = this.showDetails.bind(this);// you should bind this to the method that call the props

    }



    componentWillMount() {


        var values = this.props.navigation.state.routes.find((e) => e.routeName === 'MyClasses')

        this.user_id = values.params.user_id
        this.email = values.params.email


    }

    componentDidMount() {



    }



    _toggleModal(visible) {


        this.setState({modalVisible: visible})
    }



    _renderLabel = ({ route }) => (
        <Text style={styles.tabBarTextTitle}>{route.title}</Text>
    );
    _renderTabBar = props => {

        return (

            <TabBar
                {...props}
                style={styles.tabBarHeight}
                tabStyle={styles.tab}
                renderLabel={this._renderLabel}
                indicatorStyle={{backgroundColor: white, alignItems: 'center',minHeight:'100%'}}
                // renderIndicator={this._indicator}
            />
        );
    };


    renderScene = ({ route }) => {

        switch (route.key) {
            case 'first':
                return (
                    <Upcoming user_id={this.user_id}
                              navigation={this.props.navigation}
                              email={this.email}/>
                )
            case 'second':
                return (
                    <Attended user_id={this.user_id}
                              navigation={this.props.navigation}
                              email={this.email}/>
                )
            default:
                return null;
        }
    }


    state = {
        index: 0,
        routes: [
            { key: 'first', title: 'UPCOMING' },
            { key: 'second', title: 'ATTENDED' },
        ],
        modalVisible: false,
        courtName: '',

    };



    render() {

        return (

            <View style={{height:isIPhoneX()?'110%':'100%',}}>

                <NavigationBar
                    componentLeft     = { () =>  <ComponentLeft navigation={this.props.navigation}/>   }
                    componentCenter   = { () =>  <ComponentCenter /> }
                    componentRight   = { () =>  <ComponentRight state={this}/> }
                    navigationBarStyle= {{ backgroundColor: '#0098D1;', height:isIPhoneX()?80:55,paddingTop:isIPhoneX()?40:20,marginTop: Platform.OS === 'ios'? -20 : 0}}
                    statusBarStyle    = {{ barStyle: 'light-content', backgroundColor: '#215e79'}}
                />

                <TabView
                    navigationState={this.state}
                    renderScene={this.renderScene}
                    renderTabBar={this._renderTabBar}
                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{ width: Dimensions.get('window').width }}
                />

                {/*footer code here*/}

            </View>

        );
    }
}

export default connect(select)(MyClasses)

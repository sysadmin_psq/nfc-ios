
import React from 'react';
import { View, ListView, StyleSheet, Text, ScrollView, Dimensions, ActivityIndicator, AsyncStorage, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { black, borderGrey, white } from 'shared/styles/colors';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { ProximaNova } from 'shared/styles/fonts';
import { AUTH_ROUTES, STORAGE_USER_SESSION, MAIN_ROUTES, CHALLENGE_ROUTES } from 'constants';
import ChallengePanel from 'modules/main/screens/Challenge/ChallengePanel';
import courtStyles from 'modules/main/screens/Courts/styles';
import Moment from 'react-moment';
import moment from 'moment';
import Config from 'react-native-config'
var _ = require('lodash');
import { connect } from 'react-redux';
import select from 'modules/main/state/selectors/LocationScreen';



type $Props = {
    courts: Array<$FitnessCourt>,
    locationsEnabled: boolean,
};

const styles = StyleSheet.create({

    horizontalScrollContainer: {
        backgroundColor: white,

    },
    courtdate: {
        fontSize:12,
        color:'#ffffff',
        width:'70%',
        ...ProximaNova.CondensedSemibold,
    },
    courttime: {
        fontSize:12,
        color:'#ffffff',
        width:'30%',
        ...ProximaNova.CondensedSemibold,
    },
    upcomingContainer: {
        alignItems: 'center',
        flex: 10,
        flexDirection:'row',
        marginStart: 20,
        marginBottom: 0,
    },
    courtcontainer:{
        minHeight:'100%',
    },

    emptycontainer: {
        flex: 1,
        backgroundColor: white,
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        minHeight:'100%',
    },
    noEnteries:{
        fontSize:20,
        color:'#979797'
    }
});


class Attended extends React.Component {


    /*
     * Removed for brevity
     */
    //

    constructor(props){
        super(props);

        this.state = {
            dataSource: [],
            isLoadingData: true
        }


        this.user_id = props.user_id
        this.email = props.email
        this.navigation = props.navigation

    }


    componentWillMount(){


        this.fetchData()
    }


    numberCalculation(date, timezone){


        console.log(timezone)
        console.log("date", moment(date).utcOffset(-420).format('HH:mm a'))
        return moment(date).utcOffset(-420).format('h:mm A').replace(":00", "")
    }



    fetchData = () => {

        var yesterday = moment(new Date()).subtract(1, 'days').format('YYYY-MM-DD')
        // var apiUrl = `${Config.SERVER_URL}/api/v1/bookings/${this.user_id}/?api_key=UkNNuT0bebf2bRoCMitx&date=${today}&is_attended=${true}`

        var apiUrl = `${Config.SERVER_URL}/api/v1/appointments/?api_key=UkNNuT0bebf2bRoCMitx&end_date=${yesterday}&email=${this.email}&status=BK`


        console.log("attended url", apiUrl)
        return fetch(apiUrl , {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
            .then(response => response.json())
            .then(response => {


                console.log("attended data length", response.data.length)
                var sortedArray = _.sortBy(response.data, function(o){return o.startDateTime})

                this.setState({

                    dataSource: sortedArray,
                    isLoadingData: false
                })
            }).catch(err => {

                console.log("error is fetch", err)
            });
    }



    goToFunction(navigation, value, otherCourts) {


        if(value.serviceAllocationId != '') {

            var court = _.find(otherCourts, function(o){
                return o.location_id == value.locationId})




            console.log("value", value.serviceAllocationId)
            console.log("otherCourts", otherCourts)


            if(court) {

                var today = moment(new Date()).format("YYYY-MM-DD")
                var allociationsApi = `${Config.SERVER_URL}/api/v1/service/allocations/${value.serviceAllocationId}/?api_key=UkNNuT0bebf2bRoCMitx`

                return fetch(allociationsApi , {
                    headers: new Headers({
                        'Content-Type': 'application/json'
                    })
                })
                    .then(allocationObject => allocationObject.json())
                    .then(allocationObject => {

                        console.log(allocationObject)

                        this.navigation.navigate(CHALLENGE_ROUTES.EVENTDETAIL, {court: court, appointmentObject: value, event: allocationObject, isPastEvent: true})

                    }).catch(err => {

                        console.log("err", err)
                    })
            }



        }
        else {

            console.log("serviceAllocationEmpty")
        }

    }


    render() {


        const [ ...otherCourts ] = this.props.courts;

        return (



            <View style={styles.container}>

                <ScrollView  style={{minHeight:'100%',}}>




                    {this.state.isLoadingData == true &&


                    <View style={courtStyles.courtcontainer}>
                        <View style={courtStyles.courtbody} >
                            {/*need to check this*/}
                            <ActivityIndicator style={{height: 100, backgroundColor: white, flex: 1, height: '100%'}} size="large" color="#127DAD" />

                        </View>
                    </View>
                    }


                    {this.state.dataSource.length == 0 && this.state.isLoadingData == false &&


                    <View style={styles.emptycontainer}>

                        {/* <Image
                            source={require('assets/images/No_Entries.png')}>
                        </Image> */}
                        <Text style={styles.noEnteries}>You Haven't Attended Any Classes Yet</Text>

                    </View>
                    }

                    {this.state.dataSource.length > 0 && this.state.isLoadingData == false &&


                    <View style={courtStyles.courtcontainer}>
                        <View style={courtStyles.courtbody} >
                            {this.state.dataSource.map(value => (
                                <View>
                                    <TouchableOpacity onPress={() => { this.goToFunction(this.navigation, value, otherCourts)}}>

                                        <ImageBackground source={{uri: value.resourceImageUrl}} style={courtStyles.courtCard}>
                                        <View>
                                                        <ImageBackground
                                                            style={courtStyles.courtCardbg1}
                                                            source={require('assets/images/bgTransparent.png')} >
                                                            <View>

                                                            <ImageBackground
                                                style={courtStyles.courtCardbg}
                                                source={require('assets/images/Mask.png')} >  
                                                <Text style={courtStyles.cardtitle} numberOfLines={1}>{value.serviceName.toUpperCase()}</Text>
                                                <Text style={courtStyles.cardtrainer}>Trainer:{value.resourceName}</Text>
                                                <View style={courtStyles.courtdatetime}>
                                                    <Text style={courtStyles.myclassdate}><Moment element={Text} format="ddd, MMM D YYYY">{value.startDateTime}</Moment></Text>
                                                    <Text style={courtStyles.myclasstime}>{this.numberCalculation(value.startDateTime, value.timezone)} - {this.numberCalculation(value.endDateTime, value.timezone)}</Text>
                                                </View>
                                                </ImageBackground>   
                                            </View>
                                            </ImageBackground>
                                            </View>
                                        </ImageBackground>

                                    </TouchableOpacity>

                                </View>
                            ))}

                        </View>
                    </View>
                    }

                </ScrollView>

            </View>

        );

    }
}

export default connect(select)(Attended);


// onPress={() => this.props.navigation.navigate(CHALLENGE_ROUTES.EVENTDETAIL, {court: firstCourt, event: value,})}
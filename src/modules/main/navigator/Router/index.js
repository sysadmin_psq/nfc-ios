// @flow
import { CHALLENGE_ROUTES, MAIN_ROUTES } from 'constants';
import { StackRouter } from 'react-navigation';
import AboutScreen from 'modules/main/screens/About';
import ChallengeScreen from 'modules/main/screens/Challenge';
import LeaderBoardScreen from 'modules/main/screens/LeaderBoard';
import MyEntriesScreen from 'modules/main/screens/MyEntries';
import EditProfileScreen from 'modules/main/screens/EditProfile';
import LearnScreen from 'modules/main/screens/Learn';
import LocationsScreen from 'modules/main/screens/Locations';
import LocationDetail from 'modules/main/screens/Locations/LocationDetail';
import TrainScreen from 'modules/main/screens/Train/Train';
import TrainDetail from 'modules/main/screens/Train/TrainDetail';
import TrainVideo from 'modules/main/screens/Train/TrainVideo';
import RecordScreen from 'modules/challenge/screens/Record';
import ScoreScreen from 'modules/challenge/screens/Score';
import ShareScreen from 'modules/challenge/screens/Share';
import Courts from 'modules/main/screens/Courts';
import Settings from 'modules/main/screens/Settings'
import EventDetail from 'modules/main/screens/EventDetail'
import CourtList from 'modules/main/screens/CourtList';
import MyClasses from 'modules/main/screens/MyClasses'
import AllCourtsDetail from 'modules/main/screens/AllCourtsDetail'
import {EVENT_ROUTES} from "../../../../constants/index";

export default StackRouter(
    {
        [MAIN_ROUTES.CHALLENGE]: {
            screen: ChallengeScreen,
        },
        [MAIN_ROUTES.EDIT_PROFILE]: {
            screen: EditProfileScreen,
        },
        [MAIN_ROUTES.LEARN]: {
            screen: LearnScreen,
        },
        [MAIN_ROUTES.LOCATIONS]: {
            screen: LocationsScreen,
        },
        [MAIN_ROUTES.LOCATION_DETAIL]: {
            screen: LocationDetail,
        },
        [MAIN_ROUTES.TRAIN]: {
            screen: TrainScreen,
        },

        [MAIN_ROUTES.TRAIN_DETAIL]: {
            screen: TrainDetail,
        },

        [MAIN_ROUTES.COURTS]: {
            screen: Courts,
        },

        [MAIN_ROUTES.SETTINGS]: {
            screen: Settings,
        },
        [MAIN_ROUTES.ALLCOURTSDETAIL]: {
            screen: AllCourtsDetail,
        },



        [CHALLENGE_ROUTES.RECORD]: {
            screen: RecordScreen,
        },
        [CHALLENGE_ROUTES.SCORE]: {
            screen: ScoreScreen,
        },
        [CHALLENGE_ROUTES.SHARE]: {
            screen: ShareScreen,
        },
        [CHALLENGE_ROUTES.LEADERBOARD]: {
            screen: LeaderBoardScreen,
        },

        [CHALLENGE_ROUTES.MYENTRIES]: {
            screen: MyEntriesScreen,
        },
        [CHALLENGE_ROUTES.TRAIN_VIDEO]: {
            screen: TrainVideo,
        },
        [CHALLENGE_ROUTES.MY_CLASSES]: {
            screen: MyClasses,
        },

        [CHALLENGE_ROUTES.EVENTDETAIL]: {
            screen: EventDetail,
        },
        [CHALLENGE_ROUTES.COURTLIST]: {
            screen: CourtList,
        },
        [CHALLENGE_ROUTES.ABOUT]: {
            screen: AboutScreen,
        },


    }, {
        // initialRouteName: MAIN_ROUTES.LEARN,
        initialRouteName: MAIN_ROUTES.LEARN,
    }
);

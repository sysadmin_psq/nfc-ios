// @flow
import { CHALLENGE_ROUTES, DRILLDOWN_ROUTES, MAIN_ROUTES } from 'constants';
import { Image, View, TouchableOpacity, Text } from 'react-native';
import BottomNav from './BottomNav';
import React from 'react';
import styles from './styles';
import CloseButton from 'shared/components/CloseButton';
import * as sharedActions from 'shared/state/dux';

type $Props = {
    router: any,
    navigation: {
      dispatch: any,
      state: {
        routes: {
          routeName: string,
        }[],
        index: number,
      },
    },
}

export default ({ router, navigation }: $Props) => {
    const { routes, index } = navigation.state;

    console.log("index", index, routes)
    const routeName = routes[index].routeName;

    const ActiveScreen = router.getComponentForRouteName(routeName);

    let navBarsVisible = true;
    const inChallengeMode = Object.values(CHALLENGE_ROUTES).includes(routeName);
    const inTrainDetail = routeName === MAIN_ROUTES.TRAIN_DETAIL;
    if (inChallengeMode || inTrainDetail) {
        navBarsVisible = false;
    }

    let backButtonVisible = false;
    if (DRILLDOWN_ROUTES.includes(routeName)) {
      backButtonVisible = true;
    }

    const disableAboutButton = routeName === MAIN_ROUTES.ABOUT;

    return (
        <View style={styles.container}>
            {/*{navBarsVisible && (*/}
                {/*<View style={styles.topNav}>*/}
                    {/*<Image source={require('assets/icons/Logo.png')} />*/}
                {/*</View>*/}
            {/*)}*/}
            <ActiveScreen navigation={navigation} />
            {navBarsVisible && (
                <BottomNav
                    activeRoute={routeName}
                    visible={navBarsVisible}
                />
            )}
        </View>
    );
};

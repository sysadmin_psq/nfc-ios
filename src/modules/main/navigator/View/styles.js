// @flow
import { blueTopNav, white } from 'shared/styles/colors';
import { StyleSheet } from 'react-native';
import { ProximaNova } from 'shared/styles/fonts';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: blueTopNav,
    },
    topNav: {
        backgroundColor: blueTopNav,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
    },
    closeButton: {
        position: 'absolute',
        height: 50,
        width: 50,
        top: 20,
        left: 20
    },
    aboutButton: {
        position: 'absolute',
        top: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        height: 50
    },
    text: {
        ...ProximaNova.CondensedSemibold,
        marginTop: 5,
        fontSize: 9,
        color: white,
    }
});

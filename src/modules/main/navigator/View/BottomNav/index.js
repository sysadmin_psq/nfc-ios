// // @flow
// import { blue, blueActive } from 'shared/styles/colors';
// import { connect } from 'react-redux';
// import { Image, Text, TouchableOpacity, View, TouchableWithoutFeedback, TouchableHighlight } from 'react-native';
// import { MAIN_ROUTES } from 'constants';
// import * as sharedActions from 'shared/state/dux';
// import React from 'react';
// import styles from './styles';
// import type { $MainRoute, $Route } from 'types/routes';
// import {white, bottomBar, bottomBarFont} from "../../../../../shared/styles/colors";
//
// type $Props = {
//     activeRoute: $MainRoute,
//     dispatch: *,
//     userInfo: object
// };
//
// type $Tab = {
//     routeName: $MainRoute,
//     source: string,
//     subRoutes: Array<$Route>,
//     text: string,
// };
//
// const tabs: Array<$Tab> = [
//
//     {
//         routeName: MAIN_ROUTES.LEARN,
//         source: require('assets/icons/learnNonActive.png'),
//         active: require('assets/icons/learnActive.png'),
//         subRoutes: [],
//         text: 'LEARN',
//     },
//
//     {
//         routeName: MAIN_ROUTES.TRAIN,
//         source: require('assets/icons/coachNonActive.png'),
//         active: require('assets/icons/coachActive.png'),
//         subRoutes: [MAIN_ROUTES.TRAIN_DETAIL],
//         text: 'TRAIN',
//     },
//
//     {
//         routeName: MAIN_ROUTES.COURTS,
//         source: require('assets/icons/courtsNonActive.png'),
//         active: require('assets/icons/courtsActive.png'),
//         subRoutes: [],
//         text: 'LOCATIONS',
//     },
//     {
//         routeName: MAIN_ROUTES.CHALLENGE,
//         source: require('assets/icons/challengeNonActive.png'),
//         active: require('assets/icons/challengeActive.png'),
//         subRoutes: [],
//         text: 'CHALLENGE',
//     },
//
//     {
//         routeName: MAIN_ROUTES.SETTINGS,
//         source: require('assets/icons/settingsNonActive.png'),
//         active: require('assets/icons/settingsActive.png'),
//         subRoutes: [],
//         text: 'SETTINGS',
//     }
// ];
//
// // {
// //     routeName: MAIN_ROUTES.LOCATIONS,
// //     source: require('assets/icons/Location.png'),
// //     subRoutes: [],
// //     text: 'LOCATIONS',
// // },
//
// const BottomNav = ({ activeRoute, dispatch }: $Props) => {
//     const shouldHighlight = (routeName: string, subRoutes: Array<$Route>) => {
//         return routeName === activeRoute || subRoutes.includes(activeRoute);
//     }
//
//     const onTabPress = (routeName: string, subRoutes: Array<$Route>) => {
//         // only navigate if we are not in the clicked route or a corresponding sub route
//         if (!shouldHighlight(routeName, subRoutes)) {
//             dispatch(sharedActions.navigate(routeName))
//         }
//     };
//
//
//
//
//     return (
//         <View style={styles.container}>
//             {tabs.map(({ source, subRoutes, routeName, text, active }) => (
//
//                 <TouchableWithoutFeedback
//                     key={routeName}
//                     onPress={() => onTabPress(routeName, subRoutes)}
//                     style={[
//                         styles.tab,
//                         {
//                             backgroundColor: bottomBar,
//                         },
//                     ]}
//                 >
//
//                     <View style={[
//                         styles.tab,
//                         {
//                             backgroundColor: bottomBar,
//                         },
//                     ]}>
//
//                         <Image
//                             source={shouldHighlight(routeName, subRoutes) ? active : source}
//                             style={styles.image}
//                             resizeMode="contain"
//                         />
//                         <Text style={[
//
//                             styles.text,
//                             {
//                                 color: shouldHighlight(routeName, subRoutes) ? blue : bottomBarFont
//
//                             },
//                         ]}>
//                             {text}
//                         </Text>
//                     </View>
//                 </TouchableWithoutFeedback>
//             ))}
//
//
//         </View>
//     );
// };
//
// export default connect()(BottomNav);
//

// @flow
import { blue, blueActive } from "shared/styles/colors";
import { connect } from "react-redux";
import {
    Image,
    Text,
    TouchableOpacity,
    View,
    TouchableWithoutFeedback,
    TouchableHighlight
} from "react-native";
import { MAIN_ROUTES } from "constants";
import * as sharedActions from "shared/state/dux";
import React from "react";
import styles from "./styles";
import type { $MainRoute, $Route } from "types/routes";
import {
    white,
    bottomBar,
    bottomBarFont
} from "../../../../../shared/styles/colors";
import select from "modules/main/state/selectors/BottomNav";

type $Props = {
    activeRoute: $MainRoute,
    dispatch: *,
    userInfo: object
};

type $Tab = {
    routeName: $MainRoute,
    source: string,
    subRoutes: Array<$Route>,
    text: string
};

const tabs: Array<$Tab> = [
    {
        routeName: MAIN_ROUTES.LEARN,
        source: require("assets/icons/learnNonActive.png"),
        active: require("assets/icons/learnActive.png"),
        subRoutes: [],
        text: "LEARN"
    },

    {
        routeName: MAIN_ROUTES.TRAIN,
        source: require("assets/icons/coachNonActive.png"),
        active: require("assets/icons/coachActive.png"),
        subRoutes: [MAIN_ROUTES.TRAIN_DETAIL],
        text: "TRAIN"
    },
    {
        routeName: MAIN_ROUTES.COURTS,
        source: require("assets/icons/courtsNonActive.png"),
        active: require("assets/icons/courtsActive.png"),
        subRoutes: [],
        text: "LOCATIONS"
    },
    {
        routeName: MAIN_ROUTES.CHALLENGE,
        source: require("assets/icons/challengeNonActive.png"),
        active: require("assets/icons/challengeActive.png"),
        subRoutes: [],
        text: "CHALLENGE"
    },

    {
        routeName: MAIN_ROUTES.SETTINGS,
        source: require("assets/icons/settingsNonActive.png"),
        active: require("assets/icons/settingsActive.png"),
        subRoutes: [],
        text: "SETTINGS"
    }
];

// {
//     routeName: MAIN_ROUTES.LOCATIONS,
//     source: require('assets/icons/Location.png'),
//     subRoutes: [],
//     text: 'LOCATIONS',
// },

const BottomNav = ({ activeRoute, dispatch, userInfo }: $Props) => {
    const shouldHighlight = (routeName: string, subRoutes: Array<$Route>) => {
        return routeName === activeRoute || subRoutes.includes(activeRoute);
    };

    console.log("bottom Nav, ", userInfo);

    const onTabPress = (routeName: string, subRoutes: Array<$Route>) => {
        // only navigate if we are not in the clicked route or a corresponding sub route
        if (
            userInfo.city === "N/A" ||
            userInfo.province === "N/A" ||
            userInfo.gender === "N/A" ||
            userInfo.age_range === "N/A"
        ) {
            return;
        } else if (!shouldHighlight(routeName, subRoutes)) {
            dispatch(sharedActions.navigate(routeName));
        }
    };

    return (
        <View style={styles.container}>
            {tabs.map(({ source, subRoutes, routeName, text, active }) => (
                <TouchableWithoutFeedback
                    key={routeName}
                    onPress={() => onTabPress(routeName, subRoutes)}
                    style={[
                        styles.tab,
                        {
                            backgroundColor: bottomBar
                        }
                    ]}
                >
                    <View
                        style={[
                            styles.tab,
                            {
                                backgroundColor: bottomBar
                            }
                        ]}
                    >
                        <Image
                            source={shouldHighlight(routeName, subRoutes) ? active : source}
                            style={styles.image}
                            resizeMode="contain"
                        />
                        <Text
                            style={[
                                styles.text,
                                {
                                    color: shouldHighlight(routeName, subRoutes)
                                        ? blue
                                        : bottomBarFont
                                }
                            ]}
                        >
                            {text}
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            ))}
        </View>
    );
};

export default connect(select)(BottomNav);


// @flow
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';
import { white, bottomBarFont } from 'shared/styles/colors';
import { isIPhone4, isIPhone5, isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';


export default StyleSheet.create({
    container: {
        width: '100%',
        height: 50,
        flexDirection: 'row',

    },
    image: {
        height: 25,
        width: 25,
        marginBottom: 4,
    },
    tab: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height:105,
        paddingBottom:50,
    },
    text: {
        ...ProximaNova.CondensedSemibold,
        fontSize: 12,
        // color: bottomBarFont,
    },
});

// @flow
import type { $MainState } from 'types/state/main';

export default ({
    main: {
        train: {
            routines,
        }
    }
}: { main: $MainState}) => ({
    routines,
});

// @flow
import type { $MainState } from 'types/state/main';

export default ({ main }: { main: $MainState }) => ({
    courts: main.courts,
    locationsEnabled: main.locationsEnabled
});

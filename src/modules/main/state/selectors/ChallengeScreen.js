// @flow
import { CHALLENGE_MODE_IDS } from 'constants';

export default () => ({
  challenges: [
    {
      thumbnailImage: require('assets/images/Quick_1.png'),
      title: 'QUICK',
      subtitle: '3-MINUTE FULL BODY\n15s ON / 15s OFF',
      imageLink: require('assets/images/learnTheMoves.png'),
      description: '',
      challengeId: 1, // DO NOT MODIFY. Must be kept in sync with back end until api endpoint exists
      challengeMarker: CHALLENGE_MODE_IDS.THREE_MINUTE,
    },
    {
      thumbnailImage: require('assets/images/Endurance_1.png'),
      title: 'ENDURANCE',
      subtitle: '5-MINUTE FULL BODY\n30s ON / 15s OFF',
      imageLink: require('assets/images/learnTheMoves.png'),
      description: '',
      challengeId: 2, // DO NOT MODIFY
      challengeMarker: CHALLENGE_MODE_IDS.FIVE_MINUTE,
    },
    {
      thumbnailImage: require('assets/images/Competition_1.png'),
      title: 'COMPETITION',
      subtitle: '7-MINUTE FULL BODY\n45s ON / 15s OFF',
      imageLink: require('assets/images/learnTheMoves.png'),
      description: '',
      challengeId: 3, // DO NOT MODIFY
      challengeMarker: CHALLENGE_MODE_IDS.SEVEN_MINUTE,
    },
  ],
});

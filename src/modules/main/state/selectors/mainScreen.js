// @flow
import type { $MainState } from "types/state/main";

export default ({ main }: { main: $MainState }) => ({
    userInfo: main.userInfo
});

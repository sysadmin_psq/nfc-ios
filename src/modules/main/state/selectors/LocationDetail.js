// @flow
import type { $MainState } from 'types/state/main';

export default ({ main }: { main: $MainState }) => ({
    court: main.courts.find(court => main.currentCourt === court.id),
    firstCourt: main.courts[0].id,
    locationsEnabled: main.locationsEnabled
});

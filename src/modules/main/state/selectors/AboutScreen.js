// @flow
import type { $MainState } from 'types/state/main';

export default ({ main }: { main: $MainState }) => {
    const {
        name,
        username,
        email,
        id,
    } = main.userInfo;

    return {
        showProfile: name && username,
        name,
        username,
        email,
        id
    };
};

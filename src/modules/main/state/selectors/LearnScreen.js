// @flow
import type { $MainState } from 'types/state/main';

export default ({ main }: { main: $MainState }) => ({
    videos: main.learnFeed.videos,
    refreshing: main.learnFeed.refreshing,
    renderFeed: main.learnFeed.videos.length > 0,
    userInfo: main.userInfo
});

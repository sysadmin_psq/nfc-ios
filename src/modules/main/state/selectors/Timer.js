// @flow
import type { $MainState } from 'types/state/main';

export default ({ main }: { main: $MainState }) => ({
    paused: main.train.paused,
});

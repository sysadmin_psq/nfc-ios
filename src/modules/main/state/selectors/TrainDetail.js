// @flow
import type { $MainState } from 'types/state/main';
import { get } from 'lodash';

export default ({ main }: { main: $MainState}) => ({
    ...main.train,
    currentRoutine: get(main.train, `routines[${main.train.currentRoutineIdx}]`, {}),
});

// @flow
import { getFormSubmitErrors, getFormSyncErrors } from 'redux-form';
import type { $MainState } from 'types/state/main';

export default (state: *) => {
    const submitErrors = getFormSubmitErrors('editProfile')(state);
    const fieldErrors = getFormSyncErrors('editProfile')(state);
    let validationError;
    if (fieldErrors) {
        if (fieldErrors.email) {
            validationError = fieldErrors.email;
        } else if (fieldErrors.name) {
            validationError = fieldErrors.name;
        } else if(fieldErrors.username) {
            validationError = fieldErrors.username;
        }
    } else if (submitErrors && submitErrors.error) {
        validationError = submitErrors.error;
    }

    const {
        showResetPasswordMessage,
        userInfo: {
            email,
            marketingOptIn,
            name,
            username,
            city,
            province,
            gender,
            age_range
        },
    }: $MainState = state.main;

    return {
        initialValues: {
            email,
            name,
            username,
            city,
            province,
            gender,
            age_range
        },
        email,
        marketingOptIn,
        showResetPasswordMessage,
        username,
        validationError,
    };
};

// @flow
import { createAction } from 'redux-act';
import { FEED_LOCATIONS, PAGINATION } from 'constants';
import type { $MainState, $Video } from 'types/state/main';

type $FeedLocation = typeof FEED_LOCATIONS.TOP | typeof FEED_LOCATIONS.BOTTOM;
type $Pagination = typeof PAGINATION.INCREMENT | typeof PAGINATION.RESET;

type $FetchLearnFeedPayload = {
    overwrite: ?boolean,
    pagination: ?$Pagination,
    position: ?$FeedLocation,
};

export const fetchLearnFeed = createAction('', (payload: ?$FetchLearnFeedPayload) => ({ ...payload }));
export const fetchLearnFeedReducer = (state: $MainState, { pagination }: { pagination: ?string }) => {
    let learnFeedPage = state.learnFeed.page;
    if (pagination === PAGINATION.RESET) {
        learnFeedPage = 1;
    } else if (pagination === PAGINATION.INCREMENT) {
        learnFeedPage = state.learnFeed.page + 1;
    }

    return {
        ...state,
        learnFeed: {
            ...state.learnFeed,
            page: learnFeedPage,
        },
    };
}

// -----------------------------------------------------------------------------

type Payload = {
    location: $FeedLocation,
    overwrite: boolean,
    pagination: $Pagination,
    videos: Array<$Video>,
};

export const fetchLearnFeedSuccess = createAction('', payload => ({ ...payload }));
export const fetchLearnFeedSuccessReducer = (state: $MainState, {
    overwrite = true,
    location = FEED_LOCATIONS.TOP,
    pagination,
    videos,
}: Payload) => {
    let newVideos = videos;
    if (!overwrite && location === FEED_LOCATIONS.TOP) {
        newVideos = [...videos, ...state.learnFeed.videos]
    } else if (!overwrite && location === FEED_LOCATIONS.BOTTOM) {
        newVideos = [...state.learnFeed.videos, ...videos];
    }

    let page = state.learnFeed.page;
    if (pagination === PAGINATION.reset) {
        page = 1;
    } else if (pagination === PAGINATION.INCREMENT) {
        page = state.learnFeed.page++;
    }

    return {
        ...state,
        learnFeed: {
            ...state.learnFeed,
            videos: newVideos,
            page,
        },
    };
}

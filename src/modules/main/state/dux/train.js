// @flow
import { createAction } from 'redux-act';
import initialState from '../initialState';
import type { $MainState, $SponsorInfo, $TrainAudio } from 'types/state/main';


export const setCurrentRoutineIdx = createAction(
  '',
  (idx: number) => idx,
);

export const setCurrentRoutineIdxReducer = (
    state: $MainState,
    idx: number
): $MainState => ({
  ...state,
  train: {
    ...state.train,
    currentRoutineIdx: idx,
  },
});

export const fetchTrainAudio = createAction('');

export const fetchTrainAudioFulfilled = createAction(
  '',
  (trainAudio: $TrainAudio) => trainAudio,
);

export const fetchTrainAudioFulfilledReducer = (
    state: $MainState,
    trainAudio: $TrainAudio,
): $MainState => ({
    ...state,
    train: {
        ...state.train,
        routines: trainAudio,
    },
});

export const fetchSponsorInfo = createAction('');

export const fetchSponsorInfoFulfilled = createAction(
    '',
    (sponsorInfo: $SponsorInfo) => sponsorInfo,
);
export const fetchSponsorInfoFulfilledReducer = (
    state: $MainState,
    sponsorInfo: $SponsorInfo,
): $MainState => ({
    ...state,
    train: {
        ...state.train,
        ...sponsorInfo,
    },
});

export const resetTrainState = createAction('');
export const resetTrainStateReducer = (state: $MainState): $MainState => ({
    ...state,
    train: {
        ...initialState.train,
    },
});

export const setTrainAudioPaused = createAction(
    'Set if train detail audio is paused',
    (audioPaused: boolean) => audioPaused,
);
export const setTrainAudioPausedReducer = (state: $MainState, audioPaused: boolean): $MainState => ({
    ...state,
    train: {
        ...state.train,
        audioPaused,
    },
});

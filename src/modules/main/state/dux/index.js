// @flow
import { createAction, createReducer } from 'redux-act';
import { ONBOARDING_STORAGE_STRING } from 'constants';
import initialState from '../initialState';
import type { $FitnessCourt, $MainState, $UserInfo } from 'types/state/main';

// import submodule actions and reducers
// -----------------------------------------------------------------------------
import {
    fetchLearnFeed,
    fetchLearnFeedSuccess,
    fetchLearnFeedReducer,
    fetchLearnFeedSuccessReducer,
} from './learnFeed';

import {
    fetchTrainAudioFulfilled,
    fetchTrainAudioFulfilledReducer,
    fetchSponsorInfoFulfilled,
    fetchSponsorInfoFulfilledReducer,
    setCurrentRoutineIdx,
    setCurrentRoutineIdxReducer,
    setTrainAudioPaused,
    setTrainAudioPausedReducer,
} from './train';

// export submodule actions
// -----------------------------------------------------------------------------
export {
    fetchLearnFeed,
    fetchLearnFeedSuccess,
} from './learnFeed';

export {
    fetchTrainAudio,
    fetchTrainAudioFulfilled,
    fetchTrainAudioFulfilledReducer,
    fetchSponsorInfo,
    fetchSponsorInfoFulfilled,
    resetTrainState,
    setCurrentRoutineIdx,
    setCurrentRoutineIdxReducer,
    setTrainAudioPaused,
    setTrainAudioPausedReducer,
} from './train';

// actions
// -----------------------------------------------------------------------------
export const checkIfOnboarded = createAction('');
export const getDeviceInfo = createAction('');
export const fetchFitnessCourt = createAction('');
export const fetchAllCourts = createAction('');
export const fetchUserInfo = createAction('');
export const initializeApp = createAction('');
export const logout = createAction('');
export const promptAppPermissions = createAction('');
export const promptCameraPermission = createAction('');
export const resetPassword = createAction('');

type $EditProfilePayload = {
    email: string,
    marketingOptIn: boolean,
    name: string,
    username: string,
};
export const editProfile = createAction('', (payload: $EditProfilePayload) => payload);

// action reducer pairs
// -----------------------------------------------------------------------------
export const appPermissionsGranted = createAction('');
const appPermissionsGrantedReducer = (state: $MainState) => ({
    ...state,
    appPermissionsGranted: true,
});

export const checkIfOnboardedFulfilled = createAction('', (response: ?string) => response);
const checkIfOnboardedFulfilledReducer = (state: $MainState, response: ?string): $MainState => {
    const onboarded = response === null ? false : response === ONBOARDING_STORAGE_STRING;
    return {
        ...state,
        onboarded,
    };
};

export const fetchAllCourtsSuccess = createAction('');
const fetchAllCourtsSuccessReducer = (
  state: $MainState,
  courts: Array<$FitnessCourt>,
  locationsDisabled: boolean
): $MainState => {
    if (!courts) {
        return {
            ...state,
        };
    }

    return {
        ...state,
        courts,
        locationsEnabled: !locationsDisabled
    };
};

export const fetchFitnessCourtSuccess = createAction('');
const fetchFitnessCourtSuccessReducer = (state: $MainState, fitnessCourt: $FitnessCourt): $MainState => {
    if (!fitnessCourt) {
        return {
            ...state,
        };
    }

    return {
        ...state,
        fitnessCourt,
    };
};

export const fetchUserInfoSuccess = createAction('', (userInfo: $UserInfo) => userInfo);
const fetchUserInfoSuccessReducer = (state: $MainState, userInfo: $UserInfo): $MainState => ({
    ...state,
    userInfo,
});

export const getDeviceInfoFulfilled = createAction('', (deviceId: string) => deviceId);
const getDeviceInfoFulfilledReducer = (state: $MainState, deviceId: string): $MainState => ({
    ...state,
    deviceId,
});

export const onboarded = createAction('');
const onboardedReducer = (state: $MainState): $MainState => ({
    ...state,
    onboarded: true,
});

export const setCurrentCourt = createAction('', (currentCourt: number) => currentCourt);
const setCurrentCourtReducer = (state: $MainState, currentCourt: number): $MainState => ({
  ...state,
  currentCourt
});

export const setShowTrainModal = createAction(
    'Set show train modal',
    (object) => object
);
const setShowTrainModalReducer = (state: $MainState, RouteObject: {}): $MainState => {


    var showTrainModal = RouteObject.showTrainModal
    var navigationRoute = RouteObject.navigationRoute

    return (
        {
            ...state,
            navigationRoute,
            train: {
                ...state.train,
                showTrainModal
            },
        });

}

export const toggleMarketingOptIn = createAction('');
const toggleMarketingOptInReducer = (state: $MainState): $MainState => ({
    ...state,
    userInfo: {
        ...state.userInfo,
        marketingOptIn: !state.userInfo.marketingOptIn,
    },
});

export const toggleResetPasswordMessage = createAction('');
const toggleResetPasswordMessageReducer = (state: $MainState): $MainState => ({
    ...state,
    showResetPasswordMessage: !state.showResetPasswordMessage,
});

export const testingCourtAction = createAction('');


const testingCourtReducer = () => {

    console.log("inside the reducer")
    return {}
};


// -----------------------------------------------------------------------------
// $FlowFixMe
export default createReducer({
    [appPermissionsGranted]: appPermissionsGrantedReducer,
    [checkIfOnboardedFulfilled]: checkIfOnboardedFulfilledReducer,
    [fetchLearnFeed]: fetchLearnFeedReducer,
    [fetchLearnFeedSuccess]: fetchLearnFeedSuccessReducer,
    [fetchFitnessCourtSuccess]: fetchFitnessCourtSuccessReducer,
    [fetchAllCourtsSuccess]: fetchAllCourtsSuccessReducer,
    [fetchTrainAudioFulfilled]: fetchTrainAudioFulfilledReducer,
    [fetchSponsorInfoFulfilled]: fetchSponsorInfoFulfilledReducer,
    [fetchUserInfoSuccess]: fetchUserInfoSuccessReducer,
    [getDeviceInfoFulfilled]: getDeviceInfoFulfilledReducer,
    [onboarded]: onboardedReducer,
    [setCurrentRoutineIdx]: setCurrentRoutineIdxReducer,
    [setCurrentCourt]: setCurrentCourtReducer,
    [setShowTrainModal]: setShowTrainModalReducer,
    [setTrainAudioPaused]: setTrainAudioPausedReducer,
    [toggleMarketingOptIn]: toggleMarketingOptInReducer,
    [toggleResetPasswordMessage]: toggleResetPasswordMessageReducer,
    [testingCourtAction]: testingCourtReducer,
}, initialState);

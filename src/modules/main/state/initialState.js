// @flow
import type { $MainState } from 'types/state/main';

const initialState: $MainState = {
    appPermissionsGranted: false,
    deviceId: '',
    currentCourt: null,
    courts: [],
    fitnessCourt: {
        id: undefined,
        city: undefined,
        name: undefined,
        street: undefined,
        image: undefined,
        sponsorName: undefined,
        callToActionLink: undefined,
        callToActionDisplay: undefined,
        openingSoon: false
    },
    onboarded: false,
    learnFeed: {
        page: 1,
        refreshing: false,
        videos: [],
    },
    locationsEnabled: true,
    showResetPasswordMessage: false,
    train: {
        audioPaused: true,
        routines: null,
        showTrainModal: false,
        currentRoutineIdx: undefined,
    },
    userInfo: {
        email: undefined,
        id: undefined,
        marketingOptIn: undefined,
        name: undefined,
        username: undefined,
    },
}

export default initialState;

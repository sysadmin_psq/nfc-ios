// @flow
import { SponsorInfoSerializer } from 'api/serializers';
import * as mainActions from 'modules/main/state/dux';
import * as sharedActions from 'shared/state/dux';
import Api from 'api';


export default (action$: *) =>
    action$
        .ofType(mainActions.fetchSponsorInfo)
        .mergeMap(() => Api.fetchSponsorInfo())
        .map(response => mainActions.fetchSponsorInfoFulfilled(SponsorInfoSerializer(response)))
        .catch(() => sharedActions.logErrorAsObservable('Error fetching sponsor info'));

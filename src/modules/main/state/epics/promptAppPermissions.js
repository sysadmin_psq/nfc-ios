// @flow
import { Observable } from 'rxjs';
import { PermissionsAndroid } from 'react-native'; // eslint-disable-line
import * as mainActions from '../dux';
import * as sharedActions from 'shared/state/dux';

const permissions = [
    // PermissionsAndroid.PERMISSIONS.CAMERA,
    PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
    PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
];

const permission = PermissionsAndroid.PERMISSIONS.CAMERA;

const appPermissionsGrantedObservable = () => Observable.concat(
    Observable.of(mainActions.appPermissionsGranted()),
    Observable.of(mainActions.initializeApp()),
);

const requestPermissionObservable = () =>
    Observable
        .fromPromise(PermissionsAndroid.requestMultiple(permissions))
        .switchMap((result) => {
            // send denied action if any of the permissions are denied
            if (Object.values(result).filter(granted => granted !== PermissionsAndroid.RESULTS.GRANTED).length > 0) {
                return sharedActions.emptyActionAsObservable();
            }
            return appPermissionsGrantedObservable();
        })
        .catch(error => sharedActions.logErrorAsObservable(error));

export default (action$: Object) =>
    action$
        .ofType(mainActions.promptAppPermissions)
        .switchMap(() =>
            Observable
                .fromPromise(PermissionsAndroid.check(permission))
                .switchMap(granted =>
                    granted ? appPermissionsGrantedObservable() : requestPermissionObservable()
                )
        )

// @flow
import { Observable } from 'rxjs';
import * as mainActions from '../dux';

export default (action$: *) =>
    action$
        .ofType(mainActions.initializeApp)
        .switchMap(() =>
            Observable.concat(
                Observable.of(mainActions.checkIfOnboarded()),
                Observable.of(mainActions.fetchFitnessCourt()),
                Observable.of(mainActions.fetchAllCourts()),
            )
        );

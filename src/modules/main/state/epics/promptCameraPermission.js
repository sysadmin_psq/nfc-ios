// @flow
import { CameraRoll } from 'react-native'; // eslint-disable-line
import { Observable } from 'rxjs';
import * as mainActions from '../dux';
import * as sharedActions from 'shared/state/dux';
import Permissions from 'react-native-permissions';


const requestCameraPermissionObservable = () =>
    Observable
        .fromPromise(Permissions.request('camera'))
        .map((result) => {
            if (result === 'authorized') {
                return mainActions.appPermissionsGranted();
            } else {
                return sharedActions.emptyAction();
            }
        });


const checkCameraPermissionObservable = () =>
    Observable
        .fromPromise(Permissions.check('camera'))
        .switchMap((result) => {
            if (result === 'undetermined') {
                return requestCameraPermissionObservable();
            } else if (result === 'authorized') {
                return Observable.of(mainActions.appPermissionsGranted());
            } else {
                return sharedActions.emptyActionAsObservable();
            }
        });


export default (action$: Object) =>
    action$
        .ofType(mainActions.promptCameraPermission)
        .switchMap(() =>
            Observable
                .fromPromise(CameraRoll.getPhotos({first: 1, assetType: 'All'}))
                .switchMap(() => {
                    return checkCameraPermissionObservable();
                })
                .catch((error) => sharedActions.logErrorAsObservable(error))
        )

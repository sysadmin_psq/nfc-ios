// @flow
import { Observable } from 'rxjs';
import Api from 'api';
import * as mainActions from '../dux';
import * as sharedActions from 'shared/state/dux';
import { MAIN_ROUTES } from "constants";

const successText = 'Your profile is up to date.'

export default (action$: *, store: *) =>
    action$
        .ofType(mainActions.editProfile)
        .switchMap(action =>  {
            const {
                main: {
                    userInfo: {
                        id,
                    },
                },
            } = store.getState();

            return Api
                .editProfile({
                    userId: id,
                    ...action.payload,
                })
                .mergeMap(() => Observable.concat(
                    Observable.of(sharedActions.createAutoCloseNotification(successText)),
                    Observable.of(sharedActions.navigate(MAIN_ROUTES.SETTINGS))
                ))
                .catch(error => sharedActions.logErrorAsObservable(error))
    });

// @flow
import { TrainAudioSerializer } from 'api/serializers';
import * as mainActions from 'modules/main/state/dux';
import * as sharedActions from 'shared/state/dux';
import Api from 'api';


export default (action$: *, store: *) =>
    action$
        .ofType(mainActions.fetchTrainAudio)
        .switchMap(() => {
            const {
                main: {
                    fitnessCourt,
                },
            } = store.getState();

            return Api
                .fetchTrainAudio(fitnessCourt.id)
                .map(response => mainActions.fetchTrainAudioFulfilled(TrainAudioSerializer(response)))
                .catch(() => sharedActions.logErrorAsObservable('Error fetching train audio'));
        });

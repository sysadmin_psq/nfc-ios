// @flow
import { UserInfoSerializer } from 'api/serializers';
import Api from 'api';
import * as mainActions from '../dux';
import * as sharedActions from 'shared/state/dux';

export default (action$: *) =>
    action$
        .ofType(mainActions.fetchUserInfo)
        .mergeMap(() => Api.fetchUserInfo())
        .map(response => mainActions.fetchUserInfoSuccess(UserInfoSerializer(response)))
        .catch(() => sharedActions.logErrorAsObservable('Error fetching user info'));

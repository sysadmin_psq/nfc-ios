// @flow
import checkIfOnboarded from './checkIfOnboarded';
import editProfile from './editProfile';
import fetchFitnessCourt from './fetchFitnessCourt';
import fetchAllCourts from './fetchAllCourts';
import fetchLearnFeed from './fetchLearnFeed';
import fetchTrainAudio from './fetchTrainAudio';
import fetchUserInfo from './fetchUserInfo';
import fetchSponsorInfo from './fetchSponsorInfo';
import getDeviceInfo from './getDeviceInfo';
import initializeApp from './initializeApp';
import logout from './logout';
import onboarded from './onboarded';
import promptAppPermissions from './promptAppPermissions';
import promptCameraPermission from './promptCameraPermission';
import resetPassword from './resetPassword';

export default [
    checkIfOnboarded,
    editProfile,
    fetchAllCourts,
    fetchFitnessCourt,
    fetchLearnFeed,
    fetchSponsorInfo,
    fetchTrainAudio,
    fetchUserInfo,
    getDeviceInfo,
    initializeApp,
    logout,
    onboarded,
    promptAppPermissions,
    promptCameraPermission,
    resetPassword,
];

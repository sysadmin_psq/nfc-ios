// @flow
import { Observable } from 'rxjs';
import * as mainActions from '../dux';
import * as sharedActions from 'shared/state/dux';
import Api from 'api';

export default (action$: *, store: *) =>
    action$
        .ofType(mainActions.resetPassword)
        .switchMap(() => {
            const {
                main: {
                    userInfo: {
                        email,
                    },
                },
            } = store.getState();

            return Api
                .resetPassword(email)
                .switchMap(() => Observable.concat(
                    Observable.of(mainActions.toggleResetPasswordMessage()),
                    Observable.of(mainActions.toggleResetPasswordMessage()).delay(6000),
                ))
                .catch(error => Observable.of(sharedActions.logError(error)))
        });

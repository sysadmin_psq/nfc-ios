// @flow
import { FitnessCourtSerializer } from 'api/serializers';
import { Observable } from 'rxjs';
import * as mainActions from '../dux';
import * as sharedActions from 'shared/state/dux';
import Api from 'api';
import Permissions from 'react-native-permissions';
import type { $GeolocationPosition } from 'shared/types';

const getPositionAsObservable = Observable.bindCallback(navigator.geolocation.getCurrentPosition);

export default (action$: *) =>
    action$
        .ofType(mainActions.fetchFitnessCourt)
        .mergeMap(() => Observable.fromPromise(Permissions.check('location')))
        .switchMap(response => {
            if (response === 'denied') {
                return Observable.of(mainActions.fetchFitnessCourtSuccess());
            }
            // $FlowFixMe
            return getPositionAsObservable()
                .mergeMap((position: $GeolocationPosition) => Api.fetchFitnessCourt(position))
                .map(response => mainActions.fetchFitnessCourtSuccess(FitnessCourtSerializer(response)))
                // we still trigger the fetch success since we want the learn feed to be fetched regardless
                .catch(() => Observable.of(mainActions.fetchFitnessCourtSuccess()))
        })
        .catch(() => Observable.of(sharedActions.logErrorAsObservable(
            'Problem determining location permission',
        )));

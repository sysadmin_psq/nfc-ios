// @flow
import { LearnFeedSerializer } from 'api/serializers';
import * as mainActions from 'modules/main/state/dux';
import * as sharedActions from 'shared/state/dux';
import Api from 'api';

export default (action$: *, store: *) =>
    action$
        .ofType(mainActions.fetchLearnFeed)
        .switchMap((action) =>
            // wait for the fitness court to be fetched before proceeding
            action$
                .ofType(mainActions.fetchFitnessCourtSuccess)
                .switchMap(() => {
                    const {
                        main: {
                            fitnessCourt,
                            learnFeed: {
                                page,
                            },
                        },
                    } = store.getState();
                         return    Api
                            .fetchLearnFeed(page, fitnessCourt.id)
                            .map(response => mainActions.fetchLearnFeedSuccess(
                                {
                                    videos: LearnFeedSerializer(response),
                                    ...action.payload,
                                }
                            ))
                            .catch(() => sharedActions.emptyActionAsObservable())
                })
        );

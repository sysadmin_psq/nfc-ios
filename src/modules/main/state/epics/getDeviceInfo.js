// @flow
import * as mainActions from '../dux';
import * as sharedActions from 'shared/state/dux';
import DeviceInfo from 'react-native-device-info';

export default (action$: *) =>
    action$
        .ofType(mainActions.getDeviceInfo)
        .map(() => {
            const deviceId = DeviceInfo.getUniqueID();
            return mainActions.getDeviceInfoFulfilled(deviceId);
        })
        .catch(error => sharedActions.logErrorAsObservable(error));

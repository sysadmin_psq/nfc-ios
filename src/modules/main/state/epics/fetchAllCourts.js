// @flow
import { CourtSerializer } from 'api/serializers';
import { Observable } from 'rxjs';
import * as mainActions from '../dux';
import * as sharedActions from 'shared/state/dux';
import Api from 'api';
import Permissions from 'react-native-permissions';
import type { $GeolocationPosition } from 'shared/types';

const getPositionAsObservable = Observable.bindCallback(navigator.geolocation.getCurrentPosition);

export default (action$: *) =>
    action$
        .ofType(mainActions.fetchAllCourts)
        .mergeMap(() => Observable.fromPromise(Permissions.check('location')))
        .switchMap(response => {
            if (response === 'denied') {
              return Api.fetchAllCourts()
                        .map((resp) => {
                            return mainActions.fetchAllCourtsSuccess(CourtSerializer(resp), true);
                        })
            }
            // $FlowFixMe
            return getPositionAsObservable()
                .mergeMap((position: $GeolocationPosition) => Api.fetchAllCourts(position))
                .map(response => mainActions.fetchAllCourtsSuccess(CourtSerializer(response)))
                // we still trigger the fetch success since we want the learn feed to be fetched regardless
                .catch(() => Observable.of(mainActions.fetchAllCourtsSuccess()))

        })
        .catch(() => Observable.of(sharedActions.logErrorAsObservable(
            'Problem determining location permission',
        )));

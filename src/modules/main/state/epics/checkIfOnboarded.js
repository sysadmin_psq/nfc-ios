// @flow
import { AsyncStorage } from 'react-native';
import { Observable } from 'rxjs';
import { ONBOARDING_STORAGE_STRING } from 'constants';
import * as mainActions from '../dux/';
import * as sharedActions from 'shared/state/dux';

export default (action$: Object) =>
    action$
        .ofType(mainActions.checkIfOnboarded)
        .switchMap(() =>
            Observable.fromPromise(AsyncStorage.getItem(ONBOARDING_STORAGE_STRING))
                .map(response => mainActions.checkIfOnboardedFulfilled(response))
                .catch(error => sharedActions.logErrorAsObservable(error.xhr.response))
        );

// @flow
import { AsyncStorage } from 'react-native';
import { Observable } from 'rxjs';
import { ONBOARDING_STORAGE_STRING } from 'constants';
import * as mainActions from '../dux';
import * as sharedActions from 'shared/state/dux';

export default (action$: Object) =>
    action$
        .ofType(mainActions.onboarded)
        .mergeMap(() => Observable.fromPromise(AsyncStorage.setItem(
            ONBOARDING_STORAGE_STRING,
            ONBOARDING_STORAGE_STRING
        ))
        .mapTo(sharedActions.emptyAction())
        .catch(error => sharedActions.logErrorAsObservable(error.xhr.response)));

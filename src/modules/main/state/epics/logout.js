// @flow
import { AsyncStorage } from 'react-native';
import { STORAGE_USER_SESSION } from 'constants';
import { Observable } from 'rxjs';
import * as mainActions from '../dux';
import * as sharedActions from 'shared/state/dux';

export default (action$: *) =>
    action$
        .ofType(mainActions.logout)
        .mergeMap(() => Observable.from(AsyncStorage.removeItem(STORAGE_USER_SESSION)))
        .mapTo(sharedActions.resetNavState())
        .catch(error => sharedActions.logErrorAsObservable(error));

// @flow
import { blue, lightGrey, red } from 'shared/styles/colors';
import { Text, TextInput, View } from 'react-native';
import React from 'react';
import styles from './styles';

type $Props = {
    autoFocus: boolean,
    borderColor: string,
    input: *,
    keyboardType: string,
    label: string,
    labelStyles: Object,
    meta: {
        error: ?string,
        touched: boolean,
        warning: *,
    },
    secureTextEntry: boolean,
    textInputStyles: Object,
}

const FormInput = ({
    autoFocus,
    borderColor,
    input: {
        onChange,
        ...rest,
    },
    keyboardType,
    label,
    labelStyles,
    meta: {
        error,
        touched,
    },
    secureTextEntry,
    textInputStyles,
}: $Props) => {
    const showError = touched && error;
    return (
        <View style={{ marginBottom: showError ? 20 : 20 }}>
            <Text style={[styles.label, labelStyles]}>
                {label}
            </Text>
            <TextInput
                autoCapitalize='none'
                autoFocus={autoFocus}
                keyboardType={keyboardType}
                onChangeText={onChange}
                secureTextEntry={secureTextEntry}
                selectionColor={blue}
                underlineColorAndroid='transparent'
                style={[
                    styles.textInput,
                    {
                        borderColor: showError ? red : borderColor,
                    },
                    textInputStyles,
                ]}
                {...rest}
            />
            {showError && (
                <Text style={styles.errorText}>
                    {error}
                </Text>
            )}
        </View>
    );
};

FormInput.defaultProps = {
    autoFocus: false,
    borderColor: lightGrey,
    keyboardType: 'default',
    labelStyles: {},
    secureTextEntry: false,
    textInputStyles: {},
};

export default FormInput;

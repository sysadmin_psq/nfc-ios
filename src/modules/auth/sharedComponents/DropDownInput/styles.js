// @flow
import { borderGrey, lightBlack, red } from "shared/styles/colors";
import { ProximaNova } from "shared/styles/fonts";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  errorText: {
    marginTop: 6,
    fontSize: 14,
    color: red,
    ...ProximaNova.Semibold
  },
  label: {
    fontSize: 12,
    marginBottom: 6,
    color: lightBlack,
    ...ProximaNova.Bold
  },
  textInput: {
    height: 50,
    width: "100%",
    paddingHorizontal: 6,
    backgroundColor: borderGrey,
    borderRadius: 4,
    borderWidth: 1.5
  }
});

// @flow
import { blue, lightGrey, red, white, borderGrey } from "shared/styles/colors";
import { Text, TextInput, View, Picker } from "react-native";
import React from "react";
import styles from "./styles";
import ActionSheet from 'react-native-actionsheet'


type $Props = {
  autoFocus: boolean,
  borderColor: string,
  input: *,
  keyboardType: string,
  label: string,
  labelStyles: Object,
  meta: {
    error: ?string,
    touched: boolean,
    warning: *
  },
  secureTextEntry: boolean,
  textInputStyles: Object
};

const DropDownInput = ({
  autoFocus,
  borderColor,
  input: { onChange, value, ...rest },
  keyboardType,
  label,
  labelStyles,
  meta: { error, touched },
  secureTextEntry,
  textInputStyles,
  data, fieldName
}: $Props) => {
  const showError = touched && error;


    var stateArray = []
    var genderArray = []
    var age_groupArray = []

    console.log("values", fieldName)
    switch(fieldName) {

        case 'state':
            stateArray = data.map(el => el.value)
            break;

        case 'range':
            age_groupArray = data.map(el => el.value)
            break;

        case 'gender':
            genderArray = data.map(el => el.value)
            break;
        default:
            console.log("ERROR")

    }


    showActionSheet = () => {
        this.ActionSheet.show()
    }

    showActionSheet2 = () => {
        this.ActionSheet2.show()
    }

    showActionSheet3 = () => {
        this.ActionSheet3.show()
    }

  return (
    <View style={{ marginBottom: showError ? 20 : 30 }}>
      <Text style={[styles.label, labelStyles]}>{label}</Text>
      <View style={styles.container}>

          {fieldName == 'state' &&

          <View style={{height: 50,
              width: "100%",
              paddingHorizontal: 6,
              backgroundColor: borderGrey,
              borderRadius: 4 ,}}>
            <Text style={{paddingTop: 15, paddingLeft: 5}}onPress={showActionSheet2}>{value || "NA"}</Text>
            <ActionSheet

                ref={o => this.ActionSheet2 = o}
                title={'Select State'}
                options={stateArray}
                onPress={(event) => {
                    onChange(stateArray[event]);}
                }
            />
          </View>
          }


          {fieldName == 'gender' &&

          <View style={{height: 50,
              width: "100%",
              paddingHorizontal: 6,
              backgroundColor: borderGrey,
              borderRadius: 4}}>

            <Text style={{paddingTop: 15, paddingLeft: 5}} onPress={this.showActionSheet3}>{value || "NA"}</Text>
            <ActionSheet

                ref={o => this.ActionSheet3 = o}
                title={'Select gender'}
                options={genderArray}
                onPress={(event) => {
                    onChange(genderArray[event]);}}
            />
          </View>
          }

          {fieldName == 'range' &&

          <View style={{height: 50,
              width: "100%",
              paddingHorizontal: 6,
              backgroundColor: borderGrey,
              borderRadius: 4}}>

            <Text style={{paddingTop: 15, paddingLeft: 5}} onPress={this.showActionSheet}>{value || "NA"}</Text>
            <ActionSheet

                ref={o => this.ActionSheet = o}
                title={'Select age range'}
                options={age_groupArray}
                onPress={(event) => {
                    onChange(age_groupArray[event]);}}
            />
          </View>
          }
      </View>
        {showError && <Text style={styles.errorText}>{error}</Text>}

    </View>
  );
};

DropDownInput.defaultProps = {
  autoFocus: false,
  borderColor: lightGrey,
  keyboardType: "default",
  labelStyles: {},
  secureTextEntry: false,
  textInputStyles: {}
};

export default DropDownInput;

// @flow
import { blue, white } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';
import { isIPhone4,isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import { isIOS } from 'shared/utils';
import { isIphoneX } from 'react-native-iphone-x-helper';

export default StyleSheet.create({
    backArrowContainer: {
        width: 40,
        height: 40,
        position: 'absolute',
        left: 8,
        top: isIPhoneX()?45:24,
        alignItems: 'center',
        justifyContent: 'center',
    },
    container: {
        height: 70,
        paddingTop: isIOS() ? 20 : 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: blue,
    },
    text: {
        fontSize: 22,
        color: white,
        ...ProximaNova.Bold,
        paddingTop:isIphoneX()?10:0,
    },
})

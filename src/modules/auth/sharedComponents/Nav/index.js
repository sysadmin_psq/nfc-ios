// @flow
import { blue } from 'shared/styles/colors';
import { connect } from 'react-redux';
import { Image, View, Text, TouchableOpacity } from 'react-native';
import * as sharedActions from 'shared/state/dux';
import { isIPhone4,isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import React from 'react';
import styles from './styles';

type $Props = {
    color: string,
    dispatch: *,
    showBackArrow: boolean,
};

const Nav = ({ color, dispatch, showBackArrow }: $Props) => (
    <View
        style={[
            styles.container,
            {
                backgroundColor: color,paddingTop:isIPhoneX()?60:20,paddingBottom:isIPhoneX()?25:0,
            },
        ]}
    >
        {showBackArrow && (
            <TouchableOpacity
                style={styles.backArrowContainer}
                onPress={() => dispatch(sharedActions.goBack())}
            >
                <Image source={require('assets/icons/BackArrow.png')} />
            </TouchableOpacity>
        )}
        <Text style={styles.text}>
            FITNESS COURT
        </Text>
    </View>
);

Nav.defaultProps = {
    color: blue,
    showBackArrow: true,
};

export default connect()(Nav);

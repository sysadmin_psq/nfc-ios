// @flow
import { black, blue, white } from 'shared/styles/colors';
import { isIPhone4, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen} from 'shared/utils';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';


export default StyleSheet.create({
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: blue,
        height: 50,
        marginBottom: 8,
        borderRadius: 4,
    },
    buttonText: {
        fontSize: 16,
        color: white,
        ...ProximaNova.Bold,
    },
    container: {
        flex: 1,
        marginTop:isIPhoneX()?-30:0,
        paddingTop:isIPhoneX()?35:0,
    },
    formContainer: {
        backgroundColor: white,
        flex: 1,
        paddingHorizontal: '10%',
        paddingVertical: 20,
    },
    extraText: {
        textAlign: 'center',
        fontSize: 14,
        color: black,
        ...ProximaNova.Regular,
    },
    linkContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 26,
    },
    linkText: {
        fontSize: 14,
        color: blue,
        ...ProximaNova.Bold,
    },
    textContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 30
    },
});

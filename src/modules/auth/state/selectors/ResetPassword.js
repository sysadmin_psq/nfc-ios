// @flow
import type { $AuthState } from 'types/state/auth';

export default ({ auth }: { auth: $AuthState }) => ({
    passwordReset: auth.passwordReset,
    passwordResetEmail: auth.passwordResetEmail,
});

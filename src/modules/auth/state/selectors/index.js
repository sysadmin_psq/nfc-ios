// @flow
import type { $AuthState } from 'types/state/auth';

export default ({ auth }: { auth: $AuthState }) => ({
    ...auth,
});

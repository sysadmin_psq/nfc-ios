// @flow
import { getFormSubmitErrors } from 'redux-form';

export default (state: *) => {
    const submitErrors = getFormSubmitErrors('login')(state);
    
    let emailPasswordError;
    if (submitErrors && submitErrors.emailPasswordError) {
        emailPasswordError = submitErrors.emailPasswordError;
    }
    return {
        emailPasswordError,
    };
}

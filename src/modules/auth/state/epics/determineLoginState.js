// @flow
import { AsyncStorage } from 'react-native';
import { Observable } from 'rxjs';
import { AUTH_ROUTES, STORAGE_USER_SESSION } from 'constants';
import * as authActions from '../dux';
import * as sharedActions from 'shared/state/dux';

export default (action$: *) =>
    action$
        .ofType(authActions.determineLoginState)
        .switchMap(() =>
            Observable
                .fromPromise(AsyncStorage.getItem(STORAGE_USER_SESSION))
                .map((response) => {
                    // if there is no user session, prompt the welcome screen
                    if (response === null) {
                        return sharedActions.navigate(AUTH_ROUTES.WELCOME);
                    }

                    console.log("response", response)
                    return authActions.startApp();
                })
        );

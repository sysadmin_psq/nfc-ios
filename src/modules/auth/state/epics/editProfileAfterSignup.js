// @flow
import { Observable } from 'rxjs';
import * as authActions from '../dux';
import * as sharedActions from 'shared/state/dux';
import Api from 'api';

export default (action$: *, store: *) =>
    action$
        .ofType(authActions.editProfileAfterSignup)
        .switchMap(action => {
            const {
                main: {
                    userInfo: {
                        id,
                    },
                },
                form: {
                    createAccountEmail: {
                        values: {
                            email,

                        },
                    },
                    createAccountName: {
                        values: {
                            fullName,
                            username,
                            city,
                            province,
                            age_range,
                            gender
                        },
                    },
                },
            } = store.getState();

            const body = {
                marketingOptIn: action.payload,
                name: fullName,
                userId: id,
                email,
                username,
                city,
                province,
                age_range,
                gender
            };
            // just in case the users/me endpoint fails
            if (!id) {
                return Observable.of(sharedActions.logError('no user id to edit profile'));
            }

            return Api
                .editProfile(body)
                .mapTo(sharedActions.emptyAction())
                .catch(error => Observable.of(sharedActions.logError(error)))
        });

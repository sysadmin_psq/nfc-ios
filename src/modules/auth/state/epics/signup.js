// @flow
import { AUTH_ROUTES } from 'constants';
import { defaultMarketingOptInValue } from '../initialState';
import { Observable } from 'rxjs';
import * as authActions from '../dux';
import * as mainActions from 'modules/main/state/dux';
import * as sharedActions from 'shared/state/dux';
import Api from 'api';

export default (action$: *, store: *) =>
    action$
        .ofType(authActions.signup)
        .switchMap(() => {
            const {
                main: {
                    deviceId,
                },
                form: {
                    createAccountEmail: {
                        values: {
                            email,
                            password,
                        },
                    },
                    createAccountName: {
                        values: {
                            fullName,
                            // username,
                            city,
                            province,
                            age_range,
                            gender
                        },
                    },
                },
            } = store.getState();

            const params = {
                marketingOptIn: defaultMarketingOptInValue,
                deviceId,
                email,
                fullName,
                password,
                city,
                province,
                age_range,
                gender
            };

            return Api
                .signup(params)
                .switchMap(() =>
                    Api
                        .fetchToken({
                            email,
                            password,
                        })
                        .switchMap(response =>
                            Observable.concat(
                                Observable.of(sharedActions.navigate(AUTH_ROUTES.TUTORIAL)),
                                Observable.of(authActions.storeUserSession(response.access_token)),
                                Observable.of(mainActions.fetchUserInfo()),
                            )
                        )
                        .catch(error => Observable.of(sharedActions.logError(error)))
                )
                .catch(error => Observable.concat(

                    Observable.of(sharedActions.createAutoCloseNotification('Something went wrong. Please try again.')),
                    Observable.of(sharedActions.logError(error))
                ));
        });

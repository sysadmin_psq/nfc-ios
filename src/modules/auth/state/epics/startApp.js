// @flow
import { AUTH_ROUTES } from 'constants';
import { isIOS } from 'shared/utils';
import { Observable } from 'rxjs';
import * as mainActions from 'modules/main/state/dux';
import * as authActions from '../dux';
import * as sharedActions from 'shared/state/dux';

const androidActions = () =>
    Observable.concat(
        Observable.of(sharedActions.navigate(AUTH_ROUTES.APP)),
        Observable.of(mainActions.promptAppPermissions()),
    );

const iosActions = () =>
    Observable.concat(
        Observable.of(mainActions.fetchUserInfo()),
        Observable.of(sharedActions.navigate(AUTH_ROUTES.APP)),
        Observable.of(mainActions.initializeApp()),
        //Observable.of(mainActions.promptCameraPermission()),
    );

export default (action$: *) =>
    action$
        .ofType(authActions.startApp)
        .switchMap(() => isIOS() ? iosActions() : androidActions());

// @flow
import { AsyncStorage } from 'react-native';
import { Observable } from 'rxjs';
import { STORAGE_USER_SESSION } from 'constants';
import * as authActions from '../dux';
import * as sharedActions from 'shared/state/dux';

export default (action$: *) =>
    action$
        .ofType(authActions.storeUserSession)
        .mergeMap(action => Observable.fromPromise(AsyncStorage.setItem(
            STORAGE_USER_SESSION,
            action.payload
        )))
        .mapTo(sharedActions.emptyAction())
        .catch(() => sharedActions.logErrorAsObservable('Error storing session in async storage'));

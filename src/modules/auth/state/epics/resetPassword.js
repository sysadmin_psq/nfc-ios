// @flow
import { Observable } from 'rxjs';
import * as authActions from '../dux';
import * as sharedActions from 'shared/state/dux';
import Api from 'api';

export default (action$: *) =>
    action$
        .ofType(authActions.resetPassword)
        .switchMap(action =>
            Api
                .resetPassword(action.payload)
                .mapTo(authActions.resetPasswordSuccess())
                .catch(error => Observable.of(sharedActions.logError(error)))
        );

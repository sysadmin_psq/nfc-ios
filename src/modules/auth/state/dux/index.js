// @flow
import { createAction, createReducer } from 'redux-act';
import initialState from '../initialState';
import type { $AuthState } from 'types/state/auth';

// actions
// -----------------------------------------------------------------------------
export const determineLoginState = createAction('Determines if user is logged in');
export const editProfileAfterSignup = createAction('Edits profile with marketing opt in');
export const signup = createAction('');
export const startApp = createAction('Triggers necessary actions to start application');
export const storeUserSession = createAction('');

// action reducer pairs
// -----------------------------------------------------------------------------
export const resetPassword = createAction('');
const resetPasswordReducer = (state: $AuthState, passwordResetEmail: string) => ({
    ...state,
    passwordResetEmail,
});

// TODO
export const resetPasswordSuccess = createAction('');
const resetPasswordSuccessReducer = (state: $AuthState, passwordResetEmail: string) => ({
    ...state,
    passwordResetEmail,
});

export const resetState = createAction('');
const resetStateReducer = () => initialState;


export const toggleMarketingOptIn = createAction('');
export const toggleMarketingOptInReducer = (state: $AuthState) => ({
    ...state,
    marketingOptIn: !state.marketingOptIn,
});

// -----------------------------------------------------------------------------
// $FlowFixMe
export default createReducer({
    [resetPassword]: resetPasswordReducer,
    [resetPasswordSuccess]: resetPasswordSuccessReducer,
    [resetState]: resetStateReducer,
    [toggleMarketingOptIn]: toggleMarketingOptInReducer,
}, initialState);

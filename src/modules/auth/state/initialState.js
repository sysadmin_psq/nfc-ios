// @flow
export const defaultMarketingOptInValue = true;

export default {
    marketingOptIn: defaultMarketingOptInValue,
    passwordReset: false,
    passwordResetEmail: '',
};

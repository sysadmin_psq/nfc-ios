// @flow
import AuthNavigator from '../../AuthNavigator';

export default (state: *, action: *) => {
    const nextState = AuthNavigator.router.getStateForAction(action, state);
    return nextState || state;
};

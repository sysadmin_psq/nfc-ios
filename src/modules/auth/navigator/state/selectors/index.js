// @flow
export default (state: *) => ({
    navigationState: state.appNavigation,
});

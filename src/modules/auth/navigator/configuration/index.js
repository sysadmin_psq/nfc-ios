// @flow
import { AUTH_ROUTES } from 'constants';
import MainNavigator from 'modules/main/navigator';
import CreateAccountEmail from 'modules/auth/screens/CreateAccountEmail';
import CreateAccountName from 'modules/auth/screens/CreateAccountName';
import LoadingScreen from 'modules/auth/screens/Loading';
import LoginScreen from 'modules/auth/screens/Login';
import ResetPasswordScreen from 'modules/auth/screens/ResetPassword';
import TutorialScreen from 'modules/auth/screens/Tutorial'
import WelcomeScreen from 'modules/auth/screens/Welcome';


export const stackNavigatorConfiguration = {
    headerMode: 'none',
    initialRouteName: AUTH_ROUTES.LOADING,
    navigationOptions: {
        gesturesEnabled: false,
    },
};

export const routeConfiguration = {
    App: { screen: MainNavigator },
    CreateAccountEmail: { screen: CreateAccountEmail },
    CreateAccountName: { screen: CreateAccountName },
    Loading: { screen: LoadingScreen },
    Login: { screen: LoginScreen },
    ResetPassword: { screen: ResetPasswordScreen },
    Tutorial: { screen: TutorialScreen },
    Welcome: { screen: WelcomeScreen },
};

// @flow
import AuthWithNavigationState from './AuthWithNavigationState';
import React from 'react';

export default () => (
    <AuthWithNavigationState />
);

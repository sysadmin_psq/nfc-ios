// @flow
import { BACK_ACTION_IDENTIFIER} from 'constants';
import { routeConfiguration, stackNavigatorConfiguration } from './configuration';
import { StackNavigator } from 'react-navigation';

const AuthNavigator = StackNavigator(
    routeConfiguration,
    stackNavigatorConfiguration,
);

const prevGetStateForAction = AuthNavigator.router.getStateForAction;
AuthNavigator.router.getStateForAction = (action, state) => {
    // This is necessary for navigating out of a nested navigator using the back action.
    // This will exit out of a nested navigator and go back to the app navigator when key is passed
    if (state && action.type === BACK_ACTION_IDENTIFIER && action.key) {
        const newRootRoutes = state.routes.slice(0, state.routes.length - 1);

        const appRoute = state.routes[state.routes.length - 1];
        const newAppRoutes = appRoute.routes.slice(0, appRoute.routes.length - 1);
        const a = {
            ...state,
            routes: [
                ...newRootRoutes,
                {
                    ...appRoute,
                    index: newAppRoutes.length - 1,
                    routes: newAppRoutes,
                },
            ],
        };

        return a;
    }

    return prevGetStateForAction(action, state);
}

export default AuthNavigator;

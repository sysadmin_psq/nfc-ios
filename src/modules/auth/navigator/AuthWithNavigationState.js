// @flow
import { addNavigationHelpers } from 'react-navigation';
import { connect } from 'react-redux';
import { addListener } from 'redux/store';
import AuthNavigator from './AuthNavigator';
import React from 'react';
import select from './state/selectors';

type $Props = {
    dispatch: *,
    navigationState: *,
}


const AuthWithNavigationState = ({ dispatch, navigationState}: $Props) => {
    return (
        <AuthNavigator
          navigation={addNavigationHelpers({
              state: navigationState,
              dispatch,
              addListener,
          })}
        />
    );
}

export default connect(select)(AuthWithNavigationState);

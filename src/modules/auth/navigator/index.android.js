// @flow
import { BackHandler } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import AuthWithNavigationState from './AuthWithNavigationState';
import React, { Component } from 'react';
import select from './state/selectors';

type $Props = {
    dispatch: *,
    navigationState: *,
}

class AuthWithNavigationStateWrapper extends Component<$Props> {
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => this.onBackPress());
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', () => this.onBackPress());
    }

    onBackPress() {
        const {
            dispatch,
            navigationState,
        } = this.props;

        if (navigationState.index === 0) {
          return false;
        }

        dispatch(NavigationActions.back());

        return true;
    }

    render() {
        return (
            <AuthWithNavigationState />
        );
    }
}
export default connect(select)(AuthWithNavigationStateWrapper);

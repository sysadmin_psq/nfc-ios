// @flow
import { black, lightBlack } from 'shared/styles/colors';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { View, Text, TouchableOpacity } from 'react-native';
import * as authActions from '../../state/dux';
import * as sharedActions from 'shared/state/dux';
import FormInput from 'modules/auth/sharedComponents/FormInput';
import Nav from '../../sharedComponents/Nav';
import React from 'react';
import select from '../../state/selectors/ResetPassword';
import styles, { labelStyles, textInputStyles } from './styles';

type $Props = {
    dispatch: *,
    handleSubmit: (*) => void,
    passwordReset: boolean,
    passwordResetEmail: string,
    submitting: boolean,
}

const validate = values => {
    const errors = {}
    // email validation
    if (!values.email) {
        errors.email = 'Please enter an email.'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Please enter a valid email';
    }

    return errors;
}

let ResetPassword = ({ dispatch, handleSubmit, passwordReset, passwordResetEmail, submitting }: $Props) => {
    const onSubmit = (values) => dispatch(authActions.resetPassword(values.email));

    let button;
    let middle;
    if (passwordReset) {
        button = (
            <TouchableOpacity
                style={styles.button}
                onPress={() =>  dispatch(authActions.resetPassword(passwordResetEmail))}
            >
                <Text style={styles.buttonText}>
                    Send Email Again
                </Text>
            </TouchableOpacity>
        );

        middle = (
            <View>
                <Text style={styles.header}>
                    Check Your Email
                </Text>
                <Text style={styles.paragraph}>
                    We just emailed a link to reset your password to{' '}
                    <Text style={styles.submittedEmailText}>
                        {passwordResetEmail}
                    </Text>
                </Text>
                <Text style={[styles.paragraph, styles.paragraphExtraMargin]}>
                    Follow the link in that message and you&apos;ll be back on
                    the app in no time
                </Text>
                <Text style={styles.paragraph}>
                    Didn&apos;t get the reset message?
                </Text>
            </View>
        );
    } else {
        button = (
            <TouchableOpacity
                disable={submitting}
                style={styles.button}
                onPress={handleSubmit(onSubmit)}
            >
                <Text style={styles.buttonText}>
                    Reset Password
                </Text>
            </TouchableOpacity>
        );
        middle = (
            <View>
                <Text style={styles.header}>
                    Need Help Signing In?
                </Text>
                <Text style={styles.paragraph}>
                    Enter your email or username and we&apos;ll send you a link
                    to reset your password
                </Text>
                <Field
                    borderColor={lightBlack}
                    component={FormInput}
                    keyboardType='email-address'
                    label='Email'
                    labelStyles={labelStyles}
                    name='email'
                    textInputStyles={textInputStyles}
                    autoFocus
                />
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <Nav color={black} />
            <View style={styles.bottomContainer}>
                {middle}
                {button}
                <Text
                    onPress={() => {
                        dispatch(sharedActions.goBack());
                        dispatch(authActions.resetState());
                    }}
                    style={styles.linkText}
                >
                    Back to Sign In.
                </Text>
            </View>
        </View>
    );
}

ResetPassword = reduxForm({
    form: 'resetPassword',
    validate,
})(ResetPassword);

export default connect(select)(ResetPassword);

// @flow
import { black, blue, editProfileGrey, lightBlack, white } from "shared/styles/colors";
import { ProximaNova } from "shared/styles/fonts";
import { StyleSheet } from "react-native";

export const labelStyles = {
    color: white,
};

export const textInputStyles = {
    backgroundColor: lightBlack,
    color: white,
};

export default StyleSheet.create({
    button: {
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: black,
        width: "100%",
        marginBottom: 14,
        height: 60,
        borderColor: editProfileGrey,
        borderWidth: 1,
        borderRadius: 4,
    },
    buttonText: {
        color: white,
        fontSize: 18,
        ...ProximaNova.Bold,
    },
    bottomContainer: {
        flex: 1,
        paddingHorizontal: 20,
        paddingTop: 20,
        backgroundColor: black,
        alignItems: "center",
    },
    container: {
        flex: 1,
    },
    header: {
        fontSize: 20,
        color: white,
        marginBottom: 6,
        textAlign: "center",
        ...ProximaNova.Bold,
    },
    linkText: {
        color: blue,
        fontSize: 16,
        ...ProximaNova.Bold,
    },
    paragraph: {
        color: white,
        fontSize: 16,
        marginBottom: 14,
        textAlign: "center",
        paddingHorizontal: 20,
        ...ProximaNova.Regular,
    },
    paragraphExtraMargin: {
        marginBottom: 40,
    },
    submittedEmailText: {
        color: white,
        fontSize: 16,
        ...ProximaNova.Bold,
    },
});

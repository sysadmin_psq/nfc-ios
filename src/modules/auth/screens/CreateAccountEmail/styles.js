// @flow
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    linkContainer: {
        marginLeft: 4,
    },
});

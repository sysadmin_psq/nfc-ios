// @flow
import { AUTH_ROUTES } from 'constants';
import { connect } from 'react-redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { View, Text, TouchableOpacity } from 'react-native';
import * as sharedActions from 'shared/state/dux';
//import * as authActions from '../../state/dux';
import Api from 'api';
import FormInput from 'modules/auth/sharedComponents/FormInput';
import Nav from '../../sharedComponents/Nav';
import React from 'react';
import styles from './styles';
import sharedStyles from '../../sharedStyles';

type $Props = {
    dispatch: *,
    handleSubmit: (*) => void, // redux-form's submit prop
    submitting: boolean,
}
const validate = values => {
    const errors = {}
    // email validation
    if (!values.email) {
        errors.email = 'Please enter an email.'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Please enter a valid email';
    }
    // password validation
    if (!values.password) {
        errors.password = 'Please enter a password.'
    } else if (values.password.length < 6) {
        errors.password = 'Please enter a password longer than 5 characters.'
    }

    return errors;
};

let CreateAccountEmail = ({ dispatch, handleSubmit, submitting }: $Props) => {
    const onSubmit = (values) => (
        Api
            .checkEmail(values.email)
            .toPromise()
            .then(response => {
                if (response.exists) {
                    throw new SubmissionError({
                        email: 'Email already exists.',
                    });
                } else {
                    dispatch(sharedActions.navigate(AUTH_ROUTES.CREATE_ACCOUNT_NAME));

                    //console.log("inside the else", authActions)
                    //dispatch(authActions.signup());
                }
            })
            .catch(() => {
                throw new SubmissionError({
                    email: 'Email already exists.',
                });
            })
    );

    return (
        <View style={sharedStyles.container}>
            <Nav />
            <View style={sharedStyles.formContainer}>
                <Field
                    component={FormInput}
                    keyboardType='email-address'
                    label='Enter Email'
                    name='email'
                    autoFocus
                />
                <Field
                    component={FormInput}
                    label='Create Password'
                    name='password'
                    secureTextEntry
                />
                <TouchableOpacity
                    disable={submitting}
                    style={sharedStyles.button}
                    onPress={handleSubmit(onSubmit)}
                >
                    <Text style={sharedStyles.buttonText}>
                        Next
                    </Text>
                </TouchableOpacity>
                <View style={sharedStyles.textContainer}>
                    <Text style={sharedStyles.extraText}>
                        Already have an account?
                    </Text>
                    <TouchableOpacity
                        style={[sharedStyles.linkContainer, styles.linkContainer]}
                        onPress={() => dispatch(sharedActions.navigate(AUTH_ROUTES.LOGIN))}
                    >
                        <Text style={sharedStyles.linkText}>
                            Sign In
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

CreateAccountEmail = reduxForm({
    form: 'createAccountEmail',
    validate,
})(CreateAccountEmail);

export default connect()(CreateAccountEmail);

// @flow
import { blue } from 'shared/styles/colors';
import { connect } from 'react-redux';
import { View } from 'react-native';
import * as mainActions from 'modules/main/state/dux';
import * as authActions from '../../state/dux';
import React, { Component } from 'react';

type $Props = {
    dispatch: *,
};

class LoadingScreen extends Component<$Props> {
    componentWillMount() {
        // Device info is needed for the signup request
        const { dispatch } = this.props;
        dispatch(mainActions.getDeviceInfo());
        dispatch(authActions.determineLoginState());
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: blue }} />
        );
    }
}

export default connect()(LoadingScreen);

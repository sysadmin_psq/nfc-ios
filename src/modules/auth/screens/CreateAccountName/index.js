// @flow
import { connect } from 'react-redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { Linking, View, Text, TouchableOpacity, ScrollView } from 'react-native';
import * as authActions from '../../state/dux';
import Api from 'api';
import FormInput from 'modules/auth/sharedComponents/FormInput';
import Nav from '../../sharedComponents/Nav';
import React from 'react';
import sharedStyles from '../../sharedStyles';
import styles from './styles';
import { STATES_NAME, AGE_RANGES, GENDERS } from "../../../../constants/index";
import DropDownInput from "../../sharedComponents/DropDownInput/index";



type $Props = {
    dispatch: *,
    handleSubmit: (*) => void, // redux-form's submit prop
    submitting: boolean,
}

const validate = values => {
    const errors = {};
    // full name validation
    if (!values.fullName) {
        errors.fullName = 'Please enter a name'
    }
    // username validation
    if (!values.username) {
        errors.username = 'Please enter a username'
    }
    if(!values.city) {

        errors.city = 'Please enter city'
    }
    if(!values.province) {

        errors.province = 'Please select state'
    }
    if(!values.age_range) {

        errors.age_range = 'Please select age range'
    }
    if(!values.gender) {

        errors.gender = 'Please select gender'
    }


    return errors;
};

const stateMappedData = [

    ...STATES_NAME.map(el => {
        return {
            value: el.name
        };
    })
];
const ageRangeMappedData = [ ...AGE_RANGES];
const genderMappedData = [ ...GENDERS];

let CreateAccountName = ({ dispatch, handleSubmit, submitting }: $Props) => {
    const onSubmit = (values) => (


        dispatch(authActions.signup())
        // Api
        //     //.checkUsername(values.username)
        //     .toPromise()
        //     .then(response => {
        //         if (response.exists) {
        //             throw new SubmissionError({
        //                 username: 'Oops. That username is taken.',
        //             });
        //         } else {
        //             console.log("inside the success")
        //             dispatch(authActions.signup());
        //         }
        //     })
        //     .catch(() => {
        //         throw new SubmissionError({
        //             username: 'Oops. That username is taken.',
        //         });
        //     })
    );

    return (
        <View style={sharedStyles.container}>
            <Nav />
            <ScrollView style={sharedStyles.formContainer}>
                <Field
                    component={FormInput}
                    label='Full Name'
                    name='fullName'
                />


                <Field
                    component={FormInput}
                    // keyboardType="characters"
                    label="City"
                    name="city"
                />
                <Field
                    component={DropDownInput}
                    data={stateMappedData}
                    label="Province"
                    name="province"
                    fieldName="state"

                ></Field>
                <Field
                    component={DropDownInput}
                    data={ageRangeMappedData}
                    label="Age Range"
                    name="age_range"
                    fieldName="range"
                ></Field>
                <Field
                    component={DropDownInput}
                    data={genderMappedData}
                    label="Gender"
                    name="gender"
                    fieldName="gender"
                ></Field>
                {/*<Field*/}
                    {/*component={FormInput}*/}
                    {/*label='Public Username'*/}
                    {/*name='username'*/}
                {/*/>*/}
                <TouchableOpacity
                    disabled={submitting}
                    style={sharedStyles.button}
                    onPress={handleSubmit(onSubmit)}
                >
                    <Text style={sharedStyles.buttonText}>
                        Finish
                    </Text>
                </TouchableOpacity>
                <Text style={sharedStyles.extraText}>
                    By signing up, you agree to our
                </Text>
                <View style={sharedStyles.textContainer}>
                    <TouchableOpacity
                        style={sharedStyles.linkContainer}
                        onPress={() => Linking.openURL('https://nationalfitnesscampaign.com/privacy')}
                    >
                        <Text style={sharedStyles.linkText}>
                            Safety and Privacy Policy
                        </Text>
                    </TouchableOpacity>
                    <Text style={[sharedStyles.extraText, styles.text]}>
                        and
                    </Text>
                    <TouchableOpacity
                        style={sharedStyles.linkContainer}
                        onPress={() => Linking.openURL('https://nationalfitnesscampaign.com/terms-of-service')}
                    >
                        <Text style={sharedStyles.linkText}>
                            Terms.
                        </Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    );
}

CreateAccountName = reduxForm({
    form: 'createAccountName',
    validate,
})(CreateAccountName);

export default connect()(CreateAccountName);

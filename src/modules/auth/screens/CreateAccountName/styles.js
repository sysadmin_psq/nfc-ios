// @flow
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    text: {
        marginHorizontal: 4,
    },
});

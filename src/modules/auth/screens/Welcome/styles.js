// @flow
import { StyleSheet } from "react-native";
import { isIPhone4, isIPhoneX } from "shared/utils";
import { black, blue, white } from "shared/styles/colors";
import { ProximaNova } from "shared/styles/fonts";

const button = {
    width: '80%',
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 2,
};

const buttonText = {
    fontSize: 20,
    ...ProximaNova.Bold,
};

export default StyleSheet.create({
    bottomBox: {
        flex: 1,
        backgroundColor: "black",
        alignItems: "center",
        justifyContent: "center",
        marginTop: isIPhone4() ? -60 : 0,
    },
    container: {
        flex: 1,
        backgroundColor: white,
        marginTop:isIPhone4()?-30:0,
        paddingTop:isIPhone4()?45:0,
    },
    createAccountButton: {
        backgroundColor: blue,
        ...button,
    },
    createAccountButtonText: {
        color: white,
        ...buttonText,
    },
    signInButton: {
        marginBottom: 20,
        backgroundColor: white,
        ...button,
    },
    signInButtonText: {
        color: black,
        ...buttonText,
    },
});

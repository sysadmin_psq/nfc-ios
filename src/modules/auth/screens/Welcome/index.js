// @flow
import { AUTH_ROUTES, S3_BUCKET_URL } from 'constants';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import * as sharedActions from 'shared/state/dux';
import Nav from '../../sharedComponents/Nav';
import Video from 'shared/components/Video';
import React, { Component } from 'react';
import SplashScreen from 'react-native-splash-screen';
import styles from './styles';

type $Props = {
    dispatch: *,
};

class WelcomeScreen extends Component<$Props> {
    componentWillMount() {
        SplashScreen.hide();
    }

    render() {
        const {
            dispatch,
        } = this.props;

        return (
            <View style={styles.container}>
                <Nav showBackArrow={false} />
                <Video source={`${S3_BUCKET_URL}/welcome_videos/WelcomeVideo.mp4`} />
                <View style={styles.bottomBox}>
                    <TouchableOpacity
                        style={styles.signInButton}
                        onPress={() => dispatch(sharedActions.navigate(AUTH_ROUTES.LOGIN))}
                    >
                        <Text style={styles.signInButtonText}>
                            Sign In
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.createAccountButton}
                        onPress={() => dispatch(sharedActions.navigate(AUTH_ROUTES.CREATE_ACCOUNT_EMAIL))}
                    >
                        <Text style={styles.createAccountButtonText}>
                            Create Account
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default connect()(WelcomeScreen);

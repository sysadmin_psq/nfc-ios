// @flow
import { isIOS } from 'shared/utils';
import { blue, lightBlack, white } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';

export const checkboxStyle = {
    width: '100%',
};

export const checkboxTextStyle = {
    marginLeft: 14,
    color: white,
    fontSize: 16,
    ...ProximaNova.Regular,
};

export default StyleSheet.create({
    bottomBox: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 40,
    },
    button: {
        backgroundColor: blue,
        width: '100%',
        height: 85,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        color: white,
        fontSize: 20,
        ...ProximaNova.Bold,
    },
    container: {
        flex: 1,
        backgroundColor: lightBlack,
    },
    header: {
        paddingTop: isIOS() ? 20 : 0,
        height: 85,
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerText: {
        color: white,
        fontSize: 25,
        ...ProximaNova.Bold,
    },
});

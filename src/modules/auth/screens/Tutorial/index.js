// @flow
import { S3_BUCKET_URL } from 'constants';
import { blue } from 'shared/styles/colors';
import { connect } from 'react-redux';
import { defaultMarketingOptInValue } from '../../state/initialState';
import { View, Text, TouchableOpacity } from 'react-native';
import * as authActions from '../../state/dux';
import CheckBox from 'react-native-check-box';
import React from 'react';
import select from '../../state/selectors';
import styles, { checkboxStyle, checkboxTextStyle } from './styles';
import Video from 'shared/components/Video';


type $Props = {
    dispatch: *,
    marketingOptIn: boolean,
};

const checkboxText = "Let me know if i'm eligible for prizes, challenge, or updates";

const TutorialScreen = ({ dispatch, marketingOptIn }: $Props) => {
    const onEnterPress = () => {
        dispatch(authActions.startApp());
        if (defaultMarketingOptInValue !== marketingOptIn) {
            dispatch(authActions.editProfileAfterSignup(marketingOptIn));
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.headerText}>
                    You&apos;re All Set!
                </Text>
            </View>
            <Video source={`${S3_BUCKET_URL}/welcome_videos/TutorialVideo.mp4`} />
            <View style={styles.bottomBox}>
                <CheckBox
                    rightText={checkboxText}
                    onClick={() => dispatch(authActions.toggleMarketingOptIn())}
                    checkBoxColor={blue}
                    isChecked={marketingOptIn}
                    rightTextStyle={checkboxTextStyle}
                    style={checkboxStyle}
                />
            </View>
            <TouchableOpacity
                style={styles.button}
                onPress={onEnterPress}
            >
                <Text style={styles.buttonText}>
                    FIND NEAREST FITNESS COURT
                </Text>
            </TouchableOpacity>
        </View>
    );
};

export default connect(select)(TutorialScreen);

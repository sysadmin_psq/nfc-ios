// @flow
import { AUTH_ROUTES } from 'constants';
import { black } from 'shared/styles/colors';
import { connect } from 'react-redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { View, Text, TouchableOpacity } from 'react-native';
import * as authActions from 'modules/auth/state/dux';
import * as sharedActions from 'shared/state/dux';
import Api from 'api';
import FormInput from 'modules/auth/sharedComponents/FormInput';
import Nav from '../../sharedComponents/Nav';
import React from 'react';
import select from 'modules/auth/state/selectors/Login';
import styles from './styles';
import sharedStyles from '../../sharedStyles';

type $Props = {
    dispatch: *,
    handleSubmit: (*) => void, // redux-form's submit prop
    emailPasswordError: ?string,
    submitting: boolean,
}

const validate = values => {
    const errors = {}
    // email validation
    if (!values.email) {
        errors.email = 'Please enter your email.'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Please enter a valid email';
    }
    // password validation
    if (!values.password) {
        errors.password = 'Please enter your password.'
    }

    return errors;
};

let Login = ({ dispatch, emailPasswordError, handleSubmit, submitting }: $Props) => {
    const onSubmit = (values) => (
        Api
            .fetchToken({
                email: values.email,
                password: values.password,
            })
            .toPromise()
            .then(response => {
                if (response.error) {

                    console.log("response.error", response.error)

                    throw new SubmissionError({
                        emailPasswordError: 'Your Email or Password is Incorrect',
                    });
                } else {
                    dispatch(authActions.storeUserSession(response.access_token));
                    dispatch(authActions.startApp());
                }
            })
            .catch(() => {
                throw new SubmissionError({
                    emailPasswordError: 'Your Email or Password is Incorrect',
                });
            })
    );

    return (
        <View style={sharedStyles.container}>
            <Nav color={black} />
            <View style={sharedStyles.formContainer}>
                {emailPasswordError && (
                    <Text style={styles.emailPassError}>
                        {emailPasswordError}
                    </Text>
                )}
                <Field
                    component={FormInput}
                    label='Email'
                    name='email'
                    keyboardType='email-address'
                    autoFocus
                />
                <Field
                    component={FormInput}
                    label='Password'
                    name='password'
                    secureTextEntry
                />
                <TouchableOpacity
                    disable={submitting}
                    style={sharedStyles.button}
                    onPress={handleSubmit(onSubmit)}
                >
                    <Text style={sharedStyles.buttonText}>
                        Next
                    </Text>
                </TouchableOpacity>
                <View style={sharedStyles.textContainer}>
                    <Text style={sharedStyles.extraText}>
                        Forgot Your Password?
                    </Text>
                    <TouchableOpacity
                        style={[sharedStyles.linkContainer, styles.linkContainer]}
                        onPress={() => {}}
                    >
                        <Text
                            onPress={() => dispatch(sharedActions.navigate(AUTH_ROUTES.RESET_PASSWORD))}
                            style={sharedStyles.linkText}
                        >
                            Get help signing in.
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

Login = reduxForm({
    form: 'login',
    validate,
})(Login);

export default connect(select)(Login);

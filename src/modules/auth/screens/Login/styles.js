// @flow
import { red } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts'
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    emailPassError: {
        color: red,
        fontSize: 16,
        ...ProximaNova.Regular,
    },
    linkContainer: {
        marginLeft: 4,
    },
});

// @flow
import 'url-search-params-polyfill';

export const querystringCreator = (params: Object): string => {
    let queryString = new URLSearchParams();
    for (let key in params) {
        queryString.append(key, params[key]);
    }

    return queryString.toString();
}

export const createFileName = (deviceId: string) => {
    const currentTime = new Date().getTime();
    return `${deviceId}_${currentTime}.mp4`;
}

export function logServerRequest({
    path,
    url,
    options,
    response,
    error,
}: {
    path: string,
    url: string,
    options: any,
    response?: Response,
    error?: string,
}) {
    /* eslint-disable */
    console.log('');
    if (error) {
        console.log(`%c Api Response Failure: ${path}`, 'color: #F64744', url);
        console.group('information');
        console.log('error: ', error);
    } else if (response) {
        if (response.ok) {
            console.log(`%c Api Response Success: ${path}`, 'color: #66B34E', url);
            console.groupCollapsed('information');
        } else {
            console.log(`%c Api Response Failure: ${path}`, 'color: #F64744', url);
            console.group('information');
            console.log('status: ', response.status);
        }
    }
    console.log('Request Options: ', options);
    console.log('Response Object: ', response);
    console.groupEnd();
    console.log('');
    /* eslint-enable */
}

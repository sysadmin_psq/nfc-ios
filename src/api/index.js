// @flow
import { createFileName, logServerRequest, querystringCreator } from 'api/utils';
import { Observable } from 'rxjs';
import base64 from 'base-64';
import Config from 'react-native-config';
import LocalStorage from './LocalStorage';

export default class Api {
    static config = {
        apiKey: 'UkNNuT0bebf2bRoCMitx',
        clientSecret: Config.CLIENT_SECRET,
        clientId: Config.CLIENT_ID,
        serverUrl: Config.SERVER_URL,
    };

    static _addAuth(params: Object): Object {
        return {
            api_key: this.config.apiKey,
            ...params,
        };
    }

    static _addOAuth(headers: Headers, token: string): void {
        headers.append(
            'Authorization',
            'Bearer ' + token
        );
    }

    static _get(endpoint: string, params: Object) {
        const paramsWithAuth = this._addAuth(params);
        const stringifiedParams = encodeURI(querystringCreator(paramsWithAuth));
        const path = `/${endpoint}/?${stringifiedParams}`;

        const options = {
            method: 'get',
        };

        return this._request(path, options, true);
    }

    static _patch(endpoint: string, body: Object) {
        const path = `/${endpoint}/`;

        const headers = new Headers({
            'Content-Type': 'application/json',
        });

        const options = {
            body: JSON.stringify(body),
            method: 'PATCH',
            headers,
        };
        return this._request(path, options, true);
    }

    static _post(
        endpoint: string,
        body: Object | string,
        stringify: boolean = true,
        addOAuth: boolean = true,
    ) {
        const path = `/${endpoint}/`;
        const headers = new Headers();
        const options = {
            method: 'POST',
            body,
            headers,
        };

        if (stringify) {
            headers.append('Content-Type', 'application/json');

            options.body = JSON.stringify(body);
        }

        return this._request(path, options, addOAuth);
    }

    static _postVideo(endpoint: string, body: Object, deviceId: string, videoLocation: string) {
        const bodyWithAuth = this._addAuth(body);

        const formBody = new FormData();
        for (const key of Object.keys(bodyWithAuth)) {
            formBody.append(key, bodyWithAuth[key]);
        }

        // $FlowFixMe
        formBody.append('video', {
            uri: videoLocation,
            name: createFileName(deviceId),
            type: 'video/mp4',
        });

        return this._post(endpoint, formBody, false);
    }

    static _request(path: string, options: Object, addOAuth: boolean = false): Observable<*> {
        const apiUrl = `${this.config.serverUrl}/api/v1`;
        const url = `${apiUrl}${path}`;

        function makeRequest(): Observable<*> {
            const request = fetch(url, options)
                .then((response) => {
                    // TODO: make this work
                    // if (__DEV__) {
                    //     logServerRequest({ path, url, options, response });
                    // }

                    if (response.ok) {

                        var response = response.json()
                        console.log("response in request", response)
                        return response
                    } else {
                        throw new Error({status: response.status});
                    }
                })
                .catch(error => {
                    logServerRequest({ path, url, options, error });
                    throw Error(error);
                });

            return Observable.from(request);
        }

        if (addOAuth) {
            return LocalStorage
                .getToken()
                .switchMap(token => {
                    if (options.headers) {
                        this._addOAuth(options.headers, token);
                    } else {
                        const headers = new Headers();
                        this._addOAuth(headers, token);

                        options.headers = headers;
                    }

                    return makeRequest();
                });
        }

        return makeRequest();
    }

    static checkEmail(email: string) {
        const body = this._addAuth({
            email,
        });
        return this._post('check_email', body);
    }

    static checkUsername(username: string) {
        const body = this._addAuth({
            username,
        });

        return this._post('check_username', body);
    }

    static editProfile({ email, marketingOptIn, name, userId, username, city, province, age_range, gender}) {
        const body = {
            fullname: name,
            marketing_opt_in: marketingOptIn,
            email,
            // username,
            city,
            state: province,
            age_group: age_range,
            gender,

        };

        return this._patch(`users/${userId}`, body);
    }

    static fetchFitnessCourt(position) {

        const params = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
        };

        return this._get('fitness_court', params);
    }

    static fetchAllCourts(position) {
        let params = {};

        if (position) {
          params = {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
          };
        }
        return this._get('courts', params);
    }

    static fetchLearnFeed(page: number, fitnessCourtId: number) {
        const params = {
            page,
        };

        if (fitnessCourtId) {
            // $FlowFixMe
            params.fitness_court_id = fitnessCourtId;
        }

        return this._get('learn_videos', params)
    }

    static fetchSponsorInfo() {
        return this._get('guided_circuit', {});
    }

    static fetchTrainAudio(fitnessCourtId: number) {
        const params = {
            show_trainers: true,
            is_new: true
        };

        if (fitnessCourtId) {
            // $FlowFixMe
            params.fitness_court_id = fitnessCourtId;
        }

        return this._get('train_audio', params);
    }

    static fetchToken({ email, password }) {


        // console.log("url", this.config.serverUrl)
        // console.log("this.config.clientId", this.config.clientId)
        // console.log("this.config.clientSecret", this.config.clientSecret)

        const body = {
            client_id: this.config.clientId,
            client_secret: this.config.clientSecret,
            grant_type: 'password',
            username: email.toLowerCase(),
            password,
        };

        const formBody = new FormData();
        for (const key of Object.keys(body)) {
            formBody.append(key, body[key]);
        }

        // TODO: Refactor this into this._request
        // This endpoint responds to a 401 response, unlike this._request,
        // which purposely throws an error on any (response.ok === false) in
        // the existing implementation
        const apiUrl = `${this.config.serverUrl}/api/v1`;
        const url = `${apiUrl}/o/token/`;

        const options = {
            body: formBody,
            method: 'POST',
        };

        const request = fetch(url, options)
            .then((response) => {
                if (response.ok || response.status === 401) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .catch(error => console.log(error)); // eslint-disable-line

        return Observable.from(request);
    }

    static fetchUserInfo() {


        return this._get('users/me', {});
    }

    static postChallengeVideo({
        challengeId,
        deviceId,
        email,
        fitnessCourtId,
        totalScore,
        videoLocation,
    }) {
        const body = {
            challenge: challengeId,
            device_id: deviceId,
            fitness_court: fitnessCourtId,
            score: totalScore,
            email,
        };

        return this._postVideo('challenge_videos', body, deviceId, videoLocation);
    }

    static postWorkoutVideo({ deviceId, exerciseIndex, fitnessCourtId, videoLocation }) {
        const body = {
            device_id: deviceId,
            exercise: exerciseIndex,
            fitness_court: fitnessCourtId,
        };

        return this._postVideo('workout_videos', body, deviceId, videoLocation);
    }

    static reportVideo({ deviceId, reportReason, videoId }) {
        const body = this._addAuth({
            device_id: deviceId,
            workout_video: videoId,
            reason: reportReason,
        });

        return this._post('flag', body);
    }

    static login(email, password) {
        const headers = new Headers({
            'Authorization': 'Basic ' + base64.encode(`${email}:${password}`),
        });

        const options = {
            method: 'get',
            headers,
        };
        // TODO: Refactor this into this._request
        // This endpoint responds to a 401 response, unlike this._request,
        // which purposely throws an error on any (response.ok === false) in
        // the existing implementation
        const apiUrl = `${this.config.serverUrl}/api/v1`;
        const url = `${apiUrl}/login/`;

        const request = fetch(url, options)
            .then((response) => {
                if (response.ok || response.status === 401) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            });

        return Observable.from(request);
    }

    static resetPassword(email: string) {
        const body = {
            email,
        };

        return this._post('password/reset', body, true, false);
    }

    static signup({ deviceId, email, fullName, marketingOptIn, password, username, city, province, age_range, gender }) {
        const body = {
            accepted_terms: new Date(),
            device_id: deviceId,
            email: email.toLowerCase(),
            fullname: fullName,
            marketing_opt_in: marketingOptIn,
            password: base64.encode(password),
            username,
            city,
            state: province,
            age_group: age_range,
            gender

        };

        return this._post('sign_up', body, true, false);
    }
}

// @flow
import { AsyncStorage } from "react-native";
import { Observable } from "rxjs";
import { STORAGE_USER_SESSION } from "constants";

let cacheToken;

export default class LocalStorage {
    static getToken(): Observable {
        // store token in cache (basically just a variable) after success so we
        // don't have to access localStorage storage every api call. Even though
        // AsyncStorage *shouldn't* be volatile, it's better not to risk it for every
        // api call. React even recommends putting a wrapper around AsyncStorage just in
        // case.
        // This is not really best practice since it makes this file stateful,
        // but it's better than making the api stateful or having to pass the
        // token through redux / epics for every api call that needs it.
        if (cacheToken) {
            return Observable.just(cacheToken);
        }

        return Observable.fromPromise(AsyncStorage.getItem(STORAGE_USER_SESSION));
    }
}

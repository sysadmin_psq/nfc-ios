// @flow
import type { $Response, $ServerFitnessCourt } from 'types/api/FitnessCourt';
import type { $FitnessCourt } from 'types/state/main';

export const FitnessCourtSerializer = (response: $Response): $FitnessCourt => {
    if (typeof response === 'string') {
        // default FitnessCourt in case back end errors
        return {
            city: 'San Francisco',
            id: 1,
            name: 'Marina Green',
            street: null,
            image: null,
            sponsorName: null,
            callToActionLink: null,
            callToActionDisplay: null,
            openingSoon: false

        }
    }

    const fitnessCourt: $ServerFitnessCourt = response[0];

    const serializedFitnessCourt: $FitnessCourt = {
        city: fitnessCourt.city_name,
        id: fitnessCourt.id,
        name: fitnessCourt.name,
        street: fitnessCourt.street,
        image: fitnessCourt.court_image,
        sponsorName: fitnessCourt.sponsor_name,
        callToActionLink: fitnessCourt.call_to_action_link,
        callToActionDisplay: fitnessCourt.call_to_action_display,
        openingSoon: fitnessCourt.opening_soon,
        latitude: fitnessCourt.latitude,
        longitude: fitnessCourt.longitude,
        location_id: fitnessCourt.location_id
    };
    return serializedFitnessCourt;
};

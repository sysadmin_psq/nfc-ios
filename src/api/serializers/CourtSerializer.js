// @flow
import type { $ServerFitnessCourt } from 'types/api/FitnessCourt';
import type { $FitnessCourt } from 'types/state/main';

export const CourtSerializer = (response: Array<$ServerFitnessCourt>): Array<$FitnessCourt> => {

    return response.map((fitnessCourt: $ServerFitnessCourt) => (
      {
          city: fitnessCourt.city_name,
          id: fitnessCourt.id,
          name: fitnessCourt.name,
          street: fitnessCourt.street,
          image: fitnessCourt.court_image,
          sponsorName: fitnessCourt.sponsor_name,
          callToActionLink: fitnessCourt.call_to_action_link,
          callToActionDisplay: fitnessCourt.call_to_action_display,
          openingSoon: fitnessCourt.opening_soon,
          latitude: fitnessCourt.latitude,
          longitude: fitnessCourt.longitude,
          location_id: fitnessCourt.location_id
      })
  );
};

// @flow
import type { $SponsorInfo } from 'types/state/main';
import type { $SponsorInfoResponse } from 'types/api/SponsorInfo';

export const SponsorInfoSerializer = (response: $SponsorInfoResponse): $SponsorInfo => {
    const info = response.results[0];

    const exerciseTimes: Array<number> = [
        info.exercise_one_start,
        info.exercise_two_start,
        info.exercise_three_start,
        info.exercise_four_start,
        info.exercise_five_start,
        info.exercise_six_start,
        info.exercise_seven_start,
    ];
    return {
        sponsorImage: info.sponsor_image,
        sponsorLink: info.sponsor_link,
        exerciseTimes,
    };
}

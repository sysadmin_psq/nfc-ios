// @flow
import type { $UserInfo } from 'types/state/main';
import type { $UserInfoResponse } from 'types/api/UserInfo';

export const UserInfoSerializer = (response: $UserInfoResponse): $UserInfo => ({
    email: response.email,
    id: response.id,
    marketingOptIn: response.marketing_opt_in,
    name: response.fullname,
    username: response.username,
    city: response.city,
    province: response.state,
    gender: response.gender,
    age_range: response.age_group

});

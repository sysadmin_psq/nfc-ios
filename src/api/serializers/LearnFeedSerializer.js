// @flow
import type { $Video } from 'types/state/main';
import type { $Response } from 'types/api/Exercise';

export const LearnFeedSerializer = (response: $Response) => {
    if (response.results) {
        return response.results.map(({id, video}): $Video => ({
            url: video,
            id,
        }));
    } else {
        // page is invalid
        return [];
    }
};

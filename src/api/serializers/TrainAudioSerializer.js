// @flow
import type { $TrainAudio } from 'types/state/main';
import type { $TrainAudioResponse } from 'types/api/TrainAudio';

export const TrainAudioSerializer = (response: $TrainAudioResponse): $TrainAudio => {

    console.log("serilioszaers", response)
    return response.results.map(item => ({
        sponsorImage: item.sponsor_image,
        sponsorLink: item.sponsor_link,
        audioFileName: item.audio_file_name,
        outroStartTime: item.outro_start,
        thumbnailImage: item.thumbnail_image,
        trainerName: item.trainer_name,
        trainerTitle: item.trainer_title,
        trainerImage: item.trainer_image,
        trainerExternalLink: item.trainer_external_link,
        trainerVideo: item.trainer_video,
        exerciseTimes: item.chapters.map(chapter => chapter.start_time),
        seconds_to_skip: item.seconds_to_skip,
    }));
}

// @flow
export const ProximaNova = {
    Bold: {
        fontFamily: "Proxima Nova",
        fontWeight: "bold"
    },
    CondensedBold: {
        fontFamily: "Proxima Nova Condensed",
        fontWeight: "bold"
    },
    CondensedSemibold: {
        fontFamily: "Proxima Nova Condensed",
        fontWeight: "600",
    },
    Medium: {
        fontFamily: "Proxima Nova",
        fontWeight: "500",
    },
    Regular: {
        fontFamily: "Proxima Nova"
    },
    Condensed: {
        fontFamily: "Proxima Nova Condensed",
    },
    Semibold: {
        fontFamily: "Proxima Nova",
        fontWeight: "600",
    },
    SemiboldItalic: {

        fontFamily: "Proxima Nova",
        fontWeight: "600",

    }
}

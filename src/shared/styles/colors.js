// @flow
export const black = "rgb(30, 30, 30)";
export const blue = "rgb(0, 174, 238)";
export const blueActive = "rgb(0, 152, 209)";
export const blueTopNav = "rgb(0, 152, 209)";
export const borderGrey = "rgb(235, 237, 242)";
export const challengeCountdownGrey = "rgb(219, 219, 219)";
export const challengeDividerGrey = "rgba(255, 255, 255, 0.07)";
export const challengeTextBlue = "rgba(0, 0, 0, 0.32)"
export const darkBlue = "rgb(84, 147, 189)";
export const mediumBlue = "rgb(18, 125, 173)";
export const greyBlue = "rgb(0, 68, 94)";
export const darkGrey = "rgb(38, 38, 38)";
export const dotActiveGrey = "rgb(159, 159, 159)";
export const dotGrey = "rgba(159, 159, 159, 0.48)";
export const editProfileGrey = "rgb(85, 85, 85)";
export const grey = "rgba(47, 47, 47, 0.42)";
export const lightBlack = "rgb(47, 47, 47)";
export const lightBlue = "rgb(97, 172, 236)";
export const lighterBlue = "rgb(52, 190, 242)";
export const lightGrey = "rgb(232, 232, 232)";
export const skyBlue = "rgb(220, 238, 250)";
export const modalBlue = "rgb(20, 130, 226)";
export const modalRed = "rgb(226, 20, 20)";
export const red = "rgb(214, 60, 60)";
export const progressGrey = "rgb(88, 88, 88)";
export const uploadedButtonGrey = "rgb(70, 70, 70)";
export const uploadingButtonGrey = "rgb(210, 210, 210)";
export const transparent = "rgba(0, 0, 0, 0)";
export const transparentBlack = "rgba(0, 0, 0, 0.85)";
export const transparentBlue = "rgba(0, 174, 239, 0.5)";
export const transparentGrey = "rgba(0, 0, 0, 0.18)";
export const white = "rgb(255, 255, 255)";
export const yellow = "rgb(226, 245, 116)";
export const bottomBar = "rgba(245, 245, 245, 1.2)";
export const bottomBarFont = "rgb(169, 169, 169)";
export const navBarColor = "rgb(0, 152, 209)";
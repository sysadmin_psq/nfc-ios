// @flow
import { shouldAutoplay, shouldRender } from './utils';
import React, { Component, type ComponentType } from 'react';

type $Props = {
    index: number,
    scrollY: number,
}

type $State = {
    autoplay: boolean,
    render: boolean,
}

export default function withAutoplay(
    WrappedComponent: ComponentType<any>,
    componentHeight: number
): ComponentType<any> {
    return class AutoplayVideo extends Component<$Props, $State> {
        constructor(props: $Props) {
            super(props);
            this.state = {
                autoplay: true,
                render: false,
            };

            // $FlowFixMe
            this.onLoad = this.onLoad.bind(this);
        }

        componentWillReceiveProps(newProps: $Props) {
            if (newProps.scrollY) {
                this.updateState(newProps.scrollY)
            }
        }

        componentWillMount() {
            this.updateState(this.props.scrollY);
        }

        shouldComponentUpdate(nextProps: $Props, nextState: $State) {
            const { autoplay, render } = this.state;
            if (autoplay !== nextState.autoplay) {
                return true;
            }
            if (render !== nextState.render) {
                return true;
            }
            return false;
        }

        updateState(scrollY: number) {
            const {
                index,
            } = this.props;
            const { render } = this.state;

            let autoplay = shouldAutoplay(componentHeight, scrollY, index);
            const newRender = shouldRender(componentHeight, scrollY, index);

            // always autoplay until video loads because of an android bug where if
            // paused prop is true when the ReactNativeVideo component is initially
            // rendered the video will not be rendered. onLoad will set this to false
            // when necessary
            if (!render && newRender) {
                autoplay = true;
            }

            this.setState({
                autoplay,
                render: newRender,
            });
        }

        onLoad() {
            // always autoplay first video
            // stop playing once the video is loaded
            if (this.props.index > 0) {
                this.setState({
                    autoplay: false,
                });
            }
        }

        render() {
            const {
                autoplay,
                render
            } = this.state;

            return (
                <WrappedComponent
                    onLoad={this.onLoad}
                    shouldAutoplay={autoplay}
                    shouldRender={render}
                    {...this.props}
                />
            );
        }
    }
}

// @flow
export function shouldAutoplay(componentHeight: number, scrollY: number, videoIndex: number) {
    const offsetFromCurrentPosition = (videoIndex * componentHeight) - scrollY;
    if (offsetFromCurrentPosition <= 0) {
        // video is above the top
        return offsetFromCurrentPosition > -(componentHeight * 0.33);
    } else {
        // video is coming in from bottom
        return offsetFromCurrentPosition < (componentHeight * 0.5);
    }
}

export function shouldRender(componentHeight: number, scrollY: number, videoIndex: number) {
    const absoluteOffsetFromCurrentPosition = Math.abs(scrollY - (videoIndex * componentHeight));
    return absoluteOffsetFromCurrentPosition < (2 * componentHeight);
}

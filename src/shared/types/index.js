// @flow
export type $Exercise = "CORE" | "SQUAT" | "PUSH" | "LUNGE" | "PULL" | "AGILITY" | "BEND";

export type $GeolocationPosition = {
    mocked: ?boolean,
    timestamp: number,
    coords: {
        accuracy: number,
        altitude: number,
        altitudeAccuracy: number,
        heading: number,
        latitude: number,
        longitude: number,
        speed: number,
    },
};

export type $SharedState = {
    notification: {
        +text: string,
        +showCloseButton: boolean,
        +visible: boolean,
    },
};

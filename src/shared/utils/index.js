// @flow
import { Dimensions, Platform } from 'react-native';
import Orientation from 'react-native-orientation';

export function getScreenHeight(): number {
    return Dimensions.get('window').height;
}

export function getScreenWidth(): number {
    return Dimensions.get('window').width;
}

export const isSmallScreen = () => getScreenHeight() <= 480;

export const isMediumScreen = () => getScreenHeight() <= 660;
export const isPixelScreen = () => getScreenHeight() <= 823;

export const isLargeScreen = () => getScreenWidth() >= 410;


export function isAndroid(): boolean {
    return Platform.OS === 'android';
}

export function isIOS(): boolean {
    return Platform.OS === 'ios';
}

export function isIPhone4(): boolean {
    return isIOS() && getScreenWidth() === 320 && getScreenHeight() === 480;
}

export function isIPhone5(): boolean {
    return isIOS() && getScreenWidth() === 320 && getScreenHeight() === 568;
}

export function isIPhone6(): boolean {
    return isIOS() && getScreenWidth() === 375 && getScreenHeight() === 667;
}


export function isIPhoneX(): boolean {
    return isIOS() && (getScreenWidth() ===812 || getScreenHeight() === 812);
}

export function isNotchDevice(): boolean {

    return isIOS() && (getScreenWidth() >= 812 || getScreenHeight() >= 812);

}

//
// export function getOrientation(): String {
//     Orientation.getOrientation((err, orientation) => {
//
//         console.log("orientation", orientation)
//         return orientation
//     });
// }



type $StyleObject = {
    android: Object,
    ios: Object,
    iphone4?: Object,
    iphoneX?: Object,
};
export function deviceStyleSelector(styles: $StyleObject): Object {
    if (isIOS()) {
        if (isIPhone4() && styles.iphone4) {
            return styles.iphone4;
        } else if (isIPhoneX() && styles.iphoneX) {
            return styles.iphoneX;
        }

        return styles.ios;
    }

    return styles.android;
}

type $SizeStyleObject = {
    small?: Object,
    medium?: Object,
    default: Object,
};

export function sizeStyleSelector(
    styles: $SizeStyleObject,
): Object {
    if (isSmallScreen() && styles.small) {
        return styles.small;
    } else if (isMediumScreen() && styles.medium) {
        return styles.medium;
    }

    return styles.default;
}

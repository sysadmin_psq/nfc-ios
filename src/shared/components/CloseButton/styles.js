// @flow
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        height: 40,
        width: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

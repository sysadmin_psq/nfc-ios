// @flow
import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import styles from './styles';

type $Props = {
    onPress: any,
    style: ?any,
};

export default ({ onPress, style }: $Props) => (
    <TouchableOpacity
        style={style || styles.container}
        onPress={onPress}
    >
        <Image source={require('assets/icons/Close.png')} />
    </TouchableOpacity>
);

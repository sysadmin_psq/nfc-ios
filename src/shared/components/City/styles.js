import { StyleSheet} from 'react-native';
import { darkGrey, white } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';

const container = {
    fontSize: 30,
    lineHeight: 32,
    letterSpacing: 1.2
};

export default StyleSheet.create({
  container: {
      ...container,
      ...ProximaNova.Medium,
      color: darkGrey,
      marginVertical: 4
  },
  bold: {
      ...ProximaNova.CondensedBold
  },
  boldLight: {
      ...ProximaNova.CondensedBold,
      color: white
  }
});

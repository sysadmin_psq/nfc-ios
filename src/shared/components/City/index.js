// @flow
import React from 'react';
import { Text } from 'react-native';
import styles from './styles';

type $Props = {
  city: string,
  style: *
};

export default function City ({ city, style }: $Props) {
  const [ cityName, stateName ] = city.split(',');
  return (
      <Text style={style ? [styles.container, style] : styles.container}>
          <Text style={styles.bold}>{cityName.toUpperCase()}</Text>,{stateName}
      </Text>
  );
}

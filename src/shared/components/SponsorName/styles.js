import { StyleSheet} from 'react-native';
import { ProximaNova } from 'shared/styles/fonts';
import { skyBlue, greyBlue, lighterBlue } from 'shared/styles/colors';

const basicStyle = {
    ...ProximaNova.Bold,
    alignSelf: "flex-start",
    fontSize: 14,
    lineHeight: 20,
    letterSpacing: 0.7,
    paddingHorizontal: 8,
    marginVertical: 4
}
export default StyleSheet.create({
  theme1: {
    ...basicStyle,
    color: greyBlue,
    backgroundColor: lighterBlue
  },
  theme2: {
    ...basicStyle,
    color: skyBlue,
    backgroundColor: greyBlue
  }
});

// @flow
import React from 'react';
import { Text } from 'react-native';
import styles from './styles';

type $Props = {
  name: ?string,
  index: ?number
};

function getTheme (index: ?number): * {
  switch (index) {
    case 0:
      return styles.theme1;
    case 1:
    case 2:
    default:
      return styles.theme2;
  }
}

export default function SponsorName ({ name, index }: $Props) {
  if (name) {
    return (
        <Text style={getTheme(index)}>
            {name.toUpperCase()}
        </Text>
    );
  }
  return null;
}

// @flow
import { blue } from 'shared/styles/colors';
import { View } from 'react-native';
import React from 'react';
import styles from './styles';

type $Props = {
    color: string,
    height: number,
    tilt: number,
    width: number,
}

const Parallelogram = ({ color, height, tilt, width }: $Props) => {
    const triangleMixin = {
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderRightWidth: tilt,
        borderTopWidth: height,
        borderRightColor: 'transparent',
        borderTopColor: color,
    };

    return (
        <View>
            <View
                style={[
                    styles.leftTriangle,
                    {left: -tilt}, //eslint-disable-line
                    triangleMixin,
                ]}
            />
            <View
                style={[
                    styles.square,
                    {
                        backgroundColor: color,
                        height,
                        width,
                    }
                ]}
            />
            <View
                style={[
                    styles.rightTriangle,
                    {left: width},
                    triangleMixin,
                ]}
            />
        </View>
    );
};

Parallelogram.defaultProps = {
    color: blue,
    tilt: 10,
};

export default Parallelogram;

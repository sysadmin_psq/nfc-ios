// @flow
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    square: {
        position: 'absolute',
        left: 0,
        top: 0,
    },
    rightTriangle: {
        top: 0,
        position: 'absolute',
    },
    leftTriangle: {
        top: 0,
        position: 'absolute',
        transform: [
          {rotate: '180deg'},
        ],
    },
});

// @flow
import { Animated, View } from 'react-native';
import { HEIGHT, TAB_WIDTH_MULTIPLIER } from './styleConstants';
import { EXERCISE_LIST, NUMBER_OF_EXERCISES, SCREEN_WIDTH } from 'constants';
import Parallelogram from 'shared/components/Parallelogram';
import React from 'react';
import styles from './styles';
import Tab from './components/Tab';


type $Props = {
    activeExerciseIndex: number,
    checkmarkAfterFinish: boolean,
    onPress: *,
    tabOffset: number,
}

const ExerciseNav = ({
    activeExerciseIndex,
    checkmarkAfterFinish,
    onPress,
    tabOffset,
}: $Props) => (
    <View style={styles.container}>
        <Animated.View
            style={[
                styles.tabSlider,
                {
                    marginLeft: tabOffset,
                }
            ]}
        >
            <Parallelogram
                height={HEIGHT}
                width={(SCREEN_WIDTH / NUMBER_OF_EXERCISES) * TAB_WIDTH_MULTIPLIER}
            />
        </Animated.View>
        {EXERCISE_LIST.map((exercise, index) => (
            <Tab
              active={index === activeExerciseIndex}
              checkmark={checkmarkAfterFinish && index < activeExerciseIndex}
              exercise={exercise}
              index={index}
              key={exercise}
              onPress={onPress}
            />
        ))}
    </View>
);

ExerciseNav.defaultProps = {
    onPress: () => {},
    checkmarkAfterFinish: false,
};

export default ExerciseNav;

// @flow
import { connect } from 'react-redux';
import { white, black } from 'shared/styles/colors';
import { Image, Text, TouchableOpacity } from 'react-native';
import React from 'react';
import styles from './styles';

type $Props = {
    active: boolean,
    checkmark: boolean,
    index: number,
    exercise: string,
    onPress: any,
}

const Tab = ({
    active,
    checkmark = false,
    index,
    exercise,
    onPress = () => {}
}: $Props) => {
    const extraNumberStyle = {
        color: active ? white : black,
    };

    let item;
    if (checkmark) {
        item = (
            <Image source={require('assets/icons/Complete.png')} />
        );
    } else  {
        item = (
            <Text style={[styles.number, extraNumberStyle]}>
                {index + 1}
            </Text>
        );
    }

    return (
        <TouchableOpacity
          onPress={() => onPress(index)}
          style={styles.tab}
        >
            {item}
            <Text style={styles.exercise}>
                {exercise}
            </Text>
        </TouchableOpacity>
    );
}

export default connect(null)(Tab);

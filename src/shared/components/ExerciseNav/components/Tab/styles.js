// @flow
import { black, grey, transparent } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    exercise: {
        fontSize: 12,
        color: grey,
        ...ProximaNova.CondensedBold,
    },
    number: {
        color: black,
        fontSize: 20,
        lineHeight: 20,
        ...ProximaNova.CondensedBold,
    },
    tab: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: transparent,
    },
});

// @flow
import { NUMBER_OF_EXERCISES, SCREEN_WIDTH } from 'constants';
import { TAB_WIDTH_MULTIPLIER } from '../styleConstants';

export function determineTabOffset(exerciseIndex: number) {
    const tabWidth = SCREEN_WIDTH / NUMBER_OF_EXERCISES;
    const correction = exerciseIndex === 0 ? 0 : (tabWidth * (1 - TAB_WIDTH_MULTIPLIER));
    const newOffset = (exerciseIndex * tabWidth) + correction;

    return newOffset;
}

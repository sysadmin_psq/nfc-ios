// @flow
import { HEIGHT } from './styleConstants';
import { lightGrey, white } from 'shared/styles/colors';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    tabSlider: {
        position: 'absolute',
        left: 0,
        top: 0,
    },
    container: {
        backgroundColor: white,
        borderTopWidth: 1,
        borderColor: lightGrey,
        flexDirection: 'row',
        height: HEIGHT,
    },
});

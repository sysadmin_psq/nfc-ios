// @flow
import { black, white } from 'shared/styles/colors';
import { deviceStyleSelector } from 'shared/utils';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';


export default StyleSheet.create({
    closeButtonContainer: {
        marginLeft: 8,
    },
    container: {
        position: 'absolute',
        alignItems: 'center',
        flexDirection: 'row',
        top: 0,
        left: 0,
        width: '100%',
        backgroundColor: black,
        zIndex: 1,
        ...deviceStyleSelector({
            android: {
                paddingTop: 0,
            },
            ios: {
                paddingTop: 20,
            },
            iphoneX: {
                paddingTop: 35,
            },
        }),
    },
    sideSection: {
        flex: 1,
    },
    text: {
        paddingVertical: 24,
        width: '70%',
        textAlign: 'center',
        color: white,
        fontSize: 14,
        lineHeight: 15,
        ...ProximaNova.Regular,
    },
});

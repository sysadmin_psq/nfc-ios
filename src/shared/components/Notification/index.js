// @flow
import { connect } from 'react-redux';
import { Text, View } from 'react-native';
import * as sharedActions from 'shared/state/dux';
import CloseButton from '../CloseButton';
import React from 'react';
import select from 'shared/state/selectors/Notification';
import styles from './styles';

type $Props = {
    dispatch: *,
    showCloseButton: boolean,
    text: string,
    visible: boolean,
};

const Notification = ({ dispatch, showCloseButton, text, visible }: $Props) => {
    if (!visible) {
        return null;
    }

    return (
        <View style={styles.container}>
            <View style={styles.sideSection}>
                {showCloseButton && (
                    <View style={styles.closeButtonContainer}>
                        <CloseButton onPress={() => dispatch(sharedActions.hideNotification())} />
                    </View>
                )}
            </View>
            <Text style={styles.text}>
                {text}
            </Text>
            <View style={styles.sideSection} />
        </View>
    );
};

export default connect(select)(Notification);

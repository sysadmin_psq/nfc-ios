// @flow
import { SCREEN_WIDTH } from 'constants';
import { StyleSheet } from 'react-native';

const fullWidthBox = {
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
}

// Square aspect ratio
const aspectLength = SCREEN_WIDTH - 80;
const newAspectRatioBox = {
    height: aspectLength,
    width: aspectLength,
}

export default StyleSheet.create({
    activityIndicatorContainer: {
        zIndex: 1,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        top: 0,
        left: 0,
    },
    iconContainer: {
        width: 30,
        height: 30,
        marginHorizontal: 15,
        alignItems: 'center',
        justifyContent: 'center',
    },
    indicatorContainerDefault: {
        ...fullWidthBox,
    },
    indicatorContainerNewAspect: {
        ...newAspectRatioBox
    },
    container: {
        ...fullWidthBox,
    },
    newAspectContainer: {
        ...newAspectRatioBox,
    },
    pauseButton: {
        height: 19,
        width: 16,
    },
    videoControlsContainer: {
        position: 'absolute',
        zIndex: 1,
        bottom: 0,
        left: 0,
        height: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
    },
})

// @flow
import React, { Component } from 'react';
import ReactNativeVideo from 'react-native-video';
import { ActivityIndicator, Image, TouchableOpacity, View } from 'react-native';
import { white } from 'shared/styles/colors';
import styles from './styles';
import {MAIN_ROUTES} from "../../../constants/index";
import {navigation} from 'react-navigation'
import * as sharedActions from 'shared/state/dux';


type $Props = {
    backgroundColor?: string,
    indicatorColor?: string,
    isNewAspectRatio?: boolean,
    muted: boolean,
    onEnd: () => void,
    onLoad: () => void,
    paused?: boolean,
    repeat?: boolean,
    source: string,
    styleOverrides: *,
    withControls: boolean,
    fullScreenMode: boolean,
}
type $State = {
    muted: boolean,
    paused: boolean,
    showIndicator: boolean,
}

export default class Video extends Component<$Props, $State> {
    player: *;

    static defaultProps = {
        backgroundColor: '#eee',
        indicatorColor: white,
        isNewAspectRatio: false,
        muted: true,
        onEnd: () => {},
        onLoad: () => {},
        paused: false,
        repeat: true,
        styleOverrides: {},
        withControls: false,
        fullScreenMode: false
    }

    constructor(props: any) {
        super(props);
        // this state (besides showIndicator) is entirely meant for the video controls
        // and supercedes any props passed in. We just initialize muted with the corresponding prop
        this.state = {
            muted: props.muted,
            paused: false,
            showIndicator: false,
            fullScreenEnabled: false
        };
        this.player = null;

    }

    onLoad() {
        setTimeout(
            () => this.setState({ showIndicator: false, fullScreenEnabled: this.props.fullScreenMode
            }),
            1200 // ReactNativeVideo takes about a second to render the video after it loads
        );


        this.props.onLoad();
    }

    onLoadStart() {
        this.setState({ showIndicator: true });
    }

    onFinishFull() {

        console.log("isnide hte function of finish", this.props)

        var dismissFunction  = this.props.dismissFunction

        dismissFunction(this.props.navigationRoute)


    }

    onMuteButtonPress = () => {
        this.setState((previousState) => ({
            muted: !previousState.muted,
        }));
    }

    onPauseButtonPress = () => {
        this.setState((previousState) => ({
            paused: !previousState.paused,
        }));
    }

    onReplayPress = () => {
        if (this.player) {
            this.player.seek(0);
        }
    }

    setPauseVideo = (paused: boolean) => {
        this.setState({ paused });
    }

    render() {
        const {
            backgroundColor,
            indicatorColor,
            isNewAspectRatio,
            onEnd,
            repeat,
            paused,
            source,
            styleOverrides,
            withControls,
            fullScreenMode
        } = this.props;

        const {
            muted,
            paused: pausedByControls,
            showIndicator,
        } = this.state;



        const muteButtonSource = this.state.muted ?
            require('assets/icons/Muted.png') :
            require('assets/icons/Unmuted.png');
        const pauseButtonSource = this.state.paused ?
            require('assets/icons/PlayWhite.png') :
            require('assets/icons/PauseWhite.png');

        return (
            <View
                style={[
                    isNewAspectRatio ? styles.newAspectContainer : styles.container,
                    styleOverrides,
                ]}
            >
                <ReactNativeVideo
                    ignoreSilentSwitch={"ignore"}
                    muted={muted}
                    onLoad={() => this.onLoad()}
                    onLoadStart={() => this.onLoadStart()}
                    onFullscreenPlayerWillDismiss={() =>  this.onFinishFull()}
                    onEnd={onEnd}
                    paused={pausedByControls || paused}
                    ref={(ref) => {this.player = ref;}}
                    repeat={repeat}
                    resizeMode='cover'
                    fullscreen={this.state.fullScreenEnabled}

                    source={{uri: source}}
                    style={[
                        isNewAspectRatio ? styles.newAspectContainer : styles.container,
                        styleOverrides,
                    ]}
                    volume={1.0}
                />
                {withControls && (
                    <View style={styles.videoControlsContainer}>
                        <TouchableOpacity
                            onPress={this.onPauseButtonPress}
                            style={styles.iconContainer}
                        >
                            <Image
                                style={styles.pauseButton}
                                source={pauseButtonSource}
                                resizeMode="contain"
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={this.onReplayPress}
                            style={styles.iconContainer}
                        >
                            <Image source={require('assets/icons/ReplayWhite.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={this.onMuteButtonPress}
                            style={styles.iconContainer}
                        >
                            <Image source={muteButtonSource} />
                        </TouchableOpacity>
                    </View>
                )}
                {showIndicator && (
                    <View
                        style={[
                            styles.activityIndicatorContainer,
                            isNewAspectRatio ? styles.indicatorContainerNewAspect : styles.indicatorContainerDefault,
                            {
                                backgroundColor,
                            },
                            styleOverrides,
                        ]}
                    >
                        <ActivityIndicator
                            size='large'
                            color={indicatorColor}
                            animating
                        />
                    </View>
                )}
            </View>
        );
    }
}

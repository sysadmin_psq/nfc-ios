// @flow
import { blue, white } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        alignSelf: 'flex-end',
        height: 65,
        width: '100%',
        backgroundColor: blue,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: white,
        fontSize: 20,
        ...ProximaNova.CondensedBold,
    },
});

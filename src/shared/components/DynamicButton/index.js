// @flow
import { Text, TouchableOpacity } from 'react-native';
import React from 'react';
import styles from './styles';

type $Props = {
    enabled: boolean,
    onPress: () => void,
    text: string,
    style: ?any,
}

export default ({ enabled, onPress, style, text }: $Props) => (
    <TouchableOpacity
        style={style ? [styles.container, style] : styles.container}
        onPress={onPress}
    >
        <Text style={styles.text}>
            {enabled ? text : 'PLEASE WAIT...'}
        </Text>
    </TouchableOpacity>
);

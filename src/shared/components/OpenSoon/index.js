// @flow
import React from 'react';
import { Text } from 'react-native';
import styles from './styles';

type $Props = {
  isVisible: boolean,
  index: ?number,
  style: *
};

function getTheme (index: ?number): * {
  switch (index) {
    case 0:
    case 2:
      return styles.theme1;
    case 1:
    default:
      return styles.theme2;
  }
}

export default function OpenSoon ({ isVisible, index, style }: $Props) {
  const textStyle = style ? [ getTheme(index), style ] : getTheme(index);
  if (isVisible) {
    return (
        <Text style={textStyle}>
            OPEN SOON
        </Text>
    );
  }
  return null;
}

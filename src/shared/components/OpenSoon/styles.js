import { StyleSheet} from 'react-native';
import { topLineText } from 'modules/main/screens/Locations/styles';
import { ProximaNova } from 'shared/styles/fonts';
import { skyBlue, greyBlue } from 'shared/styles/colors';

const basicStyle = {
    ...topLineText,
    ...ProximaNova.Medium
}
export default StyleSheet.create({
  theme1: {
    ...basicStyle,
    color: skyBlue
  },
  theme2: {
    ...basicStyle,
    color: greyBlue
  }
});

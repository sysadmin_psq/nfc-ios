// @flow
import React from 'react';
import { View, Text } from 'react-native';
import type { $FitnessCourt } from 'types/state/main';
import NearMe from '../NearMe';
import OpenSoon from '../OpenSoon';
import SponsorName from '../SponsorName';
import City from '../City';
import styles from './styles';

type $Props = {
  court: $FitnessCourt,
  firstCourt: ?number,
  locationsEnabled: boolean,
  standalone: ?boolean
};

export default function FirstCourtPanel({
  court,
  firstCourt,
  locationsEnabled,
  standalone
}: $Props) {
    const containerStyle = standalone
      ? styles.standalone
      : styles.court;
    return (
        <View style={containerStyle}>
            <View style={styles.topLine}>
                <NearMe isVisible={
                  court.id === firstCourt && locationsEnabled
                }
                />
                <OpenSoon isVisible={court.openingSoon} index={null} style={null} />
            </View>
            <SponsorName name={court.sponsorName} index={null} />
            {court.city &&
            <City city={court.city} style={null} />
            }
            <Text style={styles.header}>
                {(court.name || '')}
            </Text>
            <Text style={styles.address}>
                {court.street}
            </Text>
        </View>
    );
}

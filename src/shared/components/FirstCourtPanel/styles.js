import { StyleSheet } from 'react-native';
import {
  white,
  borderGrey,
  greyBlue
} from 'shared/styles/colors';
import { header } from 'modules/main/screens/Locations/styles';

const court = {
  position: "relative",
  justifyContent: "flex-start",
  alignItems: "flex-start",
  paddingHorizontal: 16,
  paddingVertical: 16,
  backgroundColor: white,
  borderColor: borderGrey,
  borderTopWidth: 4
};

export default StyleSheet.create({
  topLine: {
      flexDirection: "row",
      flexWrap: "nowrap",
      justifyContent: "flex-start",
      marginVertical: 8,
      width: 160
  },
  court: {
      ...court,
      borderBottomWidth: 4,
  },
  standalone: {
      ...court
  },
  stretch: {
    flexGrow: 1
  },
  header: {
      ...header,
      color: greyBlue,
      marginBottom: 4
  },
  address: {
      color: greyBlue,
      fontSize: 14,
      lineHeight: 18,
      letterSpacing: 0.3,
      opacity: 0.75
  },
});

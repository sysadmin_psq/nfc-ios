import { StyleSheet} from 'react-native';
import { topLineText } from 'modules/main/screens/Locations/styles';
import { ProximaNova } from 'shared/styles/fonts';

export default StyleSheet.create({
  container: {
      ...topLineText,
      ...ProximaNova.Bold,
      marginRight: 16
  }
});

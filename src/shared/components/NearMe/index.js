// @flow
import React from 'react';
import { Text } from 'react-native';
import styles from './styles';

type $Props = {
  isVisible: boolean
};

export default function NearMe ({ isVisible }: $Props) {
  if (isVisible) {
    return <Text style={styles.container}>NEAR ME</Text>;
  }
  return null;
}

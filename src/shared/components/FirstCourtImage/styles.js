import { StyleSheet,  } from 'react-native';
import { getScreenWidth } from 'shared/utils';

const imageAspectRatio = 375 / 360
const width = getScreenWidth();
export const height = width * imageAspectRatio;

export default StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        alignSelf: "stretch",
        width,
        height
    },
});

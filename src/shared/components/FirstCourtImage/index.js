// @flow
import React from 'react';
import { Image, View } from 'react-native';
import type { $FitnessCourt } from 'types/state/main';
import styles from './styles';

type $Props = {
  court: $FitnessCourt
};

export default function FirstCourtImage ({ court }: $Props) {
    if (court.image) {
      return (<View style={styles.container}>
          <Image
            source={{ uri: court.image }}
            style={styles.image}
          />
      </View>);
    }
    return null;
}

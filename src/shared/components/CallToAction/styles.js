import { StyleSheet } from 'react-native';
import { blueActive, white } from 'shared/styles/colors';
import { ProximaNova } from 'shared/styles/fonts';
import { height } from '../FirstCourtImage/styles';

export default StyleSheet.create({
  container: {
      position: 'absolute',
      flexDirection: 'row',
      flexWrap: 'nowrap',
      alignItems: 'center',
      top: height - 20,
      right: 16,
      backgroundColor: blueActive,
      borderRadius: 4,
      paddingVertical: 12,
      paddingHorizontal: 14,
      zIndex: 100
  },
  text: {
      ...ProximaNova.Bold,
      color: white,
      marginRight: 8
  }
});

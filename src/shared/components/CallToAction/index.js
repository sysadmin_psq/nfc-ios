// @flow
import React from 'react';
import { Linking, TouchableOpacity, Text, Image } from 'react-native';
import styles from './styles';

type $Props = {
  display: ?string,
  link: ?string
};

export default function CallToAction ({ display, link }: $Props) {
  if (display && link) {
    return (
        <TouchableOpacity
          style={styles.container}
          onPress={() => Linking.openURL(link)}
        >
            <Text style={styles.text}>{display}</Text>
            <Image
              source={require('assets/icons/arrowTopRight.png')}
            />
        </TouchableOpacity>
    );
  }
  return null;
}

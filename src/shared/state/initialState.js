// @flow
export default {
    notification: {
        text: '',
        showCloseButton: false,
        visible: false,
    },
};

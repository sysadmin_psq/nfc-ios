// @flow
import { Observable } from 'rxjs';
import * as sharedActions from 'shared/state/dux';
import SendIntentAndroid from 'react-native-send-intent';

export default (action$: *) =>
    action$
        .ofType(sharedActions.deepLinkInstagram)
        .switchMap((action) => {
            return Observable
                .fromPromise(SendIntentAndroid.isAppInstalled('com.instagram.android'))
                .map(isInstalled => {
                    if (isInstalled) {
                        SendIntentAndroid.shareImageToInstagram('video/*', action.payload.path);
                    }
                    return sharedActions.emptyAction();
                })
                .catch(error => sharedActions.logErrorAsObservable(error))
        });

// @flow
import { NOTIFICATION_DURATION } from 'constants';
import * as sharedActions from '../dux';

export default (action$: *) =>
    action$
        .ofType(sharedActions.createAutoCloseNotification)
        .delay(NOTIFICATION_DURATION)
        .mapTo(sharedActions.hideNotification())

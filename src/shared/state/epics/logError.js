// @flow
import * as sharedActions from 'shared/state/dux';

export default (action$: *) =>
    action$
        .ofType(sharedActions.logError)
        .do(action => console.warn(action.payload.error)) // eslint-disable-line
        .mapTo(sharedActions.emptyAction())

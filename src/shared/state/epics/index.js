// @flow
import createAutoCloseNotification from './createAutoCloseNotification';
import deepLinkInstagram from './deepLinkInstagram';
import logError from './logError';

export default [
    createAutoCloseNotification,
    deepLinkInstagram,
    logError,
];

// @flow
import { Linking } from 'react-native';
import { Observable } from 'rxjs';
import * as sharedActions from 'shared/state/dux';

export default (action$: *) =>
    action$
        .ofType(sharedActions.deepLinkInstagram)
        .switchMap((action) => {
            const encodedAssetPath = encodeURIComponent(action.payload.path);
            const url = `instagram://library?AssetPath=${encodedAssetPath}`;

            return Observable
                .fromPromise(Linking.canOpenURL(url))
                .map((supported) => {
                    if (supported) {
                        Linking.openURL(url);
                    }
                    return sharedActions.emptyAction();
                })
                .catch(error => sharedActions.logErrorAsObservable(error))
        });

// @flow
import { createAction, createReducer } from 'redux-act';
import { Observable } from 'rxjs';
import initialState from '../initialState';
import type { $SharedState } from 'types/state/shared';

// export actions from sub modules
// -----------------------------------------------------------------------------
export * from './navigation';

// actions
// -----------------------------------------------------------------------------
export const deepLinkInstagram = createAction('', (path) => ({ path }));

export const emptyAction = createAction('');
export const emptyActionAsObservable = () => Observable.of(emptyAction());

export const logError = createAction('', (error) => ({ error }));
export const logErrorAsObservable = (error: string) => Observable.of(logError(error));

// action reducer pairs
// -----------------------------------------------------------------------------
export const createAutoCloseNotification = createAction('');
const createAutoCloseNotificationReducer = (state: $SharedState, text: string) => ({
    ...state,
    notification: {
        showCloseButton: false,
        visible: true,
        text,
    },
});

export const createNotification = createAction('', (text) => ({ text }));
const createNotificationReducer = (state: $SharedState, { text }: { text: string }) => ({
    ...state,
    notification: {
        showCloseButton: true,
        visible: true,
        text,
    },
});

export const hideNotification = createAction('');
const hideNotificationReducer = (state: $SharedState) => ({
    ...state,
    notification: {
        ...state.notification,
        visible: false,
    },
});

// -----------------------------------------------------------------------------
// $FlowFixMe
export default createReducer({
        [createAutoCloseNotification]: createAutoCloseNotificationReducer,
        [createNotification]: createNotificationReducer,
        [hideNotification]: hideNotificationReducer,
}, initialState);

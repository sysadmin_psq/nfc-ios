// @flow
import { AUTH_ROUTES } from 'constants';
import { NavigationActions } from 'react-navigation';
import type { $Route } from 'types/routes';

// Passing in true will make the app exit the current navigator and return to its parent navigator.
// Don't pass in a value when you are navigating back one screen within the same navigator.
export const goBack = (exitCurrentNavigator: ?boolean = null) => NavigationActions.back({
    key: exitCurrentNavigator,
});

export const navigate = (routeName: $Route, params: ?Object) => NavigationActions.navigate({
    routeName,
    params,
});

export const navigateAndReset = (routeName: $Route) => NavigationActions.reset({
    index: 0,
    actions: [
        navigate(routeName),
    ]
});

export const resetNavState = () => NavigationActions.reset({
    index: 0,
    actions: [
        navigate(AUTH_ROUTES.WELCOME),
    ],
    key: null, // reset root navigator
});

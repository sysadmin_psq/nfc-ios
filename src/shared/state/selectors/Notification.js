// @flow
import type { $SharedState } from 'types/state/shared';

export default ({ shared }: { shared: $SharedState }) => ({
    ...shared.notification,
});

// flow-typed signature: 703cda8170fa5e74064d9a3903816f85
// flow-typed version: <<STUB>>/react-native-check-box_v^2.0.2/flow_v0.56.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'react-native-check-box'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'react-native-check-box' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */


// Filename aliases
declare module 'react-native-check-box/index' {
  declare module.exports: $Exports<'react-native-check-box'>;
}
declare module 'react-native-check-box/index.js' {
  declare module.exports: $Exports<'react-native-check-box'>;
}

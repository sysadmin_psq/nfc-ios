import { AppRegistry } from 'react-native';
import App from './App';

import { Sentry } from 'react-native-sentry';

if (!__DEV__) {
    Sentry.config("https://92c2e1a7770f48c4981d2abdf2fb4ca2:14cb372ecc0d4832a66e8ba6a0ce28ed@sentry.io/215142").install();
}

AppRegistry.registerComponent('NationalFitnessCampaign', () => App);

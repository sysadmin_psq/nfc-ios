package com.nationalfitnesscampaign.FitnessCourtLive;

import android.app.Application;

import com.brentvatne.react.ReactVideoPackage;
import com.burnweb.rnsendintent.RNSendIntentPackage;
import com.facebook.react.ReactApplication;
import com.github.yamill.orientation.OrientationPackage;
import com.parryworld.rnappupdate.RNAppUpdatePackage;
import com.rnfs.RNFSPackage;
import io.rumors.reactnativesettings.RNSettingsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;
import com.lwansbrough.RCTCamera.RCTCameraPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.zmxv.RNSound.RNSoundPackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
        new MainReactPackage(),
            new OrientationPackage(),
            new RNAppUpdatePackage(),
            new RNFSPackage(),
            new RNSettingsPackage(),
        new VectorIconsPackage(),
        new ReactNativeConfigPackage(),
        new RNSoundPackage(),
        new SplashScreenReactPackage(),
        new RNDeviceInfo(),
        new RNSendIntentPackage(),
        new ReactVideoPackage(),
        new RCTCameraPackage(),
        new FitnessCourtPackage()
      );
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();

    if (BuildConfig.DEBUG) {
      StethoWrapper.initialize(this);
      StethoWrapper.addInterceptor();
    }

    SoLoader.init(this, /* native exopackage */ false);
  }
}

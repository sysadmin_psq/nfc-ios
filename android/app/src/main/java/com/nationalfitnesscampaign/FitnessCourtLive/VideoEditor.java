package com.nationalfitnesscampaign.FitnessCourtLive;

import android.content.ContentValues;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextPaint;
import android.util.Pair;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import processing.ffmpeg.videokit.Command;
import processing.ffmpeg.videokit.CommandBuilder;
import processing.ffmpeg.videokit.LogLevel;
import processing.ffmpeg.videokit.VideoKit;


public class VideoEditor extends ReactContextBaseJavaModule {
    private String videoQualityCommand = "-qscale 15";

    public VideoEditor(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "VideoEditor";
    }

    private File createOrGetFile(String filename, boolean isExternal) {
        File directory = getReactApplicationContext().getFilesDir();
        if (isExternal) {
            directory = Environment.getExternalStorageDirectory();
        }

        File file = new File(directory, filename);
        if (!file.exists()) {
            try {
                if (file.createNewFile()) {
                    return file;
                } else {
                    return null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }

    private File getResourceFile(Resources resources, String filename, int width, int height) {
        File accessibleFile = new File(getReactApplicationContext().getFilesDir(), filename + ".png");
        if (!accessibleFile.exists()) {
            FileOutputStream outputStream = null;
            try {
                outputStream = new FileOutputStream(accessibleFile);
                int resourceId = resources.getIdentifier(filename, "drawable", "com.nationalfitnesscampaign.FitnessCourtLive");
                Bitmap bitmap = BitmapFactory.decodeResource(resources, resourceId);
                Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);
                resizedBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return accessibleFile;
    }

    private Pair<File,Integer> getTextFile(String text, Float fontSize, int color, Typeface typeface, String filename, int alpha, boolean replaceFile) {
        TextPaint textPaint = new TextPaint();
        textPaint.setColor(color);
        textPaint.setTextAlign(Paint.Align.LEFT);
        textPaint.setTextSize(fontSize);
        textPaint.setAntiAlias(true);
        textPaint.setTypeface(typeface);
        textPaint.setAlpha(alpha);

        float baseline = -textPaint.ascent(); // ascent() is negative
        int width = (int) (textPaint.measureText(text) + 0.5f); // round
        int height = (int) (baseline + textPaint.descent() + 0.5f);

        final Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);

        canvas.drawText(text, 0, baseline, textPaint);

        File textFile = new File(getReactApplicationContext().getFilesDir(), filename);
        if (replaceFile && textFile.exists()) {
            textFile.delete();
        }

        if (!textFile.exists()) {
            FileOutputStream outputStream = null;
            try {
                outputStream = new FileOutputStream(textFile);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                bitmap.recycle();
                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return new Pair<>(textFile, width);
    }

    @ReactMethod
    public void transformChallengeVideo(
            final ReadableMap videoLocations,
            final ReadableArray exercises,
            final int totalScore,
            final String city,
            final String fitnessCourtName,
            final String challengeTitle,
            final String challengeSubtitle,
            final Callback callback) {
        new Thread(new Runnable() {
            public void run() {
                Resources resources = getReactApplicationContext().getResources();

                final VideoKit videoKit = new VideoKit();
                videoKit.setLogLevel(LogLevel.FULL);

                // Concatenate all of our individual exercise files together
                File firstFrameFile = createOrGetFile("first_frame.jpg", false);
                if (firstFrameFile == null) {
                    callback.invoke(null, null);
                    return;
                }

                String firstVideoPath = videoLocations.getString("CORE");
                String firstRelativeVideoPath = firstVideoPath.substring(7, firstVideoPath.length());
                Command getFirstFrame = videoKit.createCommand()
                        .overwriteOutput()
                        .inputPath(firstRelativeVideoPath)
                        .outputPath(firstFrameFile.getAbsolutePath())
                        .customCommand("-vf select=eq(n\\,0)", true)
                        .customCommand("-q:v 3", true)
                        .build();
                getFirstFrame.execute();

                // Create 6 second video out of the first frame, which will be sped up 2x later
                File stillFrameFile = createOrGetFile("first_frame_video.mp4", false);
                if (stillFrameFile == null) {
                    callback.invoke(null, null);
                    return;
                }

                final Command introCommand = videoKit.createCommand()
                        .overwriteOutput()
                        .inputFlag("-loop 1", true)
                        .inputPath(firstFrameFile.getAbsolutePath())
                        .outputPath(stillFrameFile.getAbsolutePath())
                        .customCommand("-t 6")
                        .build();
                introCommand.execute();

                // Concatenate all of our individual exercise files together
                File concatFile = createOrGetFile("concat_video.mp4", false);
                if (concatFile == null) {
                    callback.invoke(null, null);
                    return;
                }

                String[] exerciseKeys = {"CORE", "SQUAT", "PUSH", "LUNGE", "PULL", "AGILITY", "BEND"};

                CommandBuilder concatCommandBuilder = videoKit.createCommand()
                        .overwriteOutput()
                        .inputPath(stillFrameFile.getAbsolutePath());

                for (String exerciseKey : exerciseKeys) {
                    String videoPath = videoLocations.getString(exerciseKey);
                    String relativeVideoPath = videoPath.substring(7, videoPath.length());
                    concatCommandBuilder.inputPath(relativeVideoPath);
                }

                concatCommandBuilder.customCommand("-vcodec mpeg4");
                concatCommandBuilder.customCommand("-movflags +faststart");
                concatCommandBuilder.customCommand(videoQualityCommand);

                String filterCommand = "-filter_complex ";

                // Concat all of the files together
                filterCommand += "[0:v:0][1:v:0][2:v:0][3:v:0][4:v:0][5:v:0][6:v:0][7:v:0]concat=n=8:v=1[a];";

                // Crop to 720 x 720
                filterCommand += "[a]crop=720:720:0:280[b];";

                // Scale to 640 x 640
                filterCommand += "[b]scale=640:640[c];";

                // Speed up the video to 2x
                filterCommand += "[c]setpts=0.5*PTS[d];";

                // Set the fitness court title screen overlay
                File badgeOverlayFile = getResourceFile(resources, "challenge_intro_badge", 640, 640);
                concatCommandBuilder.inputPath(badgeOverlayFile.getAbsolutePath());
                filterCommand += "[d][8:v:0]overlay=0:0:enable=between(t\\,0\\,3)[e];";

                Typeface proximaNovaTypeFace = Typeface.createFromAsset(getReactApplicationContext().getAssets(), "fonts/Proxima Nova_bold.ttf");

                // Set the score
                Pair<File, Integer> scoreTextPair = getTextFile(String.valueOf(totalScore), 73f, Color.BLACK, proximaNovaTypeFace, "score_text.png", 255, true);
                concatCommandBuilder.inputPath(scoreTextPair.first.getAbsolutePath());
                int scoreTextX = 563 - (scoreTextPair.second / 2);
                filterCommand += "[e][9:v:0]overlay=" + scoreTextX + ":493:enable=between(t\\,0\\,3)[f];";

                // Set the challenge mode title
                Pair<File, Integer> challengeTitleTextPair = getTextFile(challengeTitle.toUpperCase(), 33f, Color.WHITE, proximaNovaTypeFace, challengeTitle.toLowerCase().replace(" ", "_") + "_city_text.png", 255, false);
                concatCommandBuilder.inputPath(challengeTitleTextPair.first.getAbsolutePath());
                int challengeTitleTextX = 117;
                filterCommand += "[f][10:v:0]overlay=" + challengeTitleTextX + ":485:enable=between(t\\,0\\,3)[g];";

                // Set the challenge mode subtitle + court name
                Pair<File, Integer> courtNameTextPair = getTextFile(
                        challengeSubtitle.toUpperCase() + " | " + fitnessCourtName.toUpperCase(),
                        16f,
                        Color.WHITE,
                        proximaNovaTypeFace,
                        challengeSubtitle.toLowerCase().replace(" ", "_") + fitnessCourtName.toLowerCase().replace(" ", "_") + "_court_text.png",
                        255,
                        false
                );
                concatCommandBuilder.inputPath(courtNameTextPair.first.getAbsolutePath());
                int courtNameTextX = 117;
                // No semicolon on the end of this command since we're handling them in the for loop below
                filterCommand += "[g][11:v:0]overlay=" + courtNameTextX + ":523:enable=between(t\\,0\\,3)[h]";

                // Use this variable for development purposes, when speeding up the video for quicker testing cycles
                double exerciseLengthSeconds = 7.5; //  sped up double time
                double currentTimeOffset = 3.0;
                int currentVideoOffset = 11;
                int index = 0;
                int currentVideoCharOffset = "h".charAt(0);
                for (String exerciseKey : exerciseKeys) {
                    // Set the exercise name badge overlay
                    File repBadgeOverlayFile = getResourceFile(resources, exerciseKey.toLowerCase() + "_badge", 358, 88);
                    concatCommandBuilder.inputPath(repBadgeOverlayFile.getAbsolutePath());

                    double currentOverlayOffset = currentTimeOffset + 1;
                    // If this is the first exercise, start the overlay 2 seconds late
                    if (index == 0) {
                        currentOverlayOffset++;
                    }
                    double nextTimeOffset = currentTimeOffset + exerciseLengthSeconds;

                    currentVideoOffset++;
                    String currentCharOffset = String.valueOf((char) (currentVideoCharOffset));
                    String nextCharOffset = String.valueOf((char) (currentVideoCharOffset + 1));
                    filterCommand += ";[" + currentCharOffset + "][" + currentVideoOffset + ":v:0]overlay=282:487:enable=between(t\\," + currentOverlayOffset + "\\," + (nextTimeOffset - 1) + ")[" + nextCharOffset + "]";
                    currentVideoCharOffset++;

                    // Setup rep count overlay
                    int repCount = 0;
                    for (int exerciseIndex = 0; index < exercises.size(); exerciseIndex++) {
                        ReadableMap exerciseScore = exercises.getMap(exerciseIndex);
                        if (exerciseScore.getString("exercise").equals(exerciseKey)) {
                            repCount = exerciseScore.getInt("reps");
                            break;
                        }
                    }

                    Pair<File, Integer> repCountPair = getTextFile(String.valueOf(repCount), 52f, Color.BLACK, proximaNovaTypeFace, exerciseKey + "_rep_count_overlay.png", 255, true);
                    concatCommandBuilder.inputPath(repCountPair.first.getAbsolutePath());

                    currentVideoOffset++;
                    currentCharOffset = String.valueOf((char) (currentVideoCharOffset));
                    nextCharOffset = String.valueOf((char) (currentVideoCharOffset + 1));
                    int xPos = 575 - (repCountPair.second / 2); // use the width of the image to calculate the exact distance
                    filterCommand += ";[" + currentCharOffset + "][" + currentVideoOffset + ":v:0]overlay=" + xPos + ":495:enable=between(t\\," + currentOverlayOffset + "\\," + (nextTimeOffset - 1) + ")[" + nextCharOffset + "]";
                    currentVideoCharOffset++;


                    // Setup transition animations
                    File animationOverlayFile = getResourceFile(resources, exerciseKey.toLowerCase() + "_transition", 1405, 640);
                    concatCommandBuilder.inputPath(animationOverlayFile.getAbsolutePath());
                    currentVideoOffset++;
                    currentCharOffset = String.valueOf((char) (currentVideoCharOffset));
                    nextCharOffset = String.valueOf((char) (currentVideoCharOffset + 1));
                    // If this is any transition besides the first, have it start 1 second before the exercise switch happens
                    double transitionTimeOffset = index > 0 ? currentTimeOffset - 1 : currentTimeOffset;
                    filterCommand += ";[" + currentCharOffset + "][" + currentVideoOffset + ":v:0]overlay=enable=between(t\\," + transitionTimeOffset + "\\," + (transitionTimeOffset + 2) + "):x=640-(t-" + transitionTimeOffset + ")*1022:y=0[" + nextCharOffset + "]";
                    currentVideoCharOffset++;

                    currentTimeOffset = nextTimeOffset;
                    index++;
                }

                final Command concatCommand = concatCommandBuilder.outputPath(concatFile.getAbsolutePath())
                        .customCommand(filterCommand, true)
                        .customCommand("-map [" + String.valueOf((char) (currentVideoCharOffset)) + "]")
                        .build();
                concatCommand.execute();

                // Save the video so it's accessible in the Gallery app
                ContentValues values = new ContentValues(3);
                values.put(MediaStore.Video.Media.TITLE, "NFC Challenge Video");
                values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
                values.put(MediaStore.Video.Media.DATA, concatFile.getAbsolutePath());
                getReactApplicationContext().getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);

                // Send our final video file back
                WritableMap returnData = Arguments.createMap();
                returnData.putString("videoPath", "file://" + concatFile.getAbsolutePath());
                callback.invoke(null, returnData);
            }
        }).start();
    }

    @ReactMethod
    public void transformWorkoutVideo(String videoPath, String exerciseName, final Callback callback) {
        Resources resources = getReactApplicationContext().getResources();

        // Remove an extra "file://" at the beginning of the input video path
        String inputPath = videoPath.substring(7, videoPath.length());

        final VideoKit videoKit = new VideoKit();
        videoKit.setLogLevel(LogLevel.FULL);

        // Crop the file to a 720 x 720 square and remove the audio
        File croppedFile = createOrGetFile("cropped_video.mp4", false);
        if (croppedFile == null) {
            callback.invoke(null, null);
            return;
        }

        final Command cropCommand = videoKit.createCommand()
                .overwriteOutput()
                .inputPath(inputPath)
                .outputPath(croppedFile.getAbsolutePath())
                .withoutAudio()
                .crop(0, 280, 720, 720)
                .customCommand("-vcodec mpeg4")
                .customCommand(videoQualityCommand)
                .build();
        cropCommand.execute();

        // Resize the video down to 640 x 640
        File scaledFile = createOrGetFile("scaled_video.mp4", false);
        if (scaledFile == null) {
            callback.invoke(null, null);
            return;
        }

        final Command scaleCommand = videoKit.createCommand()
                .overwriteOutput()
                .inputPath(croppedFile.getAbsolutePath())
                .outputPath(scaledFile.getAbsolutePath())
                .customCommand("-vcodec mpeg4")
                .customCommand(videoQualityCommand)
                .withoutAudio()
                .customCommand("-vf scale=640:640")
                .build();
        scaleCommand.execute();

        // Need to save our exercise overlay locally on the filesystem in order for ffmpeg to use it
        String overlayImageName = exerciseName + "_overlay";
        File overlayFile = new File(getReactApplicationContext().getFilesDir(), overlayImageName + ".png");
        if (!overlayFile.exists()) {
            FileOutputStream outputStream = null;
            try {
                outputStream = new FileOutputStream(overlayFile);
                int resourceId = resources.getIdentifier(overlayImageName, "drawable", "com.nationalfitnesscampaign.FitnessCourtLive");
                Bitmap bitmap = BitmapFactory.decodeResource(resources, resourceId);
                Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 640, 114, false);
                resizedBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        // Add our exercise overlay / watermark to the video
        File watermarkFile = createOrGetFile("watermarked_video.mp4", false);
        if (watermarkFile == null) {
            callback.invoke(null, null);
            return;
        }

        final Command overlayCommand = videoKit.createCommand()
                .overwriteOutput()
                .inputPath(scaledFile.getAbsolutePath())
                .inputPath(overlayFile.getAbsolutePath())
                .outputPath(watermarkFile.getAbsolutePath())
                .customCommand("-vcodec mpeg4")
                .customCommand(videoQualityCommand)
                .withoutAudio()
                .customCommand("-filter_complex overlay=0:526")
                .build();
        overlayCommand.execute();

        // Speed up the video
        File spedUpFile = createOrGetFile("sped_up_video.mp4", false);
        if (spedUpFile == null) {
            callback.invoke(null, null);
            return;
        }

        final Command spedUpFileCommand = videoKit.createCommand()
                .overwriteOutput()
                .inputPath(watermarkFile.getAbsolutePath())
                .outputPath(spedUpFile.getAbsolutePath())
                .customCommand("-vcodec mpeg4")
                .customCommand(videoQualityCommand)
                .withoutAudio()
                .customCommand("-vf setpts=0.5*PTS")
                .build();
        spedUpFileCommand.execute();

        // Save a reversed version of the video
        File reversedFile = createOrGetFile("reversed_video.mp4", false);
        if (reversedFile == null) {
            callback.invoke(null, null);
            return;
        }

        final Command reverseCommand = videoKit.createCommand()
                .overwriteOutput()
                .inputPath(spedUpFile.getAbsolutePath())
                .outputPath(reversedFile.getAbsolutePath())
                .customCommand("-vcodec mpeg4")
                .customCommand(videoQualityCommand)
                .withoutAudio()
                .customCommand("-vf reverse")
                .build();
        reverseCommand.execute();

        // Combine the sped up forward and reverse videos together
        Locale currentLocale = resources.getConfiguration().locale;
        String currentDateTime = new SimpleDateFormat("yyMMddHHmmssZ", currentLocale).format(new Date());
        final File concatFile = createOrGetFile("nfc-workout-video-" + currentDateTime + ".mp4", true);
        if (concatFile == null) {
            callback.invoke(null, null);
            return;
        }

        final Command concatCommand = videoKit.createCommand()
                .overwriteOutput()
                .inputPath(spedUpFile.getAbsolutePath())
                .inputPath(reversedFile.getAbsolutePath())
                .outputPath(concatFile.getAbsolutePath())
                .customCommand("-vcodec mpeg4")
                .customCommand("-movflags +faststart")
                .customCommand(videoQualityCommand)
                .customCommand("-filter_complex [0:v:0] [1:v:0] concat=n=2:v=1 [v]", true)
                .customCommand("-map [v]")
                .build();
        concatCommand.execute();


        // Save the video so it's accessible in the Gallery app
        ContentValues values = new ContentValues(3);
        values.put(MediaStore.Video.Media.TITLE, "NFC Workout Video");
        values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
        values.put(MediaStore.Video.Media.DATA, concatFile.getAbsolutePath());
        Uri mediaUri = getReactApplicationContext().getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
        String mediaPath = mediaUri.toString();

        // Send our final video file back
        WritableMap returnData = Arguments.createMap();
        returnData.putString("videoPath", mediaPath);
        callback.invoke(null, returnData);
    }
}

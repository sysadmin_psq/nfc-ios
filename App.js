// @flow
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import { SafeAreaView, StatusBar, YellowBox, Text,Platform, Dimensions } from 'react-native';
import { bottomBar } from 'shared/styles/colors';
import AuthWithNavigationState from 'modules/auth/navigator/AuthWithNavigationState';
import { isIPhone4, isIPhone5, isSmallScreen, isIPhone6, isMediumScreen, isIPhoneX, isLargeScreen, isPixelScreen, getOrientation, isOr} from 'shared/utils';
import Notification from 'shared/components/Notification';
import React, {Component} from 'react';
import store from 'redux/store';
import {createStore, applyMiddleware} from 'redux';
import Orientation from 'react-native-orientation';

Text.defaultProps.allowFontScaling = false;

YellowBox.ignoreWarnings([
    'Remote debugger is in a background tab',
    'Warning: Failed child context type: Invalid child context',
    'Module',
    'Warning: Can only update a mounted or mounting component.',
    'Required dispatch_sync to load constants for RNDeviceInfo',
]);

class App extends Component {

    state = {
        marginLeftInLandscape : 0,
        marginRightInLandscape: 0
    }

    componentDidMount() {


        Orientation.addOrientationListener(this._orientationDidChange);
    }

    _orientationDidChange = (orientation) => {

        console.log('orientation Changed', this.props);
        if (orientation === 'LANDSCAPE') {
            this.setState({
                marginLeftInLandscape : isIPhoneX() ? -45 : 0,
                marginRightInLandscape : isIPhoneX() ? -55 : 0,
            })
        } else {

            this.setState({
                marginLeftInLandscape : 0,
                marginRightInLandscape : 0,
            })

            // do something with portrait layout
        }
    }

    render () {

        console.log("landscape")
        return ( <Provider store={store}>
            <SafeAreaView style={{
                flex: 1,
                paddingTop: Platform.OS === 'android' ? 0 : 0,
                marginTop: isIPhoneX() ? this.state.marginLeftInLandscape !== 0 ? -30 : -50 : -20,
                marginBottom: isIPhoneX() ? this.state.marginLeftInLandscape !== 0 ? -15 : 0 : 0,
                height: isIPhoneX() ? '110%' : '100%',
                marginLeft: this.state.marginLeftInLandscape,
                backgroundColor: 'black',
                marginRight: this.state.marginRightInLandscape
            }}>
                <StatusBar barStyle='light-content' />
                <AuthWithNavigationState />
                <Notification />
            </SafeAreaView>
        </Provider>)

    }
}

export default App;